const readXlsxFile = require("read-excel-file/node");

export function readXLS(): Promise<any[]> {
  return new Promise((resolve, reject) => {
    readXlsxFile("./resources/simply_hours_amenities.xlsx").then(
      (rows: any) => {
        rows.shift();
        resolve(rows);
      }
    );
  });
}

export function readRedirects(): Promise<any[]> {
  return new Promise((resolve, reject) => {
    readXlsxFile("./resources/redirect.xlsx").then((rows: any) => {
      rows.shift();
      resolve(rows);
    });
  });
}
