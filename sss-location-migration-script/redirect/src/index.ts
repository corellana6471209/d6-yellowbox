import * as management from "contentful-management";
import { readRedirects } from "./readXLS";
// import { Iamenity } from "./interfaces/amenity";

// import * as fs from "fs";

// import getOfficeHours from "./officeHours";
// import createAddress from "./address";
// import buildAmenitiesArr from "./amenities";
// import convertHtmlToMarkdown from "./markdown";
// import { cleanseHtml, handleExtra } from "./utils";
// import { createAmentiesEntry } from "./amenities";

// import {
//   // amenities,
//   amenitiesEntries,
//   createAmenity,
//   createRichText,
//   exportTocontentfull,
//   getAmenities,
//   readAmenities,
//   readFolders,
//   saveAmenities,
//   uploadFiles,
// } from "./services";
// import { Ilocation } from "./interfaces/location";
// import { writeFile } from "fs";

import {
  // createReference,
  SPACE_ID,
  ENVIRONMENT_ID,
  ACCESS_TOKEN,
} from "./utils";

// connect to contenful instance
const contentfulManagememt = management.createClient({
  //this is optional here, should be only here if you know the specific space to use
  space: SPACE_ID,
  //the access token should be different.
  //accessToken: "GSoMrRGnvMqAMSvj0DzJZp8voHOBl8Ip4byWXoqS3D0"
  accessToken: ACCESS_TOKEN,
});

// ==============================
// start the whole thing
// ==============================
async function importRedreict(importToContentful: boolean, env: string) {
  //The information from CSV
  const csv = await readRedirects();

  for (let index = 0; index < csv.length; index++) {
    // Items from locations spreadsheet

    const redirectFrom = csv[index][0];
    const redirectTo = csv[index][1];
    console.log(redirectFrom, redirectTo);

    const redirect: any = {
      slug: redirectFrom,
      newPath: redirectTo,
    };

    if (importToContentful === true) {
      const enUs: any = [];
      for (const key of Object.keys(redirect)) {
        enUs[key] = {
          "en-US": redirect[key],
        };
      }
      const CONTENT_TYPE_ID = "redirect";
      const fields = {
        fields: { ...enUs },
      };
      // console.log("fields", fields);
      contentfulManagememt
        .getSpace(SPACE_ID)
        .then((space) => space.getEnvironment(ENVIRONMENT_ID))
        .then((environment) => environment.createEntry(CONTENT_TYPE_ID, fields))
        //publish the entry
        //.then((entry) => entry.publish())
        // .then((entry) => console.log(entry))
        .catch(console.error);
    }

    // console.log(`Finished Importing redirect: ${realLocationId}`);

    // if (index == 0) {
    //   break; // only do 2 for testing
    // }
  }
}
importRedreict(true, "develop");
