// create contentful reference format
export function createReference(id: string, type: string = "Entry") {
  return { sys: { type: "Link", linkType: type, id: id } };
}

export const SPACE_ID = "rfh1327471ch";
export const ENVIRONMENT_ID = "stage";
export const ACCESS_TOKEN = "CFPAT-mhmefS6B3rbzYAIKE861DZa9dlg8ELkofHz1h-3M5S4";

// remove bad characters that comes from excel
export function cleanseHtml(text: string) {
  let clean: string = "";
  if (text) {
    clean = text.replace(/_x000D_/g, " ");
  }
  return clean;
}

export async function handleExtra(
  facility: string,
  new_facility_long_name: string,
  new_legal_name: string,
  ppc_phone: string,
  cell_number: string,
  fax_number: string,
  description: string
) {
  const text =
    "<strong>facility: </strong>" +
    facility +
    "<br /><br />" +
    "<strong>new_facility_long_name: </strong>" +
    new_facility_long_name +
    "<br /><br />" +
    "<strong>new_legal_name: </strong>" +
    new_legal_name +
    "<br /><br />" +
    "<strong>ppc_phone: </strong>" +
    ppc_phone +
    "<br /><br />" +
    "<strong>cell_number: </strong>" +
    cell_number +
    "<br /><br />" +
    "<strong>fax_number: </strong>" +
    fax_number +
    "<br /><br />" +
    "<strong>description: </strong>" +
    description;
  return text;
}
