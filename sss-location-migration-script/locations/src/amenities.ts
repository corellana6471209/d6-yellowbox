import {
  createReference,
  SPACE_ID,
  ENVIRONMENT_ID,
  ACCESS_TOKEN,
} from "./utils";
import * as management from "contentful-management";
import { readXLS } from "./readXLS";

import * as fs from "fs";

// connect to contenful instance
const contentfulManagememt = management.createClient({
  //this is optional here, should be only here if you know the specific space to use
  space: SPACE_ID,
  //the access token should be different.
  //accessToken: "GSoMrRGnvMqAMSvj0DzJZp8voHOBl8Ip4byWXoqS3D0"
  accessToken: ACCESS_TOKEN,
});

// ===================================
// Amenities
// ===================================
// return array of unique amenities from csv file
async function getUniqueAmenities() {
  const csv = await readXLS();
  let uniqueAmenities: any = [];
  for (let index = 0; index < csv.length; index++) {
    const amenitiesIndex = csv[index].length - 1;
    const tempCsv = csv[index][amenitiesIndex];
    // console.log(tempCsv);
    const arrayAmanities = tempCsv.split("\n");
    // console.log(arrayAmanities);

    for (let i = 0; i < arrayAmanities.length; i++) {
      // console.log(`first: ${i}`);
      // loop through amenities rows
      for (let n = 0; n < arrayAmanities.length; n++) {
        // loop through amenities items
        // console.log(`second: ${n}`);
        if (uniqueAmenities.indexOf(arrayAmanities[n].trim()) === -1) {
          // check if exists in array
          uniqueAmenities.push(arrayAmanities[n].trim()); // push into array if unique
        }
      }
    }
    // break;
    // console.log(uniqueAmenities);
  }
  return uniqueAmenities;
}

// import unique amenities to contentful and write to file
export async function createAmentiesEntry() {
  const uniqueAmentities: any = await getUniqueAmenities();
  // console.log(uniqueAmentities);
  let amenityEntries: any = [];

  for (let index = 0; index < uniqueAmentities.length; index++) {
    const amenity = uniqueAmentities[index];
    // console.log(amenity);

    const amenityPayload: any = {
      name: amenity,
      description: amenity,
      amenityId: index,
    };

    console.log("importing:");
    console.log(amenityPayload);

    const enUs: any = [];
    for (const key of Object.keys(amenityPayload)) {
      enUs[key] = {
        "en-US": amenityPayload[key],
      };
    }
    const CONTENT_TYPE_ID = "amenity";
    const fields = {
      fields: { ...enUs },
    };

    await contentfulManagememt
      .getSpace(SPACE_ID)
      .then((space) => space.getEnvironment(ENVIRONMENT_ID))
      .then((environment) => environment.createEntry(CONTENT_TYPE_ID, fields))
      //publish the entry
      //.then((entry) => entry.publish())
      .then((entry) => {
        const key = entry.fields.name["en-US"];
        const value = entry.sys.id;
        const entryPayload: any = {
          [key]: value,
        };
        amenityEntries.push(entryPayload);
        // console.log(amenityEntries);
        return;
      })
      .catch(console.error);
    // break;
  }

  fs.writeFile(
    "./resources/amenitiesEntries.json",
    JSON.stringify(amenityEntries),
    (result) => {
      console.log(result);
    }
  );

  console.log("DONE");
}

// function build contentful (mulit-reference) amenities array
export default async function buildAmenitiesArr(locationID: string) {
  const csv = await readXLS();
  let amenitiesArr: any = [];
  for (let index = 0; index < csv.length; index++) {
    if (csv[index][0] == locationID) {
      const amenitiesIndex = csv[index].length - 1;
      const tempCsv = csv[index][amenitiesIndex];
      const arrayAmanities = tempCsv.split("\n");

      for (let i = 0; i < arrayAmanities.length; i++) {
        for (const [key, value] of Object.entries(amenities)) {
          if (key === arrayAmanities[i]) {
            amenitiesArr.push(createReference(value));
            // return value;
          }
        }
      }
    }
  }

  console.log(
    `Done Building amenity reference array for location: ${locationID}`
  );
  return amenitiesArr;
}

// all amenities in contentful with ref id
// const amenities = {
//   // DEV
//   "Wide Aisles for Truck Access": "5fvDAYyUzXV1NJsiTjlyhk",
//   "Security Cameras": "dA6ZIIrCpindDO0mX7Dq6",
//   "Keypad Entry": "4Q9XPqxF4AmpIWeFp0SNGl",
//   "Fully Fenced": "43HBeQb6dPrmvuMDbJAHFg",
//   "Indoor Lighting": "5PmnXpiSw2HcEPVvdVF0s6",
//   "Outdoor Lighting": "2EY0hKS6V5GYwmGxnXPqsl",
//   "Drive-Up Unit Access": "2Dy5LPOLiF4u7uBwM06Mtm",
//   "Vehicle Storage": "3C1wMdZlvPgCAG8gQwOUTD",
//   "Climate Controlled Units": "dEQ7GVVndnWgmwMfU5ibc",
//   "Packing Supplies": "7i2aV3IX97ABI7PtQxGPOV",
//   "Touchless Gate Entry Available": "6XqfnQEjTXjZNF57zwkDKh",
//   "Carts & Dollies": "2FQlyJ53I9hTErnPHfFAnH",
//   "Deliveries Accepted": "43DKQgm7XBcnhBc7XXl8fn",
//   Elevator: "47AM3dY3bPa1EUyovGwT8h",
//   "Boat Storage": "2ETleRuEWwYtptojBLK55I",
//   "Drive-up Access": "6REcSHq8cu82CFFuKy7BHf",
//   "Ground Level Access": "6rr4OtOWOxUcgBvmVtaYO1",
//   "Professional Property Manager": "6I26XxjYcpUJMqdwO4grw3",
//   "Boat/RV Storage Available": "4oLRp7iOqaccW3kO55Y5Ep",
//   "Computer Controlled Access": "2nHxHqFLKEX99yP2RAGtLP",
//   "All Drive-Up Access": "6LbsQvpd5kofOgSxcSxxuU",
//   "Exterior Facility Lighting": "7bw0dvq8NjXC6vnF6fHoAf",
//   "Safeguard Program": "7nQv1KklccA6omaBJW5cKG",
//   "Records Storage": "1HTLZ2DRGSMlqwInXPfQ8y",
//   "Video Surveillance Cameras": "7f5rhW5QDa8Gkwg37VyvKw",
//   "Business/Conference Room": "2Hc5Fgyt0xzNm9a3zOT0eX",
//   "Retail/Commercial Space": "4HHj6dloJ3moYXWiuvQO7N",
//   "Loading Dock": "2rkz0KIT7JzGCP9wL09Amc",
//   "Drive-in Loading & Unloading": "1VNKFnfkh71kt9xZTNFAnJ",
//   "Online Rent Payments": "5AeItAHOv0nzntAVcC5mgk",
//   "Boxes and Packing Supplies": "7cTqPY3WJ2Abci57LEjXeJ",
//   "No Security Deposit": "3CH47z9TO3utaidav3B6ob",
//   "Automatic Monthly Credit Card Billing": "39Yrp5WQVMTlaZGugu26Pn",
//   "Month to Month Leasing": "3aSvctesmsQxTQBWvkEc6n",
//   "Month to Month Leases": "5im1GoqnybNX5Y2Vpi6aAn",
//   "Computerized Entry Systems": "2EqjQo9NRlVYOcTdGL8e5e",
//   "Boxing and Packing Supplies": "30fzJmzJjpMahIrNcl4uDX",
//   "No Security Deposits": "7LswqculotCQI8gwVVuiba",
//   "Automatic Monthly Credit Card Billing Available": "Z1IP3SL8jeUgspV552BYB",
//   "Heated & Cooled Spaces Available": "7vUmw3m8VkAxxMqYPAakVp",
//   "Climate Controlled Loading Bay": "lReMFcHrnbz0kH3s1Ylpe",
//   "Rental Kiosk Available": "1DbQHN7a34dVH5oGCe6OT1",
//   "Paved and Non-paved Parking": "3P9XyEWr4bUuAdaw5rh3lw",
//   "Deliveries Accepted Touchless Gate Entry Available":
//     "7CGB3sPmw0w0Diw8D16kXh",
//   "Surveillance Cameras": "4x1iEd89M6nQ3nPoGKRCi7",
//   "Drive-Up Access": "3QRygJh9OUWodHr3sCnVTz",
//   "Fenced Facility": "6V8eZZU0fu8oi8Q3c25M9h",
//   "Inside Lights": "5XLqxbes5OqLx90yoei17e",
//   "Referral Program with Rewards": "15p2XOtCpQUTgmAe3OJKHb",
//   "Online Payment Options": "62aNf07TuVspK38eHv1yQh",
//   "Ground Level Access<br/>": "DhGFqZA0Wcd8bLATlsTPY",
//   "Wide Aisles": "3vF2lfKkKQZy4b2clJKfHD",
//   "Accessible ADA Compliant Storage Units": "6RaKK4gSuPyHkgNb833k0x",
//   "RV Storage": "18xatwQL5TjvaayTWcsdPd",
//   "Enclosed Drive-Thru Truck Access": "49IR3iuQYlfngOTj63jPx7",
//   "Fully Enclosed": "5Jxd5FsjKB4bZNAk1S3Py5",
//   "Heated Units": "6zxkGp87umWPGZzKJvPNrZ",
//   "Parking Available": "P1ISZ2iVfcT9X5Aue4kEM",
//   "Gated Access": "5Vr4xppSWNe5GIACxQJKZf",
//   "Video Surveillance": "5shd3ysYNfnlRwOasUA9Uk",
//   "Fenced and Lighted": "j51O1gaojA9IuYjUjqEg1",
//   "Alarmed Units": "4IZl9Z5pypkjWtr0yAJPkd",
//   "Handcarts Available": "gcFo9I907X2BDzEAuG7XK",
// };

const amenities = {
  "Wide Aisles for Truck Access": "1SK7LfmPy6PyZ0YH9t9cDm",
  "Security Cameras": "51FGSzkZGxnnKPMbygu0VT",
  "Keypad Entry": "4UjpEcaCW7CXPSl7cs9V3N",
  "Fully Fenced": "130V6f3DC2wi7tLLq2Go1i",
  "Indoor Lighting": "ig3GSwoExQVAZrqp0qnJ3",
  "Outdoor Lighting": "3JQeJq5eT8kqsapRhn0Clo",
  "Drive-Up Unit Access": "5Xeu564WxMMuTkyCpWsZJg",
  "Vehicle Storage": "1pWyP63D5Jo9ndpzwFHolR",
  "Climate Controlled Units": "1RA3UMPqqOq46y0zkPKKuu",
  "Packing Supplies": "Y0DGmR99LTVRoWHVCZUE3",
  "Touchless Gate Entry Available": "3RjA5o4m5VJ4Pnp1Qt7hv6",
  "Carts & Dollies": "7fCemv76IhcHkqsar2PmVy",
  "Deliveries Accepted": "6Ezd07kCxD2C1tzaxLjVnL",
  Elevator: "6Rorh5NFAAP2QmRIHqDiW4",
  "Boat Storage": "4JKgQwRYDsG2bKLh9FSTAH",
  "Drive-up Access": "4NOI1g1B1IljrykHEPGyKh",
  "Ground Level Access": "1QjgtODHL3Byaie3dEgLVV",
  "Professional Property Manager": "1L8WKe7EvNJRHACt4a4oBI",
  "Boat/RV Storage Available": "138wgOvhTdvOHc07GsicAC",
  "Computer Controlled Access": "43GPESTgSwsHzdHko57IkJ",
  "All Drive-Up Access": "2wPNCrF311NZhuDKi37QQC",
  "Exterior Facility Lighting": "MtlKvERFCN7SNMcWTc73Y",
  "Safeguard Program": "ecawRCl8qPmKIzwxRtRgB",
  "Records Storage": "42ZOPtDM0qdB8zUlOzAZTL",
  "Video Surveillance Cameras": "3FyBkHk60gY7NPer7pOF8e",
  "Business/Conference Room": "JTgdiMQKrvcJQ1mOgN2Lg",
  "Retail/Commercial Space": "1wd7Kzd5ZcLPzTzKwzyRgZ",
  "Loading Dock": "4e8cfqrfTCyfqWpVC6e5im",
  "Drive-in Loading & Unloading": "3y5iTSMsjq3kakG0dK1KOa",
  "Online Rent Payments": "5DgH173I3EpI0wftGXmivb",
  "Boxes and Packing Supplies": "2pBQu7T3tulbVGYE3fHYC3",
  "No Security Deposit": "3fep3DohL8PMexlZmfC6lb",
  "Automatic Monthly Credit Card Billing": "dOLFP1B8QMCQxEnCbLCGC",
  "Month to Month Leasing": "aCJaUTg3jBMy2KyzMKLlY",
  "Month to Month Leases": "7rmShrIaWna07VPABFKrQg",
  "Computerized Entry Systems": "01jNhailW97E5Eq52HtFHQ",
  "Boxing and Packing Supplies": "4Nbx9gNr5GcMWRZYAsEWgD",
  "No Security Deposits": "4bT4hCrwFMD7iP98Q5GiqB",
  "Automatic Monthly Credit Card Billing Available": "64I9X1bualIfrWfxBNOh2W",
  "Heated & Cooled Spaces Available": "1bP2dx98aTwTwKK3Ut63Qa",
  "Climate Controlled Loading Bay": "1EmtqebNJQykmqaSDRrlcg",
  "Rental Kiosk Available": "28aVEamwbsl8oK3znYga84",
  "Paved and Non-paved Parking": "5lTR79o6RSUnkrO3aGd1vZ",
  "Deliveries Accepted  Touchless Gate Entry Available":
    "5kqbnl4DPCoaJNYNwIlYkj",
  "Surveillance Cameras": "2SVMEhRZHwfqomUpBRRwWB",
  "Drive-Up Access": "GvIEYy7RWwDxnoSaFa2Lw",
  "Fenced Facility": "2V3AhcJoUDyzM4NbyCtckG",
  "Inside Lights": "o90aGB9UGftOK0sTe17VI",
  "Referral Program with Rewards": "4hc3DGCrg7EZDIDTC9A9rv",
  "Online Payment Options": "7wRHYv0qnWIGdmJCE7qljV",
  "Ground Level Access<br/>": "6X2sMsyLpFJyUPvl9QLllv",
  "Wide Aisles": "45cib55nBHZHPCvFurQcDD",
  "Accessible ADA Compliant Storage Units": "5EqooSL9JpspIbXiDv09qs",
  "RV Storage": "3YYBlblu2ajp1SmHoDQT8a",
  "Enclosed Drive-Thru Truck Access": "1TPxsvtL6YjzEKk9FeUzOh",
  "Fully Enclosed": "1c9WySBa3LezoqsfoJwvH7",
  "Heated Units": "302HzxxUbR2cx03GYudM5w",
  "Parking Available": "1Fa3gT80cLvKtoDGtonOCM",
  "Gated Access": "4UR0lOidL1fIgf5bZhwnox",
  "Video Surveillance": "5Z2xXfKIix3XTVcbjYSvix",
  "Fenced and Lighted": "5X5lY8db4pCItFQlrEG6ZG",
  "Alarmed Units": "vQGb4PmupKWra8eks5t3L",
  "Handcarts Available": "6EDC8Y84P0tA6EKM2vGBpY",
};
