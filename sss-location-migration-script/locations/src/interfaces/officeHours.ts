export interface IofficeHours{
    officeHoursName : string;
    dayOfTheWeek : string;
    openTime : string;
    closeTime : string;
}