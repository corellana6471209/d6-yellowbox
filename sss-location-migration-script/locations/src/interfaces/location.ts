export interface Ilocation{
 locationName : string;
 locationId: string;
 slug:string;
 enabled : boolean;
 googleBusinessProfileId : string;
 address? : string // reference
 phoneNumber : string;
 officeHours : string; //reference
 getAccessHours : string;
 locationDirections : string;
 storeEmailAddress : string;
 siteLinkLocationId : number;
 holdReservationTime : number;
 blogPosts : string;// reference
 aboutOurFacility : string;
 facilityAmenities : string[],
 serving : string;
 pointsOfInterest : string;
 titleBody : string;
 aboutTheArea : string;
 localNeighborhoods : string;
 units : string[];//references many
 sizeGuide : string; //reference
 requireLock : boolean;
}