import TurndownService from "turndown";
/**
 * Markdown / Content conversion functions.
 */
const turndownService = new TurndownService({
  codeBlockStyle: "fenced",
});

/**
 * Convert HTML codeblocks to Markdown codeblocks.
 */

export default async function convertHtmlToMarkdown(html: string) {
  const markdown = turndownService.turndown(html);
  return markdown;
  //   turndownService.addRule("fencedCodeBlock", {
  //     filter: function (node, options) {
  //       return (
  //         options.codeBlockStyle === "fenced" &&
  //         node.nodeName === "PRE" &&
  //         node.firstChild &&
  //         node.firstChild.nodeName === "CODE"
  //       );
  //     },
  //     replacement: function (content, node, options) {
  //       let className = node.firstChild.getAttribute("class") || "";
  //       let language = (className.match(/language-(\S+)/) || [null, ""])[1];

  //       return (
  //         "\n\n" +
  //         options.fence +
  //         language +
  //         "\n" +
  //         node.firstChild.textContent +
  //         "\n" +
  //         options.fence +
  //         "\n\n"
  //       );
  //     },
  //   });
}
