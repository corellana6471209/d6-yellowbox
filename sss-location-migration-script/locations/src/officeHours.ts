import { readXLS } from "./readXLS";

export default async function getOfficeHours(locationId: number) {
  const csv = await readXLS();
  let officeHours: any = [];
  // console.log(`Getting Office Hours for location: ${locationId}`);

  for (let index = 0; index < csv.length; index++) {
    const ooSunIndex = csv[index].length - 10;
    const ooMonIndex = csv[index].length - 9;
    const ooTuesIndex = csv[index].length - 8;
    const ooWedIndex = csv[index].length - 7;
    const ooThurIndex = csv[index].length - 6;
    const ooFriIndex = csv[index].length - 5;
    const ooSatIndex = csv[index].length - 4;
    const accessHoursIndex = csv[index].length - 3;
    const LunchHoursIndex = csv[index].length - 2;
    if (csv[index][0] == locationId) {
      officeHours.push(csv[index][ooMonIndex]);
      officeHours.push(csv[index][ooTuesIndex]);
      officeHours.push(csv[index][ooWedIndex]);
      officeHours.push(csv[index][ooThurIndex]);
      officeHours.push(csv[index][ooFriIndex]);
      officeHours.push(csv[index][ooSatIndex]);
      officeHours.push(csv[index][ooSunIndex]);
      officeHours.push(csv[index][accessHoursIndex]);
      officeHours.push(csv[index][LunchHoursIndex]);
    }

    // console.log(`
    //     LocationId: ${csv[index][0]}
    //     Monday: ${csv[index][ooMonIndex]}
    //     Tuesday: ${csv[index][ooTuesIndex]}
    //     Wednesday: ${csv[index][ooWedIndex]}
    //     Thursday: ${csv[index][ooThurIndex]}
    //     Friday: ${csv[index][ooFriIndex]}
    //     Saturday: ${csv[index][ooSatIndex]}
    //     Sunday: ${csv[index][ooSunIndex]}
    //  `);

    //   const tempCsv = csv[index][ooIndex];
    //   // console.log(tempCsv);
    //   const arrayOO = tempCsv.split("\n");
    //   // console.log(arrayAmanities);

    // //   for (let i = 0; i < arrayOO.length; i++) {
    // //     // console.log(`first: ${i}`);
    // //     // loop through OO rows
    // //     for (let n = 0; n < arrayOO.length; n++) {
    // //       // loop through OO items
    // //       // console.log(`second: ${n}`);
    // //       if (uniqueOO.indexOf(arrayOO[n].trim()) === -1) {
    // //         // check if exists in array
    // //         uniqueOO.push(arrayOO[n].trim()); // push into array if unique
    // //       }
    // //     }
    //   }
    // break;
    // console.log(uniqueAmenities);
  }
  return officeHours;
}
