import * as fs from "fs";
import { Iamenity } from "./interfaces/amenity";
import { IofficeHours } from "./interfaces/officeHours";
import * as management from "contentful-management";
import { createReference, ENVIRONMENT_ID, SPACE_ID } from "./utils";

/**
 * Convert inline HTML images to inline markdown image format.
 */
// turndownService.addRule('replaceWordPressImages', {
//   filter: ['img'],
//   replacement: function(content, node, options) {
//     let assetUrl = contentfulData.assets.filter(asset => {
//       let assertFileName = asset.split('/').pop()
//       let nodeFileName = node.getAttribute('src').split('/').pop()

//       if (assertFileName === nodeFileName) {
//         return asset
//       }
//     })[0];

//     return `![${node.getAttribute('alt')}](${assetUrl})`
//   }
// })

export const amenitiesEntries: any[] = [];
export const amenities: Iamenity[] = [];
export const officeHours: IofficeHours[] = [];

export async function createAmenity(
  contentfulManagememt: management.ClientAPI,
  payload: Iamenity
) {
  //model Amenity
  const CONTENT_TYPE_ID = "amenity";
  const fields = {
    fields: {
      name: {
        "en-US": payload.name,
      },
      description: {
        "en-US": payload.description,
      },
      amenityId: {
        "en-US": payload.amenityId,
      },
    },
  };
  const space = await contentfulManagememt.getSpace(SPACE_ID);
  const environment = await space.getEnvironment(ENVIRONMENT_ID);
  const entry = await environment.createEntry(CONTENT_TYPE_ID, fields);
  const publishedEntry = await entry.publish();
  payload.id = publishedEntry.sys.id;

  return publishedEntry;
}

export function getAmenities() {
  return amenities;
}

export async function saveAmenities() {
  return new Promise((resolve, reject) => {
    fs.writeFile(
      "./resources/amenities.json",
      JSON.stringify(amenities),
      (result) => {
        resolve(true);
      }
    );
    fs.writeFile(
      "./resources/amenitiesEntries.json",
      JSON.stringify(amenitiesEntries),
      (result) => {
        console.log(result);
      }
    );
  });
}

export async function readAmenities() {
  let hashMap = [];
  const amenitiesString = await fs.readFileSync("./resources/amenities.json");
  const amenitiesJson = JSON.parse(amenitiesString.toString());
  for (const amenity of amenitiesJson) {
    amenities.push(amenity);
    if (!hashMap[amenity.name]) {
      hashMap[amenity.name] = amenity.id;
    }
  }
  return hashMap;
}
async function createOfficeHours() {
  //model Daily Office Hours
}

export function createRichText(text: string) {
  return {
    nodeType: "document",
    data: {},
    content: [
      {
        nodeType: "paragraph",
        content: [
          {
            nodeType: "text",
            marks: [],
            value: text,
            data: {},
          },
        ],
        data: {},
      },
    ],
  };
}

// export function createMarkdown(text : string){
//     const content = TurndownService.turndown(text);
//     return content;
// }

export async function readFolders(folderName: string) {
  const folders: string[] = [];
  fs.readdirSync(folderName).forEach((folder) => {
    if (!folder.includes("DS_Store")) {
      folders.push(folder);
    }
  });
  return folders;
}

export async function readFile(fileName: string) {
  return fs.readFileSync(fileName);
}

export async function uploadFiles(
  env: management.Environment,
  path: string,
  files: string[]
) {
  const array: any[] = [];
  for (const file of files) {
    const bufferFile = await fs.readFileSync(path + "/" + file);
    const result = await exportTocontentfull(env, bufferFile, file, path);
    const id = createReference(result.sys.id, "Asset");
    array.push(id);
  }
  return array;
}

export async function exportTocontentfull(
  env: management.Environment,
  file: ArrayBuffer,
  filename: string,
  folder: string
) {
  const result = await env.createAssetFromFiles({
    fields: {
      title: {
        "en-US": filename,
      },
      description: {
        "en-US": folder,
      },
      file: {
        "en-US": {
          contentType: "image/jpeg",
          fileName: filename,
          file: file,
        },
      },
    },
  });
  console.log("result ", result);
  const asset = await result.processForAllLocales();
  return asset;
}

export async function createAddress() {}
export async function create() {}
