import { readCSVPackage } from "./readCSV";
import * as management from "contentful-management";
import { readXLS, readLocations } from "./readXLS";
import { Iamenity } from "./interfaces/amenity";

import * as fs from "fs";

import getOfficeHours from "./officeHours";
import createAddress from "./address";
import buildAmenitiesArr from "./amenities";
import convertHtmlToMarkdown from "./markdown";
import { cleanseHtml, handleExtra } from "./utils";
import { createAmentiesEntry } from "./amenities";

import {
  // amenities,
  amenitiesEntries,
  createAmenity,
  createRichText,
  exportTocontentfull,
  getAmenities,
  readAmenities,
  readFolders,
  saveAmenities,
  uploadFiles,
} from "./services";
import { Ilocation } from "./interfaces/location";
import { writeFile } from "fs";

import {
  createReference,
  SPACE_ID,
  ENVIRONMENT_ID,
  ACCESS_TOKEN,
} from "./utils";

// connect to contenful instance
const contentfulManagememt = management.createClient({
  //this is optional here, should be only here if you know the specific space to use
  space: SPACE_ID,
  //the access token should be different.
  //accessToken: "GSoMrRGnvMqAMSvj0DzJZp8voHOBl8Ip4byWXoqS3D0"
  accessToken: ACCESS_TOKEN,
});

// create slug functionality
function createSlug(name: string) {
  name = name.replace(" ", "-");
  name = name.replace(".", "");
  while (name.indexOf(" ") != -1) {
    name = name.replace(" ", "-");
  }

  while (name.indexOf(".") != -1) {
    name = name.replace(".", "");
  }

  while (name.indexOf("/") != -1) {
    name = name.replace("/", "");
  }

  while (name.indexOf("--") != -1) {
    name = name.replace("--", "-");
  }

  return name;
}

// ==============================
// start the whole thing
// ==============================
async function importLocations(
  importToContentful: boolean,
  importImage: boolean,
  env: string
) {
  //The information from CSV
  const csv = await readLocations();

  for (let index = 0; index < csv.length; index++) {
    // Items from locations spreadsheet

    let location_id = csv[index][1];
    let realLocationId = csv[index][1]; // using this in places where I need the real id to fetch values from excel docs
    if (env === "develop") {
      // use dev location id instead of real ones
      location_id = "9002";
    } else if (env === "quality") {
      // use qa location id instead of real ones
      location_id = "9003";
    }

    // console.log(location_id);

    const old_location_id = csv[index][2];
    const facility = csv[index][3];
    const new_facility_long_name = csv[index][4];
    const new_legal_name = csv[index][5];
    const address = csv[index][7];
    const city_state_zip = csv[index][8];
    const office_number = csv[index][9];
    const ppc_phone = csv[index][10];
    const cell_number = csv[index][11];
    const fax_number = csv[index][12];
    const new_customer_number = csv[index][13];
    const email = csv[index][14];
    const description = csv[index][15];
    const directions = csv[index][16];
    const state_name = csv[index][17];
    const short_facility_name = csv[index][18];
    const page_description = csv[index][19];
    const about_text = csv[index][20];
    const custom_section_one_title = csv[index][21];
    const custom_section_one_content = csv[index][22];
    const has_custom_section_two = csv[index][23];
    const custom_section_two_title = csv[index][24];
    const custom_section_two_content = csv[index][25];
    const has_custom_section_three = csv[index][26];
    const custom_section_three_title = csv[index][27];
    const custom_section_three_content = csv[index][28];
    const has_custom_section_four = csv[index][29];
    const custom_section_four_title = csv[index][30];
    const custom_section_four_content = csv[index][31];
    const has_custom_section_directions = csv[index][32];
    const custom_section_directions_title = csv[index][33];
    const custom_section_directions_content = csv[index][34];
    const locCity = csv[index][37];
    const locState = csv[index][38];
    const locZip = csv[index][39];

    // Get Office Hours
    const officHoursArr = await getOfficeHours(realLocationId);
    const monOfficeHours = officHoursArr[0];
    const tuesOfficeHours = officHoursArr[1];
    const wedOfficeHours = officHoursArr[2];
    const thuOfficeHours = officHoursArr[3];
    const friOfficeHours = officHoursArr[4];
    const satOfficeHours = officHoursArr[5];
    const sunOfficeHours = officHoursArr[6];
    const accessHours = officHoursArr[7];
    const lunchHours = officHoursArr[8];
    // console.log(accessHours);

    const extraDataConverted = await handleExtra(
      facility,
      new_facility_long_name,
      new_legal_name,
      ppc_phone,
      cell_number,
      fax_number,
      description
    );

    let images: string | any[];
    if (importImage) {
      images = await loadFiles(realLocationId);
    } else {
      // add a random default image
      const imgRef = createReference("3Kc2ZDph6Dmrk7fWl8cXOo");
      const imgArray = [];
      imgArray.push(imgRef);
      images = imgArray;
    }

    // Build address data
    // const city = city_state_zip.split(",")[0];
    // const stateAbbreviation = city_state_zip.split(" ")[1];
    // const zip = city_state_zip.split(" ")[2];
    const addressRef = (await createAddress(
      address,
      locCity,
      state_name,
      String(locZip),
      locState,
      importToContentful
    )) as any;

    // get Amenities
    const amenitiesArr = await buildAmenitiesArr(realLocationId);

    const location: any = {
      locationName: short_facility_name,
      slug: createSlug(short_facility_name),
      locationId: String(location_id),
      siteLinkLocationId: String(location_id),
      locationImage: images,
      enabled: true,
      googleBusinessProfileId: "",
      address: createReference(addressRef),
      phoneNumberNewCustomer: new_customer_number,
      phoneNumberExistingCustomer: office_number,
      // officeHours: officeHours,
      lunchHours: lunchHours,
      gateAccessHours: accessHours,
      amenities: amenitiesArr,
      locationDirections: await convertHtmlToMarkdown(cleanseHtml(directions)),
      storeEmailAddress: email,
      holdReservationTime: 3,
      blogPosts: createReference("avV4ys0j25ZoSXnAjlRtK"),
      sizeGuide: createReference("1agRqIhQXi5D9DvUmAotCG"),
      aboutFacilityTitle: "About Our Facilities",
      aboutOurFacility: await convertHtmlToMarkdown(cleanseHtml(about_text)),
      displayPointsOfInterest: has_custom_section_directions ? true : false,
      pointsOfInterestTitle: custom_section_directions_title,
      pointsOfInterest: await convertHtmlToMarkdown(
        cleanseHtml(custom_section_directions_content)
      ),
      displayCustomSection1: true,
      customSection1Title: custom_section_one_title,
      customSection1Content: await convertHtmlToMarkdown(
        cleanseHtml(custom_section_one_content)
      ),
      displayCustomSection2: has_custom_section_two ? true : false,
      customSection2Title: custom_section_two_title,
      customSection2Content: await convertHtmlToMarkdown(
        cleanseHtml(custom_section_two_content)
      ),
      displayCustomSection3: has_custom_section_three ? true : false,
      customSection3Title: custom_section_three_title,
      customSection3Content: await convertHtmlToMarkdown(
        cleanseHtml(custom_section_three_content)
      ),
      displayCustomSection4: has_custom_section_four ? true : false,
      customSection4Title: custom_section_four_title,
      customSection4Content: await convertHtmlToMarkdown(
        cleanseHtml(custom_section_four_content)
      ),
      requireLock: true,
      oldLocationId: String(old_location_id),
      pageDescription: await convertHtmlToMarkdown(
        cleanseHtml(page_description)
      ),
      mondayOfficeHours: monOfficeHours,
      tuesdayOfficeHours: tuesOfficeHours,
      wednesdayOfficeHours: wedOfficeHours,
      thursdayOfficeHours: thuOfficeHours,
      fridayOfficeHours: friOfficeHours,
      saturdayOfficeHours: satOfficeHours,
      sundayOfficeHours: sunOfficeHours,
      extraData: await convertHtmlToMarkdown(cleanseHtml(extraDataConverted)),
    };

    if (importToContentful === true) {
      const enUs: any = [];
      for (const key of Object.keys(location)) {
        enUs[key] = {
          "en-US": location[key],
        };
      }
      const CONTENT_TYPE_ID = "location";
      const fields = {
        fields: { ...enUs },
      };
      // console.log("fields", fields);
      contentfulManagememt
        .getSpace(SPACE_ID)
        .then((space) => space.getEnvironment(ENVIRONMENT_ID))
        .then((environment) => environment.createEntry(CONTENT_TYPE_ID, fields))
        //publish the entry
        //.then((entry) => entry.publish())
        // .then((entry) => console.log(entry))
        .catch(console.error);
    }

    console.log(`Finished Importing location: ${realLocationId}`);

    // if (index == 0) {
    //   break; // only do 2 for testing
    // }
  }
}
importLocations(true, true, "prod");
// createAmentiesEntry();
// =========================================
// =========================================
// =========================================
// Run below function to import all unique amenities from csv to contentful then write enteries and their contentful id to amenitiesEntries.json
// createAmentiesEntry();
// =========================================
// =========================================
// =========================================

// async function loadAmenities(PropertyNo: number = 0) {
//   const csv = await readXLS();
//   for (let index = 0; index < csv.length; index++) {
//     let findProperty = false;
//     if (PropertyNo) {
//       const propertyNumber = csv[index][0];
//       console.log("propertyNumber", propertyNumber);
//       if (propertyNumber == PropertyNo) {
//         findProperty = true;
//         console.log("found the property");
//       }
//     }
// const officeHours: any = loadOfficeHours();
// const monday = csv[index][3];
// const tuesday = csv[index][4];
// const wednesday = csv[index][5];
// const thursday = csv[index][6];
// const friday = csv[index][7];
// const sunday = csv[index][8];
// if (monday.trim() == "Closed") {
//   officeHours.monday = officeHours.closed;
// }
// if (tuesday.trim() == "Closed") {
//   officeHours.tuesday = officeHours.closed;
// }
// if (wednesday.trim() == "Closed") {
//   officeHours.westnesday = officeHours.closed;
// }
// if (thursday.trim() == "Closed") {
//   officeHours.thursday = officeHours.closed;
// }
// if (friday.trim() == "Closed") {
//   officeHours.friday = officeHours.closed;
// }
// if (sunday.trim() == "Closed") {
//   //officeHours. = officeHours.closed
// }
// const days = [];
// for (let day of Object.keys(officeHours)) {
//   days.push(createReference(officeHours[day]));
// }

//     const amenitiesIndex = csv[index].length - 1;
//     const tempCsv = csv[index][amenitiesIndex];
//     const arrayAmanities = tempCsv.split("\n");
//     console.log(arrayAmanities);
//     let id = 0;
//     const am = await readAmenities();
//     const temporalAmenities = [];
//     for (const amenity of arrayAmanities) {
//       const realAmenity = amenity.trim();
//       if (am[realAmenity]) {
//         //already exist
//         temporalAmenities.push(createReference(am[realAmenity]));
//       } else {
//         const amenityInterface: Iamenity = {
//           name: realAmenity,
//           description: realAmenity,
//           amenityId: id,
//         };
//         try {
//           const createdAmenity = await createAmenity(
//             contentfulManagememt,
//             amenityInterface
//           );
//           amenitiesEntries.push(createdAmenity);
//           amenities.push(amenityInterface);
//           temporalAmenities.push(
//             createReference(amenityInterface.id as string)
//           );
//         } catch (error) {
//           console.log(amenityInterface);
//           console.log("error catch", error);
//         }
//       }
//       id += 1;
//     }
//     // if (findProperty) {
//     //   return { amenities: temporalAmenities, officeHours: days };
//     // }
//     // await saveAmenities();
//     break;
//   }
// }

// Load images
async function loadFiles(id: string) {
  // the top in the filename define which image is the first one to be assigned to the location
  const path = "./resources/images";
  //const folder = await readFolders(path);

  const fullPath = path + "/location_" + id;
  console.log("fullPath", fullPath);
  const filesInFolder = await readFolders(fullPath);
  const space = await contentfulManagememt.getSpace(SPACE_ID);
  const env = await space.getEnvironment(ENVIRONMENT_ID);

  const filesArray = await uploadFiles(env, fullPath, filesInFolder);
  console.log(filesArray);
  if (filesArray.length) {
    writeFile(
      "./resources/jsons/" + fullPath,
      JSON.stringify(filesArray),
      () => {
        console.log("done");
      }
    );
  }
  return filesArray;

  //exportTocontentfull(env,)
  // //environment.createAssetFromFiles
  // contentfulManagememt.getSpace(SPACE_ID)
  //          .then((space) => space.getEnvironment(ENVIRONMENT_ID))
  //          .then((environment) => environment.createAssetFromFiles({
  //             fields: {
  //               title: {
  //                 'en-US': 'example profile from script'
  //               },
  //               description: {
  //                 'en-US': 'Asset description'
  //               },
  //               file: {
  //                 'en-US': {
  //                   contentType: 'image/png',
  //                   fileName: 'profile.png',
  //                   file: picture
  //                 }
  //               }
  //             }
  //           }))
  //         .then((asset) => asset.processForAllLocales())
  //          //publish the entry
  //          //.then((entry) => entry.publish())
  //          .then((entry) => console.log(entry))
  //          .catch(console.error)
}

// function loadOfficeHours() {
//   const tuesday = "55JzbgrJUrw7VjiMG4Py8b";
//   const thursday = "7pdOmvgvmGv808oHBF1gB9";
//   const westnesday = "26WyqJYomzbXm1IFlqH7WO";
//   const monday = "3jNlFRfOl1qZsxB2Mq46We";
//   const friday = "klVtZaN6RXkcmj34aOkpp";
//   const closed = "7aKTPsOwYUbbmKRO7sFGGh";
//   return { monday, tuesday, westnesday, thursday, friday, closed };
// }
