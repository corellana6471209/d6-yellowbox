import {
  createReference,
  SPACE_ID,
  ENVIRONMENT_ID,
  ACCESS_TOKEN,
} from "./utils";
const NodeGeocoder = require("node-geocoder");
import * as management from "contentful-management";

const options = {
  provider: "google",
  // Optional depending on the providers
  apiKey: "AIzaSyCpbHnhHkqRiS6jS7aU0kRge3C47ESjxws", // for Mapquest, OpenCage, Google Premier
  formatter: null, // 'gpx', 'string', ...
};

const geocoder = NodeGeocoder(options);

// connect to contenful instance
const contentfulManagememt = management.createClient({
  //this is optional here, should be only here if you know the specific space to use
  space: SPACE_ID,
  //the access token should be different.
  //accessToken: "GSoMrRGnvMqAMSvj0DzJZp8voHOBl8Ip4byWXoqS3D0"
  accessToken: ACCESS_TOKEN,
});

// ===================================
// Address
// ===================================
// function to create an address in contentful
export default async function createAddress(
  address1: string,
  city: string,
  state: string,
  postalCode: string,
  stateAbbreviation: string = "",
  importToContentful: boolean = false
) {
  // get geocode data from address
  const res = await geocoder.geocode(
    `${address1}, ${city} ${state} ${postalCode}`
  );

  const stateRef = getStateRef(state.toLowerCase());

  //data to import into address field
  const address: any = {
    address1: address1,
    city: city,
    stateAbbreviation: stateAbbreviation,
    postalCode: postalCode,
    state: createReference(stateRef),
    coordinates: {
      lat: res[0].latitude,
      lon: res[0].longitude,
    },
  };

  const enUs: any = [];
  for (const key of Object.keys(address)) {
    enUs[key] = {
      "en-US": address[key],
    };
  }
  const CONTENT_TYPE_ID = "address";
  const fields = {
    fields: { ...enUs },
  };

  // console.log("fields", fields);

  if (importToContentful) {
    const addressRefId = await contentfulManagememt
      .getSpace(SPACE_ID)
      .then((space) => space.getEnvironment(ENVIRONMENT_ID))
      .then((environment) => environment.createEntry(CONTENT_TYPE_ID, fields))
      //publish the entry
      //.then((entry) => entry.publish())
      .then((entry) => {
        return entry.sys.id;
      })
      .catch(console.error);
    return addressRefId;
  } else {
    console.log("data NOT imported:");
    console.log(address);
  }
}

// gets the state contentful reference ID from object
function getStateRef(state: string) {
  for (const [key, value] of Object.entries(states)) {
    if (key === state) {
      return value;
    }
  }
  return "";
}

// states reference IDs
const states = {
  illinois: "2RvTmdEfjhEywRHHTDKDuZ",
  michigan: "5OetOrXeloWnf4IqiqwiHy",
  kansas: "3tVhYwXfUbSLP12LVmkGZS",
  kentucky: "37xELx30Wpog5IO1n5hX95",
  indiana: "662WagrzPGFZUk1kbJ2IQJ",
  louisiana: "3lc5czk9ivuNodbJemedPP",
  missouri: "36eB9JpLNf18ZeFROEIjgI",
  mississippi: "5vcYvPt2ppXTX854m2AtCI",
  oklahoma: "11jpmJWY64TbZXDUsTXwwB",
  minnesota: "1ZKFhXV5gTcaL7WADCheLb",
  "new york": "4WeWLmof7tRri3mlCtya3b",
  pennsylvania: "45LSsoj8L4YcV5k77ZIqZU",
  maryland: "6RnBpPBZCMczjC5DDQs7G3",
  massachusetts: "3M2D62WSYL0wwTvPOt389S",
  "new jersey": "mx2ywENQg7fpBt0RDURB5",
  ohio: "1fYdxws9MOKh8kdz1vx7xr",
  washington: "512UV9eun9Rk0EuqvxFozG",
  texas: "6rIO5eSM1mxDXtFiiKT4ld",
  tennessee: "6ijCqjyUAwc9pYgdBdzru",
  "rhode island": "vHlbuSPekgSi3JXp0HoxW",
  california: "6KDSk3nltMgolVhNW2EyHL",
  georgia: "2UfzgScFN7dfdQTFF0snwz",
  florida: "5q18NQhh0tC1CFe2eTGcFG",
  "south carolina ": "BLOMGwMfSLekN8WXKhJCZ",
};
