import * as React from "react";
import { RemixBrowser } from "@remix-run/react";
import { hydrateRoot } from "react-dom/client";
import { cacheAssets } from "remix-utils";

function hydrate() {
  React.startTransition(() => {
    hydrateRoot(
      document,
      <>
        <RemixBrowser />
      </>
    );
  });
}

if (window.requestIdleCallback) {
  window.requestIdleCallback(hydrate);
} else {
  window.setTimeout(hydrate, 1);
}

cacheAssets().catch((error) => {
  // do something with the error, or not
});