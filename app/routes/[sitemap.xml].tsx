import type { LoaderFunction} from "@remix-run/node";
import { Response } from "@remix-run/node";
import type { Slug } from "~/utils/helpers/services";
import { getSlugs } from "~/utils/helpers/services";


export const loader: LoaderFunction = async ({ request }) => {

  const slugs = await getSlugs();
  return new Response(renderXML(slugs), {
    headers: {
      "Content-Type": "application/xml; charset=utf-8",
      "x-content-type-options": "nosniff",
      "Cache-Control": `public, max-age=${60 * 10}, s-maxage=${60 * 60 * 24}`,
    },
  });
};

const renderXML = (slugs: Slug[]) => {
  const url = "https://simplyss.com";

  const sourceXML = `<?xml version="1.0" encoding="UTF-8"?>
  <urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.sitemaps.org/schemas/sitemap/0.9 http://www.sitemaps.org/schemas/sitemap/0.9/sitemap.xsd">
    ${slugs.filter(Boolean).map((item) => `<url>
      <loc>${url}/${item.slug}</loc>
      <priority>0.7</priority>
      <lastmod>${item.lastmod}</lastmod>
    </url>`)}
  </urlset>`;

  return sourceXML;
};
