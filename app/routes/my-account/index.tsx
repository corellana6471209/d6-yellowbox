import { useState, useEffect } from "react";
import Carousel from "react-multi-carousel";
import { useLoaderData, useSubmit } from "@remix-run/react";
import { useMediaQuery } from "react-responsive";
import type { LoaderFunction, ActionFunction } from "@remix-run/node";
import { getPaymentType } from "../../utils/getPaymentType";
import moment from "moment";
import LoaderList from "~/routes/common/LoaderList";

import * as payment from "../api/Payment/ChargeCard/$information";
import * as paymentTypeRetrieve from "../api/Payment/PaymentTypesRetrieve/$locationId";
import * as tenantBillingInfoUpdate from "../api/Authentication/TenantBillingInfoUpdate/$updateInformation";
import * as tenantUpdate from "../api/Authentication/TenantUpdate/$updateInformation";
import { setCacheValueByKey } from "../api/shared/elasticCache";
//import { deleteCookie } from "../../utils/cookie";
//import contentfulKeys from "../../utils/config";

import {
  LedgersByTenantId,
  unitsInformationByUnitID,
} from "../page-components/LocationMapList/backend";
import {
  getUnitImageInformation,
  loadLocationContentByLocationId,
} from "app/routes/locations/backend";

import ManageMyAccount from "./manage-my-unit";
import { getCacheValueByKey } from "../api/shared/elasticCache";
import Icons from "../page-components/Icons";
import Unit from "../page-components/Unit";
import AlertPaymentDue from "../common/AlertPaymentDue";
import Header from "../common/Header";
import Footer from "../common/Footer";
//import Spinner from "../common/Spinner";

const responsive = {
  tablet: {
    breakpoint: { max: 768, min: 464 },
    items: 1,
  },
  mobile: {
    breakpoint: { max: 464, min: 0 },
    items: 1,
  },
};

// const delay = (ms : number) => new Promise(res => setTimeout(res, ms));

// export const validateAccessToken = async (request: Request,retry : number = 0) => {
//   const miliseconds = 200;
//   const maxRetries = 20;
//   if(! await getCacheValueByKey("accessToken", request)){
//     console.log('dont have access retry number ',retry)
//     await delay( miliseconds * retry);
//     if(retry === maxRetries){
//       console.log( maxRetries + ' retries, just fail');
//     }else{
//       await validateAccessToken(request,retry+1);
//     }
//   }
//   return true
// }

export const loader: LoaderFunction = async ({ params, request }) => {
  // await validateAccessToken(request);
  const token = await getCacheValueByKey("accessToken", request);
  const sitelinkUserAndTenantInfo = await getCacheValueByKey(
    "sitelinkUserAndTenantInfo",
    request
  );
  const user = await getCacheValueByKey("tenantInfo", request);
  const units = await getCacheValueByKey("units", request);

  return { token, sitelinkUserAndTenantInfo, user, units };
};

export const action: ActionFunction = async ({ request }) => {
  const body = await request.formData();
  let form = body.get("form") as string;
  let unit = body.get("unit") as string;
  let units = body.get("units") as string;
  let errors = body.get("errors") as string;
  let creditCardType = body.get("creditCardType") as string;
  let total = body.get("total") as string;
  let numberMonth = body.get("numberMonth") as string;
  let saveUnits = body.get("saveUnits") as string;

  //Variable to determine if we are disabling the autopa on the paymentSection
  let disableAutoPay = body.get("disableAutoPay") as string;

  let formFormatted = JSON.parse(form);
  let errorsFormatted = JSON.parse(errors);
  let unitFormatted = JSON.parse(unit);
  let unitsFormatted = JSON.parse(units);

  if (saveUnits === "true") {
    await setCacheValueByKey("units", JSON.parse(units), request);
    return "";
  }

  if (disableAutoPay === "true") {
    const tenantBillingParams: any = {
      params: {
        information: {
          locationId: unitFormatted.locationId,
          ledgerId: unitFormatted.ledgerId,
          autoBillType: 0,
        },
      },
    };
    const billingData = await tenantBillingInfoUpdate.loader(
      tenantBillingParams
    );

    if (!billingData || !billingData.data?.ledgerId) {
      return {
        showModal: true,
        message: "Disabling Auto Pay did not work. Please try again later.",
      };
    }

    /****************UPDATE UNIT AND SAVE UNITS ON MEMCACHED */
    //Get the index of the unit
    const filterUnit = (unit: any) => unit.unitId === unitFormatted.unitId;
    const indexUnit = unitsFormatted.findIndex(filterUnit);

    const unitsFiltered = unitsFormatted.filter(
      (unit: any) => unit.unitId != unitFormatted.unitId
    );

    const unitEdited = {
      ...unitFormatted,
      autoBillType: 0,
    };

    unitsFiltered.splice(indexUnit, 0, unitEdited);
    await setCacheValueByKey("units", unitsFiltered, request);
    await setCacheValueByKey("unit", unitEdited, request);

    /****************UPDATE UNIT AND SAVE UNITS ON MEMCACHED */
    return {
      showSuccessFulModal: true,
      units: unitsFiltered,
      unit: unitEdited,
    };
  } else {
    //Validate of the card date
    const numberOfDays = moment(
      `${formFormatted.expirationYear}-${formFormatted.expirationMonth}`,
      "YYYY-MM"
    ).daysInMonth();

    if (formFormatted.expirationYear && formFormatted.expirationMonth) {
      const actualDate = moment();
      const dateToValidate = moment(
        `${formFormatted.expirationYear}-${formFormatted.expirationMonth}-${numberOfDays}`
      );

      const validation = moment(actualDate).isSameOrBefore(dateToValidate);

      if (!validation) {
        return {
          showModal: true,
          message:
            "You entered an expiration date in the past. Re-enter the correct expiration date or enter a new card.",
        };
      }
    }

    if (errorsFormatted.permitTransaction) {
      return {
        showModal: true,
        message: "You must need accept this transaction.",
      };
    }

    if (
      errorsFormatted.nameOnCard ||
      errorsFormatted.cardNumber ||
      errorsFormatted.cw ||
      errorsFormatted.expirationMonth ||
      errorsFormatted.expirationYear ||
      !creditCardType ||
      creditCardType === "null"
    ) {
      return {
        showModal: true,
        message: "You must need enter valid credit card information.",
      };
    }

    if (errorsFormatted.autoPayFutureMonth) {
      return {
        showModal: true,
        message: "You must need enter valid month to autopay",
      };
    }

    if (
      errorsFormatted.fullName ||
      errorsFormatted.addressOne ||
      errorsFormatted.addressTwo ||
      errorsFormatted.city ||
      errorsFormatted.state ||
      errorsFormatted.zipCode
    ) {
      return {
        showModal: true,
        message: "You must need enter valid billing information.",
      };
    }
    const isTestMode =
      process.env.ENVIRONMENT && process.env.ENVIRONMENT == "production"
        ? false
        : true;
    //PaymentSimple
    const paymentParams: any = {
      params: {
        information: {
          locationId: unitFormatted.locationId,
          tenantId: unitFormatted.tenantId,
          unitId: unitFormatted.unitId,
          paymentAmount: total,
          creditCardType: creditCardType,
          creditCardNumber: formFormatted.cardNumber,
          creditCardCVV: formFormatted.cw,
          expirationDate: `${formFormatted.expirationYear}-${formFormatted.expirationMonth}-${numberOfDays}`,
          billingName: formFormatted.fullName,
          billingAddress: formFormatted.addressOne,
          billingZipCode: formFormatted.zipCode,
          isTestMode: isTestMode, //TestMode,
          source: 10, //Source
          paymentMethod: 0, //Payment method ---> online,
          ledgerId: unitFormatted.ledgerId,
        },
      },
    };

    const response = await payment.loader(paymentParams);

    if (!response || !response.data || !response.data?.paymentReceiptsList) {
      return {
        showModal: true,
        message: response.data?.rt?.returnMessage || null,
      };
    } else {
      //TenantBillingInfoUpdate Update data to AutoPay Detect if the user has AutoPay only for check
      const paramsPaymentType: any = {
        params: {
          locationId: unitFormatted.locationId,
        },
      };

      if (formFormatted.autoPay) {
        //console.log("-----------------------------------------------");
        const paymentTypesRetrieveData = await paymentTypeRetrieve.loader(
          paramsPaymentType
        );

        //console.log("paymentTypesRetrieveData", paymentTypesRetrieveData);

        //--------------------Get CreditCardIdType
        //Autopay endpoint if apply
        if (
          paymentTypesRetrieveData &&
          paymentTypesRetrieveData != "error" &&
          paymentTypesRetrieveData?.data?.paymentTypesList
        ) {
          const type = await getPaymentType(
            creditCardType ? creditCardType : "5",
            paymentTypesRetrieveData?.data?.paymentTypesList
          );

          //console.log("type", type);

          const tenantBillingParams: any = {
            params: {
              information: {
                locationId: unitFormatted.locationId,
                ledgerId: unitFormatted.ledgerId,
                creditCardTypeID: type[0].paymentTypeId, //CreditCardIdType
                autoBillType: 1,
                creditCardNumber: formFormatted.cardNumber,
                creditCardExpiration: `${formFormatted.expirationYear}-${formFormatted.expirationMonth}-${numberOfDays}`,
                creditCardHolderName: formFormatted.fullName,
                creditCardStreet: formFormatted.addressOne,
                creditCardZip: formFormatted.zipCode,
              },
            },
          };

          if (type && type.length > 0) {
            await tenantBillingInfoUpdate.loader(tenantBillingParams);

            //console.log("billingData", billingData);
          }
        }
      }

      ///CustomerProfile/TenantUpdate To update data of the tenant

      const tenantUpdateParams: any = {
        params: {
          information: {
            LocationId: unitFormatted.locationId,
            TenantId: unitFormatted.tenantId,
            Address1: formFormatted.addressOne,
            Address2: formFormatted.addressTwo,
            City: formFormatted.city,
            Region: formFormatted.state,
            PostalCode: formFormatted.zipCode,
          },
        },
      };
      await tenantUpdate.loader(tenantUpdateParams);

      //console.log("tenantUpdateData", tenantUpdateData);
    }

    /****************UPDATE UNIT AND SAVE UNITS ON MEMCACHED */

    //Get the index of the unit
    const filterUnit = (unit: any) => unit.unitId === unitFormatted.unitId;
    const indexUnit = unitsFormatted.findIndex(filterUnit);

    const unitsFiltered = unitsFormatted.filter(
      (unit: any) => unit.unitId != unitFormatted.unitId
    );

    const newPaidDate = moment(unitFormatted.paidThruDateTime)
      .add(formFormatted.autoPay ? 1 : numberMonth, "M")
      .format("YYYY-MM-DD");

    const unitEdited = {
      ...unitFormatted,
      autoBillType: formFormatted.autoPay ? 1 : 0,
      paidThruDateTime: newPaidDate,
    };

    unitsFiltered.splice(indexUnit, 0, unitEdited);

    await setCacheValueByKey("units", unitsFiltered, request);
    await setCacheValueByKey("unit", unitEdited, request);

    /****************UPDATE UNIT AND SAVE UNITS ON MEMCACHED */
    return {
      showSuccessFulModal: true,
      units: unitsFiltered,
      unit: unitEdited,
    };
  }
};

const MyAccount = () => {
  const inf = useLoaderData();
  const submit = useSubmit();
  const [verifyingToken, setVerifyingToken] = useState(true);
  const [units, setUnits] = useState<any>([]);
  const [showDetailPaymentView, setShowDetailPaymentView] = useState(false);
  const [detailPaymentViewType, setDetailPaymentViewType] =
    useState("detailsSection");
  const [unitSelected, setUnitSelected] = useState(null);

  useEffect(() => {
    if (!inf.token) {
      window.location.href = "/login";
      return;
    } else {
      if (inf.units) {
        setUnits(inf.units);
        setVerifyingToken(false);
      } else {
        try {
          async function fetchData() {
            /***********************************CODE TO MAKE THE OBJECT******************************************** */
            const unitsInformation: any = [];
            const siteLinkArray = inf.sitelinkUserAndTenantInfo
              ? inf.sitelinkUserAndTenantInfo.filter(
                  (site: any) =>
                    site.sitelinkUserLoginResponse === "Sitelink Login Success"
                )
              : null;

            //const siteLinkArray = inf.sitelinkUserAndTenantInfo.filter((site: any) => site.sitelinkUserTenant.sitelinkTenantId === 516839); // id works
            // const siteLinkArray = [
            // inf.sitelinkUserAndTenantInfo.filter(
            //  (site: any) =>
            // site.sitelinkUserTenant.sitelinkTenantId === 518134
            // )[0],
            //]; // id works
            //console.log('siteLinkArray',siteLinkArray)

            if (siteLinkArray && siteLinkArray.length > 0) {
              for await (const data of siteLinkArray) {
                const ledgersByTenantIdData = await LedgersByTenantId([
                  data.sitelinkUserTenant.sitelinkLocationId,
                  data.sitelinkUserTenant.sitelinkTenantId,
                ]);
                if (
                  ledgersByTenantIdData &&
                  !ledgersByTenantIdData.returnMessage
                ) {
                  if (
                    ledgersByTenantIdData.ledgers &&
                    ledgersByTenantIdData.ledgers.length > 0
                  ) {
                    // const arrayLedgers = [
                    //   ledgersByTenantIdData.ledgers[1],
                    //  ledgersByTenantIdData.ledgers[3],
                    // ];

                    for await (const ledger of ledgersByTenantIdData.ledgers) {
                      //arrayLedgers ======> ledgersByTenantIdData.ledgers
                      const unitsInformationByUnitIDData =
                        await unitsInformationByUnitID([
                          data.sitelinkUserTenant.sitelinkLocationId,
                          ledger.unitId,
                        ]);

                      if (
                        unitsInformationByUnitIDData &&
                        !unitsInformationByUnitIDData.message &&
                        unitsInformationByUnitIDData.length > 0
                      ) {
                        //Contenful endpoints === optional Information
                        const loadLocationContentData =
                          await loadLocationContentByLocationId(
                            data.sitelinkUserTenant.sitelinkLocationId
                          );
                        const unitImageInformation =
                          await getUnitImageInformation(
                            { ...unitsInformationByUnitIDData[0] },
                            data.sitelinkUserTenant.sitelinkLocationId
                          );

                        if (loadLocationContentData) {
                          unitsInformation.push({
                            ...data,
                            ...ledger,
                            ...loadLocationContentData,
                            ...unitsInformationByUnitIDData[0],
                            ...unitImageInformation,
                          });
                        } else {
                          unitsInformation.push({
                            ...data,
                            ...ledger,
                            ...unitsInformationByUnitIDData[0],
                            ...unitImageInformation,
                          });
                        }
                        setUnits([...unitsInformation]);
                        setVerifyingToken(false);
                      }
                    }
                  }
                }
              }
            }
            /***********************************CODE TO MAKE THE OBJECT******************************************** */

            const formData = new FormData();
            formData.set("units", JSON.stringify(unitsInformation));
            formData.set("saveUnits", "true");
            submit(formData, { method: "post", action: "/my-account?index" });
            //setVerifyingToken(false);
          }

          fetchData();
        } catch (error) {
          setVerifyingToken(false);
        }
      }
    }
    //deleteCookie(contentfulKeys.constants.lastPage);
  }, []);

  const openDetailsPaymentSection = (
    typeView = "detailsSection",
    unitInformation: any
  ) => {
    setUnitSelected(unitInformation);
    setDetailPaymentViewType(typeView);
    setShowDetailPaymentView(true);
  };

  const isMobile = useMediaQuery({ query: "(max-width: 768px)" });
  const mobileSection = (units: Array<any>) => {
    return (
      <div>
        {units.length > 1 ? (
          <Carousel responsive={responsive} infinite showDots arrows={false}>
            {units.map((unit: any, index) => (
              <Unit
                unit={unit}
                setNewUnits={(units: any) => setUnits(units)}
                key={index}
                openDetailsPaymentSection={(typeView: any, unit: any) =>
                  openDetailsPaymentSection(typeView, unit)
                }
              />
            ))}
          </Carousel>
        ) : (
          <Unit
            unit={units[0]}
            openDetailsPaymentSection={(typeView: any, unit: any) =>
              openDetailsPaymentSection(typeView, unit)
            }
          />
        )}
      </div>
    );
  };

  const desktopSection = (units: Array<any>) => {
    return (
      <div>
        {units.map((unit: any, index) => (
          <Unit
            ket={index}
            unit={unit}
            key={index}
            openDetailsPaymentSection={(typeView: any, unit: any) =>
              openDetailsPaymentSection(typeView, unit)
            }
          />
        ))}
      </div>
    );
  };

  return (
    <section className="div-my-account">
      <Header />
      {showDetailPaymentView ? (
        <>
          <ManageMyAccount
            units={units}
            setNewUnits={(units: any, unit: any) => {
              setUnits(units);
              setUnitSelected(unit);
            }}
            detailPaymentViewType={detailPaymentViewType}
            unitSelected={unitSelected}
            setShowDetailPaymentView={() => setShowDetailPaymentView(false)}
            setUnitSelected={(unit: any) => setUnitSelected(unit)}
            openDetailsPaymentSection={(typeView: any, unit: any) =>
              openDetailsPaymentSection(typeView, unit)
            }
          />
        </>
      ) : (
        <div className="relative mx-auto max-w-[900px]">
          <AlertPaymentDue units={units} />
          {inf && inf.user && (
            <h1 className="mb-4 text-center font-serif text-36 text-greyHeavy">
              Hi, {inf.user.firstName}
            </h1>
          )}

          {verifyingToken ? (
            <div className="unit-my-account">
              <LoaderList type="Unit" num={4} height="" />
            </div>
          ) : (
            ""
          )}

          {units && units.length > 0 ? (
            isMobile ? (
              mobileSection(units)
            ) : (
              desktopSection(units)
            )
          ) : (
            <>
              {verifyingToken ? (
                ""
              ) : (
                <div className="d-flex not-units items-center justify-center">
                  <p className="text-36">No reserved units</p>
                </div>
              )}
            </>
          )}
          <Icons />
        </div>
      )}
      <Footer />
    </section>
  );
};

export default MyAccount;
