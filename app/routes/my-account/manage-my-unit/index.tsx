import { useState, useEffect } from "react";
import background from "../../../resources/dropdown.svg";
import ManageMyAccountDetails from "../manage-my-unit/components/details";
import ManageMyAccountPayment from "../manage-my-unit/components/payment";
import ChevronLeft from "../../common/svg/ChevronLeft";
import { getLabel } from "../../../utils/getLabel";

const ManageMyAccount = (props: any) => {
  const [showBackButton, setShowBackButton] = useState<any>(true);
  const [unitTitle, setUnitTitle] = useState("Manage My Unit");

  const {
    units,
    detailPaymentViewType,
    unitSelected,
    setShowDetailPaymentView,
    setUnitSelected,
    openDetailsPaymentSection,
    setNewUnits,
  } = props;

  useEffect(() => {
    window.scrollTo({ top: 0, behavior: "smooth" });
  }, [detailPaymentViewType]);

  useEffect(() => {
    async function fetchData() {
      //Labels
      const myAccountTitle = await getLabel("myAccountTitle");
      setUnitTitle(myAccountTitle ? myAccountTitle : unitTitle);
    }

    fetchData();
  }, []);

  // ================================
  // Tabs
  // ================================

  const handleTab = (tab: string) => {
    openDetailsPaymentSection(tab, unitSelected);
    setShowBackButton(true);
  };

  const handleChange = async (evt: any) => {
    const value = evt.target.value;
    const unit = units.filter((unit: any) => unit.unitId == value);
    if (unit && unit.length > 0) {
      setUnitSelected(unit[0]);
    } else {
      setShowDetailPaymentView();
    }
  };

  return (
    <section className="div-my-manage-unit">

      <h1 className="mb-5 text-center text-28 text-greyHeavy">{unitTitle}</h1>

      {units &&
        units.length > 0 &&
        detailPaymentViewType != "paymentNormal" &&
        detailPaymentViewType != "paymentNormalAutoPay" && (
          <div className="d-flex mb-8 justify-center pl-8 pr-8 sm:pl-12 sm:pr-12">
            <select
              style={{ backgroundImage: `url(${background})` }}
              name="card"
              onChange={handleChange}
              value={unitSelected.unitId}
              id="card"
              className="select-filter-sort-by-sort w-100 mt-4"
            >
              <option value="">Unit name</option>
              {units.map((unit: any, index: any) => (
                <option key={index} value={unit.unitId}>
                  {unit.unitTypeName} {unit.width}’ X {unit.length}’
                </option>
              ))}
            </select>
          </div>
        )}

      <div className="mx-auto max-w-[1200px]">
        <div className="div-tab-filter">
          <ul className="LocationMapList-map-tabs space-between grid grid-cols-12 bg-secondary4 p-0 lg:grid-cols-12">
            {/* <li className="hidden lg:col-span-1 lg:block"></li> */}
            <li className="LocationMapList-map-tabs-map col-span-6 list-none text-20">
              <button
                className={`w-full py-4 font-bold lg:cursor-auto
                            ${
                              detailPaymentViewType === "detailsSection"
                                ? "bg-secondary2 text-secondary4"
                                : "bg-secondary4 text-secondary2"
                            }
                        `}
                onClick={() => handleTab("detailsSection")}
              >
                Details
              </button>
            </li>
            <li className="LocationMapList-map-tabs-list col-span-6 list-none">
              <button
                className={`w-full py-4 text-20 font-bold lg:cursor-auto
                            ${
                              detailPaymentViewType === "paymentNormal" ||
                              detailPaymentViewType ===
                                "paymentNormalAutoPay" ||
                              detailPaymentViewType === "paymentSection"
                                ? "bg-secondary2 text-secondary4 "
                                : "bg-secondary4 text-secondary2"
                            }`}
                onClick={() => handleTab("paymentSection")}
              >
                Payment
              </button>
            </li>
          </ul>
        </div>
      </div>

      <section className="div-my-account-unit-details">
        <div className="div-wrapper-my-account-details">
          {showBackButton && (
            <div onClick={() => setShowDetailPaymentView()}>
              <p className="d-flex mt-5 mb-5 cursor-pointer align-middle text-16 font-bold text-secondary1">
                <ChevronLeft size="25px" color="#00155F" />
                Back
              </p>
            </div>
          )}
          {detailPaymentViewType === "paymentNormal" ||
          detailPaymentViewType === "paymentNormalAutoPay" ||
          detailPaymentViewType === "paymentSection" ? (
            <ManageMyAccountPayment
              units={units}
              setNewUnits={(units: any, unit: any) => setNewUnits(units, unit)}
              setShowBackButton={(value: any) => setShowBackButton(value)}
              showBackButton={showBackButton}
              detailPaymentViewType={detailPaymentViewType}
              unit={unitSelected}
              openDetailsPaymentSection={(typeView: any, unit: any) =>
                openDetailsPaymentSection(typeView, unit)
              }
            />
          ) : (
            <ManageMyAccountDetails
              unit={unitSelected}
              openDetailsPaymentSection={(typeView: any, unit: any) =>
                openDetailsPaymentSection(typeView, unit)
              }
            />
          )}
        </div>
      </section>
    </section>
  );
};

export default ManageMyAccount;
