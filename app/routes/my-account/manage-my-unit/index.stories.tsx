import type { ComponentStory, ComponentMeta } from "@storybook/react";
import { BrowserRouter as Router } from "react-router-dom";
import ManageMyAccount from ".";

export default {
  title: "components/core/ManageMyAccount",
  component: ManageMyAccount,
} as ComponentMeta<typeof ManageMyAccount>;

const Template: ComponentStory<typeof ManageMyAccount> = (args) => (
  <Router>
    <ManageMyAccount />
  </Router>
  // {...args}
);

export const Primary = Template.bind({});
/**
 * Primary.args = {
  unit: fakeDataUnit,
  ctaText: "Save Now with No Obligation",
};
 */
