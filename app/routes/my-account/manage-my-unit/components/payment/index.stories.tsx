import type { ComponentStory, ComponentMeta } from "@storybook/react";
import { BrowserRouter as Router } from "react-router-dom";
import ManageMyAccountPayment from "./index";

export default {
  title: "components/core/ManageMyAccount/ManageMyAccountPayment",
  component: ManageMyAccountPayment,
} as ComponentMeta<typeof ManageMyAccountPayment>;

const Template: ComponentStory<typeof ManageMyAccountPayment> = (args) => (
  <Router>
    <ManageMyAccountPayment />
  </Router>
  // {...args}
);

export const Primary = Template.bind({});
/**
 * Primary.args = {
  unit: fakeDataUnit,
  ctaText: "Save Now with No Obligation",
};
 */
