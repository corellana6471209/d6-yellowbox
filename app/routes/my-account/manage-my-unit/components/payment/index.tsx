import UnitPayment from "../../../../page-components/UnitPayment";

const ManageMyAccountPayment = (props: any) => {
  const {
    setShowBackButton,
    showBackButton,
    detailPaymentViewType,
    unit,
    units,
    openDetailsPaymentSection,
    setNewUnits,
  } = props;

  return (
    <div className="div-body-unit-payment mb-[40px]">
      <UnitPayment
        setNewUnits={(units: any, unit:any) => setNewUnits(units, unit)}
        setShowBackButton={(value: any) => setShowBackButton(value)}
        showBackButton={showBackButton}
        units={units}
        detailPaymentViewType={detailPaymentViewType}
        openDetailsPaymentSection={(typeView: any, unit: any) =>
          openDetailsPaymentSection(typeView, unit)
        }
        unit={unit}
      />
    </div>
  );
};

export default ManageMyAccountPayment;
