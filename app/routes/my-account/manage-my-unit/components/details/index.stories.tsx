import type { ComponentStory, ComponentMeta } from "@storybook/react";
import { BrowserRouter as Router } from "react-router-dom";
import ManageMyAccountDetails from "./index";

export default {
  title: "components/core/ManageMyAccount/ManageMyAccountDetails",
  component: ManageMyAccountDetails,
} as ComponentMeta<typeof ManageMyAccountDetails>;

const Template: ComponentStory<typeof ManageMyAccountDetails> = (args) => (
  <Router>
    <ManageMyAccountDetails />
  </Router>
  // {...args}
);

export const Primary = Template.bind({});
/**
 * Primary.args = {
  unit: fakeDataUnit,
  ctaText: "Save Now with No Obligation",
};
 */
