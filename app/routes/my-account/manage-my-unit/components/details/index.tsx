import UnitDetails from "../../../../page-components/UnitDetails";

const ManageMyAccountDetails = (props: any) => {
  const { unit, openDetailsPaymentSection } = props;

  const unitTypeImage = unit?.locationImageCollection?.items[0]?.url || null;
  return (
    <div className="div-body-unit-details">
      <div className="div-my-account-unit-details-img">
        <img
          src={
            unitTypeImage
              ? unitTypeImage
              : "https://via.placeholder.com/900x600"
          }
          alt="unit"
        />
      </div>

      <UnitDetails
        unit={unit}
        openDetailsPaymentSection={(typeView: any, unit: any) =>
          openDetailsPaymentSection(typeView, unit)
        }
      />
    </div>
  );
};

export default ManageMyAccountDetails;
