import type { ComponentStory, ComponentMeta } from "@storybook/react";
import { BrowserRouter as Router } from "react-router-dom";
import MyAccount from "./index";

export default {
  title: "components/core/MyAccount",
  component: MyAccount,
} as ComponentMeta<typeof MyAccount>;

const Template: ComponentStory<typeof MyAccount> = (args) => (
  <Router>
    <MyAccount />
  </Router>
  // {...args}
);

export const Primary = Template.bind({});
/**
 * Primary.args = {
  unit: fakeDataUnit,
  ctaText: "Save Now with No Obligation",
};
 */
