import type { LoaderArgs } from "@remix-run/server-runtime";
import { handlePageData } from "../models/component.server";
import type { DynamicLinksFunction } from "remix-utils";
import type { LinksFunction, MetaFunction } from "@remix-run/node";
import { promiseHash } from "remix-utils";
import { getCacheValueByKey } from "./api/shared/elasticCache";
//import { getCacheValueByKey } from "./api/shared/elasticCache";
export * from "./$";
export { default } from "./$";

export const meta: MetaFunction = ({ data }) => {
  if (data?.pageCollection?.items?.length > 0) {
    return {
      title: `${data.pageCollection.items[0].seoTitle}`,
      description: `${data.pageCollection.items[0].seoDescription}`,
      robots: `${data.pageCollection.items[0].robotsMetaSettings}`,
      "og:image": `${
        data.pageCollection.items[0].pageImage
          ? data.pageCollection.items[0].pageImage.url
          : ""
      }`,
    };
  } else {
    return {};
  }
};

// let dynamicLinks: DynamicLinksFunction = ({ data }) => {
//   if (data.pageCollection.items.length > 0) {
//     // if (!data.user) return [];
//     return [
//       {
//         rel: "canonical",
//         href: data.pageCollection.items[0].canonicalUrl
//           ? data.pageCollection.items[0].canonicalUrl
//           : "",
//       },
//     ];
//   }
// };

// export let handle = { dynamicLinks };

export async function loader({ params, request }: LoaderArgs) {
  // get slug from params
  const slug = params["*"];
  if (slug && slug.startsWith("_static")) {
    throw new Response("Not Found", {
      status: 404,
    });
  }
  // homepage doesnt have a slug so we have to get it another way
  let w = `{title: "Home Page"}`;
  if (params["*"]) {
    w = `{slug: "${slug}"}`;
  }
  const page = await handlePageData(w);
  if (!page || !page.pageCollection || !page.pageCollection.items) {
    throw new Response("Not Found", {
      status: 404,
    });
  }
  //   return page;
  const token = await getCacheValueByKey("accessToken", request);
  const user = await getCacheValueByKey("tenantInfo", request);
  const sitelinkUserAndTenantInfo = getCacheValueByKey(
    "sitelinkUserAndTenantInfo",
    request
  );
  page.token = token;
  page.user = user;
  page.sitelinkUserAndTenantInfo = sitelinkUserAndTenantInfo;
  return promiseHash(page);
}
