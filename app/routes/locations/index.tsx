import type { LoaderArgs } from "@remix-run/server-runtime";
import { handlePageData } from "../../models/component.server";
import { getCacheValueByKey } from "../api/shared/elasticCache";
//import { run,read } from "../api/shared/elasticCache";
export * from "./$";
export { default } from "./$";

export async function loader({ params, request }: LoaderArgs) {
  const token = await getCacheValueByKey("accessToken", request);
  const user = await getCacheValueByKey("tenantInfo", request);
  const slug = `{slug: "locations"}`;
  const page = await handlePageData(slug);
  page.token = token;
  page.user = user;
  return page;
}
