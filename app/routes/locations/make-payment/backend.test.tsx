import { describe, expect, it } from "vitest";
import { getReservationList } from "../../locations/backend";
import { sendPayment, getPrices } from "./backend";

const locationId = 5;
const information = ["jose", 15.6, 5];

const fetchFunction = {
  json: function () {
    return {
      data: [
        {
          name: "Jose Ortega",
          reservationId: 411768,
          tenatId: 34234,
          nameLock: "Lock BestLock",
          price: 15.6,
        },
      ],
      errors: null,
    };
  },
};
global.fetch = async () => {
  return fetchFunction;
};

describe("Reservation-list-BE", () => {
  it("Verify the name of the reservation-list-BE", async () => {
    const data = await getReservationList(locationId);
    expect(data.name).toBe("Jose Ortega");
  });

  it("Verify the reservationId of the reservation-list-BE", async () => {
    const data = await getReservationList(locationId);
    expect(data.reservationId).toBe(411768);
  });
});

describe("SendPayment-BE", () => {
  it("Verify the name of the sendPayment-BE", async () => {
    const data = await sendPayment(information);
    expect(data?.data[0].name).toBe("Jose Ortega");
  });

  it("Verify the tenatId of the sendPayment-BE", async () => {
    const data = await sendPayment(information);
    expect(data?.data[0].tenatId).toBe(34234);
  });
});

describe("GetPrices-BE", () => {
  it("Verify the name-lock of the getPrices-BE", async () => {
    const data = await getPrices(information);
    expect(data?.data[0].nameLock).toBe("Lock BestLock");
  });

  it("Verify the total of the getPrices-BE", async () => {
    const data = await getPrices(information);
    expect(data?.data[0].price).toBe(15.6);
  });
});
