import type { ComponentStory, ComponentMeta } from "@storybook/react";
import { BrowserRouter as Router } from "react-router-dom";

//import { fakeDataUnit } from "../../../utils/fakedataToTests";
import ReservationUnit from "./$reservation";

export default {
  title: "components/core/MakePayment",
  component: ReservationUnit,
} as ComponentMeta<typeof ReservationUnit>;

const Template: ComponentStory<typeof ReservationUnit> = (args) => (
  <Router>
    <ReservationUnit />
  </Router>
  // {...args}
);

export const Primary = Template.bind({});
/**
 * Primary.args = {
  unit: fakeDataUnit,
  ctaText: "Save Now with No Obligation",
};
 */
