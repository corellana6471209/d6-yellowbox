import { useState } from "react";
import Mastercard from "../../../../common/svg/Mastercard";
import Visa from "../../../../common/svg/Visa";
import AmericanExpress from "../../../../common/svg/AmericanExpress";
import Discover from "../../../../common/svg/Discover";
import { validationCard } from "../../../../../utils/validationCard";
import background from "../../../../../resources/dropdown.svg";

function PaymentDetails(props: any) {
  const {
    getInformation,
    errors,
    errorsOnValidation,
    setErrors,
    setErrorsOnValidation,
    labels,
  } = props;
  const actualYear = new Date().getFullYear();
  const [form, setForm] = useState({
    card: "5",
    cardNumber: "",
    cw: "",
    expirationMonth: "",
    expirationYear: "",
    autoPay: true,
  });
  const [CWLenght, setCWLenght] = useState<number>(3);

  const handleChange = async (evt: any) => {
    //Only permit number on the cardNumber and cw input
    const re = /^[0-9\b]+$/;
    if (
      (evt.target.name === "cardNumber" || evt.target.name === "cw") &&
      !re.test(evt.target.value) &&
      evt.target.value !== ""
    ) {
      return;
    }

    //Recheck when the card changes
    if (evt.target.name === "card") {
      const lenCW = evt.target.value == 7 ? 4 : 3;
      setCWLenght(lenCW);
      checkCW(form.cw, lenCW);
      verifyData(
        { target: { name: "cardNumber", value: form.cardNumber } },
        evt.target.value
      );
    }

    const value = evt.target.value;
    getInformation(evt.target.name, value);

    setForm({
      ...form,
      [evt.target.name]: value,
    });
  };

  const handleCheck = (evt: any) => {
    //Function to send the information
    getInformation(evt.target.value, evt.target.checked);
    setForm({
      ...form,
      [evt.target.value]: evt.target.checked,
    });
  };

  //Verify data onBlur
  const verifyData = async (evt: any, cardType: any = null) => {
    //Get the errors
    const errorsSelected = { ...errors };
    const errorsToValidate = { ...errorsOnValidation };

    const value = evt.target.value;
    const name = evt.target.name;
    //Verify onBlur the cardNumber
    if (name === "cardNumber") {
      const validation = validationCard(value, cardType);

      if (!validation) {
        setErrors({ ...errorsSelected, [name]: true });
        setErrorsOnValidation({ ...errorsToValidate, [name]: true });
      } else {
        setErrors({ ...errorsSelected, [name]: false });
        setErrorsOnValidation({ ...errorsToValidate, [name]: false });
      }
    } else if (name === "cw") {
      //Verify onBlur the CVV
      checkCW(value, CWLenght);
    } else if (
      name === "expirationMonth" &&
      (value === "Expiration MM" || value === "")
    ) {
      //Verify onBlue the expiration month
      setErrors({ ...errorsSelected, expirationMonth: true });
      setErrorsOnValidation({ ...errorsToValidate, [name]: true });
    } else if (
      name === "expirationYear" &&
      (value === "Expiration YYYY" || value === "")
    ) {
      //Verify onBlue the expirationYear
      setErrors({ ...errorsSelected, expirationYear: true });
      setErrorsOnValidation({ ...errorsToValidate, [name]: true });
    } else {
      setErrors({ ...errorsSelected, [name]: false });
      setErrorsOnValidation({ ...errorsToValidate, [name]: false });
    }
  };

  //Get the CW errors
  const checkCW = (value: any, CWLenght: any) => {
    const re = new RegExp(`^[0-9]{${CWLenght}}$`);
    const validate = !re.test(value);
    errors.cw = validate;
    errorsOnValidation.cw = validate;
    setErrors({ ...errors });
    setErrorsOnValidation({ ...errorsOnValidation });
  };

  //Adding the years dynamically
  const optionSelect = () => {
    const options = [
      <option value="" key={actualYear - 1}>
        Expiration YYYY
      </option>,
    ];

    for (let i = actualYear; i < actualYear + 10; i++) {
      options.push(
        <option value={i} key={i}>
          {i}
        </option>
      );
    }

    return options;
  };

  return (
    <section className="div-payment-details">
      <p className="mt-16 mb-4 text-center font-serif text-28 text-greyHeavy md:text-36">
        {labels.paymentDetails}
      </p>

      <div className="div-icon-cards flex justify-center">
        <Mastercard />
        <Visa />
        <AmericanExpress />
        <Discover />
      </div>
      <div className="div-form-payment-details mt-3">
        <div className="mt-4 bg-white">
          <select
            name="card"
            onChange={handleChange}
            style={{ backgroundImage: `url(${background})` }}
            value={form.card}
            id="card"
            className="select-filter-sort-by-payment w-100"
          >
            <option value="5">MasterCard</option>
            <option value="6">Visa</option>
            <option value="7">American Express</option>
            <option value="8">Discover</option>
          </select>
        </div>

        <div className="flex">
          <div className="div-body-input-card-number">
            <div className="div-input-payment-detail mr-4">
              <input
                autoComplete="off"
                autoComplete="chrome-off"
                onChange={handleChange}
                onBlur={(e) => verifyData(e, form.card)}
                value={form.cardNumber}
                name="cardNumber"
                className="input input-card-number w-100"
                type="text"
                placeholder="Card Number"
              />
            </div>
            {errors && errors.cardNumber && (
              <p className="message-error text-left">This number is invalid</p>
            )}
          </div>

          <div className="div-body-input-card-cw">
            <div className="div-input-payment-detail-cw">
              <input
                autoComplete="off"
                autoComplete="chrome-off"
                onChange={handleChange}
                onBlur={(e) => verifyData(e)}
                value={form.cw}
                name="cw"
                className="input input-cw w-100"
                type="text"
                placeholder="CW"
                maxLength={CWLenght}
              />
            </div>
            {errors && errors.cw && (
              <p className="message-error text-left">Cw invalid</p>
            )}
          </div>
        </div>

        <div className="grid grid-cols-1 gap-1 sm:grid-cols-2 sm:gap-2 lg:grid-cols-1 lg:gap-1">
          <div>
            <div className="mt-4 bg-white">
              <select
                onChange={handleChange}
                onBlur={(e) => verifyData(e)}
                style={{ backgroundImage: `url(${background})` }}
                value={form.expirationMonth}
                name="expirationMonth"
                id="expirationMonth"
                className="select-filter-sort-by-payment w-100"
              >
                <option value="" key="select your state">
                  Expiration MM
                </option>
                <option value="01">January</option>
                <option value="02">February</option>
                <option value="03">March</option>

                <option value="04">April</option>
                <option value="05">May</option>
                <option value="06">June</option>
                <option value="07">July</option>
                <option value="08">August</option>

                <option value="09">September</option>
                <option value="10">October</option>
                <option value="11">November</option>
                <option value="12">December</option>
              </select>
            </div>

            {errors && errors.expirationMonth && (
              <p className="message-error text-left">The month is required</p>
            )}
          </div>

          <div>
            <div className="mt-4 bg-white">
              <select
                autoComplete="off"
                onChange={handleChange}
                onBlur={(e) => verifyData(e)}
                style={{ backgroundImage: `url(${background})` }}
                value={form.expirationYear}
                name="expirationYear"
                id="expirationYear"
                className="select-filter-sort-by-payment w-100"
              >
                {optionSelect()}
              </select>
            </div>

            {errors && errors.expirationYear && (
              <p className="message-error text-left">
                The expiration's year is required
              </p>
            )}
          </div>
        </div>

        <label className="mt-8 flex items-center pb-16 text-12 text-16 text-greyHeavy">
          <input
            autoComplete="off"
            onChange={handleCheck}
            type="checkbox"
            className="radio mr-4"
            value="autoPay"
            checked={form.autoPay}
          />
          {labels.autoPayCheckBox}
        </label>
      </div>
    </section>
  );
}

export default PaymentDetails;
