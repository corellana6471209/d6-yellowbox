import React from "react";
import { describe, it } from "vitest";
import { render, screen } from "../../../../../utils/test-utils";
import { BrowserRouter as Router } from "react-router-dom";
import ProtectionBelong from "./protectionBelong";
import { locationContent } from "../../../../../utils/fakedataToTests";

vi.mock("./RichTextComponent", () => {
  return {
    default: "RichTextComponent-component-string",
  };
});

describe("ProtectionBelong component unit test", () => {
  beforeEach(() => {
    const setState = vi.fn();
    const useStateSpy = vi.spyOn(React, "useState");
    useStateSpy.mockImplementation((initialValue) => [initialValue, setState]);
  });
  afterEach(() => {
    vi.clearAllMocks();
  });

  it("verify the render the ProtectionBelong component", () => {
    render(
      <Router>
        <ProtectionBelong
          locationContent={locationContent}
          getInformation={() => console.log()}
        />
      </Router>
    );
  });
});
