import React, { useEffect } from "react";
import ReactTooltip from "react-tooltip";
import moment from "moment";
import InfoCircle from "../../../../common/svg/InfoCircle";

const actualDay = moment().format("MM/DD/YY");

function AmountPrices(props: any) {
  const {
    sendPaymentInformation,
    infReservation,
    dataPrices,
    total,
    labels,
    isSubmiting,
  } = props;

  useEffect(() => {
    ReactTooltip.rebuild();
  });

  const tooltip = (labels: any, type: any = "default") => {
    let text = labels.tooltipText;

    if (type === "Administrative Fee") {
      text = labels.tooltipTextAdministrative;
    } else if (type === "First Month Insurance") {
      text = labels.tooltipTextFirstMonth;
    } else if (type === "Lock Fee (Taxes Included)") {
      text = labels.tooltipTextLockFee;
    }

    return (
      <span
        className="ml-1 cursor-pointer"
        data-tip={text}
        data-place="right"
        data-background-color="white"
        data-text-color="black"
      >
        <InfoCircle size="16" color="#FFFFFF" />
      </span>
    );
  };

  return (
    <section className="section-amount-prices mb-14 text-white">
      <ReactTooltip />

      <div className="div-section-amount-prices">
        <p className="text-16">
          <span className="mr-1 text-28 font-bold">
            ${infReservation.pushRate}
          </span>
          /mo
          <span className="slash-price ml-5 text-28 font-bold text-greyLight">
            ${infReservation.inStoreRate}
          </span>
        </p>
        <p className="text-16">Online only price</p>

        {/**
         *  <p className="mt-10 mb-4 flex items-center text-16 font-bold">
          Promotion: Rest of {informationDate.monthName} Free
          <span
            className="ml-1 cursor-pointer"
            data-tip={labels.tooltipText}
            data-place="right"
            data-background-color="white"
            data-text-color="black"
          >
            <InfoCircle size="16" color="#FFFFFF" />
          </span>
        </p>
         */}

        <div className="divider-amount-prices mt-6 mb-6"></div>

        {dataPrices && dataPrices.length > 0 && (
          <>
            {dataPrices.map((price: any) => (
              <div
                className="mt-3 flex justify-between"
                key={price.chargeDescription}
              >
                <div className="flex items-center">
                  <p className="text-16">{price.chargeDescription}</p>

                  {price.chargeDescription != "First Monthly Rent Fee" &&
                    tooltip(labels, price.chargeDescription)}
                </div>

                <p className="text-16 font-bold">
                  {price.chargeDescription === "First Monthly Rent Fee" && (
                    <span className="slash-price mr-5 font-bold text-greyLight">
                      ${infReservation.pushRate}
                    </span>
                  )}
                  ${price.total.toFixed(2)}
                </p>
              </div>
            ))}
          </>
        )}

        <div className="mt-3 mb-6 flex justify-between">
          <p className="text-16 font-bold text-primary1">Your Total Savings</p>
          <p className="text-16 font-bold text-primary1">
            -$
            {(infReservation.inStoreRate - infReservation.pushRate).toFixed(2)}
            /Mo
          </p>
        </div>

        <div className="divider-amount-prices"></div>

        {infReservation && (
          <div className="mt-6 flex justify-between">
            <p className="text-16 text-white">
              Your lease start date: {actualDay}
            </p>
          </div>
        )}

        <div className="div-total-due-today mt-1 flex">
          <p className="text-20">
            Total Due Today:{" "}
            <span className="text-30 font-bold md:text-40">
              ${total}
            </span>
          </p>
        </div>

        <div
          className="div-button-pay-now mt-10"
          onClick={() => sendPaymentInformation()}
        >
          <button
            className={`${
              isSubmiting
                ? "button-disabled  block py-3 px-2.5"
                : "block bg-primary1 yellow-button py-3 text-center text-secondary1 no-underline"
            }`}
            disabled={isSubmiting}
          >
            {labels.paymentButton}
          </button>
        </div>
      </div>
    </section>
  );
}

export default AmountPrices;
