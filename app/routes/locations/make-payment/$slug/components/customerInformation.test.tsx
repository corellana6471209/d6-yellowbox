import React from "react";
import { describe, it } from "vitest";
import { render, screen } from "../../../../../utils/test-utils";
import { BrowserRouter as Router } from "react-router-dom";
import CustomerInformation from "./customerInformation";
import {
  unitInformation,
  labels,
  infReservation,
  initialStateErrors,
  errorsOnValidation,
} from "../../../../../utils/fakedataToTests";

describe("CustomerInformation component unit test", () => {
  beforeEach(() => {
    const setState = vi.fn();
    const useStateSpy = vi.spyOn(React, "useState");
    useStateSpy.mockImplementation((initialValue) => [initialValue, setState]);
  });
  afterEach(() => {
    vi.clearAllMocks();
  });

  it("verify the render the ProtectionBelong component", () => {
    render(
      <Router>
        <CustomerInformation
          unitInformation={unitInformation}
          labels={labels}
          infReservation={infReservation}
          isLogged={true}
          errors={initialStateErrors}
          errorsOnValidation={errorsOnValidation}
          setErrors={() => console.log()}
          setErrorsOnValidation={() => console.log()}
          userInformation={infReservation}
          getInformation={() => console.log()}
        />
      </Router>
    );
  });

  it("verify the render the inputs component", async () => {
    const { container } = render(
      <Router>
        <CustomerInformation
          unitInformation={unitInformation}
          labels={labels}
          infReservation={infReservation}
          isLogged={true}
          errors={initialStateErrors}
          errorsOnValidation={errorsOnValidation}
          setErrors={() => console.log()}
          setErrorsOnValidation={() => console.log()}
          userInformation={infReservation}
          getInformation={() => console.log()}
        />
      </Router>
    );

    expect(await container.getElementsByClassName("div-input").length).toBe(8);
  });
});
