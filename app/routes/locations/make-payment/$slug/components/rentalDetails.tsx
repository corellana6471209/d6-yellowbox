//import { useState } from "react";
import UnitCard from "../../../reserve-unit/$slug/components/unitCard";
//import Button from "../../../../common/Button";
import calculateDistance from "../../../../../utils/hooks/useDistance";
import { getAddressCoords } from "../../../../../utils/cookie";
import moment from "moment";

function RentalDetails(props: any) {
  const coords = getAddressCoords();
  const { locationContent, infReservation, labels, unitInformation } = props;
  const { expiresDateTime } = infReservation;

  /**********************Information from props******* */
  const { address, phoneNumberNewCustomer } = locationContent;

  const date = moment(expiresDateTime).format("MMMM DD, YYYY");

  const distance = calculateDistance(
    address.coordinates.lat,
    address.coordinates.lon
  );

  const getDirection = () => {
    if (coords) {
      const dataCoords = JSON.parse(coords);
      window.open(
        `https://www.google.com/maps/dir/${dataCoords.lat},${dataCoords.lng}/${address.coordinates.lat},${address.coordinates.lon}/@${dataCoords.lat},${dataCoords.lng},13z`,
        "_blank"
      );
    } else {
      window.open(
        `https://www.google.com/maps/dir//${address.coordinates.lat},${address.coordinates.lon}/@${address.coordinates.lat},${address.coordinates.lon},14z/`,
        "_blank"
      );
    }
  };

  const sectionDetails = () => {
    return (
      <section className="grid grid-cols-2 gap-4 md:gap-10">
        <div>
          <div>
            <p className="text-14 font-bold xs:text-16 lg:text-24">Address</p>
            <p className="text-14 xs:text-16 lg:text-24">{address.address1}</p>
            <p className="text-14 xs:text-16 lg:text-24">
              {address.city}, {address?.state?.stateAbbreviation}{" "}
              {address.postalCode}
            </p>
          </div>

          {distance && (
            <div className="mt-4">
              <p className="text-14 font-bold xs:text-16 lg:text-24">
                {distance ? `${Number(distance).toFixed(2)} mi` : ""}
              </p>
            </div>
          )}

          <div className="div-body-direction">
            <p
              className="text-direction link mt-4 cursor-pointer text-14 font-bold text-callout xs:text-16 lg:text-24"
              onClick={() => getDirection()}
            >
              Get Directions
            </p>
          </div>
        </div>

        <div>
          <div>
            <p className="text-14 font-bold xs:text-16 lg:text-24">
              Phone Number
            </p>
            <p className="text-14 xs:text-16 lg:text-24">
              {phoneNumberNewCustomer}
            </p>
          </div>
        </div>
      </section>
    );
  };

  return (
    <div className="div-body-reservation-details  ml-5 mr-5">
      <p className="mt-8 mb-6 text-center font-serif text-28 text-greyHeavy lg:text-36">
        {labels.rentalDetailsTitle}
      </p>
      {unitInformation && unitInformation ? (
        <UnitCard unitInformation={unitInformation} />
      ) : (
        ""
      )}

      <p className="div-fee-text mb-8 text-center text-10 text-greyHeavy md:text-12 lg:mt-6 lg:mb-14">
        <span className="relative text-20 text-callout">*</span>
        {labels.disclaimerLabel}
      </p>
      <div className="div-body-reservation d-flex justify-center">
        <section className="div-body-reservation-details-information">
          <div className="div-text mb-4">
            <div className="grid grid-cols-2 gap-4 md:gap-10">
              <div>
                <p className="text-14 font-bold xs:text-16 lg:text-24">Unit</p>
                <p className="text-14 xs:text-16 lg:text-24">
                  {unitInformation.unitName}
                </p>
              </div>

              <div>
                <p className="text-14 font-bold xs:text-16 lg:text-24">
                  Reservation Held Until
                </p>
                <p className="text-14 xs:text-16 lg:text-24">{date}</p>
              </div>
            </div>
          </div>

          {/*<TermsModal open={true} /> */}
          <div className="div-text-location mt-3">{sectionDetails()}</div>
        </section>
      </div>
    </div>
  );
}

export default RentalDetails;
