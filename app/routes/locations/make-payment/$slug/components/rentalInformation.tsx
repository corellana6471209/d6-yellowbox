import { useState } from "react";
import GlobalRichTextModal from "../../../../common/Modals/GlobalRichText";

function RentalInformation(props: any) {
  const { labels, dataToSend, getInformation } = props;
  const [openModalTerm, setOpenModalTerm] = useState<any>(false);

  const [form, setForm] = useState({
    rentalRenew: false,
  });

  const handleCheck = (evt: any) => {
    //Function to send the information
    getInformation(evt.target.value, evt.target.checked);
    setForm({
      ...form,
      [evt.target.value]: evt.target.checked,
    });
  };

  const openTermConditionsModal = () => {
    //Function to send the information
    setOpenModalTerm(true);
    getInformation("rentalRenew", true);
    setForm({
      ...form,
      ["rentalRenew"]: true,
    });
  };

  return (
    <section className="div-payment-details">
      <GlobalRichTextModal
        open={openModalTerm}
        callback={() => {
          setOpenModalTerm(false);
        }}
      />

      {dataToSend && dataToSend.autoPay && (
        <p className="mb-8 mt-4 mb-10 text-16 text-greyHeavy">
          {labels.hassleFreeText}
        </p>
      )}

      <div className="mt-8 flex pb-16">
        <label className="text-12 text-16 text-greyHeavy">
          <input
            type="checkbox"
            className="radio mr-4"
            value="rentalRenew"
            onChange={handleCheck}
            checked={form.rentalRenew}
          />
        </label>

        <p>
          I agree to the rental detail above (including auto-renewal) and the
          terms and conditions of the{" "}
          <span
            onClick={() => openTermConditionsModal()}
            className="link cursor-pointer font-bold text-callout"
          >
            storage agreement
          </span>
          . I further understand that my information will be used as described
          in the Privacy Policy.
        </p>
      </div>
    </section>
  );
}

export default RentalInformation;
