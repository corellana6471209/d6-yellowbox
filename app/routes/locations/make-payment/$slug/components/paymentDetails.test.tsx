import React from "react";
import { describe, it } from "vitest";
import { render, screen } from "../../../../../utils/test-utils";
import { BrowserRouter as Router } from "react-router-dom";
import PaymentDetails from "./paymentDetails";
import {
  unitInformation,
  labels,
  infReservation,
  initialStateErrors,
  errorsOnValidation,
} from "../../../../../utils/fakedataToTests";

describe("CustomerInformation component unit test", () => {
  beforeEach(() => {
    const setState = vi.fn();
    const useStateSpy = vi.spyOn(React, "useState");
    useStateSpy.mockImplementation((initialValue) => [initialValue, setState]);
  });
  afterEach(() => {
    vi.clearAllMocks();
  });

  it("verify the render the ProtectionBelong component", () => {
    render(
      <Router>
        <PaymentDetails
          labels={labels}
          errors={initialStateErrors}
          errorsOnValidation={errorsOnValidation}
          setErrors={() => console.log()}
          setErrorsOnValidation={() => console.log()}
          userInformation={infReservation}
        />
      </Router>
    );
  });

  it("verify the render the input component", async () => {
    const { container } = render(
      <Router>
        <PaymentDetails
          labels={labels}
          errors={initialStateErrors}
          errorsOnValidation={errorsOnValidation}
          setErrors={() => console.log()}
          setErrorsOnValidation={() => console.log()}
          userInformation={infReservation}
        />
      </Router>
    );

    expect(await container.getElementsByClassName("div-input-payment-detail").length).toBe(2);
  });

  it("verify the render the select component", async () => {
    const { container } = render(
      <Router>
        <PaymentDetails
          labels={labels}
          errors={initialStateErrors}
          errorsOnValidation={errorsOnValidation}
          setErrors={() => console.log()}
          setErrorsOnValidation={() => console.log()}
          userInformation={infReservation}
        />
      </Router>
    );

    expect(
      await container.getElementsByClassName("select-filter-sort-by-payment")
        .length
    ).toBe(3);
  });

  it("verify the render the checkbox component", async () => {
    const { container } = render(
      <Router>
        <PaymentDetails
          labels={labels}
          errors={initialStateErrors}
          errorsOnValidation={errorsOnValidation}
          setErrors={() => console.log()}
          setErrorsOnValidation={() => console.log()}
          userInformation={infReservation}
        />
      </Router>
    );

    expect(
      await container.getElementsByClassName("radio")
        .length
    ).toBe(1);
  });
});
