import React from "react";
import { describe, it } from "vitest";
import { render, screen } from "../../../../../utils/test-utils";
import { BrowserRouter as Router } from "react-router-dom";
import RentalDetails from "./rentalDetails";
import {
  locationContent,
  infReservation,
  labels,
  unitInformation,
} from "../../../../../utils/fakedataToTests";

vi.mock("./UnitCard", () => {
  return {
    default: "UnitCard-component-string",
  };
});

const mockSetState = vi.fn();
vi.mock("react", () => ({
  useState: (initial) => [initial, mockSetState],
  useRef: (initial) => [initial, mockSetState],
  useEffect: (initial) => [initial, mockSetState],
}));

describe("RentalDetails component unit test", () => {
  it("verify the render the RentalDetails component", () => {
    render(
      <Router>
        <RentalDetails
          locationContent={locationContent}
          infReservation={infReservation}
          labels={labels}
          unitInformation={unitInformation}
        />
        ,
      </Router>
    );
  });

  it("Verify if exists Unit # exists on body", async () => {
    render(
      <Router>
        <RentalDetails
          locationContent={locationContent}
          infReservation={infReservation}
          labels={labels}
          unitInformation={unitInformation}
        />
      </Router>
    );

    expect(
      await screen.findByText("Unit #")
    ).toBeInTheDocument();
  });

  it("Verify if exists Reservation Held Until exists on body", async () => {
    render(
      <Router>
        <RentalDetails
          locationContent={locationContent}
          infReservation={infReservation}
          labels={labels}
          unitInformation={unitInformation}
        />
      </Router>
    );

    expect(
      await screen.findByText("Reservation Held Until")
    ).toBeInTheDocument();
  });
});
