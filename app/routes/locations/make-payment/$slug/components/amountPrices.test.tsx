import React from "react";
import { describe, it } from "vitest";
import { render, screen } from "../../../../../utils/test-utils";
import { BrowserRouter as Router } from "react-router-dom";
import AmountPrices from "./amountPrices";
import {
  labels,
  infReservation,
  dataPrices,
} from "../../../../../utils/fakedataToTests";

describe("AmountPrices component unit test", () => {
  beforeEach(() => {
    const setState = vi.fn();
    const useStateSpy = vi.spyOn(React, "useState");
    useStateSpy.mockImplementation((initialValue) => [initialValue, setState]);
  });
  afterEach(() => {
    vi.clearAllMocks();
  });

  it("verify the render the ProtectionBelong component", () => {
    render(
      <Router>
        <AmountPrices
          labels={labels}
          infReservation={infReservation}
          dataPrices={dataPrices}
          total={105.6}
          sendPaymentInformation={() => console.log()}
          openModal={() => console.log()}
        />
      </Router>
    );
  });

  it("Verify if exists Your Total Savings exists on body", async () => {
    render(
      <Router>
        <AmountPrices
          labels={labels}
          infReservation={infReservation}
          dataPrices={dataPrices}
          total={105.6}
          sendPaymentInformation={() => console.log()}
          openModal={() => console.log()}
        />
      </Router>
    );

    expect(await screen.findByText("Your Total Savings")).toBeInTheDocument();
  });

  it("Verify if exists Total Due Today: exists on body", async () => {
    render(
      <Router>
        <AmountPrices
          labels={labels}
          infReservation={infReservation}
          dataPrices={dataPrices}
          total={105.6}
          sendPaymentInformation={() => console.log()}
          openModal={() => console.log()}
        />
      </Router>
    );

    expect(await screen.findByText("Total Due Today:")).toBeInTheDocument();
  });


  it("Verify if exists Total amount exists on body", async () => {
    render(
      <Router>
        <AmountPrices
          labels={labels}
          infReservation={infReservation}
          dataPrices={dataPrices}
          total={105.6}
          sendPaymentInformation={() => console.log()}
          openModal={() => console.log()}
        />
      </Router>
    );

    expect(await screen.findByText("$105.60")).toBeInTheDocument();
  });
});
