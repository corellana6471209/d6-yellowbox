import React from "react";
import { describe, it } from "vitest";
import { render, screen } from "../../../../../utils/test-utils";
import { BrowserRouter as Router } from "react-router-dom";
import FixedPrice from "./fixedPrice";
import { labels } from "../../../../../utils/fakedataToTests";

describe("FixedPrice component unit test", () => {
  beforeEach(() => {
    const setState = vi.fn();
    const useStateSpy = vi.spyOn(React, "useState");
    useStateSpy.mockImplementation((initialValue) => [initialValue, setState]);
  });
  afterEach(() => {
    vi.clearAllMocks();
  });

  it("verify the render the FixedPrice component", () => {
    render(
      <Router>
        <FixedPrice
          labels={labels}
          total={105.6}
          sendPaymentInformation={() => console.log()}
        />
      </Router>
    );
  });

  it("Verify if exists Total Due Today: exists on body", async () => {
    render(
      <Router>
        <FixedPrice
          labels={labels}
          total={105.6}
          sendPaymentInformation={() => console.log()}
        />
      </Router>
    );

    expect(await screen.findByText("Total Due Today:")).toBeInTheDocument();
  });

  it("Verify if exists Total amount exists on body", async () => {
    render(
      <Router>
        <FixedPrice
          labels={labels}
          total={105.6}
          sendPaymentInformation={() => console.log()}
        />
      </Router>
    );

    expect(await screen.findByText("$105.60")).toBeInTheDocument();
  });
});
