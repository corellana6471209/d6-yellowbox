function FixedPrice(props: any) {
  const { total, sendPaymentInformation, labels, isSubmiting } = props;
  return (
    <section className="div-fixed-price w-100 p-2">
      <div className="div-total-due-today mt-1 block items-center justify-center md:flex">
        <p className="mr-2 mr-6 text-20 text-white">
          Total Due Today:{" "}
          <span className="text-30 font-bold md:text-40">
            ${total}
          </span>
        </p>

        <div>
          <button
            onClick={() => sendPaymentInformation()}
            disabled={isSubmiting}
            className={`${
              isSubmiting
                ? "button-disabled block py-3 px-2.5"
                : "block bg-primary1 yellow-button py-3 text-center text-secondary1 no-underline"
            }`}
          >
            {labels.paymentButton}
          </button>
        </div>
      </div>
    </section>
  );
}

export default FixedPrice;
