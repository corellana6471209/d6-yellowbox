function Stepper(props: any) {
  const { labels } = props;
  return (
    <div className="wrapper option-1 option-1-1">
      <ol className="c-stepper">
        <li className="c-stepper__item-first">
          <div className="d-flex justify-center">
            <h3 className="c-stepper__title">
              {labels.confirmationStepperOneStep}
            </h3>
          </div>
        </li>
        <li className="c-stepper__item-middle">
          <div className="d-flex justify-center">
            <h3 className="c-stepper__title">
              {labels.confirmationStepperTwoStep}
            </h3>
          </div>
        </li>
        <li className="c-stepper__item-last">
          <div className="d-flex justify-center">
            <h3 className="c-stepper__title">
              {labels.confirmationStepperThreeStep}
            </h3>
          </div>
        </li>
      </ol>
    </div>
  );
}

export default Stepper;
//<div className="div-reservations-unit">WORK IN PROGRESS</div>
