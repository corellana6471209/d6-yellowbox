import React, { useState, useEffect } from "react";
import {
  getInsuranceCoverage,
  getPosItemsByLocationId,
} from "../../../../page-components/LocationMapList/backend";
import background from "../../../../../resources/dropdown.svg";

import { getProtectionBelong } from "../../backend";
import RichTextComponent from "../../../../common/RichTextComponent";
import Spinner from "../../../../common/Spinner";

function ProtectionBelong(props: any) {
  const {
    locationContent,
    getInformation,
    errors,
    errorsOnValidation,
    setErrors,
    setErrorsOnValidation,
    unitInformation,
    isGettingInsurance,
  } = props;
  const [isHydrated, setIsHydrated] = useState(false);
  const [insurances, setInsurances] = useState([]);
  const [locks, setLocks] = useState([]);
  const [protectionBelong, setProtectionBelong] = useState<any>(null);
  const [insuranceProtection, setInsuranceProtection] = useState<any>(null);
  const [form, setForm] = useState({
    insurance: "",
    lock: "",
  });

  useEffect(() => {
    async function fetchData() {
      const insuranceData = await getInsuranceCoverage(
        locationContent.locationId
      );
      setInsurances(insuranceData.insuranceCoverageDetails);

      const lockData = await getPosItemsByLocationId(
        locationContent.locationId
      );

      const protectionBelongText = await getProtectionBelong(
        "protection-belong"
      );
      const insuranceProtectionText = await getProtectionBelong(
        "insurance-protection-belong"
      );

      setLocks(lockData.posItems);
      setProtectionBelong(protectionBelongText);
      setInsuranceProtection(insuranceProtectionText);
      setIsHydrated(true);
    }
    fetchData();
  }, []);

  //Adding the insurances dinamically
  const optionSelectInsurance = (insurances: any) => {
    const options = [
      <option value="null" key="select your insurance">
        Select your insurance
      </option>,
    ];

    for (let i = 0; i < insurances.length; i++) {
      options.push(
        <option value={insurances[i].insuranceCoverageId} key={i}>
          {`${insurances[i].insuranceProvider} ($${insurances[i].coverage})`}
        </option>
      );
    }

    return options;
  };

  //Adding the locks dinamically
  const optionSelectLock = (locks: any) => {
    const options = [
      <option value="null" key="select your lock">
        Select your lock
      </option>,
    ];

    for (let i = 0; i < locks.length; i++) {
      var value = Number(
        locks[i].price + locks[i].price * (locks[i].tax1Rate / 100)
      ).toFixed(2);

      options.push(
        <option value={JSON.stringify(locks[i])} key={i}>
          {`${locks[i].chargeDescription} ($${value})`}
        </option>
      );
    }

    return options;
  };

  // Functions to manage the form

  const handleChange = (evt: any) => {
    const value = evt.target.value;
    //Function to send the information
    getInformation(evt.target.name, evt.target.value);
    setForm({
      ...form,
      [evt.target.name]: value,
    });
  };

  const verifyData = async (evt: any) => {
    //Get the errors
    const errorsSelected = { ...errors };
    const errorsToValidate = { ...errorsOnValidation };

    const value = evt.target.value;
    const name = evt.target.name;

    //Verify onBlur
    if (value === "null") {
      setErrors({ ...errorsSelected, [name]: true });
      setErrorsOnValidation({ ...errorsToValidate, [name]: true });
    } else {
      setErrors({ ...errorsSelected, [name]: false });
      setErrorsOnValidation({ ...errorsToValidate, [name]: false });
    }
  };

  if (isHydrated) {
    return (
      <div className="div-protection-belong">
        <p className="mt-8 mb-6 text-center font-serif text-28 text-greyHeavy lg:text-36">
          {protectionBelong.title}
        </p>

        {unitInformation && unitInformation.category != 0 && (
          <RichTextComponent data={protectionBelong.richText.content} />
        )}

        <div className="div-form">
          {unitInformation && unitInformation.category != 0 && (
            <>
              {" "}
              <div className="mb-8">
                <div className="mt-8 bg-white">
                  <select
                    name="insurance"
                    id="insurance"
                    className="select-filter-sort-by-payment w-100"
                    value={form.insurance}
                    onChange={handleChange}
                    onBlur={(e) => verifyData(e)}
                    style={{ backgroundImage: `url(${background})` }}
                  >
                    {optionSelectInsurance(insurances)}
                    {locationContent.address?.state?.canUseInsurancePolicy ? (
                      <option value="-999">Provide Own Insurance</option>
                    ) : (
                      ""
                    )}
                  </select>
                </div>

                {errors && errors.insurance && (
                  <p className="message-error text-left">
                    You must select one insurance option
                  </p>
                )}
              </div>
              <RichTextComponent data={insuranceProtection.richText.content} />
            </>
          )}

          <div>
            <div className="mt-8 bg-white">
              <select
                name="lock"
                disabled={isGettingInsurance}
                id="lock"
                className="select-filter-sort-by-payment w-100"
                value={form.lock}
                onChange={handleChange}
                onBlur={(e) => verifyData(e)}
                style={{ backgroundImage: `url(${background})` }}
              >
                {optionSelectLock(locks)}
                {locationContent.requireLock ? (
                  <option value="-999">Bring my own lock</option>
                ) : (
                  ""
                )}
              </select>
            </div>

            {errors && errors.lock && (
              <p className="message-error text-left">
                You must select one lock option
              </p>
            )}
          </div>
        </div>
      </div>
    );
  } else {
    return (
      <div className="d-flex justify-center align-middle">
        <Spinner />
      </div>
    );
  }
}

export default ProtectionBelong;
