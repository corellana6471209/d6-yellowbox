import { useState } from "react";
import { statesFilterSearch } from "../../../../../utils/fakedataToTests";
import background from "../../../../../resources/dropdown.svg";
import GlobalRichTextModal from "../../../../common/Modals/GlobalRichText";

import Icon from "../../../../common/svg/Icon";
import { isAlphanumeric } from "../../../../../utils/regex";

function CustomerInformation(props: any) {
  const {
    userInformation,
    getInformation,
    errors,
    errorsOnValidation,
    setErrors,
    setErrorsOnValidation,
    isLogged,
    labels,
  } = props;

  const [showPassword, setShowPassword] = useState<any>(false);
  const [form, setForm] = useState({
    password: "",
    address: userInformation.address1 ? userInformation.address1 : "",
    addressTwo: userInformation.address2 ? userInformation.address2 : "",
    state: "",
    zipCode: "",
    city: "",
    military: false,
  });
  const [openModalMilitaryModal, setOpenModalMilitaryModal] =
    useState<any>(false);

  const handleChange = async (evt: any) => {
    //Only permit letter and numbers javascript input regex
    const re = /^[A-Za-z0-9\s]+$/; //Verify only permitted number, letter and spaces
    const value = evt.target.value;
    const name = evt.target.name;
    const errorsToValidate = { ...errorsOnValidation };

    if (!re.test(value) && value !== "") {
      return;
    }

    if (name === "password") {
      const newPasswordValidation =
        isAlphanumeric(value) && value.length <= 8 && value.length >= 6;
      if (!newPasswordValidation) {
        setErrors({ ...errors, [name]: true });
        setErrorsOnValidation({ ...errorsToValidate, [name]: true });
      } else {
        setErrors({ ...errors, [name]: false });
        setErrorsOnValidation({ ...errorsToValidate, [name]: false });
      }
    }

    //Function to send the information
    getInformation(name, value);
    setForm({
      ...form,
      [name]: value,
    });
  };

  const handleCheck = (evt: any) => {
    //Function to send the information


    if(evt.target.checked) setOpenModalMilitaryModal(true);
    
    getInformation(evt.target.value, evt.target.checked);
    setForm({
      ...form,
      [evt.target.value]: evt.target.checked,
    });
  };

  //Adding the states dynamically
  const statesSelect = () => {
    const options = [
      <option value="" key="select your state">
        Select your state
      </option>,
    ];

    for (let i = 0; i < statesFilterSearch.length; i++) {
      options.push(
        <option
          value={statesFilterSearch[i].stateFullName}
          key={statesFilterSearch[i].stateFullName}
        >
          {statesFilterSearch[i].stateFullName}
        </option>
      );
    }

    return options;
  };

  const verifyData = async (evt: any) => {
    //Get the errors
    const errorsSelected = { ...errors };
    const errorsToValidate = { ...errorsOnValidation };

    const reAddress = /^[a-zA-Z0-9\s]{6,}$/; //At least 6 characters
    const value = evt.target.value;
    const name = evt.target.name;

    //Verify onBlur
    if (name === "password") {
      const newPasswordValidation =
        isAlphanumeric(value) && value.length <= 8 && value.length >= 6;
      if (!newPasswordValidation) {
        setErrors({ ...errors, [name]: true });
        setErrorsOnValidation({ ...errorsToValidate, [name]: true });
      } else {
        setErrors({ ...errors, [name]: false });
        setErrorsOnValidation({ ...errorsToValidate, [name]: false });
      }
    } else if (
      name !== "password" &&
      name !== "address" &&
      name !== "addressTwo" &&
      value.replace(/ /g, "") === "" //Verify that the user doesnt enters blank spaces
    ) {
      setErrors({ ...errorsSelected, [name]: true });
      setErrorsOnValidation({ ...errorsToValidate, [name]: true });
    } else if (
      (name === "address" || name === "addressTwo") &&
      (value.replace(/ /g, "") === "" || !reAddress.test(value))
    ) {
      setErrors({ ...errorsSelected, [name]: true });
      setErrorsOnValidation({ ...errorsToValidate, [name]: true });
    } else {
      setErrors({ ...errorsSelected, [name]: false });
      setErrorsOnValidation({ ...errorsToValidate, [name]: false });
    }
  };

  return (
    <section className="div-customer-information">
      <GlobalRichTextModal
        open={openModalMilitaryModal}
        labelInformation={'militaryInformation'}
        callback={() => {
          setOpenModalMilitaryModal(false);
        }}
      />

      <p className="mt-8 mb-4 text-center font-serif text-28 text-greyHeavy md:text-36">
        {labels.customerInformationTitle}
      </p>
      <div className="d-flex div-customer-information-required items-center justify-end text-12 text-greyMedium">
        <span className="relative text-20 text-callout">*</span>Required
      </div>
      <div className="div-input disabled">
        <input
          disabled
          name="firstName"
          className="input w-100"
          type="text"
          placeholder={userInformation.firstName}
        />
      </div>

      <div className="div-input disabled">
        <input
          disabled
          name="lastName"
          className="input w-100"
          type="text"
          placeholder={userInformation.lastName}
        />
      </div>

      <div className="div-input disabled">
        <input
          disabled
          name="email"
          className="input w-100"
          type="email"
          placeholder={userInformation.emailAddress}
        />
      </div>

      <div className="div-input disabled">
        <input
          disabled
          name="primaryPhoneNumber"
          className="input w-100"
          type="text"
          placeholder={userInformation.primaryPhoneNumber}
        />
      </div>

      {!isLogged && (
        <>
          <div className="relative">
            <span className="span-div-input absolute text-20 text-callout">
              *
            </span>
            <div className="div-input">
              <input
                autoComplete="new-password"
                onChange={handleChange}
                onBlur={(e) => verifyData(e)}
                name="password"
                className="input w-100"
                type={showPassword ? "text" : "password"}
                value={form.password}
                placeholder="Password"
              />
            </div>

            {showPassword ? (
              <span
                onClick={() => setShowPassword(!showPassword)}
                className="bs-eye absolute cursor-pointer"
              >
                <Icon iconName="blindEye" />
              </span>
            ) : (
              <span
                onClick={() => setShowPassword(!showPassword)}
                className="bs-eye absolute cursor-pointer"
              >
                <Icon iconName="eye" />
              </span>
            )}

            {errors && errors.password && (
              <p className="message-error text-left">
                This password is invalid
              </p>
            )}
          </div>
          <p className="mb-8 mt-4 mb-10 text-16 text-greyHeavy">
            {labels.passwordText}
          </p>
        </>
      )}

      <div className={`${isLogged ? "mt-10" : ""} relative`}>
        <span className="span-div-input absolute text-20 text-callout">*</span>
        <div className="div-input">
          <input
            autoComplete="off"
            autoComplete="chrome-off"
            onChange={handleChange}
            onBlur={(e) => verifyData(e)}
            value={form.address}
            name="address"
            className="input w-100"
            type="text"
            placeholder="Address 1"
          />
        </div>
        {errors && errors.address && (
          <p className="message-error text-left">
            This address is invalid must be at least 6 characters
          </p>
        )}
      </div>

      <div className="relative">
        <div className="div-input">
          <input
            autoComplete="off"
            autoComplete="chrome-off"
            onChange={handleChange}
            onBlur={(e) => verifyData(e)}
            value={form.addressTwo}
            name="addressTwo"
            className="input w-100"
            type="text"
            placeholder="Address 2"
          />
        </div>
      </div>

      <div className="relative">
        <span className="span-div-input absolute text-20 text-callout">*</span>
        <div className="mt-4 bg-white">
          <select
            onChange={handleChange}
            onBlur={(e) => verifyData(e)}
            style={{ backgroundImage: `url(${background})` }}
            value={form.state}
            name="state"
            id="state"
            className="select-filter-sort-by-payment w-100"
          >
            {statesSelect()}
          </select>
        </div>
        {errors && errors.state && (
          <p className="message-error text-left">The state is required</p>
        )}
      </div>

      <div className="relative">
        <span className="span-div-input absolute text-20 text-callout">*</span>
        <div className="div-input">
          <input
            autoComplete="off"
            autoComplete="chrome-off"
            onChange={handleChange}
            onBlur={(e) => verifyData(e)}
            value={form.city}
            name="city"
            className="input w-100"
            type="text"
            placeholder="City"
          />
        </div>
        {errors && errors.city && (
          <p className="message-error text-left">The city is required</p>
        )}
      </div>

      <div className="relative">
        <span className="span-div-input absolute text-20 text-callout">*</span>
        <div className="div-input">
          <input
            autoComplete="off"
            autoComplete="chrome-off"
            onChange={handleChange}
            onBlur={(e) => verifyData(e)}
            value={form.zipCode}
            name="zipCode"
            className="input w-100"
            type="text"
            placeholder="Zip Code"
          />
        </div>
        {errors && errors.zipCode && (
          <p className="message-error text-left">The zipCode is required</p>
        )}
      </div>

      <label className="mt-8 flex items-center text-12 text-16 text-greyHeavy">
        <input
          onChange={handleCheck}
          type="checkbox"
          className="radio mr-4"
          value="military"
          checked={form.military}
        />
        {labels.activeMilitary}
      </label>
    </section>
  );
}

export default CustomerInformation;
