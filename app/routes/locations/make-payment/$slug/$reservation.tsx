import React, { useState, useEffect } from "react";
import type { LoaderFunction, ActionFunction } from "@remix-run/node";
import {
  useLoaderData,
  useSubmit,
  //useTransition,
  useActionData,
} from "@remix-run/react";
import { getCacheValueByKey } from "../../../api/shared/elasticCache";
import moment from "moment-timezone";

//Endpoints

//Components
import Stepper from "./components/stepper";
import ProtectionBelong from "./components/protectionBelong";
import CustomerInformation from "./components/customerInformation";
import PaymentDetails from "./components/paymentDetails";
import RentalInformation from "./components/rentalInformation";
import AmountPrices from "./components/amountPrices";
import FixedPrice from "./components/fixedPrice";
import RentalDetails from "./components/rentalDetails";

import { getLabel } from "../../../../utils/getLabel";
import { setCookie, deleteCookie } from "../../../../utils/cookie";
import contentfulKeys from "../../../../utils/config";
import { analytics } from "../../../../utils/analytics";
import { globalRedirect } from "../../../../utils/globalRedirect";
import { identify } from "../../../../utils/identify";

import { dateInformation } from "../../../../utils/getDateInformation";
import { getPaymentType } from "../../../../utils/getPaymentType";
import { useWindowScrollPositions } from "../../../../utils/hooks/useScroll";
import { getUnitImageInformation } from "../../backend";

import MessageModal from "../../../common/MessageModal";

import Header from "../../../common/Header";
import Footer from "../../../common/Footer";
import Spinner from "../../../common/Spinner";
import GlobalRichTextModal from "../../../common/Modals/GlobalRichText";

import {
  chargeCard,
  createAccount,
  getPrices,
  isExistingUser,
  isLegacy,
  moveInReservation,
  paymentTypeRetrieve,
  posItemAddToLedger,
  siteLinkeSignCreateLeaseURL,
  tenantBillingInfoUpdate,
  tenantUpdate,
  tenantUpdateMilitary,
} from "../backend";
import {
  loadLocationContentByLocationIdCompleted,
  getReservationList,
} from "../../backend";

/**
 * List Item for locations listing
 * @param {object} props content to be displayed
 * @returns {string} returns component
 */

export const loader: LoaderFunction = async ({ params, request }) => {
  const { slug, reservation } = params;
  const locationContent = await loadLocationContentByLocationIdCompleted(slug);
  const token = await getCacheValueByKey("accessToken", request);
  const user = await getCacheValueByKey("tenantInfo", request);

  return { locationContent, reservation, token, user };
};

export const action: ActionFunction = async ({ request }) => {
  const body = await request.formData();
  let dataToSend = JSON.parse(body.get("dataToSend") as string);
  let inf = JSON.parse(body.get("inf") as string);
  let infReservation = JSON.parse(body.get("infReservation") as string);
  let total = body.get("total") as string;
  let totalWithOutLock = body.get("totalWithOutLock") as string;
  let numberOfDays = body.get("numberOfDays") as string;
  let pageToRedirect = body.get("pageToRedirect") as string;
  let isLogged = body.get("isLogged") as string;

  const isTestMode =
    process.env.ENVIRONMENT && process.env.ENVIRONMENT == "production"
      ? false
      : true;
  const startDate = moment()
    .tz("America/New_York")
    .format("YYYY-MM-DDT00:00:00.000");
  const endOfMonth = moment()
    .tz("America/New_York")
    .endOf("month")
    .format("YYYY-MM-DDT00:00:00.000");

  //MoveInReservation
  const moveInReservationParams: any = {
    locationID: inf.locationContent.locationId, //locationID
    waitingID: infReservation.waitingId, //waitingID
    tenantID: infReservation.tenantId, //tenantID
    unitID: infReservation.unitId1, //unitID
    startDate: startDate, //startDate
    endDate: endOfMonth, //endDate
    paymentMethod: 0, //paymentMethod---Online
    paymentAmount: totalWithOutLock, //paymentAmount
    creditCardType: dataToSend.card ? dataToSend.card : 5, //creditCardType
    creditCardNumber: dataToSend.cardNumber, //creditCardNumber
    creditCardCVV: dataToSend.cw, //creditCardCVV
    creditCardExpirationDate: `${
      dataToSend.expirationYear
        ? dataToSend.expirationYear
        : new Date().getFullYear()
    }-${
      dataToSend.expirationMonth ? dataToSend.expirationMonth : "01"
    }-${numberOfDays}`, //creditCardExpirationDate
    creditCardBillingName: infReservation.firstName, //creditCardBillingName
    creditCardBillingAddress: dataToSend.state, //creditCardBillingAddress
    creditCardBillingZipCode: dataToSend.zipCode, //creditCardBillingZipCode
    insuranceCoverageID: dataToSend.insurance ? dataToSend.insurance : "-999", //insuranceCoverageID
    concessionPlanID: infReservation.concessionId, //concessionPlanID
    moveInSourceType: 5, //Source
    testMode: isTestMode, //testMode
    billingFrequency: 3, //billingFrequency
    sourceString: "s_source",
  };

  const moveInReservationResponse = await moveInReservation(
    moveInReservationParams
  );
  //console.log("moveInReservationParams", moveInReservationParams);
  //console.log("moveInReservationResponse", moveInReservationResponse);

  if (
    !moveInReservationResponse ||
    !moveInReservationResponse?.data?.ledgerId
  ) {
    if (!moveInReservationResponse) {
      return {
        showModal: true,
        message: null,
        redirect: false,
      };
    } else if (moveInReservationResponse?.data?.ledgerId === 0) {
      if (moveInReservationResponse?.data?.rt) {
        return {
          showModal: true,
          message: moveInReservationResponse?.data?.rt.returnMessage,
          redirect: false,
        };
      }

      return {
        showModal: true,
        message: null,
        redirect: false,
      };
    } else {
      return {
        showModal: true,
        message: null,
        redirect: false,
      };
    }
  } else {
    let tenantUpdateParams: any;
    if (isLogged === "false") {
      tenantUpdateParams = {
        LocationId: inf.locationContent.locationId, //locationID
        TenantId: infReservation.tenantId, //tenantID
        WebPassword: dataToSend.password, //Password
        FirstName: infReservation.firstName, //FirstName
        LastName: infReservation.lastName, //Lastname
        Address1: dataToSend.address, //Address
        Address2: dataToSend.addressTwo || "", //Address two
        City: dataToSend.city, //City
        PostalCode: dataToSend.zipCode, //Postalcode
        Phone: infReservation.primaryPhoneNumber, //Phone
        Email: infReservation.emailAddress, //Email
        Region: null, //Region
      };
    } else {
      tenantUpdateParams = {
        LocationId: inf.locationContent.locationId, //locationID
        TenantId: infReservation.tenantId, //tenantID
        FirstName: infReservation.firstName, //FirstName
        LastName: infReservation.lastName, //Lastname
        Address1: dataToSend.address, //Address
        Address2: dataToSend.addressTwo || "", //Address two
        City: dataToSend.city, //City
        PostalCode: dataToSend.zipCode, //Postalcode
        Phone: infReservation.primaryPhoneNumber, //Phone
        Email: infReservation.emailAddress, //Email
        Region: null, //Region
      };
    }

    //await tenantUpdate.loader(tenantUpdateParams);

    //Add lock to tenant

    let posItemAddToLedgerDataParams: any = null;
    if (
      dataToSend.lock &&
      dataToSend.lock != "null" &&
      dataToSend.lock != "999"
    ) {
      posItemAddToLedgerDataParams = {
        LocationId: inf.locationContent.locationId, //locationID
        LedgerId: moveInReservationResponse.data.ledgerId,
        ChargeDescriptionId: dataToSend.lock.chargeDescriptionId,
        PricePreTax: dataToSend.lock.price,
        Quantity: 1,
      };

      //await posItemAddToLedger.loader(posItemAddToLedgerDataParams);
    }

    //Create account if apply
    let createAccountParams: any = null;
    if (isLogged === "false") {
      createAccountParams = {
        emailAddress: infReservation.emailAddress,
        password: dataToSend.password,
        primaryPhoneNumber: infReservation.primaryPhoneNumber,
        firstName: infReservation.firstName,
        lastName: infReservation.lastName,
      };

      //await createAccount.loader(createAccountParams);
    }

    //---------------------Military endpoint if apply
    let militaryDataParams: any = null;
    if (dataToSend.military) {
      militaryDataParams = {
        locationId: inf.locationContent.locationId,
        tenantId: infReservation.tenantId,
        isMilitary: true,
      };

      //await tenantUpdateMilitary.loader(militaryDataParams);
    }

    //====================>Charge the card
    //PaymentSimple
    const paymentParams: any = {
      locationId: inf.locationContent.locationId, //locationID
      tenantId: infReservation.tenantId, //tenantID
      unitId: infReservation.unitId1, //unitID
      paymentAmount: (Number(total) - Number(totalWithOutLock)).toFixed(2),
      creditCardType: dataToSend.card ? dataToSend.card : 5, //creditCardType
      creditCardNumber: dataToSend.cardNumber,
      creditCardCVV: dataToSend.cw, //creditCardCVV
      expirationDate: `${
        dataToSend.expirationYear
          ? dataToSend.expirationYear
          : new Date().getFullYear()
      }-${
        dataToSend.expirationMonth ? dataToSend.expirationMonth : "01"
      }-${numberOfDays}`, //creditCardExpirationDate
      billingName: infReservation.firstName,
      billingAddress: dataToSend.address, //Address
      billingZipCode: dataToSend.zipCode, //Postalcode
      isTestMode: isTestMode, //TestMode,
      source: 10, //Source
      paymentMethod: 0, //Payment method ---> online,
      ledgerId: moveInReservationResponse.data.ledgerId,
    };

    //await chargeCard.loader(paymentParams);

    const siteLinkeSignCreateLeaseURLParams: any = {
      locationId: inf.locationContent.locationId,
      tenantId: infReservation.tenantId,
      ledgerId: moveInReservationResponse.data.ledgerId,
      returnUrl: pageToRedirect,
    };

    let siteLinkeSignCreateLeaseURLData: any = null;
    try {
      const [
        tenantInfo,
        posItemAddInfo,
        createAccountInfo,
        militaryInfo,
        chargeCardInfo,
        siteLinkeSignCreateLeaseURLInfo,
      ] = await Promise.all([
        tenantUpdate(tenantUpdateParams),
        posItemAddToLedgerDataParams
          ? posItemAddToLedger(posItemAddToLedgerDataParams)
          : "",
        createAccountParams ? createAccount(createAccountParams) : "",
        militaryDataParams ? tenantUpdateMilitary(militaryDataParams) : "",
        chargeCard(paymentParams),
        siteLinkeSignCreateLeaseURL(siteLinkeSignCreateLeaseURLParams),
      ]);
      siteLinkeSignCreateLeaseURLData = siteLinkeSignCreateLeaseURLInfo;
    } catch (error) {}

    if (dataToSend.autoPay) {
      //TenantBillingInfoUpdate Update data to AutoPay Detect if the user has AutoPay only for check
      const paramsPaymentTypeParams: any = {
        locationId: inf.locationContent.locationId,
      };

      const paymentTypesRetrieveData = await paymentTypeRetrieve(
        paramsPaymentTypeParams
      );

      //Autopay endpoint if apply
      if (
        paymentTypesRetrieveData &&
        paymentTypesRetrieveData?.data?.paymentTypesList
      ) {
        const type = await getPaymentType(
          dataToSend.card ? dataToSend.card : "5",
          paymentTypesRetrieveData?.data?.paymentTypesList
        );

        const tenantBillingParams: any = {
          locationId: inf.locationContent.locationId,
          ledgerId: moveInReservationResponse.data.ledgerId,
          creditCardTypeID: type[0].paymentTypeId, //CreditCardIdType
          autoBillType: 1,
          creditCardNumber: dataToSend.cardNumber,
          creditCardExpiration: `${dataToSend.expirationYear}-${dataToSend.expirationMonth}-${numberOfDays}`,
          creditCardHolderName: infReservation.firstName,
          creditCardStreet: dataToSend.address,
          creditCardZip: dataToSend.zipCode,
        };

        if (type && type.length > 0) {
          await tenantBillingInfoUpdate(tenantBillingParams);
        }
      }
    }

    if (
      siteLinkeSignCreateLeaseURLData &&
      siteLinkeSignCreateLeaseURLData?.data?.returnMessage
    ) {
      return {
        redirect: true,
        redirectTo: siteLinkeSignCreateLeaseURLData.data.returnMessage,
      };
    } else {
      return {
        showModal: true,
        redirect: false,
        message: null,
      };
    }
  }
};

const initialStateErrors = {
  password: false,
  address: false,
  addressTwo: false,
  state: false,
  zipCode: false,
  city: false,
  card: false,
  cardNumber: false,
  cw: false,
  expirationYear: false,
  expirationMonth: false,
  insurance: false,
  lock: false,
};

const errorsOnValidation = {
  password: true,
  address: false,
  addressTwo: false,
  state: true,
  zipCode: true,
  city: true,
  cardNumber: true,
  cw: true,
  expirationYear: true,
  expirationMonth: true,
  insurance: true,
  lock: true,
};

function MakePayment() {
  const inf = useLoaderData();
  const actionData = useActionData();
  const submit = useSubmit();
  //const transition = useTransition();
  //const isSubmitingAction = transition.state === "submitting";

  const [infReservation, setInfReservation] = useState<any>(null);
  const [dataToSend, setDataToSend] = useState<any>({
    autoPay: true,
    address: "",
    addressTwo: "",
  });
  const [dataPrices, setDataPrices] = useState<any>([]);
  const [total, setTotal] = useState<any>(null);
  const [totalWithOutLock, setTotalWithOutLock] = useState<any>(null);

  const [openModal, setOpenModal] = useState<any>(false);
  const [openUnitParkingModal, setOpenUnitParkingModal] = useState<any>(false);

  const [messageError, setMessageError] = useState<any>(null);
  const [unitInformation, setUnitInformation] = useState<any>(null);
  const [errors, setErrors] = useState(initialStateErrors);
  const [errorsValidation, setErrorsValidation] = useState(errorsOnValidation);
  const [userInformation, setUserInformation] = useState(null);
  const [isLogged, setIsLogged] = useState(false);
  const [isSubmiting, setIsSubmiting] = useState(false);
  const [isGettingInsurance, setGettingInsurance] = useState(false);

  const [isHydrated, setIsHydrated] = useState(false);
  const [showpriceFixed, setShowpriceFixed] = useState(true);
  const [labels, setLabels] = useState<any>({
    confirmationStepperOneStep: "Find a Unit",
    confirmationStepperTwoStep: "Reserve & hold your unit",
    confirmationStepperThreeStep: "Complete rental and pay now",
    confirmationCongratulationsMessage:
      "Congratulations, you've reserved a unit!",
    confirmationButtonLabel: "Complete Rental and Pay Now",
    confirmationDetailsTitle: "Your Reservation Details",
    disclaimerLabel: "Other fees may apply upon payment process",
    confirmationConditionsTitle: "Terms and Conditions",
    confirmationConditionsText:
      "The size reserved may not be available at time of move-in.",
    confirmationConditionsLinkText: "Read Full Terms and Conditions",

    rentalDetailsTitle: "Your Rental Details",
    customerInformationTitle: "Customer Information",
    paymentDetails: "Payment Details",
    activeMilitary: "Are you active Military?",

    autoPayCheckBox: "Enroll in hassle-free AutoPay",
    hassleFreeText:
      "Your rental will auto-renew on a monthly basis until you cancel by office visit, phone: 877.786.7343, or online by vacating when visiting your customer portal. Cancel 10 days before your renewal to avoid your next month’s charge.",
    paymentButton: "Pay now",

    tooltipText: "The prices will have additional taxes.",
    tooltipTextAdministrative: "The prices will have additional taxes.",
    tooltipTextFirstMonth: "The prices will have additional taxes.",
    tooltipTextLockFee: "The prices will have additional taxes.",
    termsConditionsText:
      "TI agree to the rental detail above (including auto-renewal) and the terms and conditions of the storage agreement. I further understand that my information will be used as described in the Privacy Policy",
    passwordText:
      "Create a password for your new account. Password must be 6 to 8 characters and/or numbers, and no special characters.",
    insuranceLockText:
      "You need enter a valid information about the insurance and lock.",
    lockText: "You need enter a valid information about the lock.",
    validPassword: "You enter a valid password.",
    personalInformation: "You need enter valid personal information.",
    billingInformation: "You need enter valid billing information.",
    privacyPolicy: "You need accept the Privacy Policy.",
  });

  const { scrollY } = useWindowScrollPositions();

  /*************************************ACTION USEEFFECT******************************/
  useEffect(() => {
    if (actionData && !actionData.redirect) {
      if (actionData.showModal) {
        setOpenModal(actionData.showModal);
        setMessageError(actionData.message);
      }
      setIsSubmiting(false);
    } else if (actionData && actionData.redirect) {
      try {
        analytics(window, "rentalCompleted", {
          locationID: inf.locationContent.locationId,
          unitID: infReservation.unitId1,
          unitTypeID: infReservation.unitTypeId1,
          unitRate: infReservation.pushRate,
          moveInDate: infReservation.createdDateTime,
          insuranceType: dataToSend.insurance ? dataToSend.insurance : "-999",
          autopay: dataToSend.autopay ? 1 : 0,
        });

        identify(
          window,
          `${infReservation.tenantId}${inf.locationContent.locationId}`,
          {
            email: infReservation.emailAddress,
            Location: inf.locationContent.locationId,
            firstName: infReservation.firstName,
            lastName: infReservation.lastName,
            address: {
              city: inf.locationContent.address.city,
              state: inf.locationContent.address.state,
              postalCode: inf.locationContent.address.postalCode,
            },
            phone: infReservation.primaryPhoneNumber,
            Military: dataToSend.military,
            UnitID: unitInformation?.unitId1 || null,
            Autopay: dataToSend.autoPay,
          }
        );

        if (!isLogged) {
          identify(
            window,
            `${infReservation.tenantId}${inf.locationContent.locationId}`,
            {
              email: infReservation.emailAddress,
              Location: inf.locationContent.locationId,
              Gate_Code: infReservation.accessCode,
              Unit_Number: infReservation.unitId1,
            }
          );
        }

        globalRedirect(window, actionData.redirectTo);
      } catch (error) {
        setIsSubmiting(false);
      }
    }
  }, [actionData]);

  /*************************************ACTION USEEFFECT******************************/

  useEffect(() => {
    if (isHydrated) {
      if (
        window.innerHeight + window.scrollY >=
        document.body.offsetHeight - document.body.offsetHeight * 0.3
      ) {
        setShowpriceFixed(false);
      } else {
        setShowpriceFixed(true);
      }
    }
  }, [isHydrated, scrollY]);

  useEffect(() => {
    async function fetchData() {
      // Reservation information

      const infReservation = await getReservationList([
        inf.locationContent.locationId,
        inf.reservation,
        "v3",
      ]);

      await getPricesInformation(
        -999,
        infReservation.concessionId || -999,
        inf,
        infReservation
      );
      setInfReservation(infReservation);

      if (inf && inf.token) {
        const userInformation = inf.user;
        setIsLogged(true);
        setErrorsValidation({ ...errorsOnValidation, password: false });

        //This code is to merge the reservation's information with the tenant information or prevent if one API fails

        if (userInformation) {
          setUserInformation({
            ...infReservation,
            ...userInformation,
            primaryPhoneNumber:
              userInformation.phoneNumber || infReservation.primaryPhoneNumber,
          });
          setDataToSend({
            ...dataToSend,
            address: userInformation.address1 ? userInformation.address1 : "",
            addressTwo: userInformation.address2
              ? userInformation.address2
              : "",
          });
        } else {
          setUserInformation(infReservation);
        }
      } else {
        const isUser = await isExistingUser(
          inf.locationContent.locationId,
          inf.reservation
        );

        if (isUser && isUser.isUser) {
          //Verify if is legacy

          const legacy = await isLegacy(infReservation.emailAddress);

          if (legacy === "error" || !legacy?.isLegacyUser) {
            window.location.href = `/login?slug=${inf.locationContent.locationId}&reservation=${inf.reservation}&islegacy=false`;
          } else {
            window.location.href = `/login?slug=${inf.locationContent.locationId}&reservation=${inf.reservation}&islegacy=true`;
          }

          return;
        } else {
          setUserInformation(infReservation);
        }
      }
      //--------------------> Code to do the redirect

      //Labels
      const confirmationStepperOneStep = await getLabel(
        "confirmationStepperOneStep"
      );
      const confirmationStepperTwoStep = await getLabel(
        "confirmationStepperTwoStep"
      );
      const confirmationStepperThreeStep = await getLabel(
        "confirmationStepperThreeStep"
      );
      const confirmationCongratulationsMessage = await getLabel(
        "confirmationCongratulationsMessage"
      );
      const confirmationButtonLabel = await getLabel("confirmationButtonLabel");
      const confirmationDetailsTitle = await getLabel(
        "confirmationDetailsTitle"
      );
      const disclaimerLabel = await getLabel("disclaimerLabel");

      const confirmationConditionsTitle = await getLabel(
        "confirmationConditionsTitle"
      );
      const confirmationConditionsText = await getLabel(
        "confirmationConditionsText"
      );
      const rentalDetailsTitle = await getLabel("rentalDetailsTitle");

      const customerInformationTitle = await getLabel(
        "customerInformationTitle"
      );
      const paymentDetails = await getLabel("paymentDetails");
      const autoPayCheckBox = await getLabel("autoPayCheckBox");
      const hassleFreeText = await getLabel("hassleFreeText");
      const paymentButton = await getLabel("paymentButton");
      const tooltipText = await getLabel("tooltipText");
      const tooltipTextAdministrative = await getLabel(
        "tooltipTextAdministrative"
      );
      const tooltipTextFirstMonth = await getLabel("tooltipTextFirstMonth");
      const tooltipTextLockFee = await getLabel("tooltipTextLockFee");

      const termsConditionsText = await getLabel("termsConditionsText");
      const passwordText = await getLabel("passwordText");
      const activeMilitary = await getLabel("activeMilitary");
      const insuranceLockText = await getLabel("insuranceLockText");
      const lockText = await getLabel("lockText");
      const validPassword = await getLabel("validPassword");
      const personalInformation = await getLabel("personalInformation");
      const billingInformation = await getLabel("billingInformation");
      const privacyPolicy = await getLabel("privacyPolicy");

      //Labels

      //Get the unit Information with the image
      const unitData = await getUnitImageInformation(
        infReservation,
        inf.locationContent.locationId
      );

      //console.log("infReservation", infReservation);

      setUnitInformation(unitData);

      setLabels({
        confirmationStepperOneStep: confirmationStepperOneStep
          ? confirmationStepperOneStep
          : labels.confirmationStepperOneStep,
        confirmationStepperTwoStep: confirmationStepperTwoStep
          ? confirmationStepperTwoStep
          : labels.confirmationStepperTwoStep,
        confirmationStepperThreeStep: confirmationStepperThreeStep
          ? confirmationStepperThreeStep
          : labels.confirmationStepperThreeStep,
        confirmationCongratulationsMessage: confirmationCongratulationsMessage
          ? confirmationCongratulationsMessage
          : labels.confirmationCongratulationsMessage,
        confirmationButtonLabel: confirmationButtonLabel
          ? confirmationButtonLabel
          : labels.confirmationButtonLabel,
        confirmationDetailsTitle: confirmationDetailsTitle
          ? confirmationDetailsTitle
          : labels.confirmationDetailsTitle,
        disclaimerLabel: disclaimerLabel
          ? disclaimerLabel
          : labels.disclaimerLabel,
        confirmationConditionsTitle: confirmationConditionsTitle
          ? confirmationConditionsTitle
          : labels.confirmationConditionsTitle,
        confirmationConditionsText: confirmationConditionsText
          ? confirmationConditionsText
          : labels.confirmationConditionsText,
        confirmationConditionsLinkText: confirmationConditionsText
          ? confirmationConditionsText
          : labels.confirmationConditionsLinkText,

        rentalDetailsTitle: rentalDetailsTitle
          ? rentalDetailsTitle
          : labels.rentalDetailsTitle,

        customerInformationTitle: customerInformationTitle
          ? customerInformationTitle
          : labels.customerInformationTitle,

        paymentDetails: paymentDetails ? paymentDetails : labels.paymentDetails,

        autoPayCheckBox: autoPayCheckBox
          ? autoPayCheckBox
          : labels.autoPayCheckBox,

        hassleFreeText: hassleFreeText ? hassleFreeText : labels.hassleFreeText,

        paymentButton: paymentButton ? paymentButton : labels.paymentButton,

        tooltipText: tooltipText ? tooltipText : labels.tooltipText,

        tooltipTextAdministrative: tooltipTextAdministrative
          ? tooltipTextAdministrative
          : labels.tooltipTextAdministrative,

        tooltipTextFirstMonth: tooltipTextFirstMonth
          ? tooltipTextFirstMonth
          : labels.tooltipTextFirstMonth,

        tooltipTextLockFee: tooltipTextLockFee
          ? tooltipTextLockFee
          : labels.tooltipTextLockFee,

        termsConditionsText: termsConditionsText
          ? termsConditionsText
          : labels.termsConditionsText,

        passwordText: passwordText ? passwordText : labels.passwordText,
        activeMilitary: activeMilitary ? activeMilitary : labels.activeMilitary,

        insuranceLockText: insuranceLockText
          ? insuranceLockText
          : labels.insuranceLockText,

        lockText: lockText ? lockText : labels.lockText,
        validPassword: validPassword ? validPassword : labels.validPassword,
        personalInformation: personalInformation
          ? personalInformation
          : labels.personalInformation,
        billingInformation: billingInformation
          ? billingInformation
          : labels.billingInformation,
        privacyPolicy: privacyPolicy ? privacyPolicy : labels.privacyPolicy,
      });
      setIsHydrated(true);
      if (unitData.category === 0) {
        setOpenUnitParkingModal(true);
      }

      analytics(window, "rentalStarted", {
        locationID: inf.locationContent.locationId,
        unitID: infReservation.unitId1,
        unitTypeID: infReservation.unitTypeId1,
        unitRate: infReservation.pushRate,
        moveInDate: infReservation.createdDateTime,
      });
    }

    async function fetchCookie() {
      deleteCookie(contentfulKeys.constants.lastPage);
      const days = await getLabel("cookieTimeDays");
      setCookie(
        contentfulKeys.constants.lastPage,
        window.location.href,
        days ? parseInt(days) : 30
      );
    }

    fetchCookie();
    fetchData();
  }, []);

  //Functions to manage the reservation

  //------------GetInformation from components (Lock, insurances and plans)

  const getInformation = async (name: string, value: string) => {
    setDataToSend({
      ...dataToSend,
      [name]: name === "lock" ? JSON.parse(value) : value,
    });

    if (name === "insurance") {
      getPricesInformation(
        value,
        infReservation.concessionId || -999,
        inf,
        infReservation
      );
    }

    if (name === "lock") {
      let pricesUpdated = [...dataPrices].filter(
        (price) => price.chargeDescription != "Lock Fee (Taxes Included)" // Filtering the lock data
      );
      getTotal(pricesUpdated, true);

      if (value != "-999" && value != "0" && value && value != "null") {
        const lockInformation = JSON.parse(value);
        //Deleting 0 value from the prices's array
        pricesUpdated.push({
          chargeDescription: "Lock Fee (Taxes Included)",
          total: Number(
            lockInformation.price +
              lockInformation.price * (lockInformation.tax1Rate / 100)
          ),
        });
      }

      setDataPrices(pricesUpdated);
      getTotal(pricesUpdated);
    }
  };

  //------------Prices

  const getPricesInformation = async (
    coverageId: any = -999,
    concessionId: any = -999,
    inf: any,
    infReservation: any
  ) => {
    setGettingInsurance(true);
    //Function to get the information date that I will need
    const information = dateInformation(infReservation.createdDateTime);
    const data = await getPrices([
      inf.locationContent.locationId,
      infReservation.unitId1,
      information.dateFormatted,
      coverageId,
      concessionId,
      infReservation.waitingId,
    ]);

    if (data != "error") {
      let prices = data.data.moveInCostRetrieveDetailsList;
      getTotal(prices, true);

      if (dataToSend && dataToSend.lock && dataToSend.lock.price) {
        prices.push({
          chargeDescription: "Lock Fee (Taxes Included)",
          total: Number(
            dataToSend.lock.price +
              dataToSend.lock.price * (dataToSend.lock.tax1Rate / 100)
          ),
        });
      }

      setDataPrices(prices || []);
      //Get the parcial-total
      getTotal(prices || []);
    }

    setGettingInsurance(false);
  };

  const getTotal = (prices: any = [], withOutLock = false) => {
    let sum = 0;
    for (let i = 0; i < prices.length; i++) {
      sum = sum + prices[i].total;
    }

    if (withOutLock) {
      setTotalWithOutLock(Number(sum).toFixed(2));
    } else {
      setTotal(Number(sum).toFixed(2));
    }
  };

  //------------Send Payment

  const sendPaymentInformation = async () => {
    setMessageError(null); //Delete last message
    const host = window.location.host; //Code to get the host
    const pageToRedirect = `http://${host}/success-rental`;
    //Function to get the information date that I will need

    const showProtectionBelongInsurance =
      unitInformation.category === 0 ? false : true;

    //console.log("unitInformation", unitInformation);
    //console.log("infReservation", infReservation);
    //console.log("showProtectionBelongInsurance", showProtectionBelongInsurance);
    //console.log("dataToSend", dataToSend);

    //GET NUMBER OF DAYS OF ONE MONTH
    const partialDate = `${
      dataToSend.expirationYear
        ? dataToSend.expirationYear
        : new Date().getFullYear()
    }-${dataToSend.expirationMonth ? dataToSend.expirationMonth : "01"}`;
    const numberOfDays = moment(partialDate, "YYYY-MM").daysInMonth();

    // Block to validate information
    if (
      errorsValidation &&
      ((errorsValidation.insurance && showProtectionBelongInsurance) ||
        errorsValidation.lock)
    ) {
      const message = showProtectionBelongInsurance
        ? labels.insuranceLockText
        : labels.lockText;
      setOpenModal(true);
      setMessageError(message);
      return;
    } else if (errorsValidation && errorsValidation.password) {
      setOpenModal(true);
      setMessageError(labels.validPassword);
      return;
    } else if (
      errorsValidation &&
      (errorsValidation.address ||
        dataToSend.address.replace(/ /g, "") === "" ||
        errorsValidation.state ||
        errorsValidation.zipCode ||
        errorsValidation.city)
    ) {
      setOpenModal(true);
      setMessageError(labels.personalInformation);
      return;
    } else if (
      errorsValidation &&
      (errorsValidation.cardNumber ||
        errorsValidation.cw ||
        errorsValidation.expirationYear ||
        errorsValidation.expirationMonth)
    ) {
      setOpenModal(true);
      setMessageError(labels.billingInformation);
      return;
    }
    // Block to validate information

    if (dataToSend && dataToSend.rentalRenew) {
      setIsSubmiting(true);
      const formData = new FormData();
      formData.set("dataToSend", JSON.stringify(dataToSend));
      formData.set("inf", JSON.stringify(inf));
      formData.set("infReservation", JSON.stringify(infReservation));
      formData.set("total", total);
      formData.set("totalWithOutLock", totalWithOutLock);
      formData.set("numberOfDays", numberOfDays.toString());
      formData.set("pageToRedirect", pageToRedirect);
      formData.set("isLogged", isLogged.toString());

      submit(formData, {
        method: "post",
        action: `/locations/make-payment/${inf.locationContent.locationId}/${inf.reservation}`,
      });
      return;
    } else {
      setIsSubmiting(false);
      setOpenModal(true);
      setMessageError(labels.privacyPolicy);
    }
  };

  if (isHydrated) {
    return (
      <section>
        <div className="div-make-payment-body">
          {isSubmiting && <Spinner />}
          <Header />
          <Stepper labels={labels} />

          <MessageModal
            open={openModal}
            message={messageError}
            title=" "
            ctaText="Close"
            callback={() => {
              setOpenModal(!openModal);
            }}
          />

          <GlobalRichTextModal
            open={openUnitParkingModal}
            labelInformation={"parkingUnit"}
            callback={() => {
              setOpenUnitParkingModal(!openUnitParkingModal);
            }}
          />

          <RentalDetails
            locationContent={inf.locationContent}
            infReservation={infReservation}
            labels={labels}
            unitInformation={unitInformation}
          />

          <section className="div-make-payment-form pt-10">
            <div className="div-body-payment-form">
              <ProtectionBelong
                errors={errors}
                isGettingInsurance={isGettingInsurance}
                errorsOnValidation={errorsValidation}
                unitInformation={unitInformation}
                setErrors={(errors: any) => {
                  setErrors(errors); //Changes original errors's object
                }}
                setErrorsOnValidation={(errors: any) => {
                  setErrorsValidation(errors); //Changes the errors's object to validate on payment
                }}
                locationContent={inf.locationContent}
                getInformation={(name: string, value: string) =>
                  getInformation(name, value)
                }
              />

              <CustomerInformation
                labels={labels}
                userInformation={userInformation}
                isLogged={isLogged}
                errors={errors}
                errorsOnValidation={errorsValidation}
                setErrors={(errors: any) => {
                  setErrors(errors); //Changes original errors's object
                }}
                setErrorsOnValidation={(errors: any) => {
                  setErrorsValidation(errors); //Changes the errors's object to validate on payment
                }}
                infReservation={infReservation}
                getInformation={(name: string, value: string) =>
                  getInformation(name, value)
                }
              />
              <PaymentDetails
                labels={labels}
                errors={errors}
                errorsOnValidation={errorsValidation}
                setErrors={(errors: any) => {
                  setErrors(errors); //Changes original errors's object
                }}
                setErrorsOnValidation={(errors: any) => {
                  setErrorsValidation(errors); //Changes the errors's object to validate on payment
                }}
                getInformation={(name: string, value: string) =>
                  getInformation(name, value)
                }
              />
            </div>
          </section>
          <section className="div-make-payment-information pt-10">
            <RentalInformation
              labels={labels}
              dataToSend={dataToSend}
              getInformation={(name: string, value: string) =>
                getInformation(name, value)
              }
            />
          </section>
        </div>

        {total && (
          <AmountPrices
            isSubmiting={isSubmiting}
            labels={labels}
            sendPaymentInformation={() => sendPaymentInformation()}
            infReservation={infReservation}
            dataPrices={dataPrices}
            total={total}
          />
        )}

        {showpriceFixed && total && (
          <FixedPrice
            isSubmiting={isSubmiting}
            labels={labels}
            total={total}
            sendPaymentInformation={() => sendPaymentInformation()}
          />
        )}
        <Footer />
      </section>
    );
  } else {
    return <Spinner />;
  }
}

export default MakePayment;
//<div className="div-reservations-unit">WORK IN PROGRESS</div>
