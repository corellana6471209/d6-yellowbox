/**
 * function used in multiple query fetch - fetching location data
 * @return {JSON} fetched data
 */

import {
  prepareFetch,
  responseObject,
} from "~/routes/api/shared/prepareSimplyFetch";
import fetchContentful from "../../../utils/fetchContentful";
import fetchCustomApi from "../../../utils/fetchCustomApi";

export async function getProtectionBelong(params: string) {
  const query = `
  {
    componentprotectionCollection(limit: 1, where: {id: "${params}"}) {
      items {
        title
        richText {
          name
          content {
            json
            links {
              assets {
                block {
                  title
                  url
                  sys {
                    id
                  }
                }
              }
            }
          }
        }
      }
    }
  }`;
  const data = await fetchContentful(query);
  if (
    data.componentprotectionCollection &&
    data.componentprotectionCollection != null
  ) {
    const protectionBelong = data.componentprotectionCollection?.items[0];
    return protectionBelong;
  } else {
    return null;
  }
}

export async function getPrices(information: any) {
  console.time("MoveInCostRetrieveWithDiscountReservation")
    
  const data: responseObject = await fetchCustomApi(
    `/api/Rental/MoveInCostRetrieveWithDiscountReservation/${information}`
  );
  if (data.err || !data) {
    //if you want details you have data.errText
    return "error";
  }
  console.timeEnd("MoveInCostRetrieveWithDiscountReservation")
  return data;
}

/**
 * This method should check if the user exists
 * @param {number} locationId
 * @param {number} waitingListId
 * @returns
 */
export const isExistingUser = async (
  locationId: number,
  waitingListId: number
): Promise<any> => {
  console.time("IsExistingUser");
  const user: responseObject = await fetchCustomApi(
    `/api/Authentication/IsExistingUser/${locationId}/${waitingListId}`
  );
  if (user.err) {
  }
  console.timeEnd("IsExistingUser")
  return user.data;
};

/**
 * This method should check if the user exists
 * @param {number} locationId
 * @param {number} tenantId
 * @returns
 */
export const getUserInformation = async (
  locationId: number,
  tenantId: number
): Promise<any> => {
  console.time("CustomerProfile")
  const user: responseObject = await fetchCustomApi(
    `/api/Authentication/CustomerProfile/${locationId}/${tenantId}`
  );
  if (user.err) {
  }
  console.timeEnd("CustomerProfile")
  return user.data;
};

/**
 * TODO: Should be use boolean as a string or integer?
 * @param {any} tenant
 * @returns
 */
export const loginTenant = async (tenant: any) => {
  console.time("LoginTenant");
  const user: responseObject = await fetchCustomApi(
    `/api/Authentication/LoginTenant/${tenant}`
  );
  if (user.err || !user) {
  }
  console.timeEnd("LoginTenant");
  return user.data;
};

/**
 *
 * @param {any} information
 * @returns
 */
export const confirmForgotPassword = async (information: any) => {
  console.time("ConfirmForgotPassword");
  const user: responseObject = await fetchCustomApi(
    `/api/Authentication/ConfirmForgotPassword/${information}`
  );
  if (user.err || !user) {
    console.log(user.errText);
  }
  console.timeEnd("ConfirmForgotPassword");
  return user.data;
};
/**
 *
 * @param {any} information
 * @returns
 */
export const confirmForgotPasswordLegacy = async (information: any) => {
  console.time("ConfirmForgotPasswordLegacy");
  const user: responseObject = await fetchCustomApi(
    `/api/Authentication/ConfirmForgotPasswordLegacy/${information}`
  );
  if (user.err || !user) {
    console.log(user.errText);
  }
  console.timeEnd("ConfirmForgotPasswordLegacy");
  return user.data;
};

/**
 *
 * @param {any} information
 * @returns
 */
export const forgotPassword = async (information: any) => {
  console.time("ForgotPassword");
  const user: responseObject = await fetchCustomApi(
    `/api/Authentication/ForgotPassword/${information}`
  );
  if (user.err || !user) {
    console.log(user.errText);
  }
  console.timeEnd("ForgotPassword");
  return user.data;
};

/**
 *
 * @param {string} emailAddress
 * @returns
 */
export const isLegacy = async (emailAddress: string) => {
  console.time("IsLegacy");
  const user: responseObject = await fetchCustomApi(
    `/api/Authentication/IsLegacy/${emailAddress}`
  );
  if (user.err) {
    return "error";
  }
  console.timeEnd("IsLegacy");
  return user.data;
};

export async function getComponentData(id: string) {
  console.time("locationsMapListing");
  const query = `
     query {
       locationsMapListing(id: "${id}") {
         disclaimerText
         viewMoreLinkText
         viewLocationButtonLabel
         markerBoxButtonLabel
       }
     }`;
  const response = await fetchContentful(query);
  console.timeEnd("locationsMapListing");
  return response.locationsMapListing;
}

export async function getLocations(
  coordinates: any,
  radius: number = 25 / 0.62137,
  searchParam?: string
) {
  console.time("getLocations");
  let coords = "";
  let sp = "";
  if (coordinates.lat) {
    // if coordinates exist
    coords = `
       {address: 
         {coordinates_within_circle: 
           {
             lat: ${coordinates.lat},
             lon: ${coordinates.lng},
             radius: ${radius}
           }
         }
       },`;
  }

  if (searchParam) {
    // if search parama exists
    sp = `
         {locationName_contains: "${searchParam}"},
         {address: {city_contains: "${searchParam}"}},
         {address: {postalCode_contains: "${searchParam}"}},
         `;
  }

  let query = `
           {
               contentTypeLocationCollection(
                 limit: 25,
                 where: {AND: [{OR: [
                     ${coords !== "" ? coords : ""}
                     ${sp !== "" ? sp : ""}
                   ]}
                     {enabled: true}
                   ]}
                 ) {
                 items {
                   locationName
                   slug
                   locationId
                   locationImage {
                     title
                     description
                     contentType
                     fileName
                     size
                     url
                     width
                     height
                   }
                   locationDirections
                   siteLinkLocationId
                   googleBusinessProfileId
                   address {
                     address1
                     address2
                     city
                     state {
                       stateFullName
                       stateAbbreviation
                     }
                     postalCode
                     coordinates {
                       lat
                       lon
                     }
                   }
                   phoneNumberNewCustomer
                   phoneNumberExistingCustomer
                   mondayOfficeHours
                   tuesdayOfficeHours
                   wednesdayOfficeHours
                   thursdayOfficeHours
                   fridayOfficeHours
                   saturdayOfficeHours
                   sundayOfficeHours
                   amenitiesCollection(limit: 10) {
                     items {
                       name
                       amenityId
                       description
                     }
                   }
                   aboutOurFacility
                   storeEmailAddress
                   holdReservationTime
                 }
               }
             }
         `;
  // if there are no searh params, dont even bother querying
  //if (sp !== "") {
  var theData = await fetchContentful(query);
  console.timeEnd("getLocations");
  return theData;
  //}
}

// =========== Payment Processing =========== //
export const moveInReservation = async (moveInReservationParams: any) => {
  //not try to make the request if there is no locations values
  const information = moveInReservationParams || "";
  if (!information) {
    return null;
  }

  const url = "/Rental/MoveInReservation";

  const value = await prepareFetch("POST", url, JSON.stringify(information));

  // await stupidAwait();
  return value;
};

export const tenantUpdate = async (tenantUpdateParams: any) => {
  const information = tenantUpdateParams || "";
  if (!information) {
    return null;
  }

  const url = `/CustomerProfile/TenantUpdate`;
  const method = "POST";

  const data = await prepareFetch(
    method,
    url,
    JSON.stringify(information),
    false
  );

  return data;
};

export const posItemAddToLedger = async (posItemAddToLedgerDataParams: any) => {
  //not try to make the request if there is no locations values
  const information = posItemAddToLedgerDataParams || "";
  if (!information) {
    return null;
  }

  const url = "/Payment/POSItemAddToLedger";
  const value = await prepareFetch("POST", url, JSON.stringify(information));

  // await stupidAwait();
  return value;
};

export const createAccount = async (createAccountParams: any) => {
  const customer = createAccountParams || "";
  if (!customer) {
    return null;
  }

  const url = `/Authentication/CreateAccount`;
  const method = "POST";

  const data = await prepareFetch(method, url, JSON.stringify(customer), false);

  return data;
};

export const tenantUpdateMilitary = async (tenantUpdateMilitaryParams: any) => {
  const information = tenantUpdateMilitaryParams || "";
  if (!information) {
    return null;
  }

  const url = `/CustomerProfile/TenantUpdateMilitary`;
  const method = "POST";

  const data = await prepareFetch(method, url, JSON.stringify(information));

  return data;
};

export const paymentTypeRetrieve = async (paymentTypeRetrieveParams: any) => {
  // here is where the real fetch is happening

  const locationId = paymentTypeRetrieveParams.locationId;
  const data = await prepareFetch(
    "GET",
    `/Payment/PaymentTypesRetrieve?locationId=${locationId}`
  );
  return data;
};

export const tenantBillingInfoUpdate = async (
  tenantBillingInfoUpdateParams: any
) => {
  const information = tenantBillingInfoUpdateParams || "";
  if (!information) {
    return null;
  }
  const url = `/CustomerProfile/TenantBillingInfoUpdate`;
  const method = "POST";

  const data = await prepareFetch(
    method,
    url,
    JSON.stringify(information),
    false
  );

  return data;
};

export const chargeCard = async (chargeCardParams: any) => {
  //not try to make the request if there is no locations values
  const information = chargeCardParams || "";
  if (!information) {
    return null;
  }

  const url = "/Payment/PaymentSimple";

  const value = await prepareFetch("POST", url, JSON.stringify(information));

  // await stupidAwait();
  return value;
};

export const siteLinkeSignCreateLeaseURL = async (
  siteLinkeSignCreateLeaseURLParams: any
) => {
  const information = siteLinkeSignCreateLeaseURLParams || "";
  if (!information) {
    return null;
  }

  const url = `/Rental/SiteLinkeSignCreateLeaseURL`;
  const method = "POST";

  const data = await prepareFetch(method, url, JSON.stringify(information));

  return data;
};
