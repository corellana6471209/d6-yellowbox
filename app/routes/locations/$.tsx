import { useState } from "react";
import { handlePageData } from "~/models/component.server";
import type { LoaderArgs } from "@remix-run/server-runtime";

import Page from "../common/Page";

// import { json } from "@remix-run/node"; // or cloudflare/deno
import { useLoaderData } from "@remix-run/react";
import Four0Four from "../Four0Four";
import { promiseHash } from "remix-utils";
import { PageDetailsContext } from "../../utils/context/PageDeatilsContext";

export async function loader({ params }: LoaderArgs) {
  // get slug from params
  const slug = params["*"];
  if (slug && slug.startsWith("_static")) {
    throw new Response("Not Found", {
      status: 404,
    });
  }
  // homepage doesnt have a slug so we have to get it another way
  let w = `{title: "Home Page"}`;
  if (params["*"]) {
    w = `{slug: "${slug}"}`;
  }
  const page = await handlePageData(w);
  if (!page || !page.pageCollection || !page.pageCollection.items) {
    throw new Response("Not Found", {
      status: 404,
    });
  }
  return promiseHash(page);
}

// export const meta: MetaFunction = ({ data, params }) => {
//   if (data.pageCollection.items.length > 0) {
//     return {
//       title: `${data.pageCollection.items[0].seoTitle}`,
//       description: `${data.pageCollection.items[0].seoDescription}`,
//     };
//   }
// };

export default function Index() {
  const pageData = useLoaderData();
  const components =
    pageData?.pageCollection?.items[0]?.componentsCollection?.items;
  const title = pageData?.pageCollection?.items[0]?.title;
  const pageName = title?.toLowerCase()?.replace(/ /g, "-");
  const slug = pageData?.pageCollection?.items[0]?.slug;

  const displayHeaderAndFooter =
    pageData?.pageCollection?.items[0]?.displayHeaderAndFooter;
  const [address, setAddress] = useState(null);
  //   return 404 if page doesnt exist
  if (pageData?.pageCollection?.items?.length < 1) {
    return <Four0Four />;
  }

  const pageDetails = {
    title,
    slug: pageName,
    displayHeader: displayHeaderAndFooter ? displayHeaderAndFooter : false,
    address: {
      address,
      setAddress,
    },
  };

  return (
    <PageDetailsContext.Provider value={pageDetails}>
      <main className={pageName}>
        <div className="content-wrapper mx-auto max-w-screen-xl">
          <Page components={components} slug={slug} />
        </div>
      </main>
    </PageDetailsContext.Provider>
  );
}
