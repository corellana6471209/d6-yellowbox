import Amenities from "../Amenities/index";
import { useMediaQuery } from "react-responsive";
import LocationMap from "../LocationMap";

import MarkDown from "../../page-components/MarkDown/index";

function LocationNearMe(props: any) {
  const {
    aboutOurFacility,
    amenitiesCollection,
    address,
    // serving,
    displayPointsOfInterest,
    pointsOfInterest,
    // titleBody,
    // aboutTheArea,
    // localNeighborhoods,
    aboutFacilityTitle,
    displayCustomSection1,
    customSection1Title,
    customSection1Content,
    displayCustomSection2,
    customSection2Title,
    customSection2Content,
    displayCustomSection3,
    customSection3Title,
    customSection3Content,
    displayCustomSection4,
    customSection4Title,
    customSection4Content,
  } = props;
  const isMobile = useMediaQuery({ query: "(max-width: 991px)" });

  function renderLocationNear(body: any, amenities?: any, serving = false) {
    return (
      <>
        {amenities && amenities.items && (
          <div>
            {!body && <h3 className="font-serif">About Our Facility</h3>}
            <Amenities amenities={amenities} />
          </div>
        )}
      </>
    );
  }

  return (
    <div className="near-me bg-primary3 xs:pb-7 lg:pb-12">
      <div className="near-me-body mx-auto max-w-screen-xl max-w-[900px] xs:px-5 lg:px-0">
        <h3 className="font-serif">
          {aboutFacilityTitle ? aboutFacilityTitle : "About Our Facility"}
        </h3>
        {renderLocationNear(aboutOurFacility, amenitiesCollection)}
      </div>

      {displayPointsOfInterest && (
        <div className="mx-auto max-w-[900px]">
          <h3 className="px-5 font-serif">
            {`${
              isMobile || (!isMobile && !pointsOfInterest)
                ? "Storage Units in"
                : "Points of Interest near"
            } 
            ${address.city},
            ${address?.state?.stateAbbreviation} 
            ${address.postalCode}`}
          </h3>

          <div className="grid grid-cols-12">
            <div
              className={`near-me-map col-span-12 mb-8 h-96 md:mb-0 ${
                pointsOfInterest ? "lg:col-span-6 lg:pr-5" : ""
              }`}
            >
              <LocationMap {...props} />
            </div>
            <div className="points col-span-12 xs:px-5 lg:col-span-6 lg:px-0">
              <MarkDown body={pointsOfInterest} />
            </div>
          </div>
        </div>
      )}

      {displayCustomSection1 && (
        <div className="mx-auto max-w-screen-xl xs:px-5 lg:px-0 lg:pt-4">
          <h3 className="font-serif">{customSection1Title}</h3>

          <div className='grid-cols-12" grid gap-8'>
            <div className="points col-span-12 lg:col-span-6 lg:pr-5">
              <MarkDown body={customSection1Content} />
            </div>
          </div>
        </div>
      )}

      {displayCustomSection2 && (
        <div className="mx-auto max-w-screen-xl xs:px-5 lg:px-0 lg:pt-4">
          <h3 className="font-serif">{customSection2Title}</h3>

          <div className='grid-cols-12" grid gap-8'>
            <div className="points col-span-12 lg:col-span-6 lg:pr-5">
              <MarkDown body={customSection2Content} />
            </div>
          </div>
        </div>
      )}

      {displayCustomSection3 && (
        <div className="mx-auto max-w-screen-xl xs:px-5 lg:px-0 lg:pt-4">
          <h3 className="font-serif">{customSection3Title}</h3>

          <div className='grid-cols-12" grid gap-8'>
            <div className="points col-span-12 lg:col-span-6 lg:pr-5">
              <MarkDown body={customSection3Content} />
            </div>
          </div>
        </div>
      )}

      {displayCustomSection4 && (
        <div className="mx-auto max-w-screen-xl xs:px-5 lg:px-0 lg:pt-4">
          <h3 className="font-serif">{customSection4Title}</h3>

          <div className='grid-cols-12" grid gap-8'>
            <div className="points col-span-12 lg:col-span-6 lg:pr-5">
              <MarkDown body={customSection4Content} />
            </div>
          </div>
        </div>
      )}

      {/* {(aboutTheArea || localNeighborhoods) && (
        <div className="mx-auto max-w-screen-xl xs:px-5 lg:px-0 lg:pt-4">
          <h3>{titleBody ? titleBody : "About the area"}</h3>

          <div className='gap-8" grid grid-cols-12'>
            <div className="points col-span-12 lg:col-span-6 lg:pr-5">
              {renderLocationNear(aboutTheArea)}
            </div>

            <div className="h-small col-span-12 xs:pt-4 lg:col-span-6 lg:pl-5 lg:pt-0">
              {renderLocationNear(localNeighborhoods)}
            </div>
          </div>
        </div>
      )} */}
    </div>
  );
}

export default LocationNearMe;
