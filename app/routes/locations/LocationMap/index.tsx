import PropTypes from 'prop-types';
import { useEffect, useState } from 'react';
import Map from '~/routes/common/Map';
import { getLocations } from '~/routes/page-components/LocationMapList/backend';
import mapStyles from "../../../resources/mapStyles.json";

function LocationMap(props: any) {
  const { address } = props;
  const [center, setCenter] = useState<any>(null);
  const [locations, setLocations] = useState(null);

  useEffect(() => {
    async function fetchData() {
      let coords = {
        lat: address.coordinates.lat, 
        lng: address.coordinates.lon 
      };
      setCenter(coords);
      const loc = await getLocations(coords);
      setLocations(loc?.contentTypeLocationCollection?.items);
      return;
    }

    if (address.coordinates) {
      fetchData();
    }
  }, [])

  return (center && locations) ? (
    <>
      <Map
        containerStyle={{
          width: "100%",
          height: "100%",
        }}
        options={{
          styles: mapStyles,
          streetViewControl: false,
          fullscreenControl: false,
          mapTypeControl: false,
        }}
        zoom={12} 
        center={center}
        locations={locations} />
    </>
  ) : null;
}

LocationMap.propTypes = {}

export default LocationMap;