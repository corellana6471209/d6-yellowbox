import fetchContentful from "../../utils/fetchContentful";
import fetchCustomApi from "../../utils/fetchCustomApi";

import * as unitInformation from "../api/Inventory/unit/$unitInformation";
import {
  prepareFetch,
  responseObject,
} from "~/routes/api/shared/prepareSimplyFetch";

const maximumRetries: number = 4;
let fallbackImage: undefined;
const getFallbackImage = async () => {
  if (fallbackImage) {
    return fallbackImage;
  }
  const query = `{
    unitTypeCollection(where: {name:"fallback"}) {
      items {
        name
        id
        width
        length
        unitTypeImage {
          title
          description
          contentType
          fileName
          size
          url
          width
          height
        }
        zoomedImage {
          title
          description
          contentType
          fileName
          size
          url
          width
          height
        }
        description {
          json
        }
        location {
          locationName
          slug
          enabled
          locationId
          googleBusinessProfileId
          phoneNumberNewCustomer
          phoneNumberExistingCustomer
          lunchHours
          gateAccessHours
          locationDirections
          storeEmailAddress
          siteLinkLocationId
          holdReservationTime
        }
      }
    }
  }`;
  var { unitTypeCollection } = await fetchContentful(query);
  if (!unitTypeCollection || !unitTypeCollection?.items) return null;
  fallbackImage = unitTypeCollection?.items[0];
  return fallbackImage;
};

/**
 * Modified from the original loadLocationContent
 * @param {string} slug
 * @returns
 */
export let loadLocationContent = async (slug: string | undefined) => {
  const query = `
  {
    contentTypeLocationCollection(limit:8,where: {slug: "${slug}"}) {
      items {
        locationName
        locationId
        requireLock
        slug
        locationImageCollection {
          items {
            title
            description
            contentType
            fileName
            size
            url
            width
            height
          }
        }
        locationDirections
        siteLinkLocationId
        googleBusinessProfileId
        address {
          address1
          address2
          city
          state {
            stateFullName
            stateAbbreviation
            canUseInsurancePolicy
          }
          postalCode
          coordinates {
            lat
            lon
          }
        }
        phoneNumberNewCustomer
        phoneNumberExistingCustomer
        mondayOfficeHours
        tuesdayOfficeHours
        wednesdayOfficeHours
        thursdayOfficeHours
        fridayOfficeHours
        saturdayOfficeHours
        sundayOfficeHours
        amenitiesCollection(limit: 8) {
          items {
            name
            amenityId
            description
          }
        }
        aboutFacilityTitle
        aboutOurFacility
        displayPointsOfInterest
        pointsOfInterest 
        displayCustomSection1
        customSection1Title
        customSection1Content
        displayCustomSection2
        customSection2Title
        customSection2Content
        displayCustomSection3
        customSection3Title
        customSection3Content
        displayCustomSection4
        customSection4Title
        customSection4Content
        storeEmailAddress
        holdReservationTime
        lunchHours
        gateAccessHours
        sizeGuide{
          title
          description
          ctaText
        }
        blogPosts {
          sys {
            id
          }
        }
      }
    }
  }
`;

  var { contentTypeLocationCollection } = await fetchContentful(query);
  if (!contentTypeLocationCollection || !contentTypeLocationCollection?.items)
    return null;

  const location = contentTypeLocationCollection?.items[0];
  // const { unitsCollection, ...rest } = location;
  // const dataUnits = await fetchUnitInformationByLocationServerSide(
  //   location.locationId
  // );
  // let unitTypes = await getUnitTypes(dataUnits?.data, location.locationId);

  // return { ...rest, units: unitTypes };
  return location;
};

/**
 * Modified from the original loadLocationContent
 * changed slug for locationId
 * @param {number} locationId
 * @returns
 */
export let loadLocationContentByLocationIdCompleted = async (
  locationId: string | undefined
) => {
  const query = `
  {
    contentTypeLocationCollection(limit:8,where: {locationId: "${locationId}"}) {
      items {
        locationName
        locationId
        requireLock
        slug
        locationImageCollection {
          items {
            title
            description
            contentType
            fileName
            size
            url
            width
            height
          }
        }
        locationDirections
        siteLinkLocationId
        googleBusinessProfileId
        address {
          address1
          address2
          city
          state {
            stateFullName
            stateAbbreviation
            canUseInsurancePolicy
          }
          postalCode
          coordinates {
            lat
            lon
          }
        }
        phoneNumberNewCustomer
        phoneNumberExistingCustomer
        mondayOfficeHours
        tuesdayOfficeHours
        wednesdayOfficeHours
        thursdayOfficeHours
        fridayOfficeHours
        saturdayOfficeHours
        sundayOfficeHours
        amenitiesCollection(limit: 8) {
          items {
            name
            amenityId
            description
          }
        }
        aboutOurFacility
        pointsOfInterest 
        storeEmailAddress
        holdReservationTime
        lunchHours
        gateAccessHours
        sizeGuide{
          title
          description
          ctaText
        }
        blogPosts {
          sys {
            id
          }
        }
      }
    }
  }
`;

  try {
    var { contentTypeLocationCollection } = await fetchContentful(query);
    if (!contentTypeLocationCollection || !contentTypeLocationCollection?.items)
      return null;

    return contentTypeLocationCollection?.items[0];
  } catch (error) {
    return null;
  }
};

/**
 * Modified from the original loadLocationContent
 * changed slug for locationId
 * @param {number} locationId
 * @returns
 */
export let loadLocationContentByLocationId = async (
  locationId: string | undefined
) => {
  const query = `
  {
    contentTypeLocationCollection(limit:8,where: {locationId: "${locationId}"}) {
      items {
        locationName
        locationId
        slug
        locationImageCollection {
          items {
            title
            description
            contentType
            fileName
            size
            url
            width
            height
          }
        }      
        address {
          address1
          address2
          city
          state {
            stateFullName
            stateAbbreviation
            canUseInsurancePolicy
          }
          postalCode
          coordinates {
            lat
            lon
          }
        }
        phoneNumberNewCustomer
        phoneNumberExistingCustomer     
      }
    }
  }
`;

  try {
    var { contentTypeLocationCollection } = await fetchContentful(query);
    if (!contentTypeLocationCollection || !contentTypeLocationCollection?.items)
      return null;

    return contentTypeLocationCollection?.items[0];
  } catch (error) {
    return null;
  }
};

export async function getReservationList(information: any) {
  const data: responseObject = await fetchCustomApi(
    `/api/ReservationList/${information}`
  );
  if (data.err || !data) {
    //if you want details you have data.errText
    return "error";
  }
  return data?.data[0];
}

export const fetchUnitInformationByLocationServerSide = async (
  locationId: string
) => {
  const url =
    "/Inventory/UnitsInformationLocalByLocation?locationid=" + locationId;
  const value = await prepareFetch("GET", url, null);
  return value;
};
const getUnitType = async (
  unit: any,
  locationId: number,
  popNumber: number = 0
) => {
  const conditionsEmpty = [{ location_exists: false }, { name_exists: false }];
  let conditions: any = [];

  conditions = [
    { width_in: unit.width },
    { length_in: unit.length },
    { name_in: unit.unitTypeName },
    {
      location: {
        locationId_in: [locationId],
      },
    },
  ];

  if (popNumber > 0) {
    for (var i = 0; i < popNumber; i++) {
      const key = conditions.length - (i + 1);
      delete conditions[key].location;
      delete conditions[key].name_in;

      Object.assign(conditions[key], conditionsEmpty[i]);
    }
    if (popNumber === maximumRetries) {
      return [];
    }
  }
  const json = JSON.stringify(conditions);
  const unquoted = json.replace(/"([^"]+)":/g, "$1:");

  //console.log("unquoted", unquoted);

  const query = `{
    unitTypeCollection(where: {AND:${unquoted}}) {
      items {
        name
        id
        width
        length
        unitTypeImage {
          title
          description
          contentType
          fileName
          size
          url
          width
          height
        }
        zoomedImage {
          title
          description
          contentType
          fileName
          size
          url
          width
          height
        }
        description {
          json
        }
        location {
          locationName
          slug
          enabled
          locationId
          googleBusinessProfileId
          phoneNumberNewCustomer
          phoneNumberExistingCustomer
          lunchHours
          gateAccessHours
          locationDirections
          storeEmailAddress
          siteLinkLocationId
          holdReservationTime
        }
      }
    }
  }`;

  const { unitTypeCollection } = await fetchContentful(query);
  if (unitTypeCollection?.items?.length <= 0) {
    return await getUnitType(unit, locationId, popNumber + 1);
  } else {
    return unitTypeCollection?.items || [];
  }
};
export const getUnitTypes = async (unitsArray: any[], locationId: number) => {
  for (const unit of unitsArray) {
    const unitPromise = await getUnitType(unit, locationId);
    if (!unitPromise.length || unitPromise === "error" || !unitPromise) {
      unit.image = await getFallbackImage();
    } else {
      try {
        unit.image = unitPromise[0];
      } catch (error) {
        unit.image = await getFallbackImage();
      }
    }
  }
  return unitsArray;
};

export async function getUnitImageInformation(unit: any, locationId: number) {
  const unitPromise = await getUnitType(unit, locationId);

  if (!unitPromise.length || unitPromise === "error" || !unitPromise) {
    unit.image = await getFallbackImage();
  } else {
    try {
      unit.image = unitPromise[0];
    } catch (error) {
      unit.image = await getFallbackImage();
    }
  }

  return unit;
}
