import { useLoaderData } from "@remix-run/react";
import { useState, useEffect } from "react";
import LocationInformation from "../common/LocationInformation";
import Footer from "~/routes/common/Footer";
import { loadLocationContent } from "./backend";
import type { LoaderFunction } from "@remix-run/node";
import LocationNearMe from "./LocationNearMe/index";
import SizeGuide from "./SizeGuide";
import Header from "../common/Header";
import UnitsCardComponent from "./UnitsCardComponent";
import { UnitDetailsContext } from "../../utils/context/UnitContext";
import { setCookie, deleteCookie } from "../../utils/cookie";
import contentfulKeys from "../../utils/config";
import { getLabel } from "../../utils/getLabel";
import BlogTeaser from "~/routes/page-components/BlogTeaser";
import { getCacheValueByKey } from "../api/shared/elasticCache";
import { analytics } from "../../utils/analytics";
import type { MetaFunction } from "@remix-run/node";
import useRating from "../../utils/hooks/useRating";

//Endpoints

import * as reservation from "../api/Reservation/$information";
import * as generateRentalCompleteUrl from "../api/Rental/GenerateRentalCompleteUrl/$information";
import * as sendReservationSMS from "../api/SendReservationSMS/$information";
import * as waitlist from "../api/Waitlist/$information";

export const loader: LoaderFunction = async ({ params, request }) => {
  const { slug } = params;
  const loactionContent = await loadLocationContent(slug);
  const token = await getCacheValueByKey("accessToken", request);
  const user = await getCacheValueByKey("tenantInfo", request);
  loactionContent.token = token;
  loactionContent.user = user;
  return loactionContent;
};

export const meta: MetaFunction = ({ data }) => {
  return {
    title: `Storage Units ${data?.address.city}, ${data?.address?.state?.stateAbbreviation} ${data?.address.postalCode} | Simply Self Storage`,
    description: data?.aboutOurFacility.substr(0, 160),
    "og:image": `${
      data?.locationImageCollection.items[0]
        ? data?.locationImageCollection.items[0].url
        : ""
    }`,
    address: {
      streetAddress: data?.address.address1,
      addressLocality: data?.address.city,
      addressRegion: data?.address?.state?.stateAbbreviation,
      postalCode: data?.address.postalCode,
    },
    geo: {
      latitude: data?.address.coordinates.lat,
      longitude: data?.address.coordinates.lon,
    },
    telephone: data?.phoneNumberNewCustomer,
    openingHoursSpecification: [
      {
        dayOfWeek: ["Monday", "Tuesday", "Wednesday", "Thursday", "Friday"],
        opens: data?.mondayOfficeHours
          ? data?.mondayOfficeHours.split("-")[0]
          : null,
        closes: data?.mondayOfficeHours
          ? data?.mondayOfficeHours.split("-")[1]
          : null,
      },
      {
        dayOfWeek: ["Saturday"],
        opens: data?.saturdayOfficeHours
          ? data?.saturdayOfficeHours.split("-")[0]
          : null,
        closes: data?.saturdayOfficeHours
          ? data?.saturdayOfficeHours.split("-")[1]
          : null,
      },
    ],
  };
};

export async function action({ request }: any) {
  const body = await request.formData();

  let totalVacancy = body.get("totalVacancy") as string;
  let firstName = body.get("firstName") as string;
  let lastName = body.get("lastName") as string;
  let email = body.get("email") as string;
  let phone = body.get("phone") as string;
  let marketing = body.get("marketing") as string;
  let date = body.get("date") as string;
  let unitTypeId = body.get("unitTypeId") as string;
  let concessionId = body.get("concessionId") as string;

  let locationContent = JSON.parse(body.get("locationContent") as string);
  let firstAvailableId = body.get("firstAvailableId") as string;

  if (totalVacancy && totalVacancy === "true") {
    const reservationParams: any = {
      params: {
        information: {
          firstName: firstName,
          lastName: lastName,
          emailAddress: email,
          primaryPhoneNumber: phone,
          locationId: locationContent.locationId,
          dateNeeded: date,
          unitId1: firstAvailableId,
          concessionId: concessionId,
          comment: "",
        },
      },
    };

    const reservationResponse = await reservation.loader(reservationParams);

    if (!reservationResponse.data || !reservationResponse.data?.reservationId) {
      return {
        error: true,
        totalVacancy: true,
        redirect: false,
      };
    } else {
      if (marketing === "Yes") {
        const generateRentalCompleteUrlParams: any = {
          params: {
            information: {
              locationId: locationContent.locationId,
              reservationWaitingId: reservationResponse.data?.reservationId,
            },
          },
        };

        const generateRentalCompleteUrlResponse =
          await generateRentalCompleteUrl.loader(
            generateRentalCompleteUrlParams
          );

        if (
          generateRentalCompleteUrlResponse &&
          generateRentalCompleteUrlResponse.data
        ) {
          const sendReservationSMSParams: any = {
            params: {
              information: {
                FirstName: firstName,
                ReservationNumber: String(
                  reservationResponse.data?.reservationId
                ),
                RentalLink: generateRentalCompleteUrlResponse.data,
                PrimaryPhoneNumber: phone,
              },
            },
          };

          const sms = await sendReservationSMS.loader(sendReservationSMSParams);
        }
      }

      return {
        redirect: true,
        redirectTo: `/locations/reserve-unit/${locationContent.locationId}/${reservationResponse.data.reservationId}`,
      };
    }
  } else {
    const waitlistParams: any = {
      params: {
        information: {
          firstName: firstName,
          lastName: lastName,
          emailAddress: email,
          PhoneNumber: phone,
          locationId: locationContent.locationId,
          UnitTypeId: unitTypeId,
        },
      },
    };

    const waitlistResponse = await waitlist.loader(waitlistParams);

    if (!waitlistResponse.data || waitlistResponse.data?.err) {
      return {
        error: true,
        totalVacancy: false,
        redirect: false,
      };
    } else {
      return {
        error: false,
        totalVacancy: false,
        redirect: false,
      };
    }
  }
}

export default function Location() {
  const loactionContent = useLoaderData();
  const [isHydrated, setIsHydrated] = useState(false);
  const [locationData, setLocationData] = useState<Location>();
  const [units, setUnits] = useState<any>(null);

  const [unitDetails, setUnitDetails] = useState<any>(null);
  let blogTeaser;
  if (locationData?.blogPosts?.sys?.id) {
    blogTeaser = locationData.blogPosts.sys.id;
  }
  useEffect(() => {
    async function fetchData() {
      // You can await here
      const qtyUnitUrgencyMsg = await getLabel("quantityShowUnitUrgencyMsg");

      const unitUrgencyMsg = await getLabel("unitUrgencyMessage");
      setUnitDetails({
        qtyUnitUrgencyMsg: qtyUnitUrgencyMsg,
        unitUrgencyMsg: unitUrgencyMsg,
      });

      return qtyUnitUrgencyMsg;
    }

    async function fetchCookie() {
      deleteCookie(contentfulKeys.constants.lastPage);
      const days = await getLabel("cookieTimeDays");
      setCookie(
        contentfulKeys.constants.lastPage,
        window.location.href,
        days ? parseInt(days) : 30
      );
    }
    analytics(window, "propertyViewed", {
      locationID: loactionContent.locationId,
    });
    fetchCookie();
    fetchData();
    setIsHydrated(true);
  }, []);

  useEffect(() => {
    setLocationData(loactionContent);
    if (loactionContent && loactionContent.units)
      setUnits(loactionContent.units);
  }, [locationData]);

  const Schema = () => {
    try {
      if (loactionContent) {
        const domain = window.location.hostname;
        return (
          <script
            type="application/ld+json"
            dangerouslySetInnerHTML={{
              __html: `
              {
                "@context": "https://schema.org",
                "@type": "SelfStorage",
                "name": "${loactionContent.address.city}",
                "url": "https://${domain}/locations/${loactionContent.slug}",
			          "logo": "https://images.ctfassets.net/rfh1327471ch/4o7lfAQpw4C9WDQKryX4Hr/961b62c9b15f79ae92c518b076b3c918/SSS_Logo_Main.svg",
			          "image": "${loactionContent?.locationImageCollection?.items[0]?.url}",
                "description": "${loactionContent.aboutOurFacility}",
                "priceRange": "$20 - $300",
                "address": {
                  "@type": "PostalAddress",
                  "streetAddress": "${loactionContent.address.address1}",
                  "addressLocality": "${
                    loactionContent.address.state.stateFullName
                  }",
                  "addressRegion": "${
                    loactionContent.address?.state?.stateAbbreviation
                  }",
                  "postalCode": "${loactionContent.address.postalCode}",
                  "addressCountry": "US"
                },
                "geo": {
                  "@type": "GeoCoordinates",
                  "latitude": "${loactionContent.address.coordinates.lat}",
                  "longitude": "${loactionContent.address.coordinates.lon}"
                },
                "contactPoint": {
                  "@type": "ContactPoint",
                  "telephone": "${loactionContent.phoneNumberNewCustomer}",
                  "contactType": "Sales contact point"
                  },
                  "openingHours": "${
                    loactionContent.mondayOfficeHours
                      ? loactionContent.mondayOfficeHours.split("-")[0]
                      : null
                  } - ${
                loactionContent.mondayOfficeHours
                  ? loactionContent.mondayOfficeHours.split("-")[1]
                  : null
              }"
              }
            `,
            }}
          />
        );
      }
      return null;
    } catch (error) {
      return null;
    }
  };

  const BreadcrumbList = () => {
    try {
      if (window) {
        const domain = window.location.hostname;
        return (
          <script
            type="application/ld+json"
            dangerouslySetInnerHTML={{
              __html: `
              {
                "@context": "https://schema.org",
                "@type": "BreadcrumbList",
                "itemListElement": [{
                  "@type": "ListItem",
                  "position": 1,
                  "name": "Home",
                  "item": "https://${domain}/"
                },{
                  "@type": "ListItem",
                  "position": 2,
                  "name": "Location List",
                  "item": "https://${domain}/locations/"
                },{
                  "@type": "ListItem",
                  "position": 3,
                  "name": "Location"
                }]
              }
            `,
            }}
          />
        );
      }
      return null;
    } catch (error) {
      return null;
    }
  };

  return (
    <>
      <Schema />
      <BreadcrumbList />
      {isHydrated && locationData ? (
        <UnitDetailsContext.Provider value={unitDetails}>
          <Header />
          {console.log("locationData", locationData)}
          <LocationInformation {...locationData} />
          {/**ON THIS COMPONENT IS SETTED THE UNITCARD/UNIT-TYPE/FilterInformationModal COMPONENT */}
          <UnitsCardComponent units={units} locationContent={loactionContent} />

          {locationData.sizeGuide && <SizeGuide {...locationData.sizeGuide} />}
          {blogTeaser && <BlogTeaser id={blogTeaser} />}

          <LocationNearMe {...locationData} />
          <Footer />
        </UnitDetailsContext.Provider>
      ) : (
        <div>Loading Location Data...</div>
      )}
    </>
  );
}

export type Location = {
  slug: string;
  aboutOurFacility: any;
  lunchHours: string;
  gateAccessHours: string;
  address: {
    address1: string;
    address2: null;
    city: string;
    state: {
      stateAbbreviation: string;
      stateFullName: string;
    };
    coordinates: {
      lat: string;
      lon: string;
    };
    postalCode: string;
  };
  amenitiesCollection: { items: any }; // we will change this later
  googleBusinessProfileId: string;
  holdReservationTime: number;
  locationDirections: string;
  locationId: number;
  locationImageCollection: {
    items: [
      {
        title: string;
        description: string;
        fileName: string;
        url: string;
      }
    ];
  };
  locationName: "Panthersville / Decatur Self Storage 2 (1)";
  mondayOfficeHours: string;
  tuesdayOfficeHours: string;
  wednesdayOfficeHours: string;
  thursdayOfficeHours: string;
  fridayOfficeHours: string;
  saturdayOfficeHours: string;
  sundayOfficeHours: string;
  phoneNumberExistingCustomer: string;
  phoneNumberNewCustomer: string;
  serving: { json: any };
  localNeighborhoods: { json: any };
  siteLinkLocationId: number;
  storeEmailAddress: string;
  units: any[];
  sizeGuide: {
    title: string;
    description: string;
    ctaText: string;
  };
  blogPosts: {
    sys: {
      id: string;
    };
  };
};
