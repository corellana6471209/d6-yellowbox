import Header from "~/routes/common/Header";
import Footer from "~/routes/common/Footer";
import { useState, useEffect, useRef } from "react";
import { useLoaderData } from "@remix-run/react";
import type { LoaderFunction, MetaFunction } from "@remix-run/node";
import { getStateData } from "./backend";

import LocationMapList from "~/routes/page-components/LocationMapList";
import RichText from "~/routes/common/RichTextComponent";
import BlogTeaser from "~/routes/page-components/BlogTeaser";
import { getCacheValueByKey } from "~/routes/api/shared/elasticCache";
import { getLocationsByState } from "~/routes/page-components/LocationMapList/backend";
import useRating from "../../../utils/hooks/useRating";

export const loader: LoaderFunction = async ({ params, request }) => {
  let { state } = params;
  state = state?.toUpperCase();
  let stateData = await getStateData(state);
  const token = await getCacheValueByKey("accessToken", request);
  const user = await getCacheValueByKey("tenantInfo", request);
  stateData.token = token;
  stateData.user = user;
  return stateData;
};

export const meta: MetaFunction = ({ data }) => {
  const param = data.stateCollection.items[0];
  return {
    title: `Storage Units ${param?.stateFullName}`,
    description: `Storage Unit location list for ${param?.stateFullName}`,
    address: {
      addressLocality: param?.stateFullName,
      addressRegion: param?.stateAbbreviation,
    },
  };
};

export default function State() {
  const [isHydrated, setIsHydrated] = useState(false);
  const mainContent = useRef(null);
  let LoadedContent = useLoaderData();

  let state = LoadedContent?.stateCollection?.items[0]?.stateFullName;
  let stateAbbreviation =
    LoadedContent?.stateCollection?.items[0]?.stateAbbreviation;
  let info = LoadedContent?.stateCollection?.items[0]?.stateInformation;
  let labels = LoadedContent?.stateCollection?.items[0];
  // console.log(LoadedContent?.stateCollection?.items[0].blogTeaser);
  let blogTeaser;
  if (LoadedContent?.stateCollection?.items[0]?.blogTeaser) {
    blogTeaser = LoadedContent?.stateCollection?.items[0]?.blogTeaser.sys.id;
  }
  useEffect(() => {
    async function fetchData() {
      setIsHydrated(false);
      let l = await getLocationsByState(stateAbbreviation);
      const locations = l.contentTypeLocationCollection.items;
      if (locations?.length <= 0) {
        window.location.href = "/no-locations";
      }
      setIsHydrated(true);
    }
    fetchData();
  }, [stateAbbreviation]);

  const Schema = () => {
    try {
      if (LoadedContent?.stateCollection?.items[0]) {
        const param = LoadedContent?.stateCollection?.items[0];
        return (
          <script
            type="application/ld+json"
            dangerouslySetInnerHTML={{
              __html: `
              {
                "@context": "https://schema.org",
                "@type": "SelfStorage",
                "name": "Store Unit ${param?.stateFullName}",
                "address": {
                  "@type": "PostalAddress",
                  "addressLocality": "${param?.stateFullName}",
                  "addressRegion": "${param?.stateAbbreviation}",
                  "addressCountry": "US"
                }
              }
            `,
            }}
          />
        );
      }
      return null;
    } catch (error) {
      return null;
    }
  };

  const BreadcrumbList = () => {
    try {
      if (window) {
        const domain = window.location.hostname;
        return (
          <script
            type="application/ld+json"
            dangerouslySetInnerHTML={{
              __html: `
              {
                "@context": "https://schema.org",
                "@type": "BreadcrumbList",
                "itemListElement": [{
                  "@type": "ListItem",
                  "position": 1,
                  "name": "Home",
                  "item": "https://${domain}/"
                },{
                  "@type": "ListItem",
                  "position": 2,
                  "name": "Location List",
                  "item": "https://${domain}/locations/state/"
                },{
                  "@type": "ListItem",
                  "position": 3,
                  "name": "Location"
                }]
              }
            `,
            }}
          />
        );
      }
      return null;
    } catch (error) {
      return null;
    }
  };

  return (
    <>
      <Schema />
      <BreadcrumbList />
      {isHydrated ? (
        <div>
          <Header />
          <LocationMapList
            {...labels}
            param={stateAbbreviation}
            type="state"
            id={null}
          />
          <div className="state-location-information-wrapper bg-primary3 px-5 py-10">
            <div className="small-wrapper">
              <RichText contentWrap="small" data={info} />
            </div>
          </div>
          {blogTeaser && <BlogTeaser id={blogTeaser} />}
          <Footer />
        </div>
      ) : (
        <div>Loading State Data...</div>
      )}
    </>
  );
}
