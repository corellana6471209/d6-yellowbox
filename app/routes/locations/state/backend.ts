import fetchContentful from "../../../utils/fetchContentful";

export let getStateData = async (state: string) => {
  const query = `
  {
    stateCollection(    where: {OR: [{stateAbbreviation_contains: "${state}"}, {stateFullName: "${state}"}]}
    ) {
        items{
        stateFullName
        stateAbbreviation
        stateInformation {
          json
        }
        blogTeaser {
          sys {
            id
          }
        }
      }
    }
  }
  `;
  var theData = await fetchContentful(query);
  return theData;
};
