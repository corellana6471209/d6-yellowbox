import { describe, expect, it, vi } from "vitest";
import { getReservationList } from "./backend";

const obj = {
  getReservationList: () => "getReservationList",
};

vi.mock("./backend", () => ({
  getReservationList: vi.fn().mockImplementation(() => "getReservationList"),
}));

describe("fetching location data component unit test", () => {
  it("LoadModuleTitle should be mock", () => {
    expect(vi.isMockFunction(getReservationList)).toBe(true);
  });

  it("Should be executed loadModuleTitle", async () => {
    const spy = vi.spyOn(obj, "getReservationList");

    const resp = await getReservationList("idTest");
    expect(spy.getMockName()).toEqual("getReservationList");
    expect(resp).toBe("getReservationList");
  });
});
