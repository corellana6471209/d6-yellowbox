import PropTypes from "prop-types";
import { useEffect, useState } from "react";

interface Items {
  name: string;
  amenityId: number;
  description: string;
}

function Amenities(props: any) {
  const { amenities } = props;
  const [items, setItems] = useState<Items[] | null>([]);

  useEffect(() => {
    if (amenities?.items?.length) setItems(amenities.items);
  }, []);

  return items?.length ? (
    <ul className="amenities-list pb-4">
      {items?.map((amenity, index) => (
        <li
          className="amenities-list-item"
          key={`${amenity.amenityId}-${index}`}
        >
          {amenity.name}
        </li>
      ))}
    </ul>
  ) : null;
}

Amenities.propTypes = {
  amenities: PropTypes.object,
};

Amenities.defaultProps = {
  amenities: null,
};

export default Amenities;
