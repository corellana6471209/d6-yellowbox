import { describe, expect, it } from "vitest";
import { fireEventAliased, render, screen } from "../../../utils/test-utils";
import { BrowserRouter as Router } from "react-router-dom";
import UnitCard from './index';
import { fakeDataUnit } from '../../../utils/fakedataToTests';

const obj = {
  handlerZoom: () => "handlerZoom",
  handlerReserve: () => "handlerReserve"
}

describe("Unit Card component", () => {
  it("Verify if Unit Card component renders", () => {
    render( 
      <Router> 
        <UnitCard unit={fakeDataUnit} />,
      </Router>
    );

    expect(screen.getByLabelText('unit-card')).toBeInTheDocument();
  });

  it("Should display CTA 'Join Waitlist' ", () => {
    const fakeUnit = {...fakeDataUnit, totalVacancy: 0};

    render( 
      <Router> 
        <UnitCard unit={fakeUnit} />,
      </Router>
    );

    expect(
      screen.getByLabelText('unit-card-btn').innerHTML
      ).toBe('Join Waitlist');
  });

  it("Should call function 'handlerZoom' when click on icon 'InfoCircle'", () => {
    const spy = vi.spyOn(obj, 'handlerZoom');

    render( 
      <Router> 
        <UnitCard unit={fakeDataUnit} />,
      </Router>
    );

    const IconCircle = screen.getByLabelText('icon-zoom');
    fireEventAliased.click(IconCircle);

    expect(spy.getMockName()).toEqual('handlerZoom');
  });

  it("Should call function 'handlerZoom' when click on CTA btn", () => {
    const spy = vi.spyOn(obj, 'handlerReserve');

    render( 
      <Router> 
        <UnitCard unit={fakeDataUnit} />,
      </Router>
    );

    const cta = screen.getByLabelText('unit-card-btn');
    fireEventAliased.click(cta);

    expect(spy.getMockName()).toEqual('handlerReserve');
  });
});
