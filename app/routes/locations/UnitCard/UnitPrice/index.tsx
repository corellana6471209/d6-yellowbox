import PropTypes from "prop-types";

const UnitPrice = (props: any) => {
  const { type, price } = props;
  return (
    <div
      className={`unit-card__price ${type} relative`}
      aria-label={`unit-card__price-${type}`}
    >
      <div className="flex items-end">
        <div
          className={`price-value ${
            type == "online" ? "text-callout" : "text-greyHeavy"
          }`}
        >
          <span
            className={`${
              type == "online"
                ? "text-25 font-bold md:text-36 "
                : "text-16 md:text-24"
            }`}
            aria-label={`price-value-${type}`}
          >
            {`$${price}`}
          </span>

          {type == "online" && <sup className="font-extralight">*</sup>}
        </div>
        <small
          className={`price-mo whitespace-pre text-12 text-greyHeavy ${
            type == "online" ? "md:text-14" : "md:text-20"
          }`}
        >
          / Mo
        </small>
      </div>

      <div
        className={`mt-1 text-center capitalize ${
          type == "online"
            ? "text-12 font-semibold "
            : "text-12 font-bold md:text-20 "
        }`}
      >
        {type == "online" && type}
        {type !== "online" && "In-Store"}
      </div>
    </div>
  );
};

UnitPrice.propTypes = {
  price: PropTypes.any,
  type: PropTypes.string,
};

UnitPrice.defaultProps = {
  type: "online",
};

export default UnitPrice;
