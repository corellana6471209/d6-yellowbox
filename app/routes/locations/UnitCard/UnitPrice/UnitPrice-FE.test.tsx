import { describe, expect, it } from "vitest";
import { render, screen } from "../../../../utils/test-utils";
import { BrowserRouter as Router } from "react-router-dom";
import UnitPrice from './index';

describe("Unit Price component", () => {
  const stPrice = 120;
  const type = "retail";

  it("Verify if Unit Price component renders", async () => {
    const { container } = render( 
      <Router> 
        <UnitPrice price={stPrice} />,
      </Router>
    );

    expect(
      await container.getElementsByClassName('unit-card__price').length
    ).toBe(1);
  });

  it("Verify if display Unit Strike through Price", () => {
    render( 
      <Router> 
        <UnitPrice price={stPrice} type={type} />,
      </Router>
    );
    
    expect(screen.getByLabelText(`price-value-${type}`).innerHTML).toBe(`$${stPrice}`);
  });

  it("No should display Unit Strike through Price", async () => {

    const { container } = render( 
      <Router> 
        <UnitPrice price={null} type={type} />,
      </Router>
    );
    
    expect(
      await container.getElementsByClassName(`unit-card__price ${type}`).length
    ).toBe(0);
  });
});