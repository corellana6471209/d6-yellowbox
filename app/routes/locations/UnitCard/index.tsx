import { useState, useEffect } from "react";
import PropTypes from "prop-types";
import { useMediaQuery } from "react-responsive";
import UnitPromoBanner from "./UnitPromoBanner";
import UnitPrice from "./UnitPrice";
import UnitUrgencyMsg from "./UnitUrgencyMessage/index";
import UnitName from "./UnitName";
import { UnitDetailsContext } from "../../../utils/context/UnitContext";
import ReservationModal from "../../common/Modals/Reservation";
import MessageModal from "../../common/MessageModal";
import { analytics } from "../../../utils/analytics";

import { useContext } from "react";

function UnitCard(props: any) {
  const {
    ctaText,
    unit,
    handlerZoom,
    setOpenModal,
    openModal,
    labels,
    locationContent,
  } = props;
  const unitDetails = useContext(UnitDetailsContext);
  const isMobile = useMediaQuery({ query: "(max-width: 991px)" });
  const [openReservationModal, setOpenReservartionModal] = useState(false);
  const [isSuccesfully, setIsSuccesfully] = useState<boolean>(false);

  const classBtn = (type?: string) => {
    switch (type) {
      case "join":
        return "join bg-white blue-button text-secondary5 px-3 mt-0 mb-0";
      default:
        return "bg-primary1 yellow-button text-secondary1 px-3 mt-0 mb-0";
    }
  };

  const getTextCTA = () => {
    let text = ctaText;

    if (unitDetails && unit.totalVacancy <= unitDetails?.qtyUnitUrgencyMsg)
      text = "Reserve Now! – Availability May Change";

    if (unit.planName)
      text = isMobile ? "Save Now with No Obligation" : "Claim Promotion";

    return text;
  };

  const renderBtn = () => {
    const type = !unit.totalVacancy ? "join" : "";

    return (
      <button
        aria-label="unit-card-btn"
        className={`unit-card-btn ${classBtn(type)}`}
        onClick={handlerReserve}
      >
        {!unit.totalVacancy ? "Join Waitlist" : getTextCTA()}
      </button>
    );
  };

  const renderImg = () => {
    const unitTypeImage = unit?.image ? unit.image.unitTypeImage.url : null;
    return (
      <div className="unit-card__img m-0">
        <img
          src={
            unitTypeImage ? unitTypeImage : "https://via.placeholder.com/150"
          }
          alt={unitTypeImage ? unit.image.unitTypeImage.description : ""}
        />
      </div>
    );
  };

  useEffect(() => {
    if (openModal) setOpenReservartionModal(false);
  }, [openModal]);

  const handlerReserve = () => {
    if (unit.totalVacancy) {
      analytics(window, "unitSelected", {
        unitID: unit.firstAvailableId,
        unitTypeID: unit.unitTypeId,
        unitRate: unit.pushRate,
      });
    }

    setOpenReservartionModal(true);
  };

  return (
    <>
      <MessageModal
        open={isSuccesfully}
        message={labels.textSuccessModal}
        title=" "
        ctaText="Close"
      />
      {openReservationModal && <div className="background-modal"></div>}
      <div
        className={`unit-card w-100 ${
          openReservationModal && "unit-card-modal"
        } `}
        aria-label="unit-card"
      >
        <div className="unit-card__wrapper">
          <div className="grid grid-cols-12 gap-2">
            <div className="col-span-4 flex items-center lg:col-span-2">
              {renderImg()}
            </div>

            <div className="item-center col-span-8 lg:col-span-10">
              <div className="grid h-full grid-cols-12 gap-2.5 md:gap-4">
                <div className="relative col-span-12 flex items-center justify-start lg:col-span-4">
                  {isMobile && <UnitPromoBanner unit={unit} />}

                  {isMobile && !unit.planName && (
                    <UnitUrgencyMsg
                      unit={unit}
                      qtyLeft={unitDetails?.qtyUnitUrgencyMsg}
                      unitUrgencyMsg={unitDetails?.unitUrgencyMsg}
                    />
                  )}

                  <UnitName unit={unit} handlerZoom={handlerZoom} />
                </div>

                <div className="col-span-12 flex items-center justify-start lg:justify-center lg:col-span-4 mt-2.5 mb-2.5">
                  <div className={`grid grid-cols-2 items-end`}>
                    <div className="col-span-1 flex justify-end">
                      <UnitPrice price={unit.inStoreRate} type="retail" />
                    </div>

                    <div className="col-span-1">
                      <UnitPrice price={unit.pushRate} type="online" />
                    </div>
                  </div>
                </div>

                {!isMobile && (
                  <div
                    className={`promo-banner flex-wrap items-center justify-end xs:hidden lg:col-span-4 lg:flex`}
                  >
                    <UnitPromoBanner unit={unit} />

                    {renderBtn()}

                    <UnitUrgencyMsg
                      unit={unit}
                      qtyLeft={unitDetails?.qtyUnitUrgencyMsg}
                      unitUrgencyMsg={unitDetails?.unitUrgencyMsg}
                    />
                  </div>
                )}
              </div>
            </div>
          </div>

          {isMobile && renderBtn()}
        </div>

        {openReservationModal && (
          <ReservationModal
            labels={labels}
            totalVacancy={unit.totalVacancy}
            unit={unit}
            locationContent={locationContent}
            setOpenPopUpModal={() => setIsSuccesfully(true)}
            setOpenReservationModal={() => {
              setOpenReservartionModal(!openReservationModal);
              setOpenModal(false);
            }}
          />
        )}
      </div>
    </>
  );
}

UnitCard.propTypes = {
  handlerZoom: PropTypes.func,
  unit: PropTypes.object,
  labels: PropTypes.object,
  locationContent: PropTypes.object,
  ctaText: PropTypes.string,
  setOpenModal: PropTypes.func,
  openModal: PropTypes.bool,
};

UnitCard.defaultProps = {
  unit: null,
  ctaText: "Reserve Now and Lock In This Rate!",
  handlerZoom: () => {
    console.log("--> handlerZoom");
  },
};

export default UnitCard;
