import { ComponentStory, ComponentMeta } from '@storybook/react';
import { fakeDataUnit } from '../../../utils/fakedataToTests';
import UnitCard from './index';

export default {
  title: 'components/core/UnitCard',
  component: UnitCard,
} as ComponentMeta<typeof UnitCard>;

const Template: ComponentStory<typeof UnitCard> = (args) => <UnitCard {...args} />;

export const Primary = Template.bind({});
Primary.args = {
  unit: fakeDataUnit,
  ctaText: 'Save Now with No Obligation',
};


