import { describe, expect, it } from "vitest";
import { render, screen } from "../../../../utils/test-utils";
import { BrowserRouter as Router } from "react-router-dom";
import { fakeDataUnit } from '../../../../utils/fakedataToTests';
import UnitPromoBanner from './index';

describe("Promo Banner component", () => {
  it("Verify if Promo Banner component renders", () => {
    const plan = "50% off for 3 months";
    const fakeUnit = {...fakeDataUnit, planName: plan};

    render( 
      <Router> 
        <UnitPromoBanner unit={fakeUnit} />,
      </Router>
    );

    expect(screen.getByLabelText('offer-promo')).toBeInTheDocument();
    expect(screen.getByLabelText('offer-promo').innerHTML).toBe(plan);
  });

  it("No should display Promo Banner", async () => {
    const plan = null;
    const fakeUnit = {...fakeDataUnit, planName: plan};

    const { container } = render( 
      <Router> 
        <UnitPromoBanner unit={fakeUnit} />,
      </Router>
    );

    expect(
        await container.getElementsByClassName('offer-promo').length
    ).toBe(0);
  });
});