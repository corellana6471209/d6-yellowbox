import { useMediaQuery } from "react-responsive";

const UnitPromoBanner = (props: any) => {
  const { unit } = props;
  const isMobile = useMediaQuery({ query: "(max-width: 991px)" });
  
  return (unit.totalVacancy && unit.planName) ? 
  (
    <div className="unit-card-promo offer-promo text-callout text-12 md:text-24 md:leading-5" 
      aria-label="offer-promo">
      {unit.planName}
    </div>
  ) 
  : 
  <div className={(isMobile) ? '': 'w-100'}></div>;
}

export default UnitPromoBanner;