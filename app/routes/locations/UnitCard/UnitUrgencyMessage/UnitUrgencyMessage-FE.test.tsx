import { describe, expect, it } from "vitest";
import { render, screen } from "../../../../utils/test-utils";
import { BrowserRouter as Router } from "react-router-dom";
import { fakeDataUnit } from '../../../../utils/fakedataToTests';
import UnitUrgencyMsg from "./index";

describe("Unit Urgency Message component", () => {
  it("Verify if Unit Urgency Message component renders", () => {
    const totalVacancy = 1;
    const fakeUnit = {...fakeDataUnit, totalVacancy: totalVacancy};

    render( 
      <Router> 
        <UnitUrgencyMsg unit={fakeUnit} qtyLeft={3} />,
      </Router>
    );

    expect(screen.getByLabelText('left-promo')).toBeInTheDocument();
  });

  it("Not should display Unit Urgency Message component", async () => {
    const totalVacancy = 5;
    const fakeUnit = {...fakeDataUnit, totalVacancy: totalVacancy};

    const { container } = render( 
      <Router> 
        <UnitUrgencyMsg unit={fakeUnit} qtyLeft={3} />,
      </Router>
    );

    expect(
      await container.getElementsByClassName('unit-card-promo left-promo').length
    ).toBe(0);
  });
});