import * as PropTypes from "prop-types";
import { useMediaQuery } from "react-responsive";
import { useState, useEffect } from "react";
import { getLabel } from "../../../../utils/getLabel";
import Clock from "../../../common/svg/Clock";

const UnitUrgencyMsg = (props: any) => {
  const { unit, qtyLeft, unitUrgencyMsg } = props;
  const count = unit.totalVacancy;
  const [labels, setLabels] = useState<any>(null);
  const isMobile = useMediaQuery({ query: "(max-width: 991px)" });
  const isUrgencyMessage = qtyLeft ? count <= Number(qtyLeft) : false;
  const [message, setMessage] = useState(`Hurry! ${count} Left`);

  useEffect(() => {
    if (unitUrgencyMsg && count != null) {
      let msg = unitUrgencyMsg.replace("$$", count);
      setMessage(msg);
    }
    async function fetchData() {
      const messageLabel = await getLabel("unitSoldOutMessage");
      setLabels({
        messageLabel: messageLabel,
      });
    }

    fetchData();
  }, [unitUrgencyMsg]);

  return isUrgencyMessage ? (
    <div
      className="unit-card-promo left-promo text-12 lg:text-16"
      aria-label="left-promo"
    >
      <span className="mr-1">
        <Clock color="#000000" />
      </span>
      <div>
        {count == 0 && (labels ? labels.messageLabel : `Sold Out`)}
        {count != 0 && message}
      </div>
    </div>
  ) : (
    <div className={isMobile ? "" : "w-100"}></div>
  );
};

UnitUrgencyMsg.propTypes = {
  qtyLeft: PropTypes.any,
  unit: PropTypes.object,
  unitUrgencyMsg: PropTypes.string,
};

UnitUrgencyMsg.defaultProps = {
  qtyLeft: 3,
};

export default UnitUrgencyMsg;
