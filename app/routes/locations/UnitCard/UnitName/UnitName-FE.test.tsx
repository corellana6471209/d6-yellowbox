import { describe, expect, it } from "vitest";
import { render, screen } from "../../../../utils/test-utils";
import { BrowserRouter as Router } from "react-router-dom";
import { fakeDataUnit } from '../../../../utils/fakedataToTests';
import UnitName from './index';

describe("Unit Name component", () => {
  it("Verify if Unit Name component renders", () => {
    const category = 1;
    const fakeUnit = {...fakeDataUnit, category: category };

    render( 
      <Router> 
        <UnitName unit={fakeUnit} />,
      </Router>
    );
      
    expect(screen.getByLabelText('unit-card-name')).toBeInTheDocument();
  });

  it("Description must contain the word 'Small'", () => {
    const category = 1;
    const fakeUnit = {...fakeDataUnit, category: category };

    render( 
      <Router> 
        <UnitName unit={fakeUnit} />,
      </Router>
    );
      
    expect(screen.getByLabelText('subtitle')).toHaveTextContent('Small, ');
  });

  it("Description must contain the word 'Medium'", () => {
    const category = 2;
    const fakeUnit = {...fakeDataUnit, category: category };

    render( 
      <Router> 
        <UnitName unit={fakeUnit} />,
      </Router>
    );
      
    expect(screen.getByLabelText('subtitle')).toHaveTextContent('Medium, ');
  });

  it("Description must contain the word 'Large'", () => {
    const category = 3;
    const fakeUnit = {...fakeDataUnit, category: category };

    render( 
      <Router> 
        <UnitName unit={fakeUnit} />,
      </Router>
    );
      
    expect(screen.getByLabelText('subtitle')).toHaveTextContent('Large, ');
  });
});