import PropTypes from "prop-types";
import { sizeCategory } from "../../../../utils/fakedataToTests";
import InfoCircle from "../../../common/svg/InfoCircle";
import { useMediaQuery } from "react-responsive";

const UnitName = (props: any) => {
  const { unit, handlerZoom, hideIcon } = props;
  const sizeCategoryText =
    unit && unit.category != null ? `${sizeCategory[unit.category]}` : "";

  const isLandScape = useMediaQuery({ query: "(min-width: 641px)" });

  const descriptionFull = `${sizeCategoryText}${
    unit.unitTypeName ? `, ${unit.unitTypeName}` : ""
  }`;

  const description = `${sizeCategoryText}${
    unit.unitTypeName
      ? `, ${
          unit.unitTypeName.length > 30
            ? unit.unitTypeName.substring(0, 30) + "..."
            : unit.unitTypeName
        }`
      : ""
  }`;

  // test
  return (
    <div
      className="unit-card__description font-bold text-secondary5 lg:pl-2"
      aria-label="unit-card-name"
    >
      <div className="div-unit-card-area flex">
        <span className="unit-card-area text-24 font-bold md:text-32">
          {unit.width}’ X {unit.length}’
        </span>

        {!hideIcon ? (
          <div onClick={() => handlerZoom(unit)}>
            <InfoCircle size="20px" color="#657AC1" />
          </div>
        ) : (
          ""
        )}
      </div>

      <div
        className="subtitle text-16 font-bold leading-5 md:text-24 md:leading-7"
        aria-label="subtitle"
      >
        {isLandScape ? descriptionFull : description}
      </div>
    </div>
  );
};

UnitName.propTypes = {
  handlerZoom: PropTypes.func,
  unit: PropTypes.object,
  hideIcon: PropTypes.bool,
};

UnitName.defaultProps = {
  hideIcon: false,
  handlerZoom: () => {
    console.log("---> handlerZoom");
  },
};

export default UnitName;
