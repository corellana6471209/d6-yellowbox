import { useState, useEffect } from "react";
import PropTypes from "prop-types";
import UnitCard from "../UnitCard";
import FilterInformationModal from "../../common/Modals/FiltersInformation";
import LoaderList from "~/routes/common/LoaderList";

import Filter from "../../common/svg/Filter";
import CloseIcon from "../../common/svg/Close";
import ImageUnitType from "../ImageUnitType";
import sortUnits from "../../../utils/hooks/useSortUnits";
import getModalFilters from "../../../utils/getFilters";
import { getLabels } from "../../../utils/getLabel";
import { getUnitTypes } from "../backend";
import fetchCustomApi from "~/utils/fetchCustomApi";

const sortUnitByDefault = (units: any) => {
  if (units) {
    //By Default price low to high
    return units.sort(function (a: any, b: any) {
      return a.pushRate - b.pushRate;
    });
  } else {
    return [];
  }
};

const UnitsCardComponent = (props: any) => {
  const { units, locationContent } = props;

  const [openModal, setOpenModal] = useState(false);
  const [openZoom, setOpenZoom] = useState(false);
  const [unitType, setUnitType] = useState(null);
  const [unitsCard, setUnitsCard] = useState([...sortUnitByDefault(units)]);
  const [unitsWithOutFilters, setUnitsWithOutFilters] = useState([
    ...sortUnitByDefault(units),
  ]);
  const [isHydrated, setIsHydrated] = useState(false);

  const [labels, setLabels] = useState<any>({
    reservationAmenityText1: "Lock in this rate",
    reservationAmenityText2: "Holds Unit/Space for 2 days",
    reservationAmenityText3: "No Credit Card needed",
    reservationAmenityText4: "No obligation to rent",
    reservationFormButtonLabel: "Reserve Unit",
    reservationFormTitle: "Reserve Now",
    reservationTextDisclaimer:
      "By selecting “Yes” and pressing “Continue”, I consent to receive autodialed text messages from and on behalf of Simply Self Storage with updates, info, and offers to the phone number I provided above.",
    textSuccessModal:
      "You have been successfully added to the waitlist for this Unit Type.",
    disclaimerLabel: "Other fees may apply upon payment processing.....",
  });

  /************************FILTERS*********************** */

  const [filtersChecked, setFiltersChecked] = useState<Array<any>>([
    { filter: "PRICELOWTOHIGH" },
  ]);
  const [filtersContentful, setFiltersContentful] = useState<any>({
    priceLowToHigh: null,
    priceHighToLow: null,
    largeToSmall: null,
    smallToLarge: null,
    parking: null,
    small: null,
    medium: null,
    large: null,
  });

  useEffect(() => {
    async function fetchData() {
      setIsHydrated(true);
      //Setting filters
      const filters = await getModalFilters("information");
      setFiltersContentful({
        priceLowToHigh: filters.find((e: any) => e.name === "priceLowToHigh"),
        priceHighToLow: filters.find((e: any) => e.name === "priceHighToLow"),
        largeToSmall: filters.find((e: any) => e.name === "largeToSmall"),
        smallToLarge: filters.find((e: any) => e.name === "smallToLarge"),
        parking: filters.find((e: any) => e.name === "parking"),
        small: filters.find((e: any) => e.name === "small"),
        medium: filters.find((e: any) => e.name === "medium"),
        large: filters.find((e: any) => e.name === "large"),
      });

      const labels = await getLabels([
        "reservationAmenityText1",
        "reservationAmenityText2",
        "reservationAmenityText3",
        "reservationAmenityText4",
        "reservationFormButtonLabel",
        "reservationFormTitle",
        "reservationTextDisclaimer",
        "textWaitListTitle",
        "textWaitListInformation",
        "waitListFormButtonLabel",
        "textSuccessModal",
        "disclaimerLabel",
      ]);

      const labelsMap = new Map(
        labels.map((object) => {
          return [object.name, object.value];
        })
      );

      function getMapValue(value) {
        return [...labelsMap].find(([key, val]) => key == value)[1];
      }

      const reservationAmenityText1 = getMapValue("reservationAmenityText1");
      const reservationAmenityText2 = getMapValue("reservationAmenityText2");
      const reservationAmenityText3 = getMapValue("reservationAmenityText3");
      const reservationAmenityText4 = getMapValue("reservationAmenityText4");
      const reservationFormButtonLabel = getMapValue(
        "reservationFormButtonLabel"
      );
      const reservationFormTitle = getMapValue("reservationFormTitle");
      const reservationTextDisclaimer = getMapValue(
        "reservationTextDisclaimer"
      );
      const textWaitListTitle = getMapValue("textWaitListTitle");
      const textWaitListInformation = getMapValue("textWaitListInformation");
      const waitListFormButtonLabel = getMapValue("waitListFormButtonLabel");
      const textSuccessModal = getMapValue("textSuccessModal");
      const disclaimerLabel = getMapValue("disclaimerLabel");

      setLabels({
        reservationAmenityText1: reservationAmenityText1
          ? reservationAmenityText1
          : "Lock in this rate",
        reservationAmenityText2: reservationAmenityText2
          ? reservationAmenityText2
          : "Holds Unit/Space for 2 days",
        reservationAmenityText3: reservationAmenityText3
          ? reservationAmenityText3
          : "No Credit Card needed",
        reservationAmenityText4: reservationAmenityText4
          ? reservationAmenityText4
          : "No obligation to rent",
        reservationFormButtonLabel: reservationFormButtonLabel
          ? reservationFormButtonLabel
          : "Reserve Unit",
        reservationFormTitle: reservationFormTitle
          ? reservationFormTitle
          : "Reserve Now",
        reservationTextDisclaimer: reservationTextDisclaimer
          ? reservationTextDisclaimer
          : "By selecting “Yes” and pressing “Continue”, I consent to receive autodialed text messages from and on behalf of Simply Self Storage with updates, info, and offers to the phone number I provided above.",
        textWaitListTitle: textWaitListTitle
          ? textWaitListTitle
          : "Be The First to Know",
        textWaitListInformation: textWaitListInformation
          ? textWaitListInformation
          : "Fill out this form below and be notified should this unit become available.",
        waitListFormButtonLabel: waitListFormButtonLabel
          ? waitListFormButtonLabel
          : "Sign Up for Waitlist",
        textSuccessModal: textSuccessModal
          ? textSuccessModal
          : "You have been successfully added to the waitlist for this Unit Type.",
        disclaimerLabel: disclaimerLabel
          ? disclaimerLabel
          : "Other fees may apply upon payment processing.....",
      });

      if (!units) {
        // const dataUnits = await fetchUnitInformationByLocationServerSide(locationContent.locationId);
        const url = "/api/Inventory/unit/" + locationContent.locationId;
        const dataUnits = await fetchCustomApi(url);
        let unitTypes = await getUnitTypes(
          dataUnits?.data,
          locationContent.locationId
        );

        setUnitsCard([...sortUnitByDefault(unitTypes)]);
        setUnitsWithOutFilters([...sortUnitByDefault(unitTypes)]);
        //        locationContent.units = unitTypes;
      }

      setIsHydrated(false);
    }

    fetchData();
  }, []);

  const assignFilters = (filters: Array<any>) => {
    setFiltersChecked(filters);

    if (filters && filters.length > 0 && unitsWithOutFilters) {
      const data = sortUnits(filters, unitsWithOutFilters);
      setUnitsCard(data);
    } else {
      setUnitsCard(unitsWithOutFilters);
    }
  };

  /************************FILTERS*********************** */

  const handlerZoom = (unit: any) => {
    // if (unit && unit.unitType && Object.keys(unit.unitType).length) {
    if (unit) {
      setOpenZoom(true);
      setUnitType(unit);
    } else {
      console.log("----> No found unit Type");
    }
  };

  if (isHydrated) {
    return  <div className="div-body-unit-card mx-auto">
              <div className="div-filter-units-open-modal h-[50px]">
                <div></div>
                <div className="div-filter-text-result"></div>
                <button className="button-filter-icon"></button>
              </div>
              <LoaderList type="Unit" num={4} />
            </div>;
  } else {
    return (
      <>
        <div className="div-body-unit-card mx-auto">
          {openModal && <div className="background-modal"></div>}
          {/** || openReservationModal */}
          <div className="div-filter-units">
            <div className="div-filter-units-open-modal h-[50px]">
              <div></div>
              <div className="div-filter-text-result">{`${unitsCard.length} Results`}</div>
              <button
                className="button-filter-icon"
                onClick={() => {
                  setOpenModal(!openModal);
                }}
              >
                {openModal ? <CloseIcon /> : <Filter size={50} />}
              </button>
            </div>

            {openModal && (
              <FilterInformationModal
                filtersChecked={filtersChecked}
                filtersContentful={filtersContentful}
                openModal={openModal}
                setOpenModal={() => setOpenModal(false)}
                setFiltersChecked={(filters: any) => assignFilters(filters)}
              />
            )}
          </div>
          {unitsCard && unitsCard.length > 0 ? (
            <>
              {unitsCard.map((unit) => (
                <div key={unit.unitTypeId} className="div-unit-card">
                  <UnitCard
                    labels={labels}
                    unit={unit}
                    locationContent={locationContent}
                    openModal={openModal}
                    handlerZoom={handlerZoom}
                    setOpenModal={() => setOpenModal(false)}
                  />
                </div>
              ))}
            </>
          ) : (
            "No units currently available"
          )}

          <p className="mt-8 mb-8 text-center text-10 text-greyHeavy md:text-12">
            <span className="relative text-20 text-callout">*</span>
            {labels.disclaimerLabel}
          </p>
        </div>

        <ImageUnitType
          open={openZoom}
          unitType={unitType}
          setIsOpen={setOpenZoom}
        />
      </>
    );
  }
};

UnitsCardComponent.propTypes = {
  units: PropTypes.array,
  locationContent: PropTypes.object,
};

UnitsCardComponent.defaultProps = {
  units: [],
  locationContent: {},
};

export default UnitsCardComponent;
