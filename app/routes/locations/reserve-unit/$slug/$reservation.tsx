import { useState, useEffect } from "react";
import Stepper from "./components/stepper";
import type { LoaderFunction } from "@remix-run/node";
import Header from "../../../common/Header";
//import Footer from "../../../common/Footer";
import Spinner from "../../../common/Spinner";

import { useLoaderData } from "@remix-run/react";
import Congratulations from "./components/congratulations";
import ReservationDetails from "./components/reservationDetails";
import { getUnitImageInformation } from "../../backend";

import {
  loadLocationContentByLocationIdCompleted,
  getReservationList,
  //getUnitImageInformation,
} from "../../backend";
import { getLabel } from "../../../../utils/getLabel";
import { getCacheValueByKey } from "~/routes/api/shared/elasticCache";
import { setCookie, deleteCookie } from "../../../../utils/cookie";
import contentfulKeys from "../../../../utils/config";

/**
 * List Item for locations listing
 * @param {object} props content to be displayed
 * @returns {string} returns component
 */

export const loader: LoaderFunction = async ({ params, request }) => {
  const { slug, reservation } = params;

  const locationContent = await loadLocationContentByLocationIdCompleted(slug);
  const token = await getCacheValueByKey("accessToken", request);
  const user = await getCacheValueByKey("tenantInfo", request);
  return { locationContent, reservation, token, user };
};

function ReservationUnit() {
  const inf = useLoaderData();
  const [infReservation, setInfReservation] = useState<any>(null);
  const [isHydrated, setIsHydrated] = useState(false);
  const [unitInformation, setUnitInformation] = useState(null);

  const [labels, setLabels] = useState<any>({
    confirmationStepperOneStep: "Find a Unit",
    confirmationStepperTwoStep: "Reserve & hold your unit",
    confirmationStepperThreeStep: "Complete rental and pay now",
    confirmationCongratulationsMessage:
      "Congratulations, you've reserved a unit!",
    confirmationButtonLabel: "Complete Rental and Pay Now",
    confirmationDetailsTitle: "Your Reservation Details",
    disclaimerLabel: "Other fees may apply upon payment process",
    confirmationConditionsTitle: "Terms and Conditions",
    confirmationConditionsText:
      "The size reserved may not be available at time of move-in.",
    confirmationConditionsLinkText: "Read Full Terms and Conditions",
  });

  useEffect(() => {
    async function fetchData() {
      const infReservation = await getReservationList([
        inf.locationContent.locationId,
        inf.reservation,
        "v3",
      ]);

      setInfReservation(infReservation);

      const confirmationStepperOneStep = await getLabel(
        "confirmationStepperOneStep"
      );
      const confirmationStepperTwoStep = await getLabel(
        "confirmationStepperTwoStep"
      );
      const confirmationStepperThreeStep = await getLabel(
        "confirmationStepperThreeStep"
      );
      const confirmationCongratulationsMessage = await getLabel(
        "confirmationCongratulationsMessage"
      );
      const confirmationButtonLabel = await getLabel("confirmationButtonLabel");
      const confirmationDetailsTitle = await getLabel(
        "confirmationDetailsTitle"
      );
      const disclaimerLabel = await getLabel("disclaimerLabel");

      const confirmationConditionsTitle = await getLabel(
        "confirmationConditionsTitle"
      );
      const confirmationConditionsText = await getLabel(
        "confirmationConditionsText"
      );
      const confirmationConditionsLinkText = await getLabel(
        "confirmationConditionsLinkText"
      );

      const unitData = await getUnitImageInformation(
        infReservation,
        inf.locationContent.locationId
      );

      setUnitInformation(unitData);

      setLabels({
        confirmationStepperOneStep: confirmationStepperOneStep
          ? confirmationStepperOneStep
          : "Find a Unit",
        confirmationStepperTwoStep: confirmationStepperTwoStep
          ? confirmationStepperTwoStep
          : "Reserve & hold your unit",
        confirmationStepperThreeStep: confirmationStepperThreeStep
          ? confirmationStepperThreeStep
          : "Complete rental and pay now",
        confirmationCongratulationsMessage: confirmationCongratulationsMessage
          ? confirmationCongratulationsMessage
          : "Congratulations, you've reserved a unit!",
        confirmationButtonLabel: confirmationButtonLabel
          ? confirmationButtonLabel
          : "Complete Rental and Pay Now",
        confirmationDetailsTitle: confirmationDetailsTitle
          ? confirmationDetailsTitle
          : "Your Reservation Details",
        disclaimerLabel: disclaimerLabel
          ? disclaimerLabel
          : "Other fees may apply upon payment process",
        confirmationConditionsTitle: confirmationConditionsTitle
          ? confirmationConditionsTitle
          : "Terms and Conditions",
        confirmationConditionsText: confirmationConditionsText
          ? confirmationConditionsText
          : "The size reserved may not be available at time of move-in.",
        confirmationConditionsLinkText: confirmationConditionsLinkText
          ? confirmationConditionsLinkText
          : "Read Full Terms and Conditions",
      });
      setIsHydrated(true);
    }

    async function fetchCookie() {
      deleteCookie(contentfulKeys.constants.lastPage);
      const days = await getLabel("cookieTimeDays");
      setCookie(
        contentfulKeys.constants.lastPage,
        window.location.href,
        days ? parseInt(days) : 30
      );
    }
    fetchCookie();
    fetchData();
  }, []);

  if (isHydrated) {
    return (
      <div className="div-reservation-body">
        <Header />
        <Stepper labels={labels} />
        <Congratulations
          labels={labels}
          locationContent={inf.locationContent}
          infReservation={infReservation}
        />
        <ReservationDetails
          locationContent={inf.locationContent}
          infReservation={infReservation}
          unitInformation={unitInformation}
          labels={labels}
        />
        {/* <Footer /> */}
      </div>
    );
  } else {
    return <Spinner/>;
  }
}

export default ReservationUnit;
//<div className="div-reservations-unit">WORK IN PROGRESS</div>
