import UnitPrice from "../../../UnitCard/UnitPrice";

import UnitName from "../../../UnitCard/UnitName";
import { useMediaQuery } from "react-responsive";

function UnitCard(props: any) {
  const { unitInformation } = props;

  const isMobile = useMediaQuery({ query: "(max-width: 991px)" });
  const renderImg = () => {
    const unitTypeImage =
      unitInformation && unitInformation.image
        ? unitInformation.image.unitTypeImage
        : null;

    return (
      <div className="unit-card__img m-0">
        <img
          src={
            unitTypeImage
              ? unitTypeImage.url
              : "https://via.placeholder.com/150"
          }
          alt={unitTypeImage ? unitTypeImage.description : ""}
          height={isMobile ? 100 : 150}
        />
      </div>
    );
  };

  return (
    <div className="unit-card w-100 lg:pl-20 lg:pr-20" aria-label="unit-card">
      <div className="unit-card__wrapper p-0 md:p-6">
        <div className="grid grid-cols-12 gap-2">
          <div className="col-span-4 flex items-center lg:col-span-2">
            {renderImg()}
          </div>

          <div className="item-center col-span-8 lg:col-span-10">
            <div className="grid h-full grid-cols-12 gap-2.5 md:gap-4">
              <div className="relative col-span-12 flex items-center justify-start lg:col-span-8">
                <UnitName unit={unitInformation} hideIcon={true} />
              </div>

              <div className="col-span-12 mt-2.5 mb-2.5 flex items-center justify-end lg:col-span-4">
                <div className={`grid grid-cols-2 items-end`}>
                  <div className="col-span-1 flex justify-end">
                    <UnitPrice
                      price={
                        unitInformation?.inStoreRate
                          ? unitInformation?.inStoreRate
                          : "0"
                      }
                      type="retail"
                    />
                  </div>

                  <div className="col-span-1">
                    <UnitPrice
                      price={
                        unitInformation?.pushRate
                          ? unitInformation?.pushRate
                          : "0"
                      }
                      type="online"
                    />
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}

export default UnitCard;
