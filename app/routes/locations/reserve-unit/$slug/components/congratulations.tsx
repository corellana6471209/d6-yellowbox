import Button from "../../../../common/Button";
import CheckCircle from "../../../../common/svg/CheckCircle";

function Congratulations(props: any) {
  /**********************Information from props******* */
  const { labels, locationContent, infReservation } = props;
  const { locationId } = locationContent;
  const { waitingId } = infReservation;

  /**********************Information from props******* */

  return (
    <div className="div-body-congratulations">
      <div className="div-icon d-flex mt-9 justify-center">
        <CheckCircle />
      </div>
      <p className="div-paragraph mt-2 text-center font-serif text-secondary2">
        {labels.confirmationCongratulationsMessage}
      </p>

      <div className="div-button-congratulations mt-10">
        <Button
          href={`/locations/make-payment/${locationId}/${waitingId}`} //Delete this
          target="_self"
          title={labels.confirmationButtonLabel}
          buttonStyle="button-primary-expanded"
        />
      </div>
    </div>
  );
}

export default Congratulations;
//<div className="div-reservations-unit">WORK IN PROGRESS</div>
