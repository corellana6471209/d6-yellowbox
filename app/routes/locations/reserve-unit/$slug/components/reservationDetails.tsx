import UnitCard from "./unitCard";
import Button from "../../../../common/Button";
import { useMediaQuery } from "react-responsive";
import calculateDistance from "../../../../../utils/hooks/useDistance";
import { getAddressCoords } from "../../../../../utils/cookie";
import formatPhoneNumber from "../../../../../utils/formatNumber";

import moment from "moment";

function ReservationDetails(props: any) {
  const coords = getAddressCoords();
  const { locationContent, infReservation, labels, unitInformation } = props;
  const isMobile = useMediaQuery({ query: "(max-width: 1024px)" });

  /**********************Information from props******* */
  const {
    address,
    phoneNumberNewCustomer,
    mondayOfficeHours,
    tuesdayOfficeHours,
    wednesdayOfficeHours,
    thursdayOfficeHours,
    fridayOfficeHours,
    saturdayOfficeHours,
    sundayOfficeHours,
    lunchHours,
    locationId,
  } = locationContent;

  const {
    firstName,
    lastName,
    emailAddress,
    primaryPhoneNumber,
    unitId1,
    waitingId,
  } = infReservation;
  /**********************Information from props******* */

  const date = moment(infReservation.expiresDateTime).format("MMMM DD, YYYY");

  const distance = calculateDistance(
    null,
    null,
    address.coordinates.lat,
    address.coordinates.lon
  );
  const getDirection = () => {
    if (coords) {
      const dataCoords = JSON.parse(coords);
      window.open(
        `https://www.google.com/maps/dir/${dataCoords.lat},${dataCoords.lng}/${address.coordinates.lat},${address.coordinates.lon}/@${dataCoords.lat},${dataCoords.lng},13z`,
        "_blank"
      );
    } else {
      window.open(
        `https://www.google.com/maps/dir//${address.coordinates.lat},${address.coordinates.lon}/@${address.coordinates.lat},${address.coordinates.lon},14z/`,
        "_blank"
      );
    }
  };

  const mobileSection = () => {
    return (
      <section className="grid grid-cols-1 lg:gap-10">
        <div>
          <p className="text-14 font-bold xs:text-16">Address</p>
          <p className="text-14 xs:text-16">{address.address1}</p>
          <p className="text-14 xs:text-16">
            {address.city}, {address?.state?.stateAbbreviation}{" "}
            {address.postalCode}
          </p>
        </div>
        {distance && (
          <div className="mt-4">
            <p className="text-14 font-bold xs:text-16">
              {" "}
              {distance ? `${Number(distance).toFixed(2)} mi` : ""}
            </p>
          </div>
        )}

        <div className="div-body-direction">
          <p
            className="text-direction link mt-4 cursor-pointer text-14 font-bold text-callout xs:text-16"
            onClick={() => getDirection()}
          >
            Get Directions
          </p>
        </div>

        <div>
          <p className="text-14 font-bold xs:text-16">Phone Number</p>
          <p className="text-14 xs:text-16">{phoneNumberNewCustomer}</p>
        </div>

        <div className="mt-4">
          <p className="text-14 font-bold xs:text-16">Office Hours</p>

          {mondayOfficeHours && (
            <p className="flex text-14 xs:text-16">
              <span className="span-text-day">Mon.</span>
              {mondayOfficeHours}
            </p>
          )}
          {tuesdayOfficeHours && (
            <p className="flex text-14 xs:text-16">
              <span className="span-text-day">Tue.</span>
              {tuesdayOfficeHours}
            </p>
          )}
          {wednesdayOfficeHours && (
            <p className="flex text-14 xs:text-16">
              <span className="span-text-day">Wed.</span>
              {wednesdayOfficeHours}
            </p>
          )}

          {thursdayOfficeHours && (
            <p className="flex text-14 xs:text-16">
              <span className="span-text-day">Thu.</span>
              {thursdayOfficeHours}
            </p>
          )}

          {fridayOfficeHours && (
            <p className="flex text-14 xs:text-16">
              <span className="span-text-day">Fri.</span>
              {fridayOfficeHours}
            </p>
          )}

          {saturdayOfficeHours && (
            <p className="flex text-14 xs:text-16">
              <span className="span-text-day">Sat.</span>
              {saturdayOfficeHours}
            </p>
          )}

          {sundayOfficeHours && (
            <p className="flex text-14 xs:text-16">
              <span className="span-text-day">Sun.</span>
              {sundayOfficeHours}
            </p>
          )}
        </div>

        {lunchHours ? (
          <div className="mt-4">
            <p className="text-14 font-bold xs:text-16">Lunch Hours (Closed)</p>
            <p className="flex text-14 xs:text-16">{lunchHours}</p>
          </div>
        ) : (
          ""
        )}

        <div className="mt-4">
          <p className="text-14 font-bold xs:text-16">First Name</p>
          <p className="text-14 xs:text-16">{firstName}</p>
        </div>

        <div className="mt-4">
          <p className="text-14 font-bold xs:text-16">Last Name</p>
          <p className="text-14 xs:text-16">{lastName}</p>
        </div>

        <div className="mt-4">
          <p className="text-14 font-bold xs:text-16">Email Address</p>
          <p className="text-14 xs:text-16">{emailAddress}</p>
        </div>

        <div className="mt-4">
          <p className="text-14 font-bold xs:text-16">Phone Number</p>
          <p className="text-14 xs:text-16">
            {formatPhoneNumber(primaryPhoneNumber)}
          </p>
        </div>
      </section>
    );
  };

  const desktopSection = () => {
    return (
      <section className="grid grid-cols-1 gap-4 md:gap-10 lg:grid-cols-2">
        <div>
          <div>
            <p className="text-14 font-bold xs:text-16 lg:text-24">Address</p>
            <p className="text-14 xs:text-16 lg:text-24">{address.address1}</p>
            <p className="text-14 xs:text-16 lg:text-24">
              {address.city}, {address?.state?.stateAbbreviation}{" "}
              {address.postalCode}
            </p>
          </div>

          {distance && (
            <div className="mt-4">
              <p className="text-14 font-bold xs:text-16 lg:text-24">
                {distance ? `${Number(distance).toFixed(2)} mi` : ""}
              </p>
            </div>
          )}

          <div className="div-body-direction">
            <p
              className="text-direction link mt-4 cursor-pointer text-14 font-bold text-callout xs:text-16 lg:text-24"
              onClick={() => getDirection()}
            >
              Get Directions
            </p>
          </div>

          <div>
            <p className="text-14 font-bold xs:text-16 lg:text-24">
              First Name
            </p>
            <p className="text-14 xs:text-16 lg:text-24">{firstName}</p>
          </div>

          <div className="mt-4">
            <p className="text-14 font-bold xs:text-16 lg:text-24">Last Name</p>
            <p className="text-14 xs:text-16 lg:text-24">{lastName}</p>
          </div>

          <div className="mt-4">
            <p className="text-14 font-bold xs:text-16 lg:text-24">
              Email Address
            </p>
            <p className="text-14 xs:text-16 lg:text-24">{emailAddress}</p>
          </div>

          <div className="mt-4">
            <p className="text-14 font-bold xs:text-16 lg:text-24">
              Phone Number
            </p>
            <p className="text-14 xs:text-16 lg:text-24">
              {formatPhoneNumber(primaryPhoneNumber)}
            </p>
          </div>
        </div>

        <div>
          <div>
            <p className="text-14 font-bold xs:text-16 lg:text-24">
              Phone Number
            </p>
            <p className="text-14 xs:text-16 lg:text-24">
              {phoneNumberNewCustomer}
            </p>
          </div>

          <div className="mt-4">
            <p className="text-14 font-bold xs:text-16 lg:text-24">
              Office Hours
            </p>

            {mondayOfficeHours && (
              <p className="flex text-14 xs:text-16 lg:text-24">
                <span className="span-text-day">Mon.</span>
                {mondayOfficeHours}
              </p>
            )}
            {tuesdayOfficeHours && (
              <p className="flex text-14 xs:text-16 lg:text-24">
                <span className="span-text-day">Tue.</span>
                {tuesdayOfficeHours}
              </p>
            )}
            {wednesdayOfficeHours && (
              <p className="flex text-14 xs:text-16 lg:text-24">
                <span className="span-text-day">Wed.</span>
                {wednesdayOfficeHours}
              </p>
            )}

            {thursdayOfficeHours && (
              <p className="flex text-14 xs:text-16 lg:text-24">
                <span className="span-text-day">Thu.</span>
                {thursdayOfficeHours}
              </p>
            )}

            {fridayOfficeHours && (
              <p className="flex text-14 xs:text-16 lg:text-24">
                <span className="span-text-day">Fri.</span>
                {fridayOfficeHours}
              </p>
            )}

            {saturdayOfficeHours && (
              <p className="flex text-14 xs:text-16 lg:text-24">
                <span className="span-text-day">Sat.</span>
                {saturdayOfficeHours}
              </p>
            )}

            {sundayOfficeHours && (
              <p className="flex text-14 xs:text-16 lg:text-24">
                <span className="span-text-day">Sun.</span>
                {sundayOfficeHours}
              </p>
            )}
          </div>

          {lunchHours ? (
            <div className="mt-4">
              <p className="text-14 font-bold xs:text-16 lg:text-24">
                Lunch Hours (Closed)
              </p>
              <p className="flex text-14 xs:text-16 lg:text-24">{lunchHours}</p>
            </div>
          ) : (
            ""
          )}
        </div>
      </section>
    );
  };

  return (
    <div className="div-body-reservation-details">
      <p className="mt-8 mb-6 pr-6 pl-6 text-center font-serif text-28 text-greyHeavy sm:pr-0 sm:pl-0 lg:text-36">
        {labels.confirmationDetailsTitle}
      </p>

      <div className="div-body-reservation d-flex justify-center">
        <section className="div-body-reservation-details-information">
          {unitInformation ? (
            <UnitCard unitInformation={unitInformation} />
          ) : (
            ""
          )}

          <p className="div-fee-text mb-8 text-center text-10 text-greyHeavy md:text-12 lg:mt-8 lg:mb-14">
            <span className="relative text-20 text-callout">*</span>
            {labels.disclaimerLabel}
          </p>

          <section className="div-image-section">
            <div className="div-image-section-body">
              <div className="div-text mb-4">
                <div className="grid grid-cols-2 gap-4 md:gap-10">
                  <div>
                    <p className="text-14 font-bold xs:text-16 lg:text-24">
                      Unit
                    </p>
                    <p className="text-14 xs:text-16 lg:text-24">
                      {unitInformation.unitName}
                    </p>
                  </div>

                  <div>
                    <p className="text-14 font-bold xs:text-16 lg:text-24">
                      Reservation Held Until
                    </p>
                    <p className="text-14 xs:text-16 lg:text-24">{date}</p>
                  </div>
                </div>
              </div>
              <div className="div-image">
                <img
                  src={
                    locationContent.locationImageCollection.items[0].url || ""
                  }
                  alt={
                    locationContent.locationImageCollection.items[0].description
                  }
                  title={locationContent.locationImageCollection.items[0].title}
                  className="img-location-information object-cover"
                />
              </div>
              {/*<TermsModal open={true} /> */}
              <div className="div-text-location mt-3">
                {isMobile ? mobileSection() : desktopSection()}
              </div>
            </div>
          </section>
        </section>
      </div>

      <section className="mt-14">
        <div className="div-body-terms">
          {/**
          *  <p className="text-center text-28 text-greyHeavy lg:text-36">
            {labels.confirmationConditionsTitle}
          </p>
          * 
          */}

          <div className="div-button-congratulations mt-8 mb-16">
            <Button
              href={`/locations/make-payment/${locationId}/${waitingId}`}
              target="_self"
              title={labels.confirmationButtonLabel}
              buttonStyle="button-primary-expanded"
            />
          </div>
        </div>
      </section>
    </div>
  );
}

export default ReservationDetails;
