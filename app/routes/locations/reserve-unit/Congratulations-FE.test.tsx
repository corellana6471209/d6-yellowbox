import React from "react";
import { describe, expect, it } from "vitest";
import Congratulations from "./$slug/components/congratulations";
import { render, screen, userEvent } from "../../../utils/test-utils";
import { BrowserRouter as Router } from "react-router-dom";
import {
  labels,
  locationContent,
  infReservation,
} from "../../../utils/fakedataToTests";

describe("Search component", () => {
  beforeEach(() => {
    const setState = vi.fn();
    const useStateSpy = vi.spyOn(React, "useState");
    useStateSpy.mockImplementation((initialValue) => [initialValue, setState]);
  });
  afterEach(() => {
    vi.clearAllMocks();
  });

  it("Verify if the component renders", () => {
    render(
      <Router>
        <Congratulations
          labels={labels}
          locationContent={locationContent}
          infReservation={infReservation}
        />
      </Router>
    );
  });

  it("Verify if exists text on body", async () => {
    render(
      <Router>
        <Congratulations
          labels={labels}
          locationContent={locationContent}
          infReservation={infReservation}
        />
      </Router>
    );

    expect(
      await screen.findByText("Congratulations, you've reserved a unit!")
    ).toBeInTheDocument();
  });
});
