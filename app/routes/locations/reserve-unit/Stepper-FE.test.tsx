import React from "react";
import { describe, expect, it } from "vitest";
import Stepper from "./$slug/components/stepper";
import { render, screen } from "../../../utils/test-utils";
import { BrowserRouter as Router } from "react-router-dom";
import { labels } from "../../../utils/fakedataToTests";

describe("Search component", () => {
  beforeEach(() => {
    const setState = vi.fn();
    const useStateSpy = vi.spyOn(React, "useState");
    useStateSpy.mockImplementation((initialValue) => [initialValue, setState]);
  });
  afterEach(() => {
    vi.clearAllMocks();
  });

  it("Verify if the component renders", () => {
    render(
      <Router>
        <Stepper labels={labels} />
      </Router>
    );
  });

  it("Verify if exists text on body", async () => {
    render(
      <Router>
        <Stepper labels={labels} />
      </Router>
    );

    expect(await screen.findByText("Find a Unit")).toBeInTheDocument();

    expect(
      await screen.findByText("Reserve & hold your unit")
    ).toBeInTheDocument();

    expect(
      await screen.findByText("Complete rental and pay now")
    ).toBeInTheDocument();
  });
});
