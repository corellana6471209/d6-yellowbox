import { ComponentStory, ComponentMeta } from '@storybook/react';
import SizeGuide from './index';

export default {
  title: 'components/core/SizeGuide',
  component: SizeGuide,
} as ComponentMeta<typeof SizeGuide>;

const Template: ComponentStory<typeof SizeGuide> = (args) => <SizeGuide {...args} />;

export const Primary = Template.bind({});

Primary.args = {
  title: 'Need Help Selecting a Size?',
  description: 'Our size guide will help you take the mystery out of selecting the perfect size unit.',
  ctaText: 'Visit Interactive Size Guide'
};


