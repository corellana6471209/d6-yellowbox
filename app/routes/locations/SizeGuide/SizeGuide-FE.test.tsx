import { describe, expect, it, vi } from "vitest";
import { fireEventAliased, render, screen } from "../../../utils/test-utils";
import { BrowserRouter as Router } from "react-router-dom";
import SizeGuide from "./index";

const obj = {
  handlerClick: () => "handlerClick",
};

describe("Size Guide component unit test", () => {
  it("Verify if SizeGuide component renders", async () => {
    const { container } = render(
      <Router>
        <SizeGuide />,
      </Router>
    );
    expect(await container.getElementsByClassName("size-guide").length).toBe(1);
  });

  it("It should display title 'Need Help' ", async () => {
    const newTitle = "Need Help";

    render(
      <Router>
        <SizeGuide title={newTitle} />
      </Router>
    );

    const titleContent = screen.getByLabelText("sg-title");
    expect(titleContent.innerHTML).toBe(newTitle);
  });

  it("It should display Description 'Lorem ipsum...' ", async () => {
    const newDescription =
      "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod…";

    render(
      <Router>
        <SizeGuide description={newDescription} />
      </Router>
    );

    const descariptionContent = screen.getByLabelText("sg-description");
    expect(descariptionContent.innerHTML).toBe(newDescription);
  });

  it("Should call function when you click on Btn", async () => {
    const spy = vi.spyOn(obj, "handlerClick");

    render(
      <Router>
        <SizeGuide />
      </Router>
    );

    const btn = screen.getByLabelText("sg-cta");
    fireEventAliased.click(btn);
    expect(spy.getMockName()).toEqual("handlerClick");
  });
});
