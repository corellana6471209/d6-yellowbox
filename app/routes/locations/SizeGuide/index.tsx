import PropTypes from "prop-types";

interface SizeGude {
  title: string;
  description: string;
  ctaText: string;
}

function SizeGuide(props: SizeGude) {
  const { title, description, ctaText } = props;

  function handlerClick(event: any) {
    event.preventDefault();
    window.location.href = "/size-guide";
  }

  return (
    <div className="size-guide bg-lightGold text-center text-secondary1">
      <h3
        className="title-size-guide font-serif capitalize"
        aria-label="sg-title"
      >
        {title}
      </h3>

      <p
        className="sg-description text-16 capitalize md:text-24"
        aria-label="sg-description"
      >
        {description}
      </p>

        <button
          onClick={(e) => handlerClick(e)}
          aria-label="sg-cta"
          className="size-guide-btn border-2 text-20 capitalize blue-button-dark"
        >
          {ctaText}
        </button>
    </div>
  );
}

SizeGuide.propTypes = {
  title: PropTypes.string,
  description: PropTypes.string,
  ctaText: PropTypes.string,
};

// Specifies the default values for props:
SizeGuide.defaultProps = {
  title: "Need Help Selecting a Size?",
  description:
    "Our size guide will help you take the mystery out of selecting the perfect size unit.",
  ctaText: "Visit Interactive Size Guide",
};

export default SizeGuide;
