import { describe, expect, it, vi } from "vitest";
import { loadLocationContent } from "./backend";

const obj = {
  loadLocationContent: () => "loadLocationContent",
};

vi.mock("./backend", () => ({
  loadLocationContent: vi.fn().mockImplementation(() => "loadLocationContent"),
}));

describe("fetching location data component unit test", () => {
  it("LoadModuleTitle should be mock", () => {
    expect(vi.isMockFunction(loadLocationContent)).toBe(true);
  });

  it("Should be executed loadModuleTitle", async () => {
    const spy = vi.spyOn(obj, "loadLocationContent");

    const resp = await loadLocationContent("idTest");
    expect(spy.getMockName()).toEqual("loadLocationContent");
    expect(resp).toBe("loadLocationContent");
  });
});
