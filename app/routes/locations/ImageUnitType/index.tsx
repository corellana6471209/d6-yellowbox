import { Fragment, useEffect, useState } from "react";
import { Dialog, Transition } from "@headlessui/react";
import * as PropTypes from "prop-types";
import Icon from "../../common/svg/Icon";
import { documentToReactComponents } from "@contentful/rich-text-react-renderer";
import { getLabel } from "../../../utils/getLabel";

function ImageUnitType(props: any) {
  const { open, callback, unitType, setIsOpen } = props;
  const [hyperlinkText, sethyperlinkText] = useState("");

  useEffect(() => {
    async function fetchData() {
      // You can await here
      const text = await getLabel("hyperlink text image unit type");
      sethyperlinkText(text);
      return text;
    }

    fetchData();
    setIsOpen(open);
  }, [open]);

  const renderImg = () => {
    // return unitType && (unitType.unitTypeImage || unitType.zoomedImage) &&
    return (
      unitType?.image?.zoomedImage?.url && (
        <div className="unit-card__img" aria-label="unit-card__img">
          <img
            className="m-auto mb-4"
            src={unitType.image.zoomedImage.url}
            alt={unitType.image.zoomedImage.description}
            height={250}
          />
        </div>
      )
    );
  };

  const renderDescription = () => {
    return unitType?.image?.description ? (
      <div
        className="text-sm text-gray-500 pb-6"
        aria-label="message-modal-text"
      >
        {unitType?.image?.description?.json?.content.map(
          (item: any, index: number) => (
            <div key={index} className="mb-3">
              {documentToReactComponents(item)}
            </div>
          )
        )}
      </div>
    ) : null;
  };

  const handlerLink = () => {
    window.location.href = '/size-guide';
    setIsOpen(false);
  };

  return (
    <Transition.Root show={open} as={Fragment}>
      <Dialog
        as="div"
        className="message-modal unit-type-modal relative z-[1001]"
        onClose={setIsOpen}
        aria-label="image-unit-type-modal"
      >
        <Transition.Child
          as={Fragment}
          enter="ease-out duration-300"
          enterFrom="opacity-0"
          enterTo="opacity-100"
          leave="ease-in duration-200"
          leaveFrom="opacity-100"
          leaveTo="opacity-0"
        >
          <div className="fixed inset-0 bg-black bg-opacity-50" />
        </Transition.Child>

        <div className="fixed inset-0 z-30 overflow-y-auto">
          <div className="flex min-h-full items-center justify-center p-4">
            <Transition.Child
              as={Fragment}
              enter="ease-out duration-300"
              enterFrom="opacity-0 translate-y-4 sm:translate-y-0 sm:scale-95"
              enterTo="opacity-100 translate-y-0 sm:scale-100"
              leave="ease-in duration-200"
              leaveFrom="opacity-100 translate-y-0 sm:scale-100"
              leaveTo="opacity-0 translate-y-4 sm:translate-y-0 sm:scale-95"
            >
              <Dialog.Panel className="w-full max-w-lg transform overflow-hidden bg-white align-middle shadow-xl transition-all">
                <div className="bg-white px-4 pt-3 pb-4 sm:pb-4">
                  <div className="w-100 text-right">
                    <button
                      onClick={() => setIsOpen(false)}
                      aria-label="message-btn-close"
                    >
                      <Icon iconName="cross" />
                    </button>
                  </div>

                  <div className="sm:flex sm:items-start">
                    <div className="mt-4 px-4">
                      {renderImg()}
                      {renderDescription()}

                      {hyperlinkText && (
                        <button
                          className="m-auto mb-6 px-5 text-center font-bold text-callout link"
                          onClick={handlerLink}
                        >
                          {hyperlinkText}
                        </button>
                      )}
                    </div>
                  </div>
                </div>
              </Dialog.Panel>
            </Transition.Child>
          </div>
        </div>
      </Dialog>
    </Transition.Root>
  );
}

export default ImageUnitType;

// Specifies the proptypes:
ImageUnitType.propTypes = {
  unitType: PropTypes.object,
  open: PropTypes.bool,
  setIsOpen: PropTypes.func,
  callback: PropTypes.func,
};

// Specifies the default values for props:
ImageUnitType.defaultProps = {
  open: false,
  callback: () => {
    return false;
  },
  setIsOpen: () => {
    console.log("--> setIsOpen");
  },
};
