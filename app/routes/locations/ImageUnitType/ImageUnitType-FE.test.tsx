import { describe, expect, it, vi } from "vitest";
import { fireEventAliased, render, screen } from "../../../utils/test-utils";
import { BrowserRouter as Router } from "react-router-dom";
import ImageUnitType from "./index";
import { fakeUnitType } from "../../../utils/fakedataToTests";

// Mock IntersectionObserver
class IntersectionObserver {
  observe = vi.fn();
  disconnect = vi.fn();
  unobserve = vi.fn();
}

Object.defineProperty(window, "IntersectionObserver", {
  writable: true,
  configurable: true,
  value: IntersectionObserver,
});

Object.defineProperty(global, "IntersectionObserver", {
  writable: true,
  configurable: true,
  value: IntersectionObserver,
});

const obj = {
  setIsOpen: () => "setIsOpen",
}

describe("Image Unit Type component unit test", () => {
  it("Verify if ImageUnitType component renders", () => {
    render( 
      <Router> 
        <ImageUnitType open={true} unitType={fakeUnitType} />,
      </Router>
    );
    
    expect( 
      screen.getByLabelText('image-unit-type-modal')
    ).toBeInTheDocument();
  });


  it("Should close modal on click the btn OK", async () => {
    const spy = vi.spyOn(obj, 'setIsOpen');

    render(
      <Router>
        <ImageUnitType open={true} unitType={fakeUnitType} />,
      </Router>
    );
    
    const button = screen.getByLabelText("message-btn-close");
    fireEventAliased.click(button);

    expect(spy.getMockName()).toEqual('setIsOpen');
  });

  it("Verify that exist the image", async () => {
    render(
      <Router>
       <ImageUnitType open={true} unitType={fakeUnitType} />,
      </Router>
    );
    
    expect( screen.getByLabelText("unit-card__img")).toBeInTheDocument();
  });
});
