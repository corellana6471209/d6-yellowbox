import type { ActionFunction } from "@remix-run/node";
import { LoaderArgs } from "@remix-run/server-runtime";

import LoginForm from "~/routes/page-components/LoginForm";
import { login, getTenantInfo, loginForm } from "../../../utils/login";
import validateLoginForm from "../../page-components/LoginForm/validateLoginForm";
import {
  setCacheValueByKey,
  getCacheValueByKey,
} from "../../api/shared/elasticCache";

import * as isLegacy from "../../api/Authentication/IsLegacy/$emailAddress";

export async function loader({ request }: LoaderArgs) {
  const url = new URL(request.url);
  const slug = url.searchParams.get("slug");
  const reservation = url.searchParams.get("reservation");
  const islegacy = url.searchParams.get("islegacy");
  const redirectTo = url.searchParams.get("redirectTo");
  return { slug, reservation, islegacy, redirectTo };
}

// const delay = (ms : number) => new Promise(res => setTimeout(res, ms));

// export const validateAccessToken = async (request: Request,retry : number = 0) => {
//   const miliseconds = 200;
//   const maxRetries = 20;
//   if(! await getCacheValueByKey("accessToken", request)){
//     console.log('dont have access retry number ',retry)
//     await delay( miliseconds * retry);
//     if(retry === maxRetries){
//       console.log( maxRetries + ' retries, just fail');
//     }else{
//       await validateAccessToken(request,retry+1);
//     }
//   }
//   return true
// }

export const action: ActionFunction = async ({ request }) => {
  const body = await request.formData();
  const slug = body.get("slug") as string;
  const reservation = body.get("reservation") as string;
  const islegacy = body.get("islegacy") as string;
  const redirectTo = body.get("redirectTo") as string;
  const EmailAddress = body.get("email") as string;
  const Password = body.get("password") as string;

  const errors = validateLoginForm({ email: EmailAddress, password: Password });
  if (!errors.isValid) return { frontErrors: errors.frontErrors };

  const data: loginForm = {
    EmailAddress: EmailAddress,
    Password: Password,
  };

  if (!islegacy) {
    const params: any = { params: { emailAddress: EmailAddress } };
    const legacyData = await isLegacy.loader(params);

    if (!legacyData || legacyData.err || !legacyData?.data.isLegacyUser) {
      data["IsLegacyUser"] = false;
    } else {
      data["IsLegacyUser"] = true;
    }
  } else {
    if (islegacy && islegacy !== "false") {
      data["IsLegacyUser"] = true;
    } else {
      data["IsLegacyUser"] = false;
    }
  }

  const response = await login(data);
  const result = response?.data;

  const accessToken =
    result?.cognitoUserSecret?.cognitoAuthResponse?.accessToken;

  if (!accessToken || !result?.cognitoUserSecret?.cognitoAuthResponse) {
    return {
      serverError: "The user or password is not valid",
      userIsLockedOut: result?.cognitoUserSecret?.userIsLockedOut,
      redirect: false,
    };
  }
  const sitelinkUserAndTenantInfo = result?.sitelinkUserAndTenantInfo;
  const idToken = result?.cognitoUserSecret?.cognitoAuthResponse?.idToken;
  const refreshToken =
    result?.cognitoUserSecret?.cognitoAuthResponse?.refreshToken;
  const expirationTime =
    result?.cognitoUserSecret?.cognitoAuthResponse?.expiresIn;
  const tenantInfo = await getTenantInfo(encodeURIComponent(EmailAddress));

  if (tenantInfo && tenantInfo.data && tenantInfo.data[0]) {
    const validInfo = tenantInfo.data.filter(
      (info: any) => info.address1 && info.address2
    );

    if (validInfo && validInfo.length > 0) {
      await setCacheValueByKey("tenantInfo", validInfo[0], request);
    } else {
      await setCacheValueByKey("tenantInfo", tenantInfo.data[0], request);
    }
  }

  await setCacheValueByKey(
    "sitelinkUserAndTenantInfo",
    sitelinkUserAndTenantInfo,
    request
  );

  await setCacheValueByKey("userEmail", EmailAddress, request);
  await setCacheValueByKey("idToken", idToken, request);
  await setCacheValueByKey("expirationTime", expirationTime, request);
  await setCacheValueByKey("refreshToken", refreshToken, request);
  await setCacheValueByKey("accessToken", accessToken, request);
  await setCacheValueByKey("units", null, request);
  await setCacheValueByKey("unit", null, request);

  // await validateAccessToken(request)

  if (slug && reservation) {
    return {
      redirect: true,
      redirectTo: `/locations/make-payment/${slug}/${reservation}`,
      email: EmailAddress,
      data: sitelinkUserAndTenantInfo,
    };
  } else if (redirectTo) {
    return {
      redirect: true,
      redirectTo: redirectTo,
      email: EmailAddress,
      data: sitelinkUserAndTenantInfo,
    };
  } else {
    return {
      redirect: true,
      redirectTo: "/my-account",
      email: EmailAddress,
      data: sitelinkUserAndTenantInfo,
    };
  }
};

const BreadcrumbList = () => {
  try {
    if (window) {
      const domain = window.location.hostname
      return (
        <script
          type="application/ld+json"
          dangerouslySetInnerHTML={{
            __html: `
            {
              "@context": "https://schema.org",
              "@type": "BreadcrumbList",
              "itemListElement": [{
                "@type": "ListItem",
                "position": 1,
                "name": "Home",
                "item": "https://${domain}/"
              },{
                "@type": "ListItem",
                "position": 2,
                "name": "Login",
                "item": "https://${domain}/login/"
              }]
            }
          `,
          }}
        />
      )
    }
    return null;
  } catch (error) {
    return null
  }
}

const Login = () => {
  return (
    <>
      <BreadcrumbList />
      <LoginForm />
    </>
  );
};

export default Login;
