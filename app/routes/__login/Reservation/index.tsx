import type { ActionFunction } from "@remix-run/node";
import { getCacheValueByKey } from "~/routes/api/shared/elasticCache";
import FindReservationForm from "~/routes/page-components/FindReservationForm";
import * as findReservationByTenanid from "../../api/Reservationlistbytenantid/$information";
import * as isExistingUser from "../../api/Authentication/IsExistingUser/$locationId/$waitingListId";
import * as findUnitByUnitId from "../../api/Inventory/UnitsInformationByUnitID/$information";
import { getUnitImageInformation, loadLocationContentByLocationId } from "../../locations/backend";
import { getTenantInfo } from '../../../utils/login';
import validateFindReservationForm from "../../page-components/FindReservationForm/validateFindReservationForm";

export const action: ActionFunction = async ({ request }) => {
  const errorModals = {
    showNotFoundModal: false,
    showNotLoggedModal: false
  }; 
  const body = await request.formData();
  const emailAddress = body.get("email") as string;

  const errors = validateFindReservationForm({ emailAddress });
  if (!errors.isValid) return { frontErrors: errors.frontErrors };

  const tenantArray = await getTenantInfo(encodeURIComponent(emailAddress));
  const email = await getCacheValueByKey("userEmail", request);
  const token = await getCacheValueByKey('accessToken',request);

  // Reservations don't exists for the input email (Email not found)
  if (!tenantArray || !tenantArray?.data || !tenantArray?.data.length) {
    errorModals.showNotFoundModal = true;
    return { errorModals };
  };

  const reservationsFromTenant = []
  for await (const tenant of tenantArray.data) {
    const params: any = { locationid: tenant.locationId, tenantid: tenant.tenantId };
    const reservation = await findReservationByTenanid.loader(params);
    if (reservation?.data && Array.isArray(reservation?.data)) reservationsFromTenant.push({ reservation: reservation.data, locationId: tenant.locationId });
  }
  
  // Reservations don't exists for the input email (Email found but not reservations)
  if (!reservationsFromTenant.length) {
    errorModals.showNotFoundModal = true;
    return { errorModals };
  }
  
  const reservations = [];
  let isUser = false;
  for await (const reservationTenant of reservationsFromTenant) {
    const location = await loadLocationContentByLocationId(reservationTenant.locationId);
    
    for await (const reservation of reservationTenant.reservation) {
      const paramsUser: any = { params: { locationId: reservationTenant.locationId, waitingListId: reservation.waitingId }};
      const paramsUnit: any = { params: { locationid: reservationTenant.locationId, unitid: reservation.unitId1 }};
      const unit = await findUnitByUnitId.loader(paramsUnit);
      const findUser = await isExistingUser.loader(paramsUser);
      const image = await getUnitImageInformation(unit, reservationTenant.locationId);
      reservation.image = image;
      reservation.locationId = reservationTenant.locationId;
      reservation.location = location;
      if (unit?.data.length) reservation.unit = unit.data[0];
      reservations.push(reservation);

      if (findUser?.data?.isUser) {
        isUser = true;
      }
    }
  };

  // User exists but is not logged OR the input email don't match with the email in session. Must Login to continue
  if (isUser && (!token || `${email}` !== emailAddress)) {
    errorModals.showNotLoggedModal = true;
    return { errorModals };
  }

  return ({ reservations });
}

const BreadcrumbList = () => {
  try {
    if (window) {
      const domain = window.location.hostname
      return (
        <script
          type="application/ld+json"
          dangerouslySetInnerHTML={{
            __html: `
            {
              "@context": "https://schema.org",
              "@type": "BreadcrumbList",
              "itemListElement": [{
                "@type": "ListItem",
                "position": 1,
                "name": "Home",
                "item": "https://${domain}/"
              },{
                "@type": "ListItem",
                "position": 2,
                "name": "Find reservation",
                "item": "https://${domain}/reservation/"
              }]
            }
          `,
          }}
        />
      )
    }
    return null;
  } catch (error) {
    return null
  }
}

const Reservation = () => {
  return (
    <>
      <BreadcrumbList />
      <FindReservationForm />
    </>
  );
};

export default Reservation;