import { useContext, useEffect, useState } from "react";
import { ClientOnly } from "remix-utils";
import { PageDetailsContext } from "~/utils/context/PageDeatilsContext";
import MessageModal from "~/routes/common/MessageModal";
import BlogHeroImage from "~/routes/page-components/BlogHeroImage";
import {
  setCookie,
  deleteCookie,
  getUserIdentificator,
} from "../../utils/cookie";
import contentfulKeys from "../../utils/config";
import {
  getLastPage,
  getFirstRender,
  getAddressCoords,
} from "../../utils/cookie";
import { getGeoLocationInformation } from "../../utils/getGeoLocation";
import { getLabel } from "../../utils/getLabel";
import Spinner from "../common/Spinner";

import Modules from "../page-components/__ModuleRouter";
import Header from "./Header";
import Footer from "./Footer";
import { generateUniqueId } from "~/session.server";

export default function Page(props: any) {
  const { components, slug, token } = props;
  const pageDetails = useContext(PageDetailsContext);
  const [lastPage, setLastPage] = useState<any>(null);

  useEffect(() => {
    function fetchData() {
      const simplyUser = getUserIdentificator();
      if (!simplyUser) {
        setCookie("simplyUser", Date.now(), 30);
      }
      if (window && window.location.pathname === "/") {
        geoLocation();
      }
    }

    async function fetchCookie() {
      if (window && window.location.pathname !== "/") {
        deleteCookie(contentfulKeys.constants.lastPage);
        const days = await getLabel("cookieTimeDays");
        setCookie(
          contentfulKeys.constants.lastPage,
          window.location.href,
          days ? parseInt(days) : 30
        );
      }
    }

    fetchCookie();
    fetchData();
    // eslint-disable-next-line
  }, []);

  const geoLocation = async () => {
    const firstRender = getFirstRender();
    if (!firstRender) {
      setTimeout(async () => {
        const coords = getAddressCoords();
        if (!coords) {
          if (confirm("SimplySS wants to know your location.")) {
            await getGeoLocationInformation();
            setTimeout(async () => {
              const coords = getAddressCoords();
              if (coords) {
                pageDetails?.address.setAddress(coords);
              }
            }, 500); //wait
          } else {
            if (!firstRender) {
              setCookie(contentfulKeys.constants.firstRenderCookie, true, 7); // This is for verify if the message appear in the beggining of system
            }
          }
        } else {
          pageDetails?.address.setAddress(coords);
        }
        getPreviousPage();
      }, 2000); //wait 2 seconds
    } else {
      const coords = getAddressCoords();
      if (coords) {
        pageDetails?.address.setAddress(coords);
      }
      getPreviousPage();
    }
  };

  const getPreviousPage = async () => {
    const cookie = getLastPage();
    const previousRoute = document.referrer;
    const currentlyRoute = window.location.href;

    if (
      (!previousRoute || previousRoute != currentlyRoute) &&
      cookie &&
      (window.history.length === 1 ||
        window.history.length === 2 ||
        window.history.length === 0)
    ) {
      deleteCookie(contentfulKeys.constants.lastPage);
      setLastPage(cookie);
    } else {
      deleteCookie(contentfulKeys.constants.lastPage);
    }
  };

  // console.log(`components: `,components)
  return pageDetails ? (
    <>
      {pageDetails?.displayHeader && <Header />}

      {pageDetails?.showBlogHero && <BlogHeroImage pageDetails={pageDetails} />}

      {lastPage && (
        <MessageModal
          open={true}
          message={null}
          type="Redirect"
          ctaText="Resume where I left off"
          callback={(value: any) => {
            if (value) {
              location.href = lastPage;
            }
          }}
        />
      )}

      <ClientOnly key="client-side-components">
        {() => (
          <Modules
            key="module-Components"
            components={components}
            pageDetails={pageDetails}
            slug={slug}
            token={token}
          />
        )}
      </ClientOnly>

      {pageDetails?.displayHeader && <Footer />}
    </>
  ) : null;
}
