import fetchContentful from '../../../utils/fetchContentful';

export async function getMessage(params: string) {
    const query = `
  query {
    genericMessageCollection(
      limit: 1
      where: { type: "${params}" }
    ) {
      items {
        title
        message {
          json
          links {
            assets {
              block {
                title
                url
                sys {
                  id
                }
              }
            }
          }
        }
      }
    }
  }
  `;

  const data = await fetchContentful(query);

  if (data && data.genericMessageCollection && data.genericMessageCollection.items[0] != null) {
    return data.genericMessageCollection.items[0];
  } else {
    return null;
  }
}


