import { ComponentStory, ComponentMeta } from "@storybook/react";
import MessageModal from "./index";

export default {
  title: "components/core/MessageModal",
  component: MessageModal,
} as ComponentMeta<typeof MessageModal>;

const Template: ComponentStory<typeof MessageModal> = (args) => (
  <MessageModal {...args} />
);

export const Primary = Template.bind({});
Primary.args = {
  open: true,
  message:
    "Sorry, something went wrong on our end. Please try again. If that doesn’t help, call 877-786-7343 or visit a Simply Self Storage location for assistance.",
  title: "",
  additionalClass: "text-center md:text-left",
  ctaText: "OK",
};
