import { describe, expect, it } from "vitest";
import { render, screen, fireEventAliased } from "../../../utils/test-utils";
import { BrowserRouter as Router } from "react-router-dom";
import MessageModal from './index';

// Mock IntersectionObserver
class IntersectionObserver {
    observe = vi.fn();
    disconnect = vi.fn();
    unobserve = vi.fn();
}
  
Object.defineProperty(window, "IntersectionObserver", {
    writable: true,
    configurable: true,
    value: IntersectionObserver,
});

Object.defineProperty(global, "IntersectionObserver", {
    writable: true,
    configurable: true,
    value: IntersectionObserver,
});

const text = "Sorry, something went wrong on our end";
const title = "Title 2";

describe("MessageModal component", () => {
  it("Verify if MessageModal component renders", () => {
    render( 
      <Router> 
        <MessageModal open={true} />,
      </Router>
    );

    expect(screen.getByLabelText('message-modal')).toBeInTheDocument();
  });

  it("Verify that exist message", () => {
    render(
      <Router>
        <MessageModal open={true} message={text} />,
      </Router>
    );
    
    const wrapperText = screen.getByLabelText("message-modal-text");
    expect(wrapperText.innerHTML).toBe(text);
  });
  
  it("Verify that exist title", () => {
    render(
      <Router>
        <MessageModal open={true} message={text} title={title} />,
      </Router>
    );
    
    const wrapperText = screen.getByLabelText("message-modal-title");
    expect(wrapperText.innerHTML).toBe(title);
  });

  it("Should close modal on click the btn OK", async () => {
    const { container } = render(
      <Router>
        <MessageModal open={true} message={text} title={title} />,
      </Router>
    );
    
    const button = screen.getByLabelText("message-btn-ok");
    fireEventAliased.click(button);

    expect(
      await container.getElementsByClassName('message-modal').length).toBe(0);
  });
});
