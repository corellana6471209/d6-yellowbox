import { Fragment, useState, useEffect } from "react";
import { Dialog, Transition } from "@headlessui/react";
import * as PropTypes from "prop-types";
import Icon from "../svg/Icon";
import { getMessage } from "./backend";
import { documentToReactComponents } from "@contentful/rich-text-react-renderer";
import { BLOCKS } from "@contentful/rich-text-types";
import type Asset from "../../../utils/interfaces/richText";
import { getLabel } from "../../../utils/getLabel";

function MessageModal(props: any) {
  const { open, title, message, additionalClass, ctaText, callback, type } =
    props;

  const [isOpen, setIsOpen] = useState(false);
  const [data, setData] = useState<any>(null);
  const [genericMessage, setGenericMessage] = useState<any>(null);
  const [redirectButton, setRedirectButton] = useState<any>(null);

  const renderOptions: any = {
    renderNode: {
      [BLOCKS.EMBEDDED_ASSET]: (node: Asset) => {
        const img = genericMessage?.links?.assets.block.find(
          (i) => i.sys.id === node.data.target.sys.id
        );
        return genericMessage ? <img src={img.url} alt={img.title} /> : null;
      },
    },
  };

  useEffect(() => {
    async function fetchData() {
      const data = await getMessage(type);
      setData(data);

      if (data && data.message) setGenericMessage(data.message);

      if (type == "Redirect") {
        const redirectButton = await getLabel("redirectButton");
        setRedirectButton(redirectButton);
      }

      return data;
    }

    if (type) fetchData();
  }, []);

  useEffect(() => {
    setIsOpen(open);
  }, [open]);

  function handlerClose(value: boolean) {
    setIsOpen(false);
    callback(value);
  }

  function renderMessage() {
    return message ? (
      <>{message}</>
    ) : (
      <>
        {genericMessage?.json?.content.map((item, index: number) => (
          <div key={index}>
            {documentToReactComponents(item, renderOptions)}
          </div>
        ))}
      </>
    );
  }

  return (
    <Transition.Root show={isOpen} as={Fragment}>
      <Dialog
        as="div"
        className={`message-modal relative z-50 ${additionalClass}`}
        onClose={handlerClose}
        aria-label="message-modal"
      >
        <Transition.Child
          as={Fragment}
          enter="ease-out duration-300"
          enterFrom="opacity-0"
          enterTo="opacity-100"
          leave="ease-in duration-200"
          leaveFrom="opacity-100"
          leaveTo="opacity-0"
        >
          <div className="fixed inset-0 bg-black bg-opacity-50" />
        </Transition.Child>

        <div className="fixed inset-0 z-10 overflow-y-auto">
          <div className="flex min-h-full items-center justify-center p-4">
            <Transition.Child
              as={Fragment}
              enter="ease-out duration-300"
              enterFrom="opacity-0 translate-y-4 sm:translate-y-0 sm:scale-95"
              enterTo="opacity-100 translate-y-0 sm:scale-100"
              leave="ease-in duration-200"
              leaveFrom="opacity-100 translate-y-0 sm:scale-100"
              leaveTo="opacity-0 translate-y-4 sm:translate-y-0 sm:scale-95"
            >
              <Dialog.Panel className="w-full max-w-md transform overflow-hidden bg-white align-middle shadow-xl transition-all">
                <div className="bg-white px-4 pt-3 pb-4 sm:pb-4">
                  <div className="w-100 text-right">
                    <button
                      onClick={() => handlerClose(false)}
                      aria-label="message-btn-close"
                    >
                      <Icon iconName="cross" />
                    </button>
                  </div>

                  <div className="sm:flex sm:items-start">
                    <div className="mt-4">
                      {(title !== "" || (data && data.title)) && (
                        <Dialog.Title
                          as="h3"
                          className="text-lg text-gray-900 mb-8 font-medium leading-6"
                          aria-label="message-modal-title"
                        >
                          {title ? title : data && data.title ? data.title : ""}
                        </Dialog.Title>
                      )}

                      <div className={title ? "mt-2" : ""}>
                        <div
                          className="text-sm text-gray-500"
                          aria-label="message-modal-text"
                        >
                          {renderMessage()}
                        </div>
                      </div>
                    </div>
                  </div>
                </div>

                <div className="py-3 px-4 text-right">
                  {ctaText && (
                    <button
                      aria-label="message-btn-ok"
                      className="bg-primary1 yellow-button py-3 px-5 text-secondary1"
                      onClick={() => handlerClose(true)}
                    >
                      {redirectButton ? redirectButton : ctaText}
                    </button>
                  )}
                </div>
              </Dialog.Panel>
            </Transition.Child>
          </div>
        </div>
      </Dialog>
    </Transition.Root>
  );
}

export default MessageModal;

// Specifies the proptypes:
MessageModal.propTypes = {
  open: PropTypes.bool,
  title: PropTypes.string,
  message: PropTypes.string,
  additionalClass: PropTypes.string,
  ctaText: PropTypes.any,
  callback: PropTypes.func,
  type: PropTypes.string,
};

// Specifies the default values for props:
MessageModal.defaultProps = {
  open: false,
  message:
    "Sorry, something went wrong on our end. Please try again. If that doesn’t help, call 877-786-7343 or visit a Simply Self Storage location for assistance.",
  title: "",
  additionalClass: "text-center md:text-left",
  ctaText: "OK",
  callback: () => {
    return false;
  },
  type: "Error",
};
