import React from "react";

export default function Discover() {
  return (
    <svg
      xmlns="http://www.w3.org/2000/svg"
      width="56.146"
      height="35.093"
      viewBox="0 0 56.146 35.093"
    >
      <defs>
        <clipPath id="clipPath">
          <rect
            id="Rectangle_840"
            data-name="Rectangle 840"
            width="56.146"
            height="35.093"
            fill="none"
          />
        </clipPath>
        <clipPath id="clipPath-2">
          <path
            id="Path_676"
            data-name="Path 676"
            d="M32.424,21.2a2.8,2.8,0,1,0,2.8-2.7,2.752,2.752,0,0,0-2.8,2.7"
            transform="translate(-32.424 -18.504)"
            fill="none"
          />
        </clipPath>
        <radialGradient
          id="radial-gradient"
          cx="0.5"
          cy="0.5"
          r="0.506"
          gradientUnits="objectBoundingBox"
        >
          <stop offset="0" stopColor="#ffead9" />
          <stop offset="0.006" stopColor="#ffead9" />
          <stop offset="0.107" stopColor="#fee4d0" />
          <stop offset="0.271" stopColor="#fcd4b7" />
          <stop offset="0.479" stopColor="#fabb8f" />
          <stop offset="0.722" stopColor="#f69758" />
          <stop offset="0.992" stopColor="#f26b13" />
          <stop offset="1" stopColor="#f26a11" />
        </radialGradient>
      </defs>
      <g id="Group_909" data-name="Group 909" transform="translate(0 0)">
        <g id="Group_912" data-name="Group 912" transform="translate(0 0)">
          <g id="Group_911" data-name="Group 911" clipPath="url(#clipPath)">
            <path
              id="Rectangle_839"
              data-name="Rectangle 839"
              d="M2.338,0h51.47a2.338,2.338,0,0,1,2.338,2.338V32.756a2.337,2.337,0,0,1-2.337,2.337H2.339A2.339,2.339,0,0,1,0,32.754V2.338A2.338,2.338,0,0,1,2.338,0Z"
              transform="translate(0 0)"
              fill="#fff"
            />
            <path
              id="Path_675"
              data-name="Path 675"
              d="M19.278,40.1H58.12a1.872,1.872,0,0,0,1.872-1.872V25.084C56.263,27.447,42.208,35.6,19.278,40.1"
              transform="translate(-3.845 -5.003)"
              fill="#f36a10"
            />
          </g>
        </g>
        <g
          id="Group_914"
          data-name="Group 914"
          transform="translate(25.957 14.813)"
        >
          <g id="Group_913" data-name="Group 913" clipPath="url(#clipPath-2)">
            <rect
              id="Rectangle_841"
              data-name="Rectangle 841"
              width="5.596"
              height="5.47"
              transform="translate(0 0)"
              fill="url(#radial-gradient)"
            />
          </g>
        </g>
        <g id="Group_916" data-name="Group 916" transform="translate(0 0)">
          <g id="Group_915" data-name="Group 915" clipPath="url(#clipPath)">
            <path
              id="Path_677"
              data-name="Path 677"
              d="M15.679,18.626H14.173v5.255h1.5a2.739,2.739,0,0,0,1.873-.606,2.644,2.644,0,0,0,.953-2.019,2.609,2.609,0,0,0-2.82-2.63m1.2,3.947a1.942,1.942,0,0,1-1.4.418H15.2V19.516h.275a1.9,1.9,0,0,1,1.4.425,1.765,1.765,0,0,1,.565,1.306,1.8,1.8,0,0,1-.565,1.325"
              transform="translate(-2.827 -3.715)"
              fill="#09141a"
            />
            <rect
              id="Rectangle_842"
              data-name="Rectangle 842"
              width="1.023"
              height="5.255"
              transform="translate(16.145 14.911)"
              fill="#09141a"
            />
            <path
              id="Path_678"
              data-name="Path 678"
              d="M24.048,20.62c-.616-.227-.8-.378-.8-.661,0-.33.323-.582.765-.582a1.066,1.066,0,0,1,.828.424l.535-.7a2.3,2.3,0,0,0-1.544-.584A1.544,1.544,0,0,0,22.2,20.022c0,.725.331,1.1,1.292,1.442a4.086,4.086,0,0,1,.709.3.632.632,0,0,1,.307.544.748.748,0,0,1-.8.741,1.211,1.211,0,0,1-1.118-.7l-.66.639a2.041,2.041,0,0,0,1.819,1A1.7,1.7,0,0,0,25.56,22.26c0-.835-.345-1.214-1.512-1.64"
              transform="translate(-4.374 -3.693)"
              fill="#09141a"
            />
            <path
              id="Path_679"
              data-name="Path 679"
              d="M26.868,21.226a2.722,2.722,0,0,0,2.774,2.743,2.818,2.818,0,0,0,1.285-.307V22.457a1.64,1.64,0,0,1-1.238.574,1.718,1.718,0,0,1-1.764-1.812,1.747,1.747,0,0,1,1.717-1.8,1.715,1.715,0,0,1,1.285.591V18.8a2.616,2.616,0,0,0-1.261-.322,2.763,2.763,0,0,0-2.8,2.748"
              transform="translate(-5.359 -3.685)"
              fill="#09141a"
            />
            <path
              id="Path_680"
              data-name="Path 680"
              d="M41.682,22.156l-1.4-3.53H39.16l2.231,5.389h.551l2.27-5.389H43.1Z"
              transform="translate(-7.811 -3.715)"
              fill="#09141a"
            />
            <path
              id="Path_681"
              data-name="Path 681"
              d="M46.053,23.881h2.906v-.89H47.076V21.572h1.812v-.889H47.076V19.516h1.883v-.89H46.053Z"
              transform="translate(-9.185 -3.715)"
              fill="#09141a"
            />
            <path
              id="Path_682"
              data-name="Path 682"
              d="M53.909,20.177c0-.985-.677-1.551-1.859-1.551H50.529v5.255h1.026V21.769h.134l1.418,2.112h1.26l-1.655-2.214a1.4,1.4,0,0,0,1.2-1.49m-2.057.867h-.3V19.452h.315c.639,0,.985.27.985.78,0,.528-.347.813-1,.813"
              transform="translate(-10.078 -3.715)"
              fill="#09141a"
            />
            <path
              id="Path_683"
              data-name="Path 683"
              d="M55.572,19.079c0-.091-.062-.142-.175-.142h-.149V19.4h.11v-.18l.129.18h.138l-.154-.191a.127.127,0,0,0,.1-.13m-.194.063h-.019v-.12h.02c.056,0,.083.019.083.059s-.028.061-.084.061"
              transform="translate(-11.019 -3.777)"
              fill="#1a1718"
            />
            <path
              id="Path_684"
              data-name="Path 684"
              d="M55.363,18.72a.4.4,0,1,0,.4.405.4.4,0,0,0-.4-.405m0,.737a.332.332,0,1,1,.319-.331.326.326,0,0,1-.319.331"
              transform="translate(-10.962 -3.734)"
              fill="#1a1718"
            />
          </g>
        </g>
      </g>
    </svg>
  );
}
