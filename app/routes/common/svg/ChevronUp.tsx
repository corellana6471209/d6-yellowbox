export default function ChevronUp(props: any) {
  const { size, color } = props;
  return (
    <svg
      xmlns="http://www.w3.org/2000/svg"
      width={size}
      height={size}
      viewBox="0 0 24 24"
      fill="none"
      stroke={color}
      strokeWidth="2"
      strokeLinecap="round"
      strokeLinejoin="round"
      className="feather feather-chevron-up"
      role="img"
      title="Up"
      aria-label="Up"
    >
      <polyline points="18 15 12 9 6 15"></polyline>
    </svg>
  );
}
