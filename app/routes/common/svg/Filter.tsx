import React from "react";

export default function Filter({ size = 62 }) {
  return (
    <svg
      xmlns="http://www.w3.org/2000/svg"
      id="Elem_Icon_Filter"
      width={size}
      height={size}
      viewBox="0 0 62 62"
    >
      <path
        id="Path_581"
        data-name="Path 581"
        d="M0,0H62V62H0Z"
        fill="#F7F8FE"
      />
      <g
        id="Group_699"
        data-name="Group 699"
        transform="translate(17.286 17.121)"
      >
        <g id="Group_630" data-name="Group 630">
          <line
            id="Line_53"
            data-name="Line 53"
            x2="14.609"
            transform="translate(11.914 4.826)"
            fill="none"
            stroke="#657ac1"
            strokeLinecap="round"
            strokeWidth="2"
          />
          <line
            id="Line_59"
            data-name="Line 59"
            x2="14.609"
            transform="translate(11.914 25.261)"
            fill="none"
            stroke="#657ac1"
            strokeLinecap="round"
            strokeWidth="2"
          />
          <g
            id="Ellipse_38"
            data-name="Ellipse 38"
            transform="translate(-0.286 -0.121)"
            fill="none"
            stroke="#657ac1"
            strokeWidth="2"
          >
            <circle cx="4" cy="4" r="4" stroke="none" />
            <circle cx="4" cy="4" r="3" fill="none" />
          </g>
          <g
            id="Ellipse_45"
            data-name="Ellipse 45"
            transform="translate(0 20.435)"
            fill="none"
            stroke="#657ac1"
            strokeWidth="2"
          >
            <circle cx="4" cy="4" r="4" stroke="none" />
            <circle cx="4" cy="4" r="3" fill="none" />
          </g>
          <line
            id="Line_55"
            data-name="Line 55"
            x2="12.522"
            transform="translate(2.523 15.261)"
            fill="none"
            stroke="#657ac1"
            strokeLinecap="round"
            strokeWidth="2"
          />
          <g
            id="Ellipse_40"
            data-name="Ellipse 40"
            transform="translate(20.714 9.879)"
            fill="none"
            stroke="#657ac1"
            strokeWidth="2"
          >
            <circle cx="4" cy="4" r="4" stroke="none" />
            <circle cx="4" cy="4" r="3" fill="none" />
          </g>
        </g>
      </g>
    </svg>
  );
}
