import React from "react";

export default function CloseIcon(props: any) {
  return (
    <svg
      xmlns="http://www.w3.org/2000/svg"
      width="32.828"
      height="32.828"
      viewBox="0 0 32.828 32.828"
      className="icon-close"
    >
      <g id="Elem_Icon_Close" transform="translate(1.414 1.414)">
        <line
          id="Line_14"
          data-name="Line 14"
          x2="30"
          y2="30"
          fill="none"
          stroke="#d6643f"
          strokeWidth="4"
        />
        <line
          id="Line_15"
          data-name="Line 15"
          x1="30"
          y2="30"
          fill="none"
          stroke="#d6643f"
          strokeWidth="4"
        />
      </g>
    </svg>
  );
}
