import React from "react";

export default function Door() {
  return (
    <svg
      xmlns="http://www.w3.org/2000/svg"
      width="86"
      height="86"
      viewBox="0 0 86 86"
    >
      <defs>
        <filter
          id="Rectangle_871"
          x="0"
          y="0"
          width="86"
          height="86"
          filterUnits="userSpaceOnUse"
        >
          <feOffset dy="3" input="SourceAlpha" />
          <feGaussianBlur stdDeviation="1" result="blur" />
          <feFlood floodOpacity="0.161" />
          <feComposite operator="in" in2="blur" />
          <feComposite in="SourceGraphic" />
        </filter>
      </defs>
      <g
        id="Group_2060"
        data-name="Group 2060"
        transform="translate(-30.43 -117.344)"
      >
        <g
          transform="matrix(1, 0, 0, 1, 30.43, 117.34)"
          filter="url(#Rectangle_871)"
        >
          <g
            id="Rectangle_871-2"
            data-name="Rectangle 871"
            transform="translate(3)"
            fill="#fff"
            stroke="rgba(234,235,239,0.75)"
            strokeWidth="0.5"
          >
            <rect width="80" height="80" rx="6" stroke="none" />
            <rect
              x="0.25"
              y="0.25"
              width="79.5"
              height="79.5"
              rx="5.75"
              fill="none"
            />
          </g>
        </g>
        <g
          id="Group_1600"
          data-name="Group 1600"
          transform="translate(-8273.752 8861.129)"
        >
          <path
            id="Path_874"
            data-name="Path 874"
            d="M8328.117-8646.246h7.453"
            transform="translate(-2.109 -40.568)"
            fill="none"
            stroke="#04145b"
            strokeWidth="2"
          />
          <g
            id="Group_1588"
            data-name="Group 1588"
            transform="translate(8325 -8725.268)"
          >
            <g
              id="Path_881"
              data-name="Path 881"
              transform="translate(6.675 6)"
              fill="none"
            >
              <path
                d="M6,0H22c4.491,0,6,.734,6,6V33a6,6,0,0,1-6,6H6a6,6,0,0,1-6-6V6C0,.969,1.522,0,6,0Z"
                stroke="none"
              />
              <path
                d="M 6 2 C 4.843820571899414 2 3.24176025390625 2.0601806640625 2.680099487304688 2.580528259277344 C 2.22882080078125 2.998619079589844 2 4.149101257324219 2 6 L 2 33 C 2 35.20560836791992 3.794389724731445 37 6 37 L 22 37 C 24.20561027526855 37 26 35.20560836791992 26 33 L 26 6 C 26 3.601371765136719 25.6459903717041 2.780399322509766 25.3489990234375 2.514068603515625 C 24.8409309387207 2.058429718017578 23.29235076904297 2 22 2 L 6 2 M 6 0 L 22 0 C 26.4914493560791 0 28 0.7336196899414062 28 6 L 28 33 C 28 36.3137092590332 25.3137092590332 39 22 39 L 6 39 C 2.686290740966797 39 0 36.3137092590332 0 33 L 0 6 C 0 0.9690513610839844 1.522319793701172 0 6 0 Z"
                stroke="none"
                fill="#00155f"
              />
            </g>
            <g
              id="Rectangle_1024"
              data-name="Rectangle 1024"
              transform="translate(0.675)"
              fill="none"
              stroke="#00155f"
              strokeWidth="2.5"
            >
              <rect width="40" height="46" rx="6" stroke="none" />
              <rect
                x="1.25"
                y="1.25"
                width="37.5"
                height="43.5"
                rx="4.75"
                fill="none"
              />
            </g>
            <path
              id="Path_867"
              data-name="Path 867"
              d="M8340.183-8703.38h25.772"
              transform="translate(-8332.504 8715.1)"
              fill="none"
              stroke="#04145b"
              strokeWidth="2"
            />
            <path
              id="Path_870"
              data-name="Path 870"
              d="M8340.183-8703.38h25.772"
              transform="translate(-8332.504 8725.693)"
              fill="none"
              stroke="#04145b"
              strokeWidth="2"
            />
            <path
              id="Path_872"
              data-name="Path 872"
              d="M8340.183-8703.38h25.772"
              transform="translate(-8332.504 8736.287)"
              fill="none"
              stroke="#04145b"
              strokeWidth="2"
            />
            <path
              id="Path_873"
              data-name="Path 873"
              d="M8340.183-8703.38h27.028"
              transform="translate(-8331.483 8737.877)"
              fill="none"
              stroke="#fff"
              strokeWidth="2"
            />
            <path
              id="Path_868"
              data-name="Path 868"
              d="M8340.183-8703.38h25.772"
              transform="translate(-8332.504 8720.396)"
              fill="none"
              stroke="#04145b"
              strokeWidth="2"
            />
            <path
              id="Path_869"
              data-name="Path 869"
              d="M8340.183-8703.38h25.772"
              transform="translate(-8332.504 8730.99)"
              fill="none"
              stroke="#04145b"
              strokeWidth="2"
            />
            <path
              id="Path_871"
              data-name="Path 871"
              d="M8340.183-8703.38h25.772"
              transform="translate(-8332.504 8741.585)"
              fill="none"
              stroke="#04145b"
              strokeWidth="2"
            />
            <rect
              id="Rectangle_1025"
              data-name="Rectangle 1025"
              width="43"
              height="9"
              transform="translate(-0.325 39)"
              fill="#fff"
            />
          </g>
          <rect
            id="Rectangle_1027"
            data-name="Rectangle 1027"
            width="11"
            height="12"
            transform="translate(8355.675 -8691.268)"
            fill="#fff"
          />
          <g
            id="Group_1589"
            data-name="Group 1589"
            transform="translate(8357.833 -8690.268)"
          >
            <g
              id="Path_875"
              data-name="Path 875"
              transform="translate(-0.158)"
              fill="none"
            >
              <path
                d="M4.5,0A4.5,4.5,0,1,1,0,4.5,4.5,4.5,0,0,1,4.5,0Z"
                stroke="none"
              />
              <path
                d="M 4.5 1.5 C 2.845789909362793 1.5 1.5 2.845789909362793 1.5 4.5 C 1.5 6.154210090637207 2.845789909362793 7.5 4.5 7.5 C 6.154210090637207 7.5 7.5 6.154210090637207 7.5 4.5 C 7.5 2.845789909362793 6.154210090637207 1.5 4.5 1.5 M 4.5 0 C 6.98528003692627 0 9 2.01471996307373 9 4.5 C 9 6.98528003692627 6.98528003692627 9 4.5 9 C 2.01471996307373 9 0 6.98528003692627 0 4.5 C 0 2.01471996307373 2.01471996307373 0 4.5 0 Z"
                stroke="none"
                fill="#00155f"
              />
            </g>
            <text
              id="i"
              transform="translate(3.842 7)"
              fill="#00155f"
              fontSize="6.5"
              fontFamily="SourceSerifPro-Regular, Source Serif Pro"
            >
              <tspan x="0" y="0">
                i
              </tspan>
            </text>
          </g>
        </g>
      </g>
    </svg>
  );
}
