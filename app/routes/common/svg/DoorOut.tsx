import React from "react";

export default function DoorOut() {
  return (
    <svg
      xmlns="http://www.w3.org/2000/svg"
      width="85"
      height="85"
      viewBox="0 0 85 85"
    >
      <defs>
        <filter
          id="Path_2684"
          x="0"
          y="0"
          width="85"
          height="85"
          filterUnits="userSpaceOnUse"
        >
          <feOffset dy="3" input="SourceAlpha" />
          <feGaussianBlur stdDeviation="1" result="blur" />
          <feFlood floodOpacity="0.161" />
          <feComposite operator="in" in2="blur" />
          <feComposite in="SourceGraphic" />
        </filter>
      </defs>
      <g
        id="Group_2059"
        data-name="Group 2059"
        transform="translate(24158.898 17669.258)"
      >
        <g
          transform="matrix(1, 0, 0, 1, -24158.9, -17669.26)"
          filter="url(#Path_2684)"
        >
          <g
            id="Path_2684-2"
            data-name="Path 2684"
            transform="translate(3)"
            fill="#fff"
          >
            <path
              d="M6,0H73a6,6,0,0,1,6,6V73a6,6,0,0,1-6,6H6a6,6,0,0,1-6-6V6A6,6,0,0,1,6,0Z"
              stroke="none"
            />
            <path
              d="M 6 0.5 C 2.967292785644531 0.5 0.5 2.967292785644531 0.5 6 L 0.5 73 C 0.5 76.03270721435547 2.967292785644531 78.5 6 78.5 L 73 78.5 C 76.03270721435547 78.5 78.5 76.03270721435547 78.5 73 L 78.5 6 C 78.5 2.967292785644531 76.03270721435547 0.5 73 0.5 L 6 0.5 M 6 0 L 73 0 C 76.31369781494141 0 79 2.686286926269531 79 6 L 79 73 C 79 76.31369781494141 76.31369781494141 79 73 79 L 6 79 C 2.686286926269531 79 0 76.31369781494141 0 73 L 0 6 C 0 2.686286926269531 2.686286926269531 0 6 0 Z"
              stroke="none"
              fill="rgba(234,235,239,0.75)"
            />
          </g>
        </g>
        <g
          id="Group_2039"
          data-name="Group 2039"
          transform="translate(-17993.963 -25861.758)"
        >
          <g
            id="Rectangle_886"
            data-name="Rectangle 886"
            transform="translate(-6133 8208)"
            fill="none"
            stroke="#04145b"
            strokeWidth="2.5"
          >
            <rect width="33" height="48" rx="2" stroke="none" />
            <rect
              x="1.25"
              y="1.25"
              width="30.5"
              height="45.5"
              rx="0.75"
              fill="none"
            />
          </g>
          <rect
            id="Rectangle_888"
            data-name="Rectangle 888"
            width="22"
            height="17"
            transform="translate(-6141 8225)"
            fill="#f7f8fe"
          />
          <g
            id="Union_2"
            data-name="Union 2"
            transform="translate(-6150.544 8220.141)"
            fill="none"
          >
            <path
              d="M0,13.36,14.575,0V7.895h16.9V18.825h-16.9V26.72Z"
              stroke="none"
            />
            <path
              d="M 12.07468795776367 5.683055877685547 L 3.699737548828125 13.36003398895264 L 12.07468795776367 21.03701210021973 L 12.07468795776367 16.32539367675781 L 28.97577667236328 16.32539367675781 L 28.97577667236328 10.39467430114746 L 12.07468795776367 10.39467430114746 L 12.07468795776367 5.683055877685547 M 14.57468795776367 3.814697265625e-06 L 14.57468795776367 7.894674301147461 L 31.47577667236328 7.894674301147461 L 31.47577667236328 18.82539367675781 L 14.57468795776367 18.82539367675781 L 14.57468795776367 26.72006416320801 L -1.9073486328125e-06 13.36003398895264 L 14.57468795776367 3.814697265625e-06 Z"
              stroke="none"
              fill="#00155f"
            />
          </g>
        </g>
      </g>
    </svg>
  );
}
