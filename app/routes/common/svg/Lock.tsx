import React from "react";

export default function Lock() {
  return (
    <svg
      xmlns="http://www.w3.org/2000/svg"
      width="85"
      height="85"
      viewBox="0 0 85 85"
      role="img"
      title="Change password"
      aria-label="Link to change password"
    >
      <defs>
        <filter
          id="Path_862"
          x="0"
          y="0"
          width="85"
          height="85"
          filterUnits="userSpaceOnUse"
        >
          <feOffset dy="3" input="SourceAlpha" />
          <feGaussianBlur stdDeviation="1" result="blur" />
          <feFlood floodOpacity="0.161" />
          <feComposite operator="in" in2="blur" />
          <feComposite in="SourceGraphic" />
        </filter>
      </defs>
      <g
        id="Group_2065"
        data-name="Group 2065"
        transform="translate(-191.498 21)"
      >
        <g transform="matrix(1, 0, 0, 1, 191.5, -21)" filter="url(#Path_862)">
          <g
            id="Path_862-2"
            data-name="Path 862"
            transform="translate(3)"
            fill="#fff"
          >
            <path
              d="M6,0H73a6,6,0,0,1,6,6V73a6,6,0,0,1-6,6H6a6,6,0,0,1-6-6V6A6,6,0,0,1,6,0Z"
              stroke="none"
            />
            <path
              d="M 6 0.5 C 2.967292785644531 0.5 0.5 2.967292785644531 0.5 6 L 0.5 73 C 0.5 76.03270721435547 2.967292785644531 78.5 6 78.5 L 73 78.5 C 76.03270721435547 78.5 78.5 76.03270721435547 78.5 73 L 78.5 6 C 78.5 2.967292785644531 76.03270721435547 0.5 73 0.5 L 6 0.5 M 6 0 L 73 0 C 76.31369781494141 0 79 2.686286926269531 79 6 L 79 73 C 79 76.31369781494141 76.31369781494141 79 73 79 L 6 79 C 2.686286926269531 79 0 76.31369781494141 0 73 L 0 6 C 0 2.686286926269531 2.686286926269531 0 6 0 Z"
              stroke="none"
              fill="rgba(234,235,239,0.75)"
            />
          </g>
        </g>
        <g
          id="Group_945"
          data-name="Group 945"
          transform="translate(-19.395 -1223.806)"
        >
          <g
            id="Rectangle_890"
            data-name="Rectangle 890"
            transform="translate(236 1231)"
            fill="none"
            stroke="#04145b"
            strokeWidth="3.5"
          >
            <rect width="34.786" height="34.711" rx="2" stroke="none" />
            <rect
              x="1.75"
              y="1.75"
              width="31.286"
              height="31.211"
              rx="0.25"
              fill="none"
            />
          </g>
          <path
            id="Path_745"
            data-name="Path 745"
            d="M16.766,26.87l.291,4.354a.972.972,0,0,1-.923,1.073H13.566a.974.974,0,0,1-.925-1.073l.292-4.354a3.66,3.66,0,1,1,3.833,0Z"
            transform="translate(238.543 1222.161)"
            fill="none"
            stroke="#04145b"
            strokeWidth="2.5"
          />
          <path
            id="Path_746"
            data-name="Path 746"
            d="M-5727.821,9118.4s-1.518-13.129,10.5-13.051,9.8,13.051,9.8,13.051"
            transform="translate(5971.014 -7886.452)"
            fill="none"
            stroke="#04145b"
            strokeWidth="3.5"
          />
        </g>
      </g>
    </svg>
  );
}
