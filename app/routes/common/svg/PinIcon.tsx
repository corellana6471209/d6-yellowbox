import React from "react";

export default function PinIcon(props: any) {
  return (
    <svg
      onClick={() => props.onClick()}
      id="Elem_Icon_LocationMarker"
      xmlns="http://www.w3.org/2000/svg"
      width="25.848"
      height="37"
      viewBox="0 0 25.848 37"
      className="pin-icon cursor-pointer"
    >
      <path
        id="Path_34"
        data-name="Path 34"
        d="M12.924,0A12.924,12.924,0,0,1,25.848,12.924c0,3.191-1.226,4.849-3.21,8.525-1.9,3.7-10.175,15.531-9.714,15.552C12.691,37,5.1,23.9,2.609,20.711,1.176,17.842,0,15.922,0,12.924A12.924,12.924,0,0,1,12.924,0Z"
        fill="#d96539"
      />
      <text
        id="_1"
        data-name="1"
        transform="translate(12.924 18)"
        fill="#d96539"
        fontSize="16"
        fontFamily="HelveticaNeue-Bold, Helvetica Neue"
        fontWeight="700"
        opacity="0"
      >
        <tspan x="-4" y="0">
          1
        </tspan>
      </text>
    </svg>
  );
}
