import React from "react";

export default function User() {
  return (
    <svg
      xmlns="http://www.w3.org/2000/svg"
      width="85"
      height="85"
      viewBox="0 0 85 85"
      role="img"
      title="My profile"
      aria-label="Link to my profile"
    >
      <defs>
        <filter
          id="Rectangle_876"
          x="0"
          y="0"
          width="85"
          height="85"
          filterUnits="userSpaceOnUse"
        >
          <feOffset dy="3" input="SourceAlpha" />
          <feGaussianBlur stdDeviation="1" result="blur" />
          <feFlood floodOpacity="0.161" />
          <feComposite operator="in" in2="blur" />
          <feComposite in="SourceGraphic" />
        </filter>
      </defs>
      <g
        id="Group_2063"
        data-name="Group 2063"
        transform="translate(-22.499 21)"
      >
        <g
          transform="matrix(1, 0, 0, 1, 22.5, -21)"
          filter="url(#Rectangle_876)"
        >
          <g
            id="Rectangle_876-2"
            data-name="Rectangle 876"
            transform="translate(3)"
            fill="#fff"
            stroke="rgba(234,235,239,0.75)"
            strokeWidth="0.5"
          >
            <rect width="79" height="79" rx="6" stroke="none" />
            <rect
              x="0.25"
              y="0.25"
              width="78.5"
              height="78.5"
              rx="5.75"
              fill="none"
            />
          </g>
        </g>
        <g id="Icon_user_outline" transform="translate(43.386 -0.404)">
          <path
            id="Shape"
            d="M8,16a8,8,0,1,1,8-8A8.009,8.009,0,0,1,8,16ZM8,4a4,4,0,1,0,4,4A4,4,0,0,0,8,4Z"
            transform="translate(13.613 -2)"
            fill="#00155f"
          />
          <path
            id="Shape-2"
            data-name="Shape"
            d="M21.613,20.5A38.458,38.458,0,0,1,9.282,18.548C6.013,17.386,1.77,15.116.122,10.9a1.778,1.778,0,0,1,0-1.3C1.733,5.48,6.515,3.155,10.244,1.93A39.769,39.769,0,0,1,21.613,0,39.769,39.769,0,0,1,32.982,1.93C36.711,3.155,41.494,5.48,43.1,9.6a1.778,1.778,0,0,1,0,1.3c-1.648,4.218-5.891,6.488-9.16,7.649A38.458,38.458,0,0,1,21.613,20.5Zm0-16.4a34.3,34.3,0,0,0-9.644,1.657C8.44,6.915,6.026,8.474,4.986,10.266c1.016,1.895,3.084,3.409,6.145,4.5A32.211,32.211,0,0,0,21.613,16.4,32.211,32.211,0,0,0,32.1,14.764c3.061-1.089,5.128-2.6,6.145-4.5-1.04-1.791-3.454-3.35-6.983-4.509A34.3,34.3,0,0,0,21.613,4.1Z"
            transform="translate(0 19.308)"
            fill="#00155f"
          />
        </g>
      </g>
    </svg>
  );
}
