import React from "react";

export default function Visa() {
  return (
    <svg
      xmlns="http://www.w3.org/2000/svg"
      width="56.086"
      height="35.054"
      viewBox="0 0 56.086 35.054"
    >
      <defs>
        <clipPath id="clipPath">
          <rect
            id="Rectangle_833"
            data-name="Rectangle 833"
            width="56.086"
            height="35.054"
            fill="none"
          />
        </clipPath>
      </defs>
      <g id="Group_906" data-name="Group 906" transform="translate(0 0)">
        <g
          id="Group_905"
          data-name="Group 905"
          transform="translate(0 0)"
          clipPath="url(#clipPath)"
        >
          <path
            id="Rectangle_832"
            data-name="Rectangle 832"
            d="M2.337,0H53.749a2.337,2.337,0,0,1,2.337,2.337V32.715a2.339,2.339,0,0,1-2.339,2.339H2.338A2.338,2.338,0,0,1,0,32.716V2.337A2.337,2.337,0,0,1,2.337,0Z"
            transform="translate(0 0)"
            fill="#164799"
          />
          <path
            id="Path_596"
            data-name="Path 596"
            d="M31.589,25.866H28.85l1.713-10.524H33.3Z"
            transform="translate(-5.779 -3.073)"
            fill="#fff"
          />
          <path
            id="Path_597"
            data-name="Path 597"
            d="M23.586,15.342,20.973,22.58l-.309-1.559h0l-.921-4.732a1.173,1.173,0,0,0-1.3-.948H14.125l-.05.178a10.23,10.23,0,0,1,2.865,1.2l2.38,9.143h2.855l4.36-10.524Z"
            transform="translate(-2.82 -3.073)"
            fill="#fff"
          />
          <path
            id="Path_598"
            data-name="Path 598"
            d="M51.105,25.867H53.62L51.428,15.343h-2.2a1.26,1.26,0,0,0-1.266.784l-4.086,9.74H46.73L47.3,24.3h3.483Zm-3.015-3.722,1.439-3.939.81,3.939Z"
            transform="translate(-8.789 -3.074)"
            fill="#fff"
          />
          <path
            id="Path_599"
            data-name="Path 599"
            d="M42.188,17.826l.39-2.26a7.9,7.9,0,0,0-2.464-.459c-1.359,0-4.589.595-4.589,3.484,0,2.719,3.79,2.752,3.79,4.18s-3.4,1.173-4.521.272l-.408,2.362A7.671,7.671,0,0,0,37.479,26c1.87,0,4.691-.968,4.691-3.6,0-2.736-3.823-2.992-3.823-4.181s2.669-1.037,3.841-.391"
            transform="translate(-6.889 -3.026)"
            fill="#fff"
          />
          <path
            id="Path_600"
            data-name="Path 600"
            d="M20.663,21.022l-.921-4.732a1.173,1.173,0,0,0-1.3-.948H14.124l-.05.178a10.615,10.615,0,0,1,4.066,2.042,8.2,8.2,0,0,1,2.524,3.46"
            transform="translate(-2.819 -3.073)"
            fill="#fc9220"
          />
        </g>
      </g>
    </svg>
  );
}
