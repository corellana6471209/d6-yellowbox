import React from "react";
import Eye from './Eye';
import BlindEye from './BlindEye';

const cross = (
  <svg
    xmlns="http://www.w3.org/2000/svg"
    width="30"
    height="30"
    viewBox="0 0 32.828 32.828"
  >
    <g id="Elem_Icon_Close" transform="translate(1.414 1.414)">
      <line
        id="Line_14"
        data-name="Line 14"
        x2="30"
        y2="30"
        fill="none"
        stroke="#d6643f"
        strokeWidth="4"
      />
      <line
        id="Line_15"
        data-name="Line 15"
        x1="30"
        y2="30"
        fill="none"
        stroke="#d6643f"
        strokeWidth="4"
      />
    </g>
  </svg>
);

const personOutline = (
  <svg
    xmlns="http://www.w3.org/2000/svg"
    id="Icon_user_outline"
    width="22.499"
    height="24"
    viewBox="0 0 22.499 25"
  >
    <path
      id="Shape"
      d="M5,10a5,5,0,1,1,5-5A5.006,5.006,0,0,1,5,10ZM5,2.5A2.5,2.5,0,1,0,7.5,5,2.5,2.5,0,0,0,5,2.5Z"
      transform="translate(6.25)"
      fill="#00155f"
    />
    <path
      id="Shape-2"
      data-name="Shape"
      d="M11.25,12.5a17.338,17.338,0,0,1-6.418-1.19A8.031,8.031,0,0,1,.064,6.645a1.258,1.258,0,0,1,0-.791C.9,3.342,3.391,1.924,5.332,1.177A17.959,17.959,0,0,1,11.25,0a17.959,17.959,0,0,1,5.918,1.177c1.941.747,4.43,2.165,5.268,4.678a1.258,1.258,0,0,1,0,.791,8.031,8.031,0,0,1-4.768,4.664A17.338,17.338,0,0,1,11.25,12.5Zm0-10A15.5,15.5,0,0,0,6.23,3.51,6.459,6.459,0,0,0,2.6,6.26,5.713,5.713,0,0,0,5.793,9a14.542,14.542,0,0,0,5.456,1,14.542,14.542,0,0,0,5.456-1A5.713,5.713,0,0,0,19.9,6.26a6.459,6.459,0,0,0-3.635-2.75A15.5,15.5,0,0,0,11.25,2.5Z"
      transform="translate(0 12.5)"
      fill="#00155f"
    />
  </svg>
);

const searchOutline = (
  <svg
    xmlns="http://www.w3.org/2000/svg"
    width="23.001"
    height="24"
    viewBox="0 0 23.001 23"
  >
    <g id="Group_1500" data-name="Group 1500" transform="translate(-30 -172)">
      <path
        id="Shape"
        d="M9.2,18.4a9.2,9.2,0,1,1,9.2-9.2A9.211,9.211,0,0,1,9.2,18.4Zm0-16.1a6.9,6.9,0,1,0,6.9,6.9A6.908,6.908,0,0,0,9.2,2.3Z"
        transform="translate(30 172)"
        fill="#00155f"
      />
      <path
        id="Path"
        d="M.337.337a1.15,1.15,0,0,1,1.626,0l6.9,6.9A1.15,1.15,0,0,1,7.237,8.863l-6.9-6.9A1.15,1.15,0,0,1,.337.337Z"
        transform="translate(43.801 185.8)"
        fill="#00155f"
      />
    </g>
  </svg>
);

const moreVertical = (
  <svg
    xmlns="http://www.w3.org/2000/svg"
    width="8"
    height="34"
    viewBox="0 0 8 34"
  >
    <g id="Group_708" data-name="Group 708" transform="translate(-699 -239)">
      <circle
        id="Ellipse_46"
        data-name="Ellipse 46"
        cx="4"
        cy="4"
        r="4"
        transform="translate(699 239)"
        fill="#00155f"
      />
      <circle
        id="Ellipse_47"
        data-name="Ellipse 47"
        cx="4"
        cy="4"
        r="4"
        transform="translate(699 252)"
        fill="#00155f"
      />
      <circle
        id="Ellipse_48"
        data-name="Ellipse 48"
        cx="4"
        cy="4"
        r="4"
        transform="translate(699 265)"
        fill="#00155f"
      />
    </g>
  </svg>
);

const logoSimply = (
  <svg
    xmlns="http://www.w3.org/2000/svg"
    xmlnsXlink="http://www.w3.org/1999/xlink"
    id="Group_1100"
    data-name="Group 1100"
    width="134.59"
    height="33.247"
    viewBox="0 0 134.59 33.247"
  >
    <defs>
      <clipPath id="clipPath">
        <rect
          id="Rectangle_912"
          data-name="Rectangle 912"
          width="134.59"
          height="33.247"
          fill="none"
        />
      </clipPath>
    </defs>
    <g id="Group_1100-2" data-name="Group 1100" clipPath="url(#clipPath)">
      <path
        id="Path_780"
        data-name="Path 780"
        d="M134.071,22.729l2.631-3.136a8.444,8.444,0,0,0,5.157,1.726c.884,0,1.263-.232,1.263-.632v-.042c0-.421-.463-.652-2.042-.968-3.3-.674-6.209-1.621-6.209-4.736V14.9c0-2.8,2.189-4.967,6.251-4.967a10.2,10.2,0,0,1,6.652,2.021l-2.4,3.325a7.727,7.727,0,0,0-4.42-1.452c-.737,0-1.073.252-1.073.61v.042c0,.4.4.653,1.958.947,3.767.695,6.293,1.789,6.293,4.757v.042c0,3.094-2.547,4.989-6.5,4.989a11.382,11.382,0,0,1-7.556-2.484"
        transform="translate(-81.352 -6.026)"
        fill="#00155f"
      />
      <rect
        id="Rectangle_911"
        data-name="Rectangle 911"
        width="4.925"
        height="14.734"
        transform="translate(68.66 4.179)"
        fill="#00155f"
      />
      <path
        id="Path_781"
        data-name="Path 781"
        d="M191.843,10.627h5.136l3.157,5.178,3.157-5.178h5.136V25.361h-4.9v-7.3L200.137,23.3h-.084l-3.389-5.241v7.3h-4.82Z"
        transform="translate(-116.408 -6.448)"
        fill="#00155f"
      />
      <path
        id="Path_782"
        data-name="Path 782"
        d="M244.929,17.847c1.263,0,2.1-.569,2.1-1.621v-.042c0-1.073-.779-1.621-2.084-1.621H243.6v3.284Zm-6.251-7.22h6.63c3.915,0,6.588,1.747,6.588,5.241v.042c0,3.557-2.716,5.452-6.694,5.452h-1.6v4h-4.925Z"
        transform="translate(-144.827 -6.448)"
        fill="#00155f"
      />
      <path
        id="Path_783"
        data-name="Path 783"
        d="M276.956,10.627h4.925V21.151h6.925v4.21h-11.85Z"
        transform="translate(-168.053 -6.448)"
        fill="#00155f"
      />
      <path
        id="Path_784"
        data-name="Path 784"
        d="M308.231,10.627l-2.547,4.841-2.526-4.841h-5.515l5.557,9.514v5.22h4.925V20.078l5.557-9.451Z"
        transform="translate(-180.606 -6.448)"
        fill="#00155f"
      />
      <path
        id="Path_785"
        data-name="Path 785"
        d="M86.7,7.967,66.747.193l3.62,10.782,18.691,3.1Z"
        transform="translate(-40.501 -0.117)"
        fill="#00155f"
      />
      <path
        id="Path_786"
        data-name="Path 786"
        d="M0,13.806l21.07-3.037L24.77,0,2.951,7.409"
        transform="translate(0 0)"
        fill="#00155f"
      />
      <path
        id="Path_787"
        data-name="Path 787"
        d="M7.515,34.174l21.934,6.435V12.133L26.858,19.84,7.515,22.234"
        transform="translate(-4.56 -7.362)"
        fill="#00155f"
      />
      <path
        id="Path_788"
        data-name="Path 788"
        d="M68.975,19.967,66.54,12.259V40.635l8.843-2.988V27.372l4.695-.272v8.962l6.5-2.2V22.438Z"
        transform="translate(-40.375 -7.439)"
        fill="#00155f"
      />
      <path
        id="Path_789"
        data-name="Path 789"
        d="M134.541,62.968l1.1-1.315a3.675,3.675,0,0,0,2.314.856c.53,0,.815-.184.815-.489V62c0-.3-.234-.459-1.2-.683-1.519-.346-2.691-.775-2.691-2.242v-.02c0-1.325,1.05-2.283,2.762-2.283a4.491,4.491,0,0,1,2.935.948l-.989,1.4a3.578,3.578,0,0,0-2-.7c-.479,0-.714.2-.714.458v.02c0,.326.245.469,1.233.693,1.641.357,2.66.887,2.66,2.222v.02c0,1.458-1.152,2.324-2.884,2.324a4.983,4.983,0,0,1-3.343-1.182"
        transform="translate(-81.638 -34.448)"
        fill="#00155f"
      />
      <path
        id="Path_790"
        data-name="Path 790"
        d="M152.816,57.083h5.738v1.682h-3.781v1.08H158.2V61.4h-3.424v1.131H158.6v1.682h-5.789Z"
        transform="translate(-92.727 -34.637)"
        fill="#00155f"
      />
      <path
        id="Path_791"
        data-name="Path 791"
        d="M170.106,57.083h1.977v5.4h3.455v1.732h-5.432Z"
        transform="translate(-103.218 -34.637)"
        fill="#00155f"
      />
      <path
        id="Path_792"
        data-name="Path 792"
        d="M186.074,57.083h5.707v1.733h-3.73v1.213h3.373v1.641h-3.373v2.548h-1.977Z"
        transform="translate(-112.907 -34.637)"
        fill="#00155f"
      />
      <path
        id="Path_793"
        data-name="Path 793"
        d="M208.678,62.968l1.1-1.315a3.674,3.674,0,0,0,2.314.856c.53,0,.815-.184.815-.489V62c0-.3-.234-.459-1.2-.683-1.519-.346-2.691-.775-2.691-2.242v-.02c0-1.325,1.05-2.283,2.762-2.283a4.491,4.491,0,0,1,2.935.948l-.989,1.4a3.578,3.578,0,0,0-2-.7c-.479,0-.714.2-.714.458v.02c0,.326.245.469,1.233.693,1.641.357,2.66.887,2.66,2.222v.02c0,1.458-1.152,2.324-2.884,2.324a4.983,4.983,0,0,1-3.343-1.182"
        transform="translate(-126.623 -34.448)"
        fill="#00155f"
      />
      <path
        id="Path_794"
        data-name="Path 794"
        d="M227.564,58.816h-2.14V57.083h6.258v1.733h-2.14v5.4h-1.977Z"
        transform="translate(-136.784 -34.637)"
        fill="#00155f"
      />
      <path
        id="Path_795"
        data-name="Path 795"
        d="M242.143,60.45v-.02a3.845,3.845,0,0,1,7.684-.02v.02a3.845,3.845,0,0,1-7.684.02m5.666,0v-.02a1.854,1.854,0,0,0-1.834-1.926,1.818,1.818,0,0,0-1.8,1.906v.02a1.852,1.852,0,0,0,1.824,1.926,1.82,1.82,0,0,0,1.814-1.906"
        transform="translate(-146.929 -34.417)"
        fill="#00155f"
      />
      <path
        id="Path_796"
        data-name="Path 796"
        d="M264.306,57.084h3.373a3.114,3.114,0,0,1,2.324.775,2.211,2.211,0,0,1,.632,1.661v.02a2.256,2.256,0,0,1-1.468,2.191l1.7,2.487h-2.283l-1.437-2.16h-.866v2.16h-1.977Zm3.281,3.424c.672,0,1.06-.326,1.06-.846v-.02c0-.561-.408-.846-1.07-.846h-1.294v1.712Z"
        transform="translate(-160.377 -34.638)"
        fill="#00155f"
      />
      <path
        id="Path_797"
        data-name="Path 797"
        d="M284.219,56.954h1.906l3.037,7.185h-2.12l-.519-1.274h-2.752l-.51,1.274h-2.079Zm1.732,4.372-.795-2.028-.805,2.028Z"
        transform="translate(-170.618 -34.559)"
        fill="#00155f"
      />
      <path
        id="Path_798"
        data-name="Path 798"
        d="M301.455,60.45v-.02a3.7,3.7,0,0,1,3.842-3.71,4.161,4.161,0,0,1,2.894,1.04l-1.162,1.4a2.517,2.517,0,0,0-1.723-.673,1.874,1.874,0,0,0-1.824,1.957v.02a1.884,1.884,0,0,0,1.937,1.977,2.018,2.018,0,0,0,1.142-.3v-.866h-1.407V59.828h3.282v3.21a4.824,4.824,0,0,1-3.1,1.1,3.668,3.668,0,0,1-3.883-3.689"
        transform="translate(-182.919 -34.417)"
        fill="#00155f"
      />
      <path
        id="Path_799"
        data-name="Path 799"
        d="M322.012,57.083h5.738v1.682h-3.781v1.08h3.424V61.4h-3.424v1.131H327.8v1.682h-5.788Z"
        transform="translate(-195.392 -34.637)"
        fill="#00155f"
      />
      <path
        id="Path_800"
        data-name="Path 800"
        d="M338.684,72.735a.881.881,0,1,1,.881-.881.882.882,0,0,1-.881.881m0-1.583a.7.7,0,1,0,.7.7.7.7,0,0,0-.7-.7"
        transform="translate(-204.974 -43.065)"
        fill="#00155f"
      />
      <path
        id="Path_801"
        data-name="Path 801"
        d="M339.671,72.957l-.188-.281h-.152v.281h-.193v-.878h.4c.207,0,.331.109.331.29v0a.273.273,0,0,1-.188.272l.214.313Zm0-.578c0-.083-.057-.125-.152-.125h-.192v.252h.2c.094,0,.148-.05.148-.124Z"
        transform="translate(-205.784 -43.736)"
        fill="#00155f"
      />
    </g>
  </svg>
);

const pin = (
  <svg
    xmlns="http://www.w3.org/2000/svg"
    id="Elem_Icon_LocationMarker"
    width="25.848"
    height="37"
    viewBox="0 0 25.848 37"
  >
    <path
      id="Path_34"
      data-name="Path 34"
      d="M12.924,0A12.924,12.924,0,0,1,25.848,12.924c0,3.191-1.226,4.849-3.21,8.525-1.9,3.7-10.175,15.531-9.714,15.552C12.691,37,5.1,23.9,2.609,20.711,1.176,17.842,0,15.922,0,12.924A12.924,12.924,0,0,1,12.924,0Z"
      fill="#d96539"
    />
    <text
      id="_1"
      data-name="1"
      transform="translate(12.924 18)"
      fill="#d96539"
      fontSize="16"
      fontFamily="HelveticaNeue-Bold, Helvetica Neue"
      fontWeight="700"
      opacity="0"
    >
      <tspan x="-4" y="0">
        1
      </tspan>
    </text>
  </svg>
);

/**
 *
 * @param {props} props props recieved
 * @returns main Icon
 */
function Icon(props: any) {
  function svgFile(iconName: string) {
    switch (iconName) {
      case "cross":
        return cross;
      case "personOutline":
        return personOutline;
      case "searchOutline":
        return searchOutline;
      case "moreVertical":
        return moreVertical;
      case "logoSimply":
        return logoSimply;
      case "pin":
        return pin;
      case "eye":
        return Eye;
      case "blindEye":
        return BlindEye;
      default:
        return cross;
    }
  }

  return (
    <div
      className={`icon-svg ${props.classIcon ? props.classIcon : ""}`}
      style={props.style}
      onClick={props.onClick}
    >
      {svgFile(props.iconName)}
    </div>
  );
}

export default Icon;
