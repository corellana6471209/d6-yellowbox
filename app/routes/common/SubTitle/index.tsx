import PropTypes from 'prop-types';

/**
 * Main app function
 * @param {props} props props recieved
 * @returns {function} main app
 */

const SubTitle = (props: any) => {
  const { text } = props; 
  return (
    <h2 className='SubTitle-root'>{text}</h2>
  );
};

SubTitle.propTypes = {
  text: PropTypes.string.isRequired
};

SubTitle.defaultProps = {
  text: '',
};

export default SubTitle;