import * as PropTypes from 'prop-types';

/**
 * Main app function
 * @param {props} props props recieved
 * @returns {function} main app
 */
function Link(props: any) {
    const { href, title, target, className } = props;

    return (
        <a
            rel={target === '_blank' ? 'noreferrer' : ''}
            target={target}
            href={href}
            className={className}
        >
            {title}
        </a>
    );
}

// Specifies the default values for props:
Link.defaultProps = {
    href: '#',
    target: '_self',
    title: 'Link Title',
    className: 'p-5',
};

// Specifies the proptypes:
Link.propTypes = {
    href: PropTypes.string,
    target: PropTypes.string,
    title: PropTypes.string,
    className: PropTypes.string,
};

export default Link;
