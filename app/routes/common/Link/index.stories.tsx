// YourComponent.stories.ts|tsx

import { ComponentStory, ComponentMeta } from '@storybook/react';

import Link from './index';

export default {
    title: 'components/core/Link',
    component: Link,
} as ComponentMeta<typeof Link>;

const Template: ComponentStory<typeof Link> = (args) => <Link {...args} />;

export const Generic = Template.bind({});
Generic.args = {
    href: '#',
    target: '_blank',
    title: 'This is the title',
};
