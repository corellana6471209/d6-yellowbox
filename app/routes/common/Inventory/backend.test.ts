import { describe, expect, it, vi } from "vitest";
import { UnitsInformationByUnitID } from './backend';
import { render, screen } from '@testing-library/react';


const obj = {

    UnitsInformationByUnitID: () => "UnitsInformationByUnitID",
}

vi.mock('./backend', () => ({
    UnitsInformationByUnitID: vi.fn().mockImplementation(() => "UnitsInformationByUnitID"),
}));



describe("fetching location data component unit test", () => {


    it('LoadModuleTitle should be mock', () => {
        expect(vi.isMockFunction(UnitsInformationByUnitID)).toBe(true)
    });

    it('Should be executed loadModuleTitle', async () => {
        const spy = vi.spyOn(obj, 'UnitsInformationByUnitID');

        const resp = await UnitsInformationByUnitID("5UWRwDi68t5buzEh3L042y");
        expect(spy.getMockName()).toEqual('UnitsInformationByUnitID');
        expect(resp).toBe("UnitsInformationByUnitID");
    });

});