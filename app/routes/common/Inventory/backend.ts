import { responseObject } from "~/routes/api/shared/prepareSimplyFetch";
import fetchCustomApi from "~/utils/fetchCustomApi";

export const UnitsInformationByUnitID = async (value: any) => {
    const data: responseObject = await fetchCustomApi(
        `/api/Inventory/UnitsInformationByUnitID/${value}`
    );
}

