import axios from "axios";

/**
 * @param params
 * @returns array
 */

export const handlerResponse = (predictions: any) => {
  if (!predictions) return [];

  const predictionsArray = predictions;
  const data = predictionsArray.map((prediction: any) => {
    return {
      name: prediction.description,
      placeId: prediction.place_id,
    };
  });

  return data;
};

export const searchAutocomplete = async (value: string) => {
  var theResponse = null;
  try {
    theResponse = await axios
      .get(`/api/content/common/search/searchAutocomplete/${value}`)
      .then((theResponse: any) => {
        return theResponse.data;
      });
  } catch (error) {
    return error;
  }
  return theResponse;
};

export const getCoords = async (data: string) => {
  var theResponse = null;
  try {
    theResponse = await axios
      .get(`/api/content/common/search/getCoords/${data}`)
      .then((theResponse: any) => {
        return theResponse.data;
      });
  } catch (error) {
    return error;
  }
  return theResponse;
};
