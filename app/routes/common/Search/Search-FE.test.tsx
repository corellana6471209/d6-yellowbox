import { describe, it } from "vitest";
import Search from ".";
import { render } from "../../../utils/test-utils";
import { BrowserRouter as Router } from "react-router-dom";

// Only verify if the element render, this component has a combination of import that is very-hard to mock
describe("Search component", () => {
  it("Verify if the component renders", () => {
    render(
      <Router>
        <Search />
      </Router>
    );
  });
});
