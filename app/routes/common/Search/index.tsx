import { useState, useRef, useReducer, useEffect } from "react";
import { useContext } from "react";
import { PageDetailsContext } from "../../../utils/context/PageDeatilsContext";

import PropTypes from "prop-types";
import { useMediaQuery } from "react-responsive";
// import { useNavigate } from "react-router-dom";
import Icon from "../svg/Icon";
import { getLocations } from "../../../routes/page-components/LocationMapList/backend";
import { handlerResponse } from "./backend";
import { getAddress, getAddressCoords } from "../../../utils/cookie";
import { boldify } from "../../../utils/boldify";
import { useOutsideAlerter } from "../../../utils/hooks/useOutsideAlerter";
import { useKeyPress } from "../../../utils/hooks/useKeyPress";
import { statesName } from "../../../utils/fakedataToTests";
import { getGeoLocationInformation } from "../../../utils/getGeoLocation";
import { useWindowScrollPositions } from "../../../utils/hooks/useScroll";
import { searchAutocomplete, getCoords } from "./backend";

const initialState = { selectedIndex: 0 };

/**
 *
 * @param {props} props props recieved
 * @returns {Function} main Search
 */
function Search(props: any) {
  const { overlayCloseBtn, showCloseBtn, name, show, config, header } = props;
  const { scrollY } = useWindowScrollPositions();
  const [scrollPosition, setScrollPosition] = useState(window.pageYOffset);
  const [writing, setWriting] = useState(false);
  const PageDetails = useContext(PageDetailsContext);

  const isMobile = useMediaQuery({ query: "(max-width: 768px)" });
  const wrapperRef = useRef(null);
  const [states, setStates] = useState<any[]>([]);
  const [value, setValue] = useState<any>("");
  const [filtering, setFiltering] = useState<boolean>(false);
  const [validValueSelected, setValidValueSelected] = useState<boolean>(false);
  const [messageError, setMessageError] = useState<any>(null);
  const arrowUpPressed = useKeyPress("ArrowUp");
  const arrowDownPressed = useKeyPress("ArrowDown");
  useOutsideAlerter(wrapperRef, setStates);

  // const navigate = useNavigate();

  useOutsideAlerter(wrapperRef, () => {
    setStates([]);
  });

  /******************************************************* Accessibility logic*****************************************/

  const reducer = (state: any, action: any) => {
    switch (action.type) {
      case "arrowUp":
        return {
          selectedIndex:
            state.selectedIndex !== 0
              ? state.selectedIndex - 1
              : states?.length - 1,
        };
      case "arrowDown":
        return {
          selectedIndex:
            state.selectedIndex !== states?.length - 1
              ? state.selectedIndex + 1
              : 0,
        };

      case "default":
        return { selectedIndex: 0 };
      default:
        throw new Error();
    }
  };

  const [stateSelected, dispatch] = useReducer(reducer, initialState);

  useEffect(() => {
    if (arrowUpPressed) {
      dispatch({ type: "arrowUp" });
    }
  }, [arrowUpPressed]);

  useEffect(() => {
    if (arrowDownPressed) {
      dispatch({ type: "arrowDown" });
    }
  }, [arrowDownPressed]);

  const selectValue = (e: any) => {
    //Function to get the value from the list using drop/up/enter key
    if (e.key === "Enter" && states?.length > 0) {
      setValue(states[stateSelected.selectedIndex].name);
      dispatch({ type: "default" });
      setValidValueSelected(true);
      setFiltering(true);
      setStates([]);
      e.preventDefault();
      routing(states[stateSelected.selectedIndex].name);
    }
  };

  /******************************************************* Accessibility logic*****************************************/

  /******************************************************* Get Location logic*****************************************/
  const getLocation = async () => {
    setStates([]);
    const cookie = getAddress();
    if (cookie) {
      setValue(cookie);
      setValidValueSelected(true);
      setFiltering(true);
      setMessageError(null);
      routing(cookie);
    } else {
      if (
        confirm(
          !isMobile
            ? "SimplySS wants to know your location."
            : "SimplySS would like to use your current location. This site will use your precise location because <browser> already has access to this information."
        )
      ) {
        await getGeoLocationInformation();
        const cookie = getAddress();
        const coords = getAddressCoords();
        if (coords) {
          PageDetails?.address.setAddress(coords);
        }
        if (cookie) {
          setValue(cookie);
          setValidValueSelected(true);
          setFiltering(true);
          setMessageError(null);
          routing(cookie);
        } else {
          setMessageError(
            "An error a ocurred getting your address, please try later."
          );
        }
      } else {
        setMessageError(
          "To locate you, location services must be turned on for this site."
        );
      }
    }
  };
  /******************************************************* Get Location logic*****************************************/

  /******************************************************* Set value states *****************************************/

  const setStateOnFilter = (state: string) => {
    setValue(state);
    setValidValueSelected(true);
    setFiltering(true);
    setStates([]);
  };

  /******************************************************* Set value states *****************************************/

  /******************************************************* Get value from input and set it*****************************************/

  const getFilter = async (event: any) => {
    dispatch({ type: "default" });
    setValue(event.target.value);
    if (!writing) {
      setWriting(true);
      setFiltering(true);
      setMessageError(null);
      if (event.target.value !== "") {
        const theResponse: any = await searchAutocomplete(event.target.value);

        if (theResponse.predictions) {
          const data = handlerResponse(theResponse.predictions);
          setStates(data);
          setFiltering(false);
        } else {
          setStates([]);
          setFiltering(false);
        }
        setWriting(false);
      } else {
        setStates([]);
        setValidValueSelected(false);
        setWriting(false);
      }
    }
  };

  /******************************************************* Get value from input and set it*****************************************/

  /*******************************************************Filter and routing logic*****************************************/

  const sendFilters = async (event: any) => {
    event.preventDefault();
    if (validValueSelected || (states && states?.length > 0)) {
      if (states && states?.length > 0) {
        const data = states[0].name;
        //Code to verify if is state or city or are using name abbreviation
        routing(data);
      } else {
        if (!value) {
          setMessageError(
            "Enter a location or use your device’s current location."
          );

          setTimeout(() => {
            getLocation();
          }, 1000);

          return;
        } else {
          //Code to verify if is state or city or are using name abbreviation
          routing(value);
        }
      }
    } else {
      if (value) {
        setMessageError(
          "Enter a location or use your device’s current location."
        );
        setFiltering(true);
      } else {
        const cookie = getAddress();
        if (!cookie) {
          setMessageError(
            "Enter a location or use your device’s current location."
          );
        }

        setTimeout(() => {
          getLocation();
        }, 1000);
      }
    }
  };

  const routing = async (data: string) => {
    let isState = null;
    try {
      if (data && (data.length === 2 || data.length === 1)) {
        isState = statesName.find((state) =>
          state.abbreviation.toLowerCase().includes(data.toLowerCase())
        );
      } else {
        isState = statesName.find((state) =>
          state.name
            .toLowerCase()
            .includes(data.toLowerCase().split(",")[0].substr(0, 6))
        );
      }

      if (isState) {
        // navigate(`locations/state/${isState.abbreviation}`);
        window.location.href = `/locations/state/${isState.abbreviation}`;
      } else {
        const coords = await fetchCoords(data);
        if (coords) {
          verifyLocations(coords, data);
        } else {
          // navigate("/no-locations");
          window.location.href = "/no-locations";
        }
      }
    } catch (error) {
      // navigate("/no-locations");
      window.location.href = "/no-locations";
    }
  };

  const fetchCoords = async (data: string) => {
    const response = await getCoords(data);
    if (response) {
      if (response.status === "OK") {
        return response.results[0].geometry.location;
      } else {
        return response;
      }
    } else {
      return null;
    }
  };

  const verifyLocations = async (coords: any, data: string) => {
    let loc: any[] = [];
    let radius = 25 / 0.62137;
    try {
      if (coords.status !== "ZERO_RESULTS") {
        do {
          let l = await getLocations(coords, radius, data);
          loc = l.contentTypeLocationCollection.items;
          radius = radius + 25 / 0.62137;
        } while (loc.length === 0 && radius <= 250 / 0.62137);

        if (loc && loc.length > 0) {
          // navigate(`/locations?s=${data}`);
          window.location.href = `/locations?s=${data}`;
        } else {
          // navigate("/no-locations");
          window.location.href = "/no-locations";
        }
        return;
      }

      // navigate("/no-locations");
      window.location.href = "/no-locations";
    } catch (error) {
      // navigate("/no-locations");
      window.location.href = "/no-locations";
    }
  };

  /*******************************************************Filter and routing logic*****************************************/

  /****************************Scroll Event****************************** */

  useEffect(() => {
    const distance = Math.abs(scrollPosition - scrollY);
    if (distance > 40) {
      setFiltering(true);
      setWriting(false);
      //setMessageError(null);
      if (!value) {
        setValidValueSelected(false);
      }
      setScrollPosition(scrollY);
    }
  }, [scrollY]);

  /*********************Use Context *********************/

  return (
    <>
      <div
        ref={wrapperRef}
        aria-label="form-search"
        className={`overlay__wrapper form-search ${
          show ? "ease-in" : "ease-out "
        }`}
      >
        <div className="container">
          {showCloseBtn && overlayCloseBtn(name)}

          <form
            className={` form-customizer max-w-[1200px] ${
              messageError && "has-error"
            }`}
            style={{ padding: "5px" }}
          >
            <div className="form-group">
              <Icon iconName="pin" onClick={() => getLocation()} />
              <input
                className="form-search__input"
                name="location"
                placeholder={
                  config ? config.searchHelperText : "Enter Zip, City or State"
                }
                value={value}
                autoComplete="off"
                onChange={(e) => getFilter(e)}
                onKeyPress={(e) => selectValue(e)}
                style={{
                  fontSize: "16px !important",
                  fontWeight: "bold !important",
                }}
              />

              {value && states?.length > 0 && (
                <div className="form-search-states-list bg-white p-3">
                  <ul tabIndex={-1}>
                    {states.map((state, i) => (
                      <li
                        className={`li-suggestion-results ${
                          i === stateSelected.selectedIndex &&
                          "li-suggestion-results-selected"
                        }`}
                        key={state.placeId}
                        onClick={() => setStateOnFilter(state.name)}
                      >
                        {boldify(value, state.name)}
                      </li>
                    ))}
                  </ul>
                </div>
              )}

              {!filtering && value && states?.length === 0 && (
                <div className="form-search-states-list bg-white p-3">
                  <ul>
                    <li className="li-suggestion-results">
                      No Matching Results Found
                    </li>
                  </ul>
                </div>
              )}
            </div>

            <button
              className={`btn-primary yellow-button font-normal text-secondary1 ${
                header ? "text-16" : "text-20"
              }`}
              onClick={sendFilters}
            >
              {config ? config.searchButtonText : "Find Storage"}
            </button>
          </form>

          {messageError ? (
            <p className="message-error text-left">{messageError}</p>
          ) : (
            ""
          )}
        </div>
      </div>
    </>
  );
}

Search.propTypes = {
  iconColor: PropTypes.string,
  overlayCloseBtn: PropTypes.func,
  showCloseBtn: PropTypes.bool,
  name: PropTypes.string,
  show: PropTypes.bool,
  toggleNav: PropTypes.any,
  config: PropTypes.object,
  isMobile: PropTypes.bool,
  scrollY: PropTypes.any,
  header: PropTypes.bool,
};

Search.defaultProps = {
  overlayCloseBtn: () => {
    return false;
  },
  showCloseBtn: false,
  show: true,
  header: false,
  isMobile: false,
  scrollY: 0,
  config: {
    searchButtonText: "Find Storage",
    searchHelperText: "Enter Zip, City or State",
  },
};

export default Search;
