import * as PropTypes from "prop-types";

/**
 * Main app function
 * @param {props} props props recieved
 * @returns {function} main app
 */
function Button(props: any) {
  const { href, title, target, buttonStyle, additionalClass } = props;
  let classList;
  switch (buttonStyle) {
    case "button-primary":
      classList = `no-underline bg-primary1 py-3 px-10 text-secondary1 yellow-button ${additionalClass}`;
      break;
    case "button-primary-expanded":
      classList = `no-underline bg-primary1 py-3 block text-center text-secondary1 yellow-button ${additionalClass}`;
      break;
    case "button-tertiary-inverted":
      classList = `no-underline border-2 border-blueDark py-3 text-center px-10 text-blueDark ${additionalClass}`;
      break;
    default:
      classList = "link";
      break;
  }
  return (
    <a
      rel={target === "_blank" ? "noreferrer" : ""}
      target={target}
      href={href}
      className={classList}
    >
      {title}
    </a>
  );
}

// Specifies the default values for props:
Button.defaultProps = {
  href: "#",
  target: "_self",
  title: "Link Title",
  buttonStyle: "link",
  additionalClass: "",
};

// Specifies the proptypes:
Button.propTypes = {
  href: PropTypes.string,
  target: PropTypes.string,
  title: PropTypes.string,
  buttonStyle: PropTypes.string,
  additionalClass: PropTypes.string,
};

export default Button;
