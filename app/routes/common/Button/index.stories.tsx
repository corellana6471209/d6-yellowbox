import { ComponentStory, ComponentMeta } from '@storybook/react';
import Button from './index';

export default {
    title: 'components/core/Button',
    component: Button,
} as ComponentMeta<typeof Button>;

const Template: ComponentStory<typeof Button> = (args) => <Button {...args} />;

export const Primary = Template.bind({});
Primary.args = {
    href: '#',
    target: '_blank',
    title: 'Primary button',
    buttonStyle: 'button-primary',
};


export const PrimaryExpanded = Template.bind({});
PrimaryExpanded.args = {
    href: '#',
    target: '_blank',
    title: 'Primary Expanded button',
    buttonStyle: 'button-primary-expanded',
};

