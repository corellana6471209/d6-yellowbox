import React from "react";
import { render, screen } from "@testing-library/react";
import "@testing-library/jest-dom";
import Button from ".";

const args = {
  href: "https://google.com",
  target: "_self",
  title: "Button Title",
  buttonStyle: "link",
};

describe("Button component unit test", () => {
  test("verify rendering of the Button component", () => {
    render(<Button {...args} />);
  });

  test("Verify Button the title", async () => {
    render(<Button {...args} />);
    expect(await screen.findByText(/Button Title/i)).toBeInTheDocument();
  });
});
