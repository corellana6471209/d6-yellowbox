import { describe, expect, it, vi } from "vitest";
import { setMoveInReservation,Reservation } from './backend';
import { render, screen } from '@testing-library/react';


const obj = {

    setMoveInReservation: () => "setMoveInReservation",
}

vi.mock('./backend', () => ({
    setMoveInReservation: vi.fn().mockImplementation(() => "setMoveInReservation"),
}));



describe("fetching location data component unit test", () => {


    it('LoadModuleTitle should be mock', () => {
        expect(vi.isMockFunction(setMoveInReservation)).toBe(true)
    });

    it('Should be executed loadModuleTitle', async () => {
        const spy = vi.spyOn(obj, 'setMoveInReservation');


        const reservation: Reservation = {
            locationID: "Test",
            waitingID: 0,
            tenantID: 0,
            unitID: 0,
            startDate: "Test",
            endDate: "Test",
            paymentMethod: 0,
            paymentAmount: 0,
            creditCardType: 0,
            creditCardNumber: "Test",
            creditCardCVV: "Test",
            creditCardExpirationDate: "Test",
            creditCardBillingName: "Test",
            creditCardBillingAddress: "Test",
            creditCardBillingZipCode: "Test",
            insuranceCoverageID: 0,
            concessionPlanID: 0,
            moveInSourceType: 0,
            testMode: 0,
            billingFrequency: 0
        }


        const resp = await setMoveInReservation(reservation);
        expect(spy.getMockName()).toEqual('setMoveInReservation');
        expect(resp).toBe("setMoveInReservation");
    });

});