import { prepareFetch } from "~/routes/api/shared/prepareSimplyFetch";

export interface Reservation {
    "locationID": string;
    "waitingID": number;
    "tenantID": number;
    "unitID": number;
    "startDate": string;
    "endDate": string;
    "paymentMethod": number;
    "paymentAmount": number;
    "creditCardType": number;
    "creditCardNumber": string;
    "creditCardCVV": string;
    "creditCardExpirationDate": string;
    "creditCardBillingName": string;
    "creditCardBillingAddress": string;
    "creditCardBillingZipCode": string;
    "insuranceCoverageID": number;
    "concessionPlanID": number;
    "moveInSourceType": number;
    "testMode": number;
    "billingFrequency": number;
}
export const setMoveInReservation = async (form: Reservation) => {
    const loged = await prepareFetch('POST', '/Rental/MoveInReservation', JSON.stringify(form));
    const data = loged?.data;
    return data;
}
