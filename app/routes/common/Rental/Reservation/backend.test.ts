import { describe, expect, it, vi } from "vitest";
import { reservationlistbytenantid } from './backend';


const obj = {

    reservationlistbytenantid: () => "reservationlistbytenantid",
}

vi.mock('./backend', () => ({
    reservationlistbytenantid: vi.fn().mockImplementation(() => "reservationlistbytenantid"),
}));



describe("fetching location data component unit test", () => {


    it('LoadModuleTitle should be mock', () => {
        expect(vi.isMockFunction(reservationlistbytenantid)).toBe(true)
    });

    it('Should be executed loadModuleTitle', async () => {
        const spy = vi.spyOn(obj, 'reservationlistbytenantid');

        const resp = await reservationlistbytenantid("5UWRwDi68t5buzEh3L042y");
        expect(spy.getMockName()).toEqual('reservationlistbytenantid');
        expect(resp).toBe("reservationlistbytenantid");
    });

});