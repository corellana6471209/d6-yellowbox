import { responseObject } from "~/routes/api/shared/prepareSimplyFetch";
import fetchCustomApi from "~/utils/fetchCustomApi";



export const reservationlistbytenantid = async (information: any) => {
    const data: responseObject = await fetchCustomApi(
        `/api/Reservationlistbytenantid/[${information}]`
    );

    return data;
};
