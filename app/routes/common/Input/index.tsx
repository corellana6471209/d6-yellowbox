import Icon from '../svg/Icon';

// Input module shape
interface Module {
  type: string;
  name: string;
  disabled: boolean;
  placeholder: string;
  ariaLabel?: string;
  iconName?: string;
  onClick?: any;
}

const Input = (props: Module) => {

  const { ariaLabel, iconName, onClick, ...rest} = props;

  return (
    <div className='Input-root'>
      <input className='Input-input' aria-label={ariaLabel} {...rest}/>
      {iconName && <Icon iconName={iconName} classIcon='Input-icon' {...(onClick) && { onClick: onClick }}/>}
    </div>
  )
}

Input.defaultProps = {
  type: 'text',
  name: 'name',
  disabled: false,
  placeholder: '',
  ariaLabel: '',
  iconName: ''
};

export default Input;