import React from "react";
import { documentToReactComponents } from "@contentful/rich-text-react-renderer";
import { INLINES } from "@contentful/rich-text-types";
import type { Hyperlink } from "@contentful/rich-text-types";

function NewLetter(props: any) {
  //const [enablePopUp, setEnablePopUp] = useState<any>(null);

  const renderOptions: any = {
    renderNode: {
      [INLINES.HYPERLINK]: ({ data }: Hyperlink, children: []) => {
        return (
          <a
            className="text-callout"
            href={data.uri}
            target={`${data.uri ? "_self" : "_blank"}`}
            rel={`${data.uri ? "" : "noopener noreferrer"}`}
          >
            {children}
          </a>
        );
      },
    },
  };

  const renderDescription = () => {
    return (
      <>
        {props.data?.content?.json?.content.map((item: any, index: number) => (
          <div key={index} className="mb-3">
            {documentToReactComponents(item, renderOptions)}
          </div>
        ))}
      </>
    );
  };

  return (
    <div className="col-span-2 md:flex md:items-center lg:pl-8 lg:pr-8">
      <div>
        {renderDescription()}

        {/**<div className="relative">
          <div className="div-input">
            <input
              name="email"
              className="input w-100"
              placeholder="Email address"
            />
          </div>
          <button className="button-sign-up absolute bg-primary1 yellow-button text-secondary1">
            Sign Up
          </button>
        </div> */}
      </div>
    </div>
  );
}

export default NewLetter;
