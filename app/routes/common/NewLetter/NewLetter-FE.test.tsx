import { vi, describe, it } from "vitest";
import NewLetter from ".";
import { render } from "../../../utils/test-utils";

vi.mock("./LocationListItem", () => {
  return {
    default: "location-list-componente-string",
  };
});

describe("NewLetter component FE", () => {
  it("Verify if the component renders", () => {
    render(<NewLetter />);
  });

  it("Verify if div element exist", async () => {
    const { container } = render(<NewLetter />);
    expect(await container.getElementsByClassName("col-span-2").length).toBe(1);
  });
});
