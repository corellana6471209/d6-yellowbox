import { describe, expect, it, vi } from "vitest";
import { setChangePassword } from './backend';
import { render, screen } from '@testing-library/react';


const obj = {

    setChangePassword: () => "setChangePassword",
}

vi.mock('./backend', () => ({
    setChangePassword: vi.fn().mockImplementation(() => "setChangePassword"),
}));



describe("fetching location data component unit test", () => {


    it('LoadModuleTitle should be mock', () => {
        expect(vi.isMockFunction(setChangePassword)).toBe(true)
    });

    it('Should be executed loadModuleTitle', async () => {
        const spy = vi.spyOn(obj, 'setChangePassword');



        const resp = await setChangePassword(
            "emailAddress",
            "newPassword",
            "oldPassword",
            "idToken",
            "accessToken",
            "refreshToken",
            "expirationTime",
            "issuedTime"
        );
        expect(spy.getMockName()).toEqual('setChangePassword');
        expect(resp).toBe("setChangePassword");
    });

});