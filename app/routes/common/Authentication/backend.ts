import { prepareFetch } from '~/routes/api/shared/prepareSimplyFetch';
import fetchContentful from '../../../utils/fetchContentful';

export async function setChangePassword(
    emailAddress: string,
    newPassword: string,
    oldPassword: string,
    idToken: string,
    accessToken: string,
    refreshToken: string,
    expirationTime: string,
    issuedTime: string,
) {


    var jsonData = {
        "emailAddress": emailAddress,
        "newPassword": newPassword,
        "oldPassword": oldPassword,
        "userSession": {
            "idToken": idToken,
            "accessToken": accessToken,
            "refreshToken": refreshToken,
            "expirationTime": expirationTime,
            "issuedTime": issuedTime,
        }

    }
    
    const loged = await prepareFetch('POST', 'Authentication/ChangePassword', JSON.stringify(jsonData));
    const data = loged?.data;
    return data;
}

