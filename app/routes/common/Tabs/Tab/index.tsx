const Tab = (props: any) => {
  const { children } = props;
  return (
    <span>{children}</span>
  )
};

export default Tab;