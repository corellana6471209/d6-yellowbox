import { Children, cloneElement } from "react";
import { Link } from "@remix-run/react";

const TabTitle = (props: any) => {
  const { title, to, isSelected } = props;

  return (
    <Link to={to}  className={`TabTitle-root ${isSelected ? 'TabTitle-root-selected' : ''}`}>
      <span className='TabTitle-span'>{title}</span>
    </Link>
  )
};

export default TabTitle;