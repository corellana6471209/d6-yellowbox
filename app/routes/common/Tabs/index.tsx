import { Children } from "react";
import { useLocation } from "@remix-run/react";

import TabTitle from './TabTitle';

const Tabs = (props: any) => {
  const { children } = props;
  const location = useLocation();
  const path = location.pathname;
  const queryParams = location.search;

  return (
    <>
      <div className='Tabs-root'>
        {Children.map(children, (child, index) => (
          <TabTitle
            key={index}
            title={child.props.title}
            to={`${child.props.to}${queryParams}`}
            isSelected={child.props.to === path}
          />
        ))}
      </div>
    </>
  )
};

export default Tabs;