import React, { useState, useEffect } from "react";

import spinnerGif from "~/resources/spinner.gif";

const Spinner = (props: any) => {
  const { notDelay } = props;
  const [showSpinner, setShowSpinner] = useState<any>(false);

  useEffect(() => {
    setTimeout(() => {
      setShowSpinner(true);
    }, 2500);
  }, []);

  if (notDelay) {
    return (
      <div className="div-custom-spinner">
        <img src={spinnerGif} alt="loading..." />
      </div>
    );
  } else {
    if (showSpinner) {
      return (
        <div className="div-custom-spinner">
          <img src={spinnerGif} alt="loading..." />
        </div>
      );
    } else {
      return <></>;
    }
  }
};

export default Spinner;
