import React from "react";
import { useState } from "react";
import type { ListItems } from "~/routes/page-components/LocationMapList";

interface Props {
  param: string;
}

export default function LocationMapList(props: Props) {
  const { param } = props;
  const [locations, setLocations] = useState<ListItems[]>([]);
  return (
    <>
      <h1 className="location-title mt-[40px] mb-[20px] text-center font-normal">
        Locations {String(param) ? `in ` : ""}
        {String(param) ? (
          <span className="mt-[20px] capitalize text-secondary2">{`"${String(
            param || ""
          )}"`}</span>
        ) : (
          ""
        )}
      </h1>
      {/* <h2 className={`${width > 479 && "pb-8"} text-center text-24 font-bold`}>
        {`${locations?.length} Result${locations?.length > 1 ? "s" : ""}`}
      </h2> */}
      {/*
      <div className="div-filters-selected  flex flex-wrap sm:hidden">
        {filtersChecked && filtersChecked.length > 0 && (
          <>
            {filtersChecked.map((filter: any) => (
              <span
                className="flex text-12"
                key={filter.filter}
                onClick={() => deleteFilter(filter.filter)}
              >
                {filter.filter}
                <AiOutlineClose size="15px" color="#D6643F" />
              </span>
            ))}
          </>
        )}
      </div>
      <div className="div-tab-filter">
        <ul className="LocationMapList-map-tabs space-between grid grid-cols-12 bg-secondary4 p-0 lg:grid-cols-12">
          {/* <li className="hidden lg:col-span-1 lg:block"></li> */}
      {/* <li className="LocationMapList-map-tabs-map col-span-5 list-none text-20 lg:col-span-6">
            <button
              className={`w-full py-4 font-bold lg:cursor-auto
                              ${
                                currentTab === "map"
                                  ? "bg-secondary2 text-secondary4 lg:bg-secondary4 lg:text-secondary2"
                                  : "bg-secondary4 text-secondary2"
                              }
                          `}
              onClick={handleTab("map")}
            >
              Map View
            </button>
          </li>
          <li className="LocationMapList-map-tabs-list col-span-5 list-none lg:col-span-5">
            <button
              role="button"
              className={`w-full py-4 text-20 font-bold lg:cursor-auto
                              ${
                                currentTab === "list"
                                  ? "bg-secondary2 text-secondary4 lg:bg-secondary4 lg:text-secondary2"
                                  : "bg-secondary4 text-secondary2"
                              }`}
              onClick={handleTab("list")}
            >
              List View
            </button>
          </li>
          <li className="col-span-2 list-none lg:col-span-1">
            <div className="button-wrap flex justify-end bg-secondary4">
              <button role="button" onClick={() => setOpenModal(!openModal)}>
                {openModal ? <CloseIcon /> : <Filter />}
              </button>
            </div>
          </li>
        </ul>
        {openModal && (
          <FilterModal
            filtersChecked={filtersChecked}
            setOpenModal={() => setOpenModal(false)}
            setFiltersChecked={(filters: any) => setFiltersChecked(filters)}
          />
        )}
      </div>
      <div className="grid grid-cols-12 gap-8">
        <div
          className={`LocationMapList-map col-span-12 lg:col-span-6  ${
            currentTab === "map" ? "activeTab" : ""
          }`}
        >
          <GoogleMap
            mapContainerStyle={containerStyle}
            zoom={zoom}
            options={options}
            center={center}
          >
            {locations.map((item: ListItems, index: number) => (
              <Marker
                key={index}
                position={{
                  lat: item.address.coordinates.lat,
                  lng: item.address.coordinates.lon,
                }}
                icon={hover && marker === item ? pinHover : pin}
                label={{ text: `$${item.price}`, className: "marker-label" }}
                onClick={() => {
                  handleMarkerClick(item);
                }}
                clickable
              >
                {infoBox && infoBox === item && (
                  <InfoWindow
                    onCloseClick={() => {
                      handleMarkerClick(item);
                    }}
                    position={{
                      lat: item.address.coordinates.lat + 0.019,
                      lng: item.address.coordinates.lon,
                    }}
                  >
                    <MapInfoBox
                      address={item.address}
                      key={index}
                      locationName={item.locationName}
                      price={`$${item.price}`}
                      locationImage={item.locationImage}
                      googleBusinessProfileId={item.googleBusinessProfileId}
                      index={index}
                      buttonLabel={component.markerBoxButtonLabel}
                    />
                  </InfoWindow>
                )}
              </Marker>
            ))}
          </GoogleMap> */}
      {/* </div> */}
      {/* <div
          className={`LocationMapList-list col-span-12 lg:col-span-6 ${
            currentTab === "list" ? "activeTab" : ""
          }`}
        > */}
      {/* {locations.length < 1 && (
            <h3 className="mt-10 text-center text-secondary1">
              Oops! Looks like there are no locations that meet your search
              criteria.
            </h3>
          )} */}

      {/* {width < 1024
            ? locations
                .slice(0, itemsToShow)
                .map((item: ListItems, index: number) => (
                  <LocationListItem
                    address={item.address}
                    key={index}
                    locationName={item.locationName}
                    slug={item.slug}
                    buttonLabel={component.viewLocationButtonLabel}
                    distance={item.distance}
                    salesFlag
                    price={`$${item.price}`}
                    locationImage={item.locationImage}
                    googleBusinessProfileId={item.googleBusinessProfileId}
                    index={index}
                    locationId={item.locationId}
                  />
                ))
            : ""}
          {width >= 1024
            ? locations.map((item: ListItems, index: number) => (
                <LocationListItem
                  address={item.address}
                  key={index}
                  locationName={item.locationName}
                  slug={item.slug}
                  buttonLabel={component.viewLocationButtonLabel}
                  distance={item.distance}
                  salesFlag
                  price={`$${item.price}`}
                  locationImage={item.locationImage}
                  googleBusinessProfileId={item.googleBusinessProfileId}
                  index={index}
                  locationId={item.locationId}
                />
              ))
            : ""}
        </div>
      </div> */}
      {/* {component.disclaimerText && (
        <p className="my-8 text-center text-12">
          <span className="text-callout">*</span>
          {component.disclaimerText}
        </p>
      )} */}
      {/* {itemsToShow < locations.length && currentTab === "list" && (
        <a
          href="#"
          className="block text-center text-16 font-bold text-callout link"
          onClick={(e) => {
            e.preventDefault();
            setItemsToShow(itemsToShow + 4);
          }}
        >
          {component.viewMoreLinkText}
        </a>
      )} */}
    </>
  );
}
