import desktopLoader from "~/resources/desktop_locations_loader.gif";
import mobileLoader from "~/resources/mobile_locations_loader.gif";
import useWindowDimensions from "../../../utils/hooks/useWindowDimensions";

interface Props {
  num: number;
}

function LoaderGrid(props: Props) {
  const { num } = props;
  let loaders = [];
  const { width } = useWindowDimensions();
  for (let index = 0; index < num; index++) {
    if (width < 1024) {
      loaders.push(
        <img className="w-full" src={mobileLoader} alt="Loading Animation" key={index} />
      );
    } else {
      loaders.push(
        <img className="w-full" src={desktopLoader} alt="Loading Animation" key={index} />
      );
    }
  }
  return (
    <div className="div-location-list my-10">
      <div className="grid grid-cols-1 gap-4 md:gap-10 lg:grid-cols-2">
        {loaders}
      </div>
    </div>
  );
}

export default LoaderGrid;
