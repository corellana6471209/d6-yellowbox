import { describe, expect, it, vi } from "vitest";
import { setTenantUpdate,TenantUpdateData } from './backend';
import { render, screen } from '@testing-library/react';


const obj = {

    setTenantUpdate: () => "setTenantUpdate",
}

vi.mock('./backend', () => ({
    setTenantUpdate: vi.fn().mockImplementation(() => "setTenantUpdate"),
}));



describe("fetching location data component unit test", () => {


    it('LoadModuleTitle should be mock', () => {
        expect(vi.isMockFunction(setTenantUpdate)).toBe(true)
    });

    it('Should be executed loadModuleTitle', async () => {
        const spy = vi.spyOn(obj, 'setTenantUpdate');


        const TenantUpdateData: TenantUpdateData = {
            LocationId: "123",
            Address1: "Test",
            TenantId: 0,
            GateCode: 0,
            WebPassword: "Test",
            MrMrs: "Test",
            FirstName: "Test",
            MiddleInitial: "Test",
            LastName: "Test",
            Company: "Test",
            Address2: "",
            City: "Test",
            Region: "Test",
            PostalCode: "Test",
            Country: "Test",
            Phone: "Test",
            MrMrsAlt: "Test",
            FirstNameAlt: "Test",
            MiddleInitialAlt: "Test",
            LastNameAlt: "Test",
            Address1Alt: "Test",
            Address2Alt: "Test",
            CityAlt: "Test",
            RegionAlt: "Test",
            PostalCodeAlt: "Test",
            CountryAlt: "Test",
            PhoneAlt: "Test",
            MrMrsBus: "Test",
            FirstNameBus: "Test",
            MiddleInitialBus: "Test",
            LastNameBus: "Test",
            CompanyBus: "Test",
            Address1Bus: "Test",
            Address2Bus: "Test",
            CityBus: "Test",
            RegionBus: "Test",
            PostalCodeBus: "Test",
            CountryBus: "Test",
            PhoneBus: "Test",
            Fax: "Test",
            Email: "Test",
            Pager: "Test",
            Mobile: "Test",
            Commercial: 0,
            CompanyIsTenant: 0,
            DateOfBirth: "Test",
            TenNote: "Test",
            License: "Test",
            LicRegion: "Test",
            SSN: "Test"
        }


        const resp = await setTenantUpdate(TenantUpdateData);
        expect(spy.getMockName()).toEqual('setTenantUpdate');
        expect(resp).toBe("setTenantUpdate");
    });

});