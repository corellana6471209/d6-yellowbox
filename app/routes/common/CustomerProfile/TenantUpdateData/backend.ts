import { prepareFetch } from "~/routes/api/shared/prepareSimplyFetch";

export interface TenantUpdateData {
    "LocationId": string;
    "TenantId":number;
    "GateCode": number;
    "WebPassword": string,
    "MrMrs": string,
    "FirstName": string;
    "MiddleInitial": string;
    "LastName": string;
    "Company": string;
    "Address1": string;
    "Address2": string;
    "City": string;
    "Region": string;
    "PostalCode": string;
    "Country": string;
    "Phone": string;
    "MrMrsAlt": string
    "FirstNameAlt": string;
    "MiddleInitialAlt": string;
    "LastNameAlt": string;
    "Address1Alt": string;
    "Address2Alt": string;
    "CityAlt": string;
    "RegionAlt": string;
    "PostalCodeAlt": string;
    "CountryAlt": string;
    "PhoneAlt": string;
    "MrMrsBus": string;
    "FirstNameBus": string;
    "MiddleInitialBus": string;
    "LastNameBus": string;
    "CompanyBus": string;
    "Address1Bus": string;
    "Address2Bus": string;
    "CityBus": string;
    "RegionBus": string;
    "PostalCodeBus": string;
    "CountryBus": string;
    "PhoneBus": string;
    "Fax": string;
    "Email": string;
    "Pager": string;
    "Mobile": string;
    "Commercial": number;
    "CompanyIsTenant": number;
    "DateOfBirth": string;
    "TenNote": string;
    "License": string;
    "LicRegion": string;
    "SSN": string;
}
export const setTenantUpdate = async (form: TenantUpdateData) => {
    const loged = await prepareFetch('POST','/CustomerProfile/TenantUpdate',JSON.stringify(form));
    const data = loged?.data;
    return data;
}
