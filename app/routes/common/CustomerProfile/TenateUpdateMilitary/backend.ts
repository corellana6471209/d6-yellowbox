import { prepareFetch } from "~/routes/api/shared/prepareSimplyFetch";

export interface DataTenantUpdateMilitary {
    "locationCode": string;
    "tenantId": number;
    "isMilitary": number;
}
export const setTenantUpdateMilitary = async (form: DataTenantUpdateMilitary) => {
    const loged = await prepareFetch('POST', '/CustomerProfile/TenantUpdateMilitary', JSON.stringify(form));
    const data = loged?.data;
    return data;
}
