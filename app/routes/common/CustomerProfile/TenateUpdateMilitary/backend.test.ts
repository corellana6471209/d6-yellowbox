import { describe, expect, it, vi } from "vitest";
import { setTenantUpdateMilitary, DataTenantUpdateMilitary } from './backend';
import { render, screen } from '@testing-library/react';


const obj = {

    setTenantUpdateMilitary: () => "setTenantUpdateMilitary",
}

vi.mock('./backend', () => ({
    setTenantUpdateMilitary: vi.fn().mockImplementation(() => "setTenantUpdateMilitary"),
}));



describe("fetching location data component unit test", () => {


    it('LoadModuleTitle should be mock', () => {
        expect(vi.isMockFunction(setTenantUpdateMilitary)).toBe(true)
    });

    it('Should be executed loadModuleTitle', async () => {
        const spy = vi.spyOn(obj, 'setTenantUpdateMilitary');


        const TenantUpdateData: DataTenantUpdateMilitary = {
            locationCode: "Test",
            tenantId: 0,
            isMilitary: 0
        }


        const resp = await setTenantUpdateMilitary(TenantUpdateData);
        expect(spy.getMockName()).toEqual('setTenantUpdateMilitary');
        expect(resp).toBe("setTenantUpdateMilitary");
    });

});