import { describe, expect, it, vi } from "vitest";
import { getTenantByEmail } from './backend';


const obj = {

    getTenantByEmail: () => "getTenantByEmail",
}

vi.mock('./backend', () => ({
    getTenantByEmail: vi.fn().mockImplementation(() => "getTenantByEmail"),
}));



describe("fetching location data component unit test", () => {


    it('LoadModuleTitle should be mock', () => {
        expect(vi.isMockFunction(getTenantByEmail)).toBe(true)
    });

    it('Should be executed loadModuleTitle', async () => {
        const spy = vi.spyOn(obj, 'getTenantByEmail');

        const resp = await getTenantByEmail("5UWRwDi68t5buzEh3L042y");
        expect(spy.getMockName()).toEqual('getTenantByEmail');
        expect(resp).toBe("getTenantByEmail");
    });

});