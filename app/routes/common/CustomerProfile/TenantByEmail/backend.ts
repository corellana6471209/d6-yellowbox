import { responseObject } from "~/routes/api/shared/prepareSimplyFetch";
import fetchCustomApi from "~/utils/fetchCustomApi";



export const getTenantByEmail = async (information: any) => {
    const data: responseObject = await fetchCustomApi(
        `/api/Authentication/GetTenantByEmail/[${information}]`
    );

    return data;
};
