import { prepareFetch } from "~/routes/api/shared/prepareSimplyFetch";

export interface DataTenantBillingInfoUpdate {
    "locationId": string;
    "ledgerId": number;
    "creditCardTypeID": number;
    "autoBillType": number;
}
export const setTenantBillingInfoUpdate = async (form: DataTenantBillingInfoUpdate) => {
    const loged = await prepareFetch('POST', '/CustomerProfile/TenantBillingInfoUpdate', JSON.stringify(form));
    const data = loged?.data;
    return data;
}
