import { describe, expect, it, vi } from "vitest";
import { setTenantBillingInfoUpdate, DataTenantBillingInfoUpdate } from './backend';
import { render, screen } from '@testing-library/react';


const obj = {

    setTenantBillingInfoUpdate: () => "setTenantBillingInfoUpdate",
}

vi.mock('./backend', () => ({
    setTenantBillingInfoUpdate: vi.fn().mockImplementation(() => "setTenantBillingInfoUpdate"),
}));



describe("fetching location data component unit test", () => {


    it('LoadModuleTitle should be mock', () => {
        expect(vi.isMockFunction(setTenantBillingInfoUpdate)).toBe(true)
    });

    it('Should be executed loadModuleTitle', async () => {
        const spy = vi.spyOn(obj, 'setTenantBillingInfoUpdate');


        const dataTenantBillingInfoUpdate: DataTenantBillingInfoUpdate = {
            locationId: "Test",
            ledgerId: 0,
            creditCardTypeID: 0,
            autoBillType: 0
        }


        const resp = await setTenantBillingInfoUpdate(dataTenantBillingInfoUpdate);
        expect(spy.getMockName()).toEqual('setTenantBillingInfoUpdate');
        expect(resp).toBe("setTenantBillingInfoUpdate");
    });

});