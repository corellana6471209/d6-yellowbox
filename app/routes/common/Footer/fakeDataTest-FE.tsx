export const fakeDataNavitationLinks = {
  items: [
    {
      title: "Google",
      pageLinksCollection: {
        items: [
          {
            __typename: "ExternalLink",
            linkText: "Google",
            externalLink: "https://www.google.com"
          }
        ]
      }
    },
    {
      title: "Home",
      pageLinksCollection: {
        items: [
          {
            __typename: "PageLink",
            linkText: "Home",
            pageLink: {
                slug: null
            }
          }
        ]
      }
    },
    {
      title: "Search",
      pageLinksCollection: {
        items: [
          {
            __typename: "PageLink",
            linkText: "Search",
            pageLink: {
                slug: "a-test-page"
            }
          }
        ]
      }
    }
  ]
}

export const fakeDataSocialNetworks = {
  items: [
    {
      title: "FacebookTest",
      image: {
        "title": "IconoFacebook",
        "description": "IconoFacebook",
        "contentType": "image/png",
        "fileName": "face.png",
        "size": 14701,
        "url": "https://images.ctfassets.net/rfh1327471ch/2KPKtGgsbmL3HkBQ2rUWhS/b9fa3564ee293c7758abacf1fcd3e4e6/face.png",
        "width": 512,
        "height": 512
      },
      link: {
        title: "GoogleLinkTest",
        pageLinksCollection: {
          items: [
            {
              __typename: "ExternalLink",
              linkText: "GoogleLinkTest",
              externalLink: "https://www.google.com/"
            }
          ]
        }
      }
    }
  ]
}