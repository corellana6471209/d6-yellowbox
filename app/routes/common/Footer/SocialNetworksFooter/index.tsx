interface Link {
  image: {
    url: string;
    description: string;
  };
  link: {
    pageLinksCollection: {
      items: [];
    };
  };
  title: string;
}

/**
 * @param props overlayCloseBtn: Fn, name: string
 * @returns {function} main Submune
 */
const SocialNetworksFooter = (props: any) => {
  const { socialNetwoks } = props;
  return (
    <ul
        className="socialnetwork-footer-root mx-auto max-w-screen-xl mb-[30px] lg:p-[0px] px-[0px]"
      aria-label="socialnetwork-footer"
    >
      {socialNetwoks.map((socialNetwok: Link, index: number) => {
        if (socialNetwok.link !== null) {
          const { image, link } = socialNetwok;
          const { pageLinksCollection } = link;
          const linkDetails = pageLinksCollection.items[0];
          const { __typename: typeName } = linkDetails;

          return (
            <li aria-label="socialnetwork" key={index}>
              {typeName === "ExternalLink" ? (
                <a
                  target="_blank"
                  href={linkDetails?.externalLink}
                  rel="noreferrer"
                  className="socialnetwork-footer-link"
                >
                  <img
                    key={index}
                    src={image.url}
                    alt={image.description}
                    className="socialnetwork-footer-image"
                  />
                </a>
              ) : (
                <a
                  target="_self"
                  href={
                    linkDetails?.pageLink?.slug == null
                      ? "/"
                      : linkDetails?.pageLink?.slug
                  }
                  className="socialnetwork-footer-link"
                >
                  <img
                    key={index}
                    src={image.url}
                    alt={image.description}
                    className="socialnetwork-footer-image"
                  />
                </a>
              )}
            </li>
          );
        }
      })}
    </ul>
  );
};

export default SocialNetworksFooter;
