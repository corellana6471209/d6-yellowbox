import { useEffect, useState } from "react";
import { useMediaQuery } from "react-responsive";

interface Link {
  pageLinksCollection: {
    items: []
  }
  __typename: string,
  linkText: string,
  externalLink: string,
  pageLink: {
    slug: string
  }
}

/**
 * @param props overlayCloseBtn: Fn, name: string
 * @returns {function} main Submune
*/
const NavigationFooter = (props: any) => {
  const [navigationLinksArray, setNavigationLinksArray] = useState([]);
  const isMobile = useMediaQuery({ query: "(max-width: 769px)" });
  const { links } = props;

  useEffect(() => {
    if (links.length) chopArray();
  }, [links, isMobile]);

  const chopArray = () => {
    const choppedArray = [links.slice(0, 7)]; // First column
    if (!isMobile) {
      choppedArray.push(links.slice(7, 13)); // Second column if Desktop
      choppedArray.push(links.slice(13)); // Third column if Desktop
    }
    else {
      choppedArray.push(links.slice(7)); // Second column if Mobile
    }
    setNavigationLinksArray(choppedArray.filter(e => e.length));
  }
  return (
    <div className='navigation-footer-root' aria-label='navigation-footer'>
      {navigationLinksArray.map((navigationLinks: any, linksIndex: number) => (
        <ul key={linksIndex} aria-label='navigation-footer-list-link'>
          {
            navigationLinks.map((link: Link, linkIndex: number) => {
              const { __typename: typeName, linkText } = link;
              return (
                <li aria-label='navigation-footer-list-link' key={linkIndex}>
                  {
                    typeName === "ExternalLink" ? (
                      <a target='_blank' href={link?.externalLink} rel='noreferrer' className='navitation-footer-link pl-[0px] link'>
                        <span>{linkText}</span>
                      </a>
                    )
                      :
                      (
                        <a target="_self" href={link?.pageLink?.slug == null ? "/" : ("/" + link?.pageLink?.slug)} className='navitation-footer-link pl-[0px] link'>
                          <span>{linkText}</span>
                        </a>
                      )
                  }
                </li>
              )
            })
          }
        </ul>
      ))}
    </div>
  )
}

export default NavigationFooter;
