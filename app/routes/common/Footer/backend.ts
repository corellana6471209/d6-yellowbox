import fetchContentful from "../../../utils/fetchContentful";


export async function getResetLinkText(){
  const query = `
  query {
    # add your query
    componentrichTextFieldCollection(where : {contentTitle : "reset-link-text"}){
      items{
        contentTitle,
        richText{
          json
        }      
      }
    }
  }
  `;
  const data = await fetchContentful(query);
  console.log('get reset link text',data);
  return (data);
}
function handleRequest(footer : any){
  
  const simplierObject = {
    navigations: footer.navigation.pageLinksCollection.items,
    socialLinks: footer.socialLinkCollection.items,
    copyright: footer.copyright,
  };
  return simplierObject;
}

export async function getFooter(params: string) {
  const query = `
  {
    footerCollection(where: {title: "${params}"} limit:1) {
      items {
        socialLinkCollection(limit: 20) {
          items {
            title
            image {
              title
              description
              contentType
              fileName
              size
              url
              width
              height
            }
            link {
              title
              pageLinksCollection(limit: 20) {
                items {
                  __typename
                  ... on ExternalLink {
                    linkText
                    externalLink
                  }
                  ... on PageLink {
                    linkText
                    pageLink {
                      slug
                    }
                    linkIcon {
                      title
                      description
                      contentType
                      fileName
                      size
                      url
                      width
                      height
                    }
                  }
                }
              }
            }
          }
        }
        navigation{
          pageLinksCollection{
            items {                
                __typename
                ... on ExternalLink {
                  linkText
                  externalLink
                }
                ... on PageLink {
                  linkText
                  pageLink {
                      slug
                  }
                }
              }
          }
        }
        copyright {
          json
          links {
            assets {
              block {
                sys {
                  id
                }
                title
                url
              }
            }
          }
        }
      }
    }
  }
  `;

  const data = await fetchContentful(query);

  if (data.footerCollection && data.footerCollection != null) {
    const footer = data.footerCollection?.items[0];
    const response = handleRequest(footer);
    return response;
  } else {
    return null;
  }
}

// getFooter("5UWRwDi68t5buzEh3L042y").then((resp: any) => {
//   console.log("resp", resp)
// })
