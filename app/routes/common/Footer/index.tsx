import { useEffect, useState } from "react";

// Custom components
import { getFooter } from "./backend";
import RichTextComponent from "../RichTextComponent";
import NavigationFooter from "./NavigationFooter";
import SocialNetworksFooter from "./SocialNetworksFooter";

interface NavigationShape {
  navigations: [];
  socialLinks: [];
  copyright: {};
}

/**
 * Main app function
 * @param {props} props props recieved
 * @returns {function} main app
 */

const Footer = () => {
  const [links, setLinks] = useState<NavigationShape>({
    navigations: [],
    copyright: {},
    socialLinks: [],
  });

  useEffect(() => {
    fetchData();
  }, []);

  const fetchData = async () => {
    const links = await getFooter("Footer");
    setLinks(links);
  };

  return (
    <div className="slant bg-secondary1" style={{ height: 'auto'}}>
      <div className="wide-wrapper footer-root">
        <NavigationFooter links={links.navigations} />
        <div>
          <SocialNetworksFooter socialNetwoks={links.socialLinks} />
          <div className="footer-copyright-container">
            <RichTextComponent data={links.copyright} />
          </div>
        </div>
      </div>
    </div>
  );
};

export default Footer;
