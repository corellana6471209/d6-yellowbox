import { describe, expect, it, vi } from "vitest";
import { getFooter } from './backend';
import { render, screen } from '@testing-library/react';


const obj = {

    getFooter: () => "getFooter",
}

vi.mock('./backend', () => ({
    getFooter: vi.fn().mockImplementation(() => "getFooter"),
}));



describe("fetching location data component unit test", () => {


    it('LoadModuleTitle should be mock', () => {
        expect(vi.isMockFunction(getFooter)).toBe(true)
    });

    it('Should be executed loadModuleTitle', async () => {
        const spy = vi.spyOn(obj, 'getFooter');

        const resp = await getFooter("5UWRwDi68t5buzEh3L042y");
        expect(spy.getMockName()).toEqual('getFooter');
        expect(resp).toBe("getFooter");
    });

});