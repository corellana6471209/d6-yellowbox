import { describe, expect, it } from "vitest";
import { render, screen } from "../../../utils/test-utils";
import { BrowserRouter as Router } from "react-router-dom";
import Footer from './index';
import NavigationFooter from './NavigationFooter';
import SocialNetworksFooter from './SocialNetworksFooter';
import { fakeDataNavitationLinks, fakeDataSocialNetworks } from './fakeDataTest-FE';

describe("Footer component", () => {
  afterEach(() => {
    vi.clearAllMocks();
  });
  
  // Copyright section renders from calling a RichTextComponent. Don't need to test.

  it("Verify if Footer component renders", () => {
    render( 
      <Router> 
        <Footer />,
      </Router>
    );
  });

  it("Verify if navigation links renders", async () => {
    render(
      <Router>
        <Footer />,
      </Router>
    );
    expect(await screen.getByLabelText('navigation-footer')).toBeInTheDocument();
  });

  it("Verify if social networks links renders", async () => {
    render(
      <Router>
        <Footer />,
      </Router>
    );
    expect(await screen.getByLabelText('socialnetwork-footer')).toBeInTheDocument();
  });

  it("Verify if navigation links renders with content", async () => {
    const { container } = render(
      <Router>
        <NavigationFooter links={fakeDataNavitationLinks.items} />,
      </Router>
    );
    expect(await container.getElementsByClassName('navitation-footer-link').length).toBe(3);
  });

  it("Verify if social networks links renders with content", async () => {
    const { container } = render(
      <Router>
        <SocialNetworksFooter socialNetwoks={fakeDataSocialNetworks.items} />,
      </Router>
    );
    expect(await container.getElementsByClassName('socialnetwork-footer-link').length).toBe(1);
  });
});
