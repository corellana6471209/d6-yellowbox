import desktopLoader from "~/resources/desktop_list_loader.gif";
import unitLoader from "~/resources/desktop_unit_loader.gif";

interface Props {
  num: number;
}

function LoaderGrid(props: Props) {
  const { num, type } = props;
  const imageSource = type == "Unit" ? unitLoader : desktopLoader;
  let loaders = [];
  for (let index = 0; index < num; index++) {
    loaders.push(
      <img
        className={`max-h-40 w-full ${type=="Unit"?"unit-card__wrapper":""}`}
        src={imageSource}
        alt="Loading Animation"
        key={index}
      />
    );
  }
  return (
    <div className="mb-10">
      <div className="">{loaders}</div>
    </div>
  );
}

export default LoaderGrid;
