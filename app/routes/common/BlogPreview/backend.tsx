import fetchContentful from "../../../utils/fetchContentful";

export let loadBlogContent = async (
    componentId = { componentId }
  ) => {
      const query = `
      query PageCollection {
          pageCollection(where: ${componentId}) {
              items {
                  type
                  visibility
                  displayHeaderAndFooter
                  title
                  slug
                  seoTitle
                  seoDescription
                  seoKeywords
                  canonicalUrl
                  robotsMetaSettings
                  excludePostFromRelatedList
              componentsCollection(limit: 25) {
                  items {
                      __typename
                          ... on Entry {
                              sys{
                                  id
                              }
                          }
                      }
                  }
              }
          }
      }
      `;
        var theData = await fetchContentful(query);
    return theData;
  }
  