import { describe, expect, it, vi } from "vitest";
import { loadBlogContent} from './backend';
import { render, screen } from '@testing-library/react';


const obj = {
 
    loadBlogContent: () => "LoadModuleTitle",
}

vi.mock('./backend', () => ({
    loadBlogContent: vi.fn().mockImplementation(() => "LoadModuleTitle"),
}));



describe("fetching location data component unit test", () => {
 

    it('LoadModuleTitle should be mock', () => {
        expect(vi.isMockFunction(loadBlogContent)).toBe(true)
    });

    it('Should be executed loadModuleTitle', async () => {
        const spy = vi.spyOn(obj, 'loadBlogContent');
        const resp = await loadBlogContent();
        expect(spy.getMockName()).toEqual('loadBlogContent');
        expect(resp).toBe("LoadModuleTitle");
    });

});