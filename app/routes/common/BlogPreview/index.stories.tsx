import { ComponentStory, ComponentMeta } from '@storybook/react';
import BlogPreview from './index';
import testImage from '/mocks/test-assets/Comp_Teaser.png'

export default {
  title: 'components/core/BlogPreview',
  component: BlogPreview,
} as ComponentMeta<typeof BlogPreview>;

const Template: ComponentStory<typeof BlogPreview> = (args) => <BlogPreview {...args} />;

export const Primary = Template.bind({});
Primary.args = {
  id: "abc123",
  module: {
    image: {
      url: testImage,
      alt: "alt"
    },
    title: "title",
    date: "August 1, 2022",
    summary: "summary here"
  }
};
export const Secondary = Template.bind({});
Secondary.args = {
  id: "def456",
  module: {
    image: {
      url: testImage,
      alt: "alt 2"
    },
    title: "title2",
    date: "August 2, 2022",
    summary: "summary 2 here"
  }
};
export const Tertiary = Template.bind({});
Tertiary.args = {
  id: "ghi789",
  module: {
    image: {
      url: testImage,
      alt: "alt 3"
    },
    title: "title3",
    date: "August 3, 2022",
    summary: "summary 3 here"
  }
};
export const Quaternary = Template.bind({});
Quaternary.args = {
  id: "ghi789",
  module: {
    image: {
      url: testImage,
      alt: "alt 4"
    },
    title: "title4",
    date: "August 4, 2022",
    summary: "summary 4 here"
  }
};
