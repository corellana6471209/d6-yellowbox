import { describe, expect, it } from "vitest";
import BlogPreview from ".";
import { render, screen } from "../../../utils/test-utils";

const module = {
  module: {
    date: "2022-08-14T14:15:33.993Z",
    id: "231MoEFaH8PsYVWTUsG23w",
    image: {
      alt: "Main image for Panthersville / Decatur Self Storage",
      url: "https://images.ctfassets.net/rfh1327471ch/79zy3LJSjYVR9mZ2jYxlrq/fc6f075c536e5f07ebbc93207a595b9b/panthersville--decatur_af947f0321d307d8e398d398a5c8fb9dd1e50cb9.jpeg",
    },
    summary: "Number Three Blog description for the SEO Description Field",
    title: "Number Three Blog Title",
  },
};

describe("BlogPreview component FE", () => {
  it("Verify if BlogPreview component renders", () => {
    render(<BlogPreview {...module} />);
  });

  it("Verify the BlogPreview title is in the document", async () => {
    render(<BlogPreview {...module} />);
    expect(
      await screen.findByText(/Number Three Blog Title/i)
    ).toBeInTheDocument();
  });

  it("Verify the BlogPreview date is in the document", async () => {
    render(<BlogPreview {...module} />);
    expect(await screen.findByText(/August 14, 2022/i)).toBeInTheDocument();
  });

  it("Verify the BlogPreview image alt text is in the document", async () => {
    render(<BlogPreview {...module} />);
    expect(
      await screen.findByAltText(
        "Main image for Panthersville / Decatur Self Storage"
      )
    ).toBeInTheDocument();
  });

  it("Verify the BlogPreview image src is in the document", async () => {
    render(<BlogPreview {...module} />);
    const testImage = document.querySelector("img") as HTMLImageElement;
    expect(testImage.src).toContain(
      "https://images.ctfassets.net/rfh1327471ch/79zy3LJSjYVR9mZ2jYxlrq/fc6f075c536e5f07ebbc93207a595b9b/panthersville--decatur_af947f0321d307d8e398d398a5c8fb9dd1e50cb9.jpeg"
    );
  });
});
