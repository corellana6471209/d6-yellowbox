import * as PropTypes from "prop-types";
import LinesEllipsis from "react-lines-ellipsis";
import { Link } from "@remix-run/react";
//import moment from "moment";

interface Module {
  module: {
    date: string;
    slug: string;
    id: string;
    image: {
      alt: string;
      url: string;
    };
    summary: string;
    title: string;
  };
  maxlines: number;
  blogPage: boolean;
}

/**
 * Main app function
 * @param {props} props props recieved
 * @returns {function} main app
 */
function BlogPreview(props: Module) {
  const { module, maxlines, blogPage } = props;
  function handlerClick(slug: any) {
    window.location.href = `/blog/${slug}`;
  }

  if (module) {
    return (
      <div
        className={`blog-preview mb-[30px] ${
          !blogPage && "ml-[20px] mr-[20px] md:ml-0 md:mr-0"
        } `}
      >
        <div
          className="blog-preview--image cursor-pointer"
          style={{ paddingRight: "0px !important" }}
          onClick={() => handlerClick(module.slug)}
        >
          <img
            className="h-[200px] object-cover sm:h-[280px]"
            src={module.image?.url}
            alt={module.image?.alt}
          />
        </div>
        <div>
          <h3
            onClick={() => handlerClick(module.slug)}
            className="mb-[15px] mt-[15px] cursor-pointer font-serif text-20 leading-[24px] text-tertiary1 md:text-24 link"
          >
            {module.title}
          </h3>
          {/**<p className="mb-[15px] text-12">
            {moment(module.date).format("MMMM DD, YYYY")}
          </p> */}

          <div className="mb-[15px]">
            <LinesEllipsis
              text={module.summary || ""}
              maxLine={maxlines}
              ellipsis="..."
              trimRight
              basedOn="letters"
            />
          </div>
          <Link
            to={`/blog/${module.slug}`}
            prefetch="intent"
            className="green-border-button inline-block w-[100%] border-2 border-tertiary1 py-1 px-5 text-center text-12 text-tertiary1 no-underline sm:py-2 sm:px-4 sm:text-16 md:w-auto"
          >
            Read More
          </Link>
        </div>
      </div>
    );
  } else {
    return <div className="min-h-[500px]"></div>;
  }
}

// Specifies the default values for props:
BlogPreview.defaultProps = {
  id: "abc123",
  image: {
    url: "http://domain.com/image.png",
    alt: "Helper text for image",
  },
  title: "Tips on Search Storage Solutions",
  date: "August 8, 2022",
  summary:
    "This should be the blog summary text or a subset of the initial words of the article",
};

// Specifies the proptypes:
BlogPreview.propTypes = {
  id: PropTypes.string,
  image: PropTypes.object,
  title: PropTypes.string,
  date: PropTypes.string,
  summary: PropTypes.string,
  blogPage: PropTypes.bool,
};

export let convertFromContentful = function (
  contentful: {
    pageCollection: { items: any };
    pageImage: { url: string; description: string };
    title: string;
    sys: { firstPublishedAt: string };
    seoDescription: string;
  },
  order: Array<string> | null
) {
  var items = contentful?.pageCollection?.items;
  let blogPreviews = [];

  for (
    let itemsCounter = 0;
    items && items.length > itemsCounter;
    itemsCounter++
  ) {
    let theItem = items[itemsCounter];
    if (order && items && order.length == items.length) {
      theItem = items.find(
        (tempItem: { sys: { id: string } }) =>
          tempItem.sys.id === order[itemsCounter]
      );
    }
    blogPreviews.push({
      module: {
        id: theItem.sys.id,
        image: {
          url: theItem.pageImage?.url,
          alt: theItem.pageImage?.description,
        },
        title: theItem.title,
        slug: theItem.slug,
        date: theItem.sys.firstPublishedAt,
        summary: theItem.seoDescription,
      },
    });
  }
  return blogPreviews;
};

export default BlogPreview;
