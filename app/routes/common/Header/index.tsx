import { useContext, useEffect, useRef, useState } from "react";
import Icon from "../svg/Icon";
import Search from "../Search";
import useToggleNav from "../../../utils/hooks/useToggleNav";
import Submenu from "./Submenu";
import { getLogo, getPages } from "./backend";
import { Link, useLoaderData } from "@remix-run/react";
import { PageDetailsContext } from "../../../utils/context/PageDeatilsContext";
import { useMediaQuery } from "react-responsive";
import { getLabel } from "../../../utils/getLabel";
import { useWindowScrollPositions } from "../../../utils/hooks/useScroll";
import UserMenu from "./UserMenu";

interface Logo {
  url: string;
  description: string;
  fileName: string;
  size: number;
}

/**
 *
 * @param {props} props props recieved
 * @returns {function} main app
 */

let isHydrating = true;

function Header() {
  const value = useLoaderData();

  const pageDetails = useContext(PageDetailsContext);
  const isInHome =
    pageDetails && pageDetails.slug ? pageDetails.slug.includes("home") : false;
  let [isHydrated, setIsHydrated] = useState(!isHydrating);
  const [logo, setLogo] = useState<Logo | null>(null);
  const [submenuOptions, setSubmenuOptions] = useState([]);
  const { showSearch, showAccount, showSubmenu, toggleNav } = useToggleNav({
    showSearch: false,
    showAccount: false,
    showSubmenu: false,
  });
  const [config, setConfig] = useState<any>(null);
  const [userName, setUserName] = useState<string>("");
  const { scrollY } = useWindowScrollPositions();
  const isMobile = useMediaQuery({ query: "(max-width: 991px)" });

  // ---- Skip to Main Content ---
  //const [searchParams, setSearchParams] = useSearchParams();
  //const query = searchParams.get("s");
  const mainContent = useRef(null);

  useEffect(() => {
    async function fetchData() {
      const submenu = await getPages("Main Navigation");
      const logo = await getLogo();
      const helperText = await getLabel("searchHelperText");
      const buttonText = await getLabel("searchButtonText");
      setConfig({ searchHelperText: helperText, searchButtonText: buttonText });
      setLogo(logo);
      setSubmenuOptions(submenu);
      setIsHydrated(true);
      return submenu;
    }
    fetchData();
  }, []);

  useEffect(() => {
    if (value?.token && value?.user) setUserName(value.user.firstName);
  }, [value]);

  function overlayCloseBtn(name: string) {
    return (
      <div className="overlay__close text-right">
        <button onClick={() => toggleNav(name)}>
          <Icon iconName="cross" />
        </button>
      </div>
    );
  }

  function handlerMainContent(event: any) {
    //event.preventDefault();

    if (mainContent && mainContent.current) {
      //query ? navigate(`?s=${query}#main-content`) : navigate('#main-content');
      mainContent.current.focus();
    }
  }

  if (isHydrated) {
    return (
      <>
        <header className={`header ${isInHome ? "" : "sticky"}`}>
          <div className="header-container">
            <div className="mx-auto max-w-[1200px]">
              <nav className="navbar">
                <div className="container-fluid">
                  <Link
                    className="skip-main"
                    to="#main-content"
                    onClick={handlerMainContent}
                  >
                    Skip to content
                  </Link>

                  <Link to="/" aria-label="link-logo">
                    <div className="navbar-brand logo">
                      {logo ? (
                        <img
                          src={logo.url}
                          alt={logo.description}
                          width="135"
                          height="34"
                        />
                      ) : (
                        <Icon iconName="logoSimply" />
                      )}
                    </div>
                  </Link>

                  <ul className="navbar-nav items-center" aria-label="nav-list">
                    <li className={`nav-item ${isInHome ? "hidden" : ""}`}>
                      {isMobile && (
                        <button
                          aria-label="search-icon"
                          className="navbar-icon"
                          onClick={() => toggleNav("showSearch")}
                        >
                          <Icon iconName="searchOutline" classIcon="y-24" />
                          <span>Search</span>
                        </button>
                      )}

                      {!isMobile && <Search config={config} header={true} />}
                    </li>
                    <li className="nav-item">
                      {userName ? (
                        <button
                          aria-label="my-account-icon"
                          className="navbar-icon"
                          onClick={() => toggleNav("showAccount")}
                        >
                          <Icon iconName="personOutline" classIcon="y-24" />
                          <span>{userName}</span>
                        </button>
                      ) : (
                        <Link to="/login" className="navbar-icon">
                          <Icon iconName="personOutline" classIcon="y-24" />
                          <span>My Account</span>
                        </Link>
                      )}
                    </li>

                    <li
                      className={`nav-item ${
                        submenuOptions.length ? "" : "hidden"
                      }`}
                    >
                      <button
                        aria-label="submenu-icon"
                        className="navbar-icon"
                        onClick={() => toggleNav("showSubmenu")}
                      >
                        <Icon iconName="moreVertical" />
                      </button>

                      {showSubmenu && (
                        <Submenu
                          toggleNav={toggleNav}
                          overlayCloseBtn={overlayCloseBtn}
                          name="showSubmenu"
                          show={showSubmenu}
                          submenuOptions={submenuOptions}
                        />
                      )}
                      {showAccount && (
                        <UserMenu
                          overlayCloseBtn={overlayCloseBtn}
                          name="showAccount"
                          value={value}
                        />
                      )}
                    </li>
                  </ul>
                </div>
              </nav>
            </div>

            {(((isInHome || isMobile) && scrollY > 5) || showSearch) && (
              <div className="form-search-dropdown">
                <Search
                  header={true}
                  config={config}
                  toggleNav={toggleNav}
                  overlayCloseBtn={overlayCloseBtn}
                  showCloseBtn={true}
                  name="showSearch"
                  show={showSearch}
                  isMobile={isMobile}
                  scrollY={scrollY}
                />
              </div>
            )}
          </div>
        </header>

        <div id="main-content" tabIndex={-1} ref={mainContent}></div>
      </>
    );
  } else {
    return <></>;
  }
}

export default Header;
