import fetchContentful from "../../../utils/fetchContentful";

/**
 *
 * @param params
 * @returns array
 */

export async function getLogo() {
  const query: string = `{
    assetCollection(where: {title: "Logo"}) {
    	items {
        fileName
        url
        size
        title
        description
      }
    }
  }`;

  const { assetCollection } = await fetchContentful(query);
  return assetCollection?.items[0];
  //return navigationCollection.items ? navigationCollection.items[0].pageLinksCollection?.items : [];
}

export async function getPages(params: string) {
  let w = `{title: "${params}"}`;
  const query = `
    query NavigationCollection {
      navigationCollection(where: ${w}) {
        items {
          pageLinksCollection(limit: 10) {
            items {
              __typename
              ... on ExternalLink {
              linkText
              externalLink
              }
              ... on PageLink {
              linkText
              pageLink {
                  slug
              }
              linkIcon {
                  title
                  description
                  contentType
                  fileName
                  size
                  url
                  width
                  height
              }
              }
            }
          }
        }
      }
    }`;

  const { navigationCollection } = await fetchContentful(query);

  return navigationCollection.items
    ? navigationCollection.items[0].pageLinksCollection?.items
    : [];
}
