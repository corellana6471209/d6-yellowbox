import PropTypes from "prop-types";
import { useOutsideAlerter } from "../../../../utils/hooks/useOutsideAlerter";
import { Link } from "react-router-dom";
import { identify } from "../../../../utils/identify";

/**
 * @param props overlayCloseBtn: Fn, name: string
 * @returns {function} main Submune
 */
function UserMenu(props: any) {
  const { name, overlayCloseBtn, value } = props;
  const analyticsInf =
    value.sitelinkUserAndTenantInfo[0]?.sitelinkUserTenant || null;

  const handleLogout = () => {
    identify(
      window,
      `${analyticsInf.sitelinkTenantId}${analyticsInf.sitelinkLocationId}`,
      {
        email: value.user?.emailAddress || null,
        locationID: analyticsInf?.locationId || null,
      }
    );
  };

  return (
    <div aria-label="usersubmenu" className="overlay__wrapper submenu ease-in">
      {overlayCloseBtn && overlayCloseBtn(name)}

      <ul>
        <li className="link pl-10 text-left" aria-label="wrapper-item-link">
          <a href="/my-account" className="font-bold text-secondary1">
            <span>My Account</span>
          </a>
        </li>

        <li className="link pl-10 text-left" aria-label="wrapper-item-link">
          <a href="/contact-details" className="font-bold text-secondary1">
            <span>Profile</span>
          </a>
        </li>
        <li className="link pl-10 text-left" aria-label="wrapper-item-link">
          <a
            href="/logout"
            className="font-bold text-secondary1"
            onClick={() => handleLogout()}
          >
            <span>Logout</span>
          </a>
        </li>
      </ul>
    </div>
  );
}

UserMenu.propTypes = {
  overlayCloseBtn: PropTypes.func,
  name: PropTypes.string,
  value: PropTypes.any,
};

UserMenu.defaultProps = {
  name: "showAccount",
  value: null,
};

export default UserMenu;
