import { describe, expect, it } from "vitest";
import { render, screen } from "../../../../utils/test-utils";
import { BrowserRouter as Router } from "react-router-dom";
import Submenu from '.';
import { fakeDataOptionsSubmenu } from "../../../../utils/fakedataToTests";

describe("Submenu component", () => {
  it("Verify if Submenu component renders", () => {
    render( 
      <Router> 
        <Submenu show={true} submenuOptions={fakeDataOptionsSubmenu}/>,
      </Router>
    );
    expect(screen.getByLabelText('submenu')).toBeInTheDocument();
  });

  it("It should show same number of links as fakeData", async () => {
    render(
      <Router>
        <Submenu show={true} submenuOptions={fakeDataOptionsSubmenu} />,
      </Router>
    );

    const links = screen.getAllByLabelText('wrapper-item-link');
    expect(links.length).toBe(fakeDataOptionsSubmenu.length);
  });

  it("Should display icon if link has", async () => {
    render(
      <Router>
        <Submenu show={true} submenuOptions={fakeDataOptionsSubmenu} />,
      </Router>
    );
    
    const iconsFakeData = fakeDataOptionsSubmenu.filter(x => x.linkIcon != null);
    const icons = screen.getAllByLabelText('icon-item-link');
    expect(icons.length).toBe(iconsFakeData.length);
  });
});
