import { useEffect, useRef } from "react";
import PropTypes from "prop-types";
import { useOutsideAlerter } from "../../../../utils/hooks/useOutsideAlerter";

interface Options {
  __typename: string;
  linkIcon?: {
    title: string;
    description: string;
    contentType: string;
    fileName: string;
    size: number;
    url: string;
    width: number;
    height: number;
  };
  linkText: string;
  externalLink?: string;
  pageLink?: {
    slug: string;
  };
}

/**
 * @param props overlayCloseBtn: Fn, name: string
 * @returns {function} main Submune
 */
function Submenu(props: any) {
  const { overlayCloseBtn, name, show, submenuOptions, toggleNav } = props;
  const wrapperRef = useRef(null);

  useEffect(() => {
    //console.log('-->Load submenu', submenuOptions);
  }, []);

  useOutsideAlerter(wrapperRef, () => {
    toggleNav(name);
  });

  function renderIcon(linkIcon: any) {
    return linkIcon.url ? (
      <img
        src={linkIcon.url}
        alt={linkIcon.title}
        aria-label="icon-item-link"
      />
    ) : null;
  }

  return submenuOptions ? (
    <div
      ref={wrapperRef}
      aria-label="submenu"
      className={`overlay__wrapper submenu ${show ? "ease-in" : "ease-out"}`}
    >
      {overlayCloseBtn && overlayCloseBtn(name)}

      <ul>
        {submenuOptions.map((item: Options, index?: number) => (
          <li key={index} className="text-left" aria-label="wrapper-item-link">
            {item?.__typename === "ExternalLink" ? (
              <a
                className="font-bold text-secondary1 no-underline link"
                target="_blank"
                href={item?.externalLink}
                rel="noreferrer"
              >
                {/* {item.linkIcon && renderIcon(item.linkIcon)} */}
                <span>{item?.linkText}</span>
              </a>
            ) : (
              <a
                className="font-bold text-secondary1 no-underline link"
                target="_self"
                href={item?.pageLink?.slug == null ? "/" : ("/"+item?.pageLink?.slug)}
              >
                {/* {item.linkIcon && renderIcon(item.linkIcon)} */}
                <span>{item?.linkText}</span>
              </a>
            )}
          </li>
        ))}
      </ul>
    </div>
  ) : null;
}

Submenu.propTypes = {
  iconColor: PropTypes.string,
  overlayCloseBtn: PropTypes.func,
  name: PropTypes.string,
  show: PropTypes.bool,
  submenuOptions: PropTypes.array,
  toggleNav: PropTypes.func,
};

Submenu.defaultProps = {
  name: "showSubmenu",
  show: false,
};

export default Submenu;
