import { describe, expect, it } from "vitest";
import { render, screen, fireEventAliased } from "../../../utils/test-utils";
import { BrowserRouter as Router } from "react-router-dom";
import Header from './index';
import React from "react-responsive";

describe("Header component", () => {
  afterEach(() => {
    vi.clearAllMocks();
  });
  
  it("Verify if Header component renders", () => {
    render( 
      <Router> 
        <Header />,
      </Router>
    );
  });

  it("Verify if logo exists", async () => {
    render(
      <Router>
        <Header />,
      </Router>
    );
    const wrapper = screen.getByLabelText("link-logo");
    expect(wrapper.baseURI).toBeTruthy();
    expect(wrapper).toBeInTheDocument();
  });

  it("Should toggle form class when you click on search icon for mobile", async () => {
    const { container } = render(
      <Router>
        <Header />,
      </Router>
    );
    
    vi.mock('react-responsive', () => ({
      useMediaQuery: vi.fn().mockReturnValue(true),
    }));

    const button = screen.getByRole("button", { name: /Search/i, });

    expect(
      await container.getElementsByClassName('form-search ease-out').length
    ).toBe(1);

    fireEventAliased.click(button);

    expect(
      await container.getElementsByClassName('form-search ease-in').length
    ).toBe(1);
  });

  it("Should show submenu when click on submenu icon (:)", async () => {

    const { container } = render(
      <Router>
        <Header />,
      </Router>
    );

    const button = screen.getByLabelText("submenu-icon");

    expect(
      await container.getElementsByClassName('submenu').length
    ).toBe(0);

    fireEventAliased.click(button);

    expect(
      await container.getElementsByClassName('submenu').length
    ).toBe(1);
  });
});
