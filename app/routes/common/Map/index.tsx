import PropTypes from 'prop-types';
import { GoogleMap, Marker, InfoWindow } from "@react-google-maps/api";
import MapInfoBox from '~/routes/page-components/LocationMapList/MapInfoBox/__index';
import pin from "../../../resources/pin.png";
import { useState } from 'react';

export type ListItems = {
  price: string;
  address: {
    address1: string;
    address2: string | null;
    city: string;
    coordinates: {
      lat: number;
      lon: number;
    };
    postalCode: number;
    state: {
      stateFullName: string;
      stateAbbreviation: string;
    };
  };
  locationName: string;
  slug: string;
  buttonLabel: string;
  distance: number;
  salesFlag: boolean;
  locationImage: {
    title: string;
    url: string;
  };
  googleBusinessProfileId: string;
  index: number;
};

function Map(props: any) {
  const { 
    containerStyle, 
    zoom, 
    options, 
    center, 
    locations, 
    component 
  } = props;
  const [infoBox, setMarkerInfoBox] = useState<ListItems | null>(null);

  const handleMarkerClick = (ib: any) => {
    const setInfo = ib === infoBox ? null : ib;
    setMarkerInfoBox(setInfo);
  };

  return (
    <>
      <GoogleMap
        mapContainerStyle={containerStyle}
        zoom={zoom}
        options={options}
        center={center}
      >
        { locations &&
          locations.map((item: ListItems, index: number) => (
            <Marker
              key={index}
              position={{
                lat: item.address.coordinates.lat,
                lng: item.address.coordinates.lon,
              }}
              icon={pin}
              label={{ text: "$67", className: "marker-label" }}
              onClick={() => { handleMarkerClick(item) }}
              clickable
            >
              {infoBox && infoBox === item && (
                <InfoWindow
                  onCloseClick={() => { handleMarkerClick(item) }}
                  position={{
                    lat: item.address.coordinates.lat + 0.019,
                    lng: item.address.coordinates.lon,
                  }}
                >
                  <MapInfoBox
                    address={item.address}
                    key={index}
                    locationName={item.locationName}
                    price="$70"
                    locationImage={item.locationImage}
                    googleBusinessProfileId={item.googleBusinessProfileId}
                    index={index}
                    buttonLabel={component ? component.markerBoxButtonLabel : 'Select' }
                  />
                </InfoWindow>
              )}
            </Marker>
          ))
        }
      </GoogleMap>
    </>
  )
}

//Map.propTypes = {}

export default Map;