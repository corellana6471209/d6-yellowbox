import type { ComponentStory, ComponentMeta } from "@storybook/react";
import { BrowserRouter as Router } from "react-router-dom";

import AlertPaymentDue from "./index";

export default {
  title: "components/core/AlertPaymentDue",
  component: AlertPaymentDue,
} as ComponentMeta<typeof AlertPaymentDue>;

const Template: ComponentStory<typeof AlertPaymentDue> = (args) => (
  <Router>
    <AlertPaymentDue />
  </Router>
);

export const AlertPaymentDueComponent = Template.bind({});
