import React from "react";
import { useState, useEffect } from "react";
import moment from "moment";

import AlertTriangle from "../../common/svg/AlertTriangle";

const AlertPaymentDue = (props: any) => {
  const { units } = props;
  const [unitsOnDebt, setUnitsOnDebt] = useState<any>([]);

  useEffect(() => {
    const unitOnDebt: any = [];

    if (units && units.length > 0) {
      units.forEach((unit: any, i: any) => {
        const actualDate = moment().format("YYYY-MM-DD");
        const dateToValidate = moment(unit.paidThruDateTime).format(
          "YYYY-MM-DD"
        );
        const validation = moment(actualDate).isBefore(dateToValidate); //Is the actualDate is less than paidThruDateTime

        if (!validation) {
          unitOnDebt.push(` ${unit.unitName}`);
        }
      });
    }

    setUnitsOnDebt(unitOnDebt);
  }, [units]);

  return (
    <>
      {unitsOnDebt && unitsOnDebt.length > 0 ? (
        <div className="div-alert-payment-due bg-primary2">
          <div className="d-flex justify-center pb-2">
            <AlertTriangle />
          </div>

          <div className="d-flex justify-center">
            <p className="text-payment-due text-center text-16 font-bold text-secondary1 md:text-20">
              Payment Due for Unit(s){unitsOnDebt.join(",")}
            </p>
          </div>

          <div className="d-flex justify-center">
            <p className="text-center text-12 text-secondary1 md:text-16	">
              {" "}
              Please make your payment to avoid penalties
            </p>
          </div>
        </div>
      ) : (
        ""
      )}
    </>
  );
};

export default AlertPaymentDue;
