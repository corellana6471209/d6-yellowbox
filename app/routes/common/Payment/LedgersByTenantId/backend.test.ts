import { describe, expect, it, vi } from "vitest";
import { LedgersByTenantId } from './backend';
import { render, screen } from '@testing-library/react';


const obj = {

    LedgersByTenantId: () => "LedgersByTenantId",
}

vi.mock('./backend', () => ({
    LedgersByTenantId: vi.fn().mockImplementation(() => "LedgersByTenantId"),
}));



describe("fetching location data component unit test", () => {


    it('LoadModuleTitle should be mock', () => {
        expect(vi.isMockFunction(LedgersByTenantId)).toBe(true)
    });

    it('Should be executed loadModuleTitle', async () => {
        const spy = vi.spyOn(obj, 'LedgersByTenantId');

        const resp = await LedgersByTenantId("5UWRwDi68t5buzEh3L042y");
        expect(spy.getMockName()).toEqual('LedgersByTenantId');
        expect(resp).toBe("LedgersByTenantId");
    });

});