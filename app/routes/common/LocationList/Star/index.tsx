import React, { useState, useEffect } from "react";
import StarRatings from "react-star-ratings";

import * as PropTypes from "prop-types";

/**
 * @param {number} size to the component
 * @param {number} ratingStar to the component
 * @param {boolean} disabled to the component
 * @return {component} with data
 *
 */

interface StateListProps {
  size: string;
  rating: number;
  disabled: boolean;
}

const StarLocationList = function StarLocationList({
  size,
  rating,
  disabled,
}: StateListProps) {
  const [ratingStar, setRatingStar] = useState(0); // initial rating value

  useEffect(() => {
    setRatingStar(rating);
  }, [rating]);

  /**
   * Catch Rating value
   * @param {number} rate The first number
   *
   */
  const handleRating = (rate: number) => {
    setRatingStar(rate);
  };

  return (
    <div>
      {disabled ? (
        <StarRatings
          rating={ratingStar}
          starRatedColor="#657AC1"
          starEmptyColor="#EAEBEF"
          numberOfStars={5}
          name="rating"
          starDimension={size}
          starSpacing="0px"
        />
      ) : (
        <StarRatings
          changeRating={handleRating}
          rating={ratingStar}
          starRatedColor="#657AC1"
          starEmptyColor="#EAEBEF"
          numberOfStars={5}
          name="rating"
          starDimension={size}
          starSpacing="0px"
        />
      )}
    </div>
  );
};
export default StarLocationList;

StarLocationList.propTypes = {
  size: PropTypes.string,
  rating: PropTypes.number,
  disabled: PropTypes.bool,
};

StarLocationList.defaultProps = {
  size: "25px",
  rating: 90,
  disabled: true,
};
