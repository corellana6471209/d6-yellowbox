import type { ComponentMeta } from '@storybook/react';
import Star from './index';

export default {
    title: 'components/core/Star',
    component: Star,
  } as ComponentMeta<typeof Star>;

/**
 * @param {object} args to the component
 * @returns {component} with data.
 */

const Template = args => <Star {...args} />;

export const Primary = Template.bind({});
Primary.args = {
  size: 25,
  rating: 0,
  disabled: false
};

export const OneStar = Template.bind({});
OneStar.args = {
  size: 25,
  rating: 1,
  disabled: true
};

export const TwoStars = Template.bind({});
TwoStars.args = {
  size: 25,
  rating: 2,
  disabled: true
};

export const FiveStars = Template.bind({});
FiveStars.args = {
  size: 25,
  rating: 5,
  disabled: true
};