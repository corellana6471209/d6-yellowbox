import type { ComponentMeta } from '@storybook/react';
import LocationListImage from './index';
import testImage from '/mocks/test-assets/home-page-image.jpeg'

export default {
  title: 'components/core/LocationListImage',
    component: LocationListImage,
  } as ComponentMeta<typeof LocationListImage>;

/**
 * @param {object} args to the component
 * @returns {component} with data.
 */

const Template = args => <LocationListImage {...args} />;

export const Primary = Template.bind({});
Primary.args = {
  image: testImage,
  promotion: false,
  alt: "This is the alt string"
};

export const WithPromotion = Template.bind({});
WithPromotion.args = {
  image: testImage,
  promotion: true,
  alt: "This is the alt string"
};