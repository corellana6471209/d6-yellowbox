import * as PropTypes from "prop-types";

/**
 * @param {string} image to the component
 * @return {component} with data
 *
 */
interface ImageProps {
  image: string;
  alt: string;
}

const LocationListImage = function LocationListImage({
  image,
  alt,
}: ImageProps) {
  if (image !== "") {
    return (
      <div className="location-list-img">
        <img className="img" src={image} alt={alt} />
      </div>
    );
  } else {
    return <></>;
  }
};
export default LocationListImage;

LocationListImage.propTypes = {
  image: PropTypes.string,
  alt: PropTypes.string,
};

LocationListImage.defaultProps = {
  image: "https://via.placeholder.com/700x400",
  alt: "Location image",
};
