import * as PropTypes from "prop-types";
import StarLocationList from "../Star";
import Button from "../../Button";
import useWindowDimensions from "../../../../utils/hooks/useWindowDimensions";
import useRating from "../../../../utils/hooks/useRating";

/**
 * @param {any} address to the component
 * @param {number} stars to the component
 * @param {number} distance to the component
 * @param {string} postal to the component
 * @param {number} price to the component
 * @return {component} with data
 *
 *
 */

interface TextLocationListProps {
  address: any;
  locationName: string;
  googleBusinessProfileId: string;
  distance: string;
  price: number;
  link: string;
}

const TextLocationList = function TextLocationList({
  address,
  locationName,
  googleBusinessProfileId,
  distance,
  price,
  link,
}: TextLocationListProps) {
  const { width } = useWindowDimensions();
  // Get rating
  const rating = useRating(googleBusinessProfileId);

  return (
    <div className="flex justify-between">
      <div className="div-text-location">
        <h4 className="site-text text-12 text-secondary5 xs:text-20 md:text-24">
          {locationName}
        </h4>

        {rating && typeof rating === "number" ? (
          <StarLocationList
            size={width > 375 ? "25px" : "15px"}
            rating={rating}
            disabled
          />
        ) : (
          ""
        )}

        <h4 className="text-12 font-bold text-callout xs:text-16">
          {distance} mi
        </h4>
        <h4 className="direction-text text-12 text-greyHeavy xs:text-16 md:text-20">
          {address.address1}
        </h4>
        <h4 className="text-12 text-greyHeavy xs:text-16 md:text-20">
          {address.city}, {address?.state?.stateAbbreviation}{" "}
          {address.postalCode}
        </h4>
      </div>

      <div className="div-text-price">
        <div className="flex items-center justify-between">
          <h4 className="text-12 font-bold xs:text-16">Starting From</h4>
          <h1 className="text-28 font-bold text-callout sm:text-36">
            ${price ? price : 0}*
          </h1>
        </div>

        <div className="button-div mt-2 sm:mt-3">
          <Button
            href={`/locations/${link}`}
            title={width > 768 ? "Explore Location" : "Select"}
            buttonStyle="button-primary-expanded"
          />
        </div>
      </div>
    </div>
  );
};
export default TextLocationList;

TextLocationList.propTypes = {
  address: PropTypes.any,
  locationName: PropTypes.string,
  googleBusinessProfileId: PropTypes.string,
  distance: PropTypes.string,
  price: PropTypes.number,
  link: PropTypes.string,
};
