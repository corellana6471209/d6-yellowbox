import { describe, expect, it, vi } from "vitest";
import { setReservation } from "./backend";

const obj = {
  setReservation: () => "setReservation",
};

vi.mock("./backend", () => ({
  setReservation: vi.fn().mockImplementation(() => "setReservation"),
}));

describe("fetching location data component unit test", () => {
  it("LoadModuleTitle should be mock", () => {
    expect(vi.isMockFunction(setReservation)).toBe(true);
  });

  it("Should be executed loadModuleTitle", async () => {
    const spy = vi.spyOn(obj, "setReservation");

    const resp = await setReservation([
      "FistNemeTest",
      "LastNameTest",
      "emailtest@test.com",
      "1234567",
      true,
      "9001",
      "1",
      "tetstComment",
    ]);
    expect(spy.getMockName()).toEqual("setReservation");
    expect(resp).toBe("setReservation");
  });
});
