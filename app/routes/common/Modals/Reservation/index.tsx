import { useState, useEffect } from "react";
import { useSubmit, useTransition, useActionData } from "@remix-run/react";
import { validateUSPhone, validateEmail } from "../../../../utils/regex";
import formatPhoneNumber from "../../../../utils/formatNumber";
import { analytics } from "../../../../utils/analytics";
import { globalRedirect } from "../../../../utils/globalRedirect";
import { identify } from "../../../../utils/identify";

import Check from "../../../common/svg/Check";

/**
 * ReservationModal function
 * @return {component} with data
 *
 */

const initialStateErrors = {
  firstName: false,
  lastName: false,
  email: false,
  phone: false,
};

const ReservationModal = function ReservationModal(props: any) {
  const { unit, locationContent, labels, totalVacancy } = props;
  const submit = useSubmit();
  const actionData = useActionData();

  const [form, setForm] = useState({
    firstName: "",
    lastName: "",
    email: "",
    phone: "",
    marketing: "Yes",
  });

  const [errors, setErrors] = useState<any>(initialStateErrors);
  const [isSending, setIsSending] = useState<boolean>(false);
  const [isError, setIsError] = useState<boolean>(false);

  const handleChange = async (evt: any) => {
    const value = evt.target.value;
    await setForm({
      ...form,
      [evt.target.name]:
        evt.target.name === "phone" ? formatPhoneNumber(value) : value,
    });
  };

  /*************************************ACTION USEEFFECT******************************/

  useEffect(() => {
    if (actionData && !actionData.redirect) {
      if (actionData.error && actionData.totalVacancy && isSending) {
        //Only to reservations error
        setIsError(true);
        setIsSending(false);
      }

      if (actionData.error && !actionData.totalVacancy && isSending) {
        //Only to JoinList with error
        setIsError(true);
        setIsSending(false);
      }

      if (!actionData.error && !actionData.totalVacancy && isSending) {
        //Only to JoinList without error
        //Setting originals values
        setForm({
          firstName: "",
          lastName: "",
          email: "",
          phone: "",
          marketing: "Yes",
        });

        setIsError(false);
        props.setOpenPopUpModal();
        close();
      }
    } else if (actionData && actionData.redirect) {
      analytics(window, "reservationCompleted", {
        locationID: locationContent.locationId,
        unitID: unit.firstAvailableId,
        unitTypeID: unit.unitTypeId,
        unitRate: unit.pushRate,
      });

      identify(window, `${locationContent.locationId}`, {
        email: form.email,
        Location: locationContent.locationId,
        firstName: form.firstName,
        lastName: form.lastName,
        phone: form.phone ? form.phone.replace(/[^\d]/g, "") : "",
        SMS_OptIn: form.marketing === "Yes" ? true : false,
        UnitID: unit.unitTypeId,
      });

      //setIsSending(false);
      globalRedirect(window, actionData.redirectTo);
      //window.location.href = actionData.redirectTo;
    }
  }, [actionData]);

  /*************************************ACTION USEEFFECT******************************/

  const sendInformation = async () => {
    //Setting previous errors
    setIsSending(true);
    setIsError(false);
    setErrors(initialStateErrors);

    //Save on a new object the errors
    const errorsObject = { ...initialStateErrors };

    //Regex functions
    const emailValidation = validateEmail(form.email);
    const numberValidation = validateUSPhone(form.phone);

    //Validations
    if (form.firstName.replace(/ /g, "") === "") errorsObject.firstName = true;
    if (form.lastName.replace(/ /g, "") === "") errorsObject.lastName = true;
    if (!emailValidation) errorsObject.email = true;
    if (!numberValidation) errorsObject.phone = true;

    setErrors({ ...errorsObject });

    if (
      errorsObject.firstName ||
      errorsObject.lastName ||
      errorsObject.email ||
      errorsObject.phone
    ) {
      setIsSending(false);

      return;
    }

    let date = new Date();
    const phoneNumber = form.phone ? form.phone.replace(/[^\d]/g, "") : "";
    const formData = new FormData();
    formData.set("totalVacancy", totalVacancy ? "true" : "false");
    formData.set("firstName", form.firstName);
    formData.set("lastName", form.lastName);
    formData.set("email", form.email);
    formData.set("phone", phoneNumber);
    formData.set("marketing", form.marketing);

    formData.set("locationContent", JSON.stringify(locationContent));
    formData.set("date", date.toISOString());
    formData.set("firstAvailableId", unit.firstAvailableId);
    formData.set("unitTypeId", unit.unitTypeId);
    formData.set("concessionId", unit.concessionId);

    submit(formData, {
      method: "post",
      action: `/locations/${locationContent.slug}`,
    });
  };

  const verifyValue = () => {
    if (
      form.firstName.replace(/ /g, "") === "" ||
      form.lastName.replace(/ /g, "") === "" ||
      form.email === "" ||
      form.phone === "" ||
      form.marketing === ""
    ) {
      return false;
    }
    return true;
  };

  const close = () => {
    props.setOpenReservationModal();
    setErrors(initialStateErrors);
  };

  return (
    <section className="section-modal-reservation-body">
      <div className="div-modal-reservation-body">
        <div className="mb-10 mt-10 text-center font-serif text-28 text-greyMedium">
          {totalVacancy
            ? labels?.reservationFormTitle
            : labels?.textWaitListTitle}
        </div>

        {!totalVacancy && (
          <h4 className="mt-8 text-center text-16">
            {labels?.textWaitListInformation}
          </h4>
        )}

        {totalVacancy ? (
          <div className="div-amenities grid">
            <div className="d-flex div-option justify-center text-greyMedium">
              <Check size="25px" color="#707070" />
              <span className="ml-2 text-16 leading-5">
                {" "}
                {labels?.reservationAmenityText1}
              </span>
            </div>
            <div className="d-flex div-option justify-center text-greyMedium">
              <Check size="25px" color="#707070" />
              <span className="ml-2 text-16 leading-5">
                {labels?.reservationAmenityText2}
              </span>
            </div>
            <div className="d-flex div-option justify-center text-greyMedium">
              <Check size="25px" color="#707070" />
              <span className="ml-2 text-16 leading-5">
                {labels?.reservationAmenityText3}
              </span>
            </div>
            <div className="d-flex div-option justify-center text-greyMedium">
              <Check size="25px" color="#707070" />
              <span className="ml-2 text-16 leading-5">
                {labels?.reservationAmenityText4}
              </span>
            </div>
          </div>
        ) : (
          ""
        )}

        <div className="div-modal-reservation-input mt-4">
          <div className="d-flex justify-end text-12 text-greyMedium">
            All required
          </div>
          <div className="div-input">
            <input
              disabled={isSending}
              value={form.firstName}
              name="firstName"
              onChange={handleChange}
              className="input w-100"
              type="text"
              placeholder="First Name"
            />
          </div>
          {errors && errors.firstName && (
            <p className="message-error text-left">This field is required</p>
          )}

          <div className="div-input">
            <input
              disabled={isSending}
              value={form.lastName}
              name="lastName"
              onChange={handleChange}
              className="input w-100"
              type="text"
              placeholder="Last Name"
            />
          </div>

          {errors && errors.lastName && (
            <p className="message-error text-left">This field is required</p>
          )}

          <div className="div-input">
            <input
              disabled={isSending}
              value={form.email}
              name="email"
              onChange={handleChange}
              className="input w-100"
              type="email"
              placeholder="Email Address"
            />
          </div>

          {errors && errors.email && (
            <p className="message-error text-left">
              This field must be example@example.com
            </p>
          )}

          <div className="div-input">
            <input
              disabled={isSending}
              value={form.phone}
              name="phone"
              onChange={handleChange}
              className="input w-100"
              type="text"
              placeholder="Phone Number"
            />
          </div>

          {errors && errors.phone && (
            <p className="message-error text-left">
              This field must be (234) 567-8901
            </p>
          )}
        </div>

        {totalVacancy ? (
          <div className="div-modal-reservation-checkbox mt-1 text-greyHeavy">
            <h4 className="mt-5 items-center text-16">
              Please text me about my reservation and with marketing
              communications.
            </h4>

            <h4 className="mt-5 mb-5 items-center text-16">
              {labels?.reservationTextDisclaimer}
            </h4>

            <div className="div-checkbox d-flex mt-5 mb-5">
              <label className="flex items-center text-16 text-greyHeavy">
                <input
                  disabled={isSending}
                  onChange={handleChange}
                  type="radio"
                  name="marketing"
                  className="radio"
                  checked={form.marketing === "Yes"}
                  value="Yes"
                />
                Yes
              </label>

              <label className="ml-12 flex items-center text-16 text-greyHeavy">
                <input
                  disabled={isSending}
                  onChange={handleChange}
                  type="radio"
                  name="marketing"
                  className="radio"
                  checked={form.marketing === "No"}
                  value="No"
                />
                No
              </label>
            </div>
          </div>
        ) : (
          ""
        )}

        <div className="div-modal-reservation-button mt-5 mb-5">
          <button
            className={`${
              verifyValue() && !isSending
                ? "bg-primary1 yellow-button text-secondary1"
                : "button-disabled"
            }`}
            onClick={() => sendInformation()}
          >
            {isSending
              ? "Sending..."
              : `${
                  totalVacancy
                    ? labels?.reservationFormButtonLabel
                    : labels?.waitListFormButtonLabel
                }`}
          </button>
        </div>

        {isError ? (
          <p className="message-error-reservation text-center">
            Something went wrong, please try again.
          </p>
        ) : (
          ""
        )}

        <div className="div-modal-reservation-button-close">
          <button disabled={isSending} onClick={() => close()} className="blue-button">
            Close
          </button>
        </div>
      </div>
    </section>
  );
};

export default ReservationModal;
