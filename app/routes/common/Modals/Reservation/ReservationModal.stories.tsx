import type { ComponentMeta } from "@storybook/react";
import ReservationModal from "./index";

export default {
  title: "components/core/ReservationModal",
  component: ReservationModal,
} as ComponentMeta<typeof ReservationModal>;

/**
 * @param {object} args to the component
 * @returns {component} with data.
 */

const Template = (args) => <ReservationModal {...args} />;

export const ReservationModalModule = Template.bind({});
ReservationModalModule.args = {};
