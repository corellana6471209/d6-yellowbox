import { describe, expect, it, vi } from "vitest";
import { setWaitList } from "./backend";

const obj = {
  setWaitList: () => "setWaitList",
};

vi.mock("./backend", () => ({
  setWaitList: vi.fn().mockImplementation(() => "setWaitList"),
}));

describe("fetching location data component unit test", () => {
  it("LoadModuleTitle should be mock", () => {
    expect(vi.isMockFunction(setWaitList)).toBe(true);
  });

  it("Should be executed loadModuleTitle", async () => {
    const spy = vi.spyOn(obj, "setWaitList");

    const resp = await setWaitList([
      "FistNemeTest",
      "LastNameTest",
      "emailtest@test.com",
      "1234567",
      true,
      "9001",
      "1",
      "tetstComment",
    ]);
    expect(spy.getMockName()).toEqual("setWaitList");
    expect(resp).toBe("setWaitList");
  });
});
