import { vi, describe, expect, it } from "vitest";
import { render, screen } from "../../../../utils/test-utils";
import ReservationModal from ".";

const mockSetState = vi.fn();
vi.mock("react", () => ({
  useState: (initial) => [initial, mockSetState],
  useRef: (initial) => [initial, mockSetState],
  useEffect: (initial) => [initial, mockSetState],
}));

describe("ReservationModal component unit test", () => {
  it("verify the render the ReservationModal component", () => {
    render(<ReservationModal />);
  });

  it("verify if the tittle appear", async () => {
    render(<ReservationModal />);
    expect(await screen.findByText(/Reserve Unit/i)).toBeInTheDocument();
  });

  describe("Verify the inputs", () => {
    it("Verify if checkboxes exists", async () => {
      const { container } = render(<ReservationModal />);
      expect(await container.getElementsByClassName("input").length).toBe(4);
    });
  });

  describe("Verify the title-checkbox and checkboxes", () => {
    it("Verify if checkboxes exists", async () => {
      const { container } = render(<ReservationModal />);
      expect(await container.getElementsByClassName("radio").length).toBe(2);
    });
  });
});
