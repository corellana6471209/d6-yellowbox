import { Fragment, useState, useEffect } from "react";
import { Dialog, Transition } from "@headlessui/react";
import Icon from "../../svg/Icon";
import CheckCircle from "../../../common/svg/CheckCircle";

function PaymenSuccessfulModal(props: any) {
  const { open, callback } = props;
  const [isOpen, setIsOpen] = useState(false);

  useEffect(() => {
    setIsOpen(open);
  }, [open]);

  const handlerClose = () => {
    setIsOpen(false);
    callback();
  };

  return (
    <Transition.Root show={isOpen} as={Fragment}>
      <Dialog
        as="div"
        className="message-modal relative"
        onClose={() => handlerClose()}
        aria-label="payment-modal"
      >
        <Transition.Child
          as={Fragment}
          enter="ease-out duration-300"
          enterFrom="opacity-0"
          enterTo="opacity-100"
          leave="ease-in duration-200"
          leaveFrom="opacity-100"
          leaveTo="opacity-0"
        >
          <div className="fixed inset-0 bg-black bg-opacity-50" />
        </Transition.Child>

        <div className="fixed inset-0 z-10 overflow-y-auto">
          <div className="flex min-h-full items-center justify-center p-4">
            <Transition.Child
              as={Fragment}
              enter="ease-out duration-300"
              enterFrom="opacity-0 translate-y-4 sm:translate-y-0 sm:scale-95"
              enterTo="opacity-100 translate-y-0 sm:scale-100"
              leave="ease-in duration-200"
              leaveFrom="opacity-100 translate-y-0 sm:scale-100"
              leaveTo="opacity-0 translate-y-4 sm:translate-y-0 sm:scale-95"
            >
              <Dialog.Panel className="w-full transform bg-white align-middle shadow-xl transition-all md:max-w-[1024px]">
                <section className="section-body-modal-payment-successful bg-white px-4 pt-3 pb-4 sm:px-6 sm:pb-4">
                  <div className="w-100 text-right">
                    <button
                      onClick={() => handlerClose()}
                      aria-label="modal-btn-close"
                    >
                      <Icon iconName="cross" />
                    </button>
                  </div>
                  <div className="div-body-modal-payment-successful">
                    <p className="mt-30 mb-14 text-center text-30 text-26 xs:text-30 sm:text-36">
                      Payment Submitted
                    </p>

                    <div className="d-flex mb-14 justify-center">
                      <CheckCircle />
                    </div>

                    <p className="mb-4 text-center text-16">
                      You have successfully made a payment.
                    </p>

                    <div
                      className="d-flex mt-8 justify-center"
                      onClick={() => handlerClose()}
                    >
                      <button className="button-sucesssful-modal yellow-button block bg-primary1 yellow-button py-3 text-center text-16 text-secondary1 no-underline">
                        Return to Payment
                      </button>
                    </div>
                  </div>
                </section>
              </Dialog.Panel>
            </Transition.Child>
          </div>
        </div>
      </Dialog>
    </Transition.Root>
  );
}

export default PaymenSuccessfulModal;
