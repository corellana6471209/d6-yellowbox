import { ComponentStory, ComponentMeta } from '@storybook/react';
import PaymenSuccessful from './index';

export default {
  title: 'components/core/PaymenSuccessful',
  component: PaymenSuccessful,
} as ComponentMeta<typeof PaymenSuccessful>;

const Template: ComponentStory<typeof PaymenSuccessful> = (args) => <PaymenSuccessful {...args} />;

export const Primary = Template.bind({});
Primary.args = {
  open: true,
  callback: () => { 
    console.log('----> finish') 
  }
};


