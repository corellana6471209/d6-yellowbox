import { describe, expect, it, vi } from "vitest";
import { render, screen } from "../../../../utils/test-utils";
import { BrowserRouter as Router } from "react-router-dom";
import TermsModal from "./index";

// Mock IntersectionObserver
class IntersectionObserver {
  observe = vi.fn();
  disconnect = vi.fn();
  unobserve = vi.fn();
}

Object.defineProperty(window, "IntersectionObserver", {
  writable: true,
  configurable: true,
  value: IntersectionObserver,
});

Object.defineProperty(global, "IntersectionObserver", {
  writable: true,
  configurable: true,
  value: IntersectionObserver,
});

describe("TermsModal component", () => {
  it("Verify if TermsModal component renders", async () => {
    render(
      <Router>
        <TermsModal open={true} />,
      </Router>
    );

    expect(screen.getByLabelText("payment-modal")).toBeInTheDocument();
  });

  it("Verify texts", async () => {
    render(
      <Router>
        <TermsModal open={true} />,
      </Router>
    );

    expect(await screen.findByText(/Payment Submitted/i)).toBeInTheDocument();
    expect(await screen.findByText(/You have successfully made a payment./i)).toBeInTheDocument();
    expect(await screen.findByText(/Return to Payment/i)).toBeInTheDocument();

  });
});
