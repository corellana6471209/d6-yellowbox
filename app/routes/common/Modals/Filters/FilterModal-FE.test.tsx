import { vi, describe, expect, it } from "vitest";
import { render, screen } from "../../../../utils/test-utils";
import FilterModal from ".";

const mockSetState = vi.fn();
vi.mock("react", () => ({
  useState: (initial) => [initial, mockSetState],
  useRef: (initial) => [initial, mockSetState],
  useEffect: (initial) => [initial, mockSetState],
}));

describe("FilterModal component unit test", () => {
  it("verify the render the FilterModal component", () => {
    render(<FilterModal />);
  });

  it("verify if the tittle appear", async () => {
    render(<FilterModal />);
    expect(await screen.findByText(/Sort By/i)).toBeInTheDocument();
  });

  it("Verify if select element exist", async () => {
    const { container } = render(<FilterModal />);
    expect(
      await container.getElementsByClassName("select-filter-sort-by").length
    ).toBe(1);
  });

  it("Verify if texts select all / clear all exists", async () => {
    render(<FilterModal />);
    expect(await screen.findByText(/Select All/i)).toBeInTheDocument();
    expect(await screen.findByText(/Clear All/i)).toBeInTheDocument();
  });

  describe("Verify the title-checkbox and checkboxes", () => {
    it("verify if the tittle appear", async () => {
      render(<FilterModal />);
      expect(await screen.findByText(/Unit Size/i)).toBeInTheDocument();
    });

    it("Verify if checkboxes exists", async () => {
      const { container } = render(<FilterModal />);
      expect(await container.getElementsByClassName("radio").length).toBe(3);
    });
  });
});
