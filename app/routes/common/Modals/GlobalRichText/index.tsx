import { Fragment, useState, useEffect } from "react";
import { Dialog, Transition } from "@headlessui/react";
import * as PropTypes from "prop-types";
import Icon from "../../svg/Icon";
import { documentToReactComponents } from "@contentful/rich-text-react-renderer";
import type { Hyperlink } from "@contentful/rich-text-types";
import { INLINES } from "@contentful/rich-text-types";
import { GetData } from "./backend";

function GlobalRichTextModal(props: any) {
  const { open, additionalClass, callback, labelInformation } = props;
  const [isOpen, setIsOpen] = useState(false);
  const [data, setData] = useState<any>(null);

  const renderOptions: any = {
    renderNode: {
      [INLINES.HYPERLINK]: ({ data }: Hyperlink, children: []) => {
        return (
          <a
            className="text-callout"
            href={data.uri}
            target={`${data.uri ? "_self" : "_blank"}`}
            rel={`${data.uri ? "" : "noopener noreferrer"}`}
          >
            {children}
          </a>
        );
      },
    },
  };

  useEffect(() => {
    async function fetchData() {
      const data = await GetData(labelInformation);
      setData(data);
    }

    fetchData();
  }, []);

  useEffect(() => {
    setIsOpen(open);
  }, [open]);

  const handlerClose = (termsAccepted: boolean = false) => {
    setIsOpen(false);
    callback(termsAccepted);
  };

  const renderDescription = () => {
    return (
      <>
        {data.richText?.content?.json?.content.map(
          (item: any, index: number) => (
            <div key={index} className="mb-3">
              {documentToReactComponents(item, renderOptions)}
            </div>
          )
        )}
      </>
    );
  };

  return (
    <Transition.Root show={isOpen} as={Fragment}>
      <Dialog
        as="div"
        className="message-modal relative"
        onClose={() => {
          setIsOpen(false);
          callback();
        }}
        aria-label="terms-modal"
      >
        <Transition.Child
          as={Fragment}
          enter="ease-out duration-300"
          enterFrom="opacity-0"
          enterTo="opacity-100"
          leave="ease-in duration-200"
          leaveFrom="opacity-100"
          leaveTo="opacity-0"
        >
          <div className="fixed inset-0 bg-black bg-opacity-50" />
        </Transition.Child>

        <div className="fixed inset-0 z-10 overflow-y-auto">
          <div className="flex min-h-full items-center justify-center p-4">
            <Transition.Child
              as={Fragment}
              enter="ease-out duration-300"
              enterFrom="opacity-0 translate-y-4 sm:translate-y-0 sm:scale-95"
              enterTo="opacity-100 translate-y-0 sm:scale-100"
              leave="ease-in duration-200"
              leaveFrom="opacity-100 translate-y-0 sm:scale-100"
              leaveTo="opacity-0 translate-y-4 sm:translate-y-0 sm:scale-95"
            >
              <Dialog.Panel className="w-full transform bg-white align-middle shadow-xl transition-all md:max-w-[700px]">
                <div className="bg-white px-4 pt-3 pb-4 sm:px-6 sm:pb-4">
                  <div className="w-100 text-right">
                    <button
                      onClick={() => {
                        setIsOpen(false);
                        callback();
                      }}
                      aria-label="modal-btn-close"
                    >
                      <Icon iconName="cross" />
                    </button>
                  </div>

                  {data && (
                    <div className="sm:flex sm:items-start">
                      <div className="mt-3">
                        {data.title && (
                          <Dialog.Title
                            as="h3"
                            className="text-lg text-gray-900 mb-6 font-medium leading-6"
                            aria-label="modal-title"
                          >
                            {data.title ? data.title : ""}
                          </Dialog.Title>
                        )}

                        <div
                          className={`mt-2 max-h-[68vh] overflow-auto ${additionalClass}`}
                          aria-label="modal-description"
                        >
                          {renderDescription()}
                        </div>
                      </div>
                    </div>
                  )}
                </div>

                <div className="py-3 px-4 text-right sm:px-6">
                  <button
                    aria-label="modal-btn-cancel"
                    className="bg-primary1 yellow-button py-3 px-5 text-secondary1"
                    onClick={() => handlerClose(true)}
                  >
                    {data ? data.ctaText : "Close"}
                  </button>

                  {/**
                 *   <button
                    aria-label="modal-btn-ok"
                    className="bg-primary1 yellow-button py-3 px-5 text-secondary1"
                    onClick={() => handlerClose(false)}
                  >
                    Cancel
                  </button>
                 * 
                 */}
                </div>
              </Dialog.Panel>
            </Transition.Child>
          </div>
        </div>
      </Dialog>
    </Transition.Root>
  );
}

export default GlobalRichTextModal;

// Specifies the proptypes:
GlobalRichTextModal.propTypes = {
  open: PropTypes.bool,
  additionalClass: PropTypes.string,
  callback: PropTypes.func,
  labelInformation: PropTypes.string,
};

// Specifies the default values for props:
GlobalRichTextModal.defaultProps = {
  open: false,
  labelInformation: "terms",
  additionalClass: "text-14",
  callback: () => {
    console.log("---> callback terms modal");
  },
};
