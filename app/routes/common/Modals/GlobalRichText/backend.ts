import fetchContentful from "../../../../utils/fetchContentful";

export async function GetData(params: string) {
  const query = `
  {
    globalRichTextModalCollection(limit: 1, where: {id: "${params}"}) {
      items {
        title
        ctaText
        richText {
          name
          content {
            json
            links {
              assets {
                block {
                  title
                  url
                  sys {
                    id
                  }
                }
              }
            }
          }
        }
      }
    }
  }
  
  `;

  const data = await fetchContentful(query);

  if (
    data.globalRichTextModalCollection &&
    data.globalRichTextModalCollection.items[0] != null
  )
    return data.globalRichTextModalCollection.items[0];
  else return null;
}
