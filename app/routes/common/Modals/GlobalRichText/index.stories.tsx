import { ComponentStory, ComponentMeta } from '@storybook/react';
import TermsModal from './index';

export default {
  title: 'components/core/TermsModal',
  component: TermsModal,
} as ComponentMeta<typeof TermsModal>;

const Template: ComponentStory<typeof TermsModal> = (args) => <TermsModal {...args} />;

export const Primary = Template.bind({});
Primary.args = {
  open: true,
  callback: () => { 
    console.log('----> finish') 
  }
};


