import { describe, expect, it, vi } from "vitest";
import { render, screen, fireEventAliased } from "../../../../utils/test-utils";
import { BrowserRouter as Router } from "react-router-dom";
import TermsModal from './index';
import React from "react";

// Mock IntersectionObserver
class IntersectionObserver {
    observe = vi.fn();
    disconnect = vi.fn();
    unobserve = vi.fn();
}
  
Object.defineProperty(window, "IntersectionObserver", {
    writable: true,
    configurable: true,
    value: IntersectionObserver,
});

Object.defineProperty(global, "IntersectionObserver", {
    writable: true,
    configurable: true,
    value: IntersectionObserver,
});

/*const mockSetState = vi.fn();
vi.mock("react", () => ({
  useState: (initial) => [initial, mockSetState],
  useEffect: (initial) => [initial, mockSetState],
}));*/

const fakeData = {
  ctaText: "Accept",
  description: {
    json: {
      data: {},
      nodeType: "document",
      content: [
        {
          content: [{ data: {}, marks: [], nodeType: "text", value: "Thank you for visiting the "}],
          data: {}, 
          nodeType: "paragraph"
        }
      ]
    }, 
    links: {}
  },
  title: "Terms & Conditions",
};

// Cache original functionality
const fetchFunction = {
  json: function () {
    return fakeData;
  },
};

const obj = {
  fetchData: async () => {
    return fetchFunction;
  }
}

describe("TermsModal component", () => {
  
  it("Verify if TermsModal component renders", async () => {
    render( 
      <Router> 
        <TermsModal open={true} />,
      </Router>
    );

    expect(screen.getByLabelText('terms-modal')).toBeInTheDocument();
  });

  it("Should close modal on click the btn OK", async () => {
    const { container } = render(
      <Router>
        <TermsModal open={true} />,
      </Router>
    );
    
    const button = screen.getByLabelText("modal-btn-ok");
    fireEventAliased.click(button);

    expect(
      await container.getElementsByClassName('terms-modal').length).toBe(0);
  });

  it("Verify that exist description", async () => {
    render(
      <Router>
        <TermsModal open={true} />,
      </Router>
    );
    
    const wrapperText = screen.getByLabelText("modal-description");
    expect(wrapperText.innerHTML).toBeDefined();
  });
});
