import { vi, describe, expect, it } from "vitest";
import { render, screen } from "../../../../utils/test-utils";
import FilterInformationModal from ".";

const filtersContentful = {
  distance: "DISTANCE",
  price: "PRICE",
  parking: "PARKING",
  small: "SMALL",
  medium: "MEDIUM",
  large: "LARGE",
};

const mockSetState = vi.fn();
vi.mock("react", () => ({
  useState: (initial) => [initial, mockSetState],
  useRef: (initial) => [initial, mockSetState],
  useEffect: (initial) => [initial, mockSetState],
}));

describe("FilterModal component unit test", () => {
  it("verify the render the FilterModal component", () => {
    render(<FilterInformationModal filtersContentful={filtersContentful} />);
  });

  it("verify if the tittle appear", async () => {
    render(<FilterInformationModal filtersContentful={filtersContentful} />);
    expect(await screen.findByText(/Sort By/i)).toBeInTheDocument();
  });

  it("Verify if select element exist", async () => {
    const { container } = render(
      <FilterInformationModal filtersContentful={filtersContentful} />
    );
    expect(
      await container.getElementsByClassName("select-filter-sort-by").length
    ).toBe(1);
  });

  it("Verify if texts select all / clear all exists", async () => {
    render(<FilterInformationModal filtersContentful={filtersContentful} />);
    expect(await screen.findByText(/Select All/i)).toBeInTheDocument();
    expect(await screen.findByText(/Clear All/i)).toBeInTheDocument();
  });

  describe("Verify the title-checkbox", () => {
    it("verify if the tittle appear", async () => {
      render(<FilterInformationModal filtersContentful={filtersContentful} />);
      expect(
        await screen.findByText(/Filter By: Unit Size/i)
      ).toBeInTheDocument();
    });
  });
});
