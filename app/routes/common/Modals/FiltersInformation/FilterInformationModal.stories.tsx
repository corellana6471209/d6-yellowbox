import type { ComponentMeta } from '@storybook/react';
import FilterModal from './index';

export default {
    title: 'components/core/FilterModal',
    component: FilterModal,
  } as ComponentMeta<typeof FilterModal>;

/**
 * @param {object} args to the component
 * @returns {component} with data.
 */

const Template = args => <FilterModal {...args} />;

export const FilterModalModule = Template.bind({});
FilterModalModule.args = {
  filtersChecked: []
};
