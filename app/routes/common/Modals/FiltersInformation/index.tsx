import React, { useState, useEffect } from "react";
import background from "../../../../resources/dropdown.svg";

/**
 * FilterInformationModal function
 * @return {component} with data
 *
 */

const FilterInformationModal = function FilterInformationModal(props: any) {
  // State with list of all checked item
  const [filtersChecked, setFiltersChecked] = useState<any[]>(
    props.filtersChecked
  );
  const [valueSelected, setValueSelected] = useState<string>("PRICELOWTOHIGH");

  // Add/Remove checked item from list
  const handleCheck = (event: any) => {
    let updatedList: any[] = [...filtersChecked];

    if (event.target.checked) {
      updatedList = [...filtersChecked, { filter: event.target.value }];
    } else {
      updatedList.splice(
        filtersChecked
          .map(function (e: any) {
            return e.filter;
          })
          .indexOf(event.target.value),
        1
      );
    }

    setFiltersChecked(updatedList);
  };

  const handleSelect = (event: any) => {
    let updatedList: any[];

    updatedList = [
      ...filtersChecked.filter((data) => data.filter !== valueSelected),
      { filter: event.target.value },
    ];

    setValueSelected(event.target.value);
    setFiltersChecked(updatedList);
  };

  // Select all checkboxes
  const selectAll = () => {
    const arrayOfFilters = [
      { filter: valueSelected },
      {
        filter:
          props.filtersContentful.parking &&
          props.filtersContentful.parking.value,
      },
      {
        filter:
          props.filtersContentful.small && props.filtersContentful.small.value,
      },
      {
        filter:
          props.filtersContentful.medium &&
          props.filtersContentful.medium.value,
      },
      {
        filter:
          props.filtersContentful.large && props.filtersContentful.large.value,
      },
    ];

    setFiltersChecked(arrayOfFilters);
  };

  // Remove all checkboxes
  const clearAll = () => {
    setFiltersChecked([{ filter: "PRICELOWTOHIGH" }]);
    setValueSelected("PRICELOWTOHIGH");
  };

  const applyFilters = () => {
    props.setOpenModal(false);
    props.setFiltersChecked(filtersChecked);
  };

  /*******************************CODE TO FILTERS************************ */

  //Get value
  const getValue = () => {
    const priceLowToHigh = props.filtersChecked.some(
      (data: any) => data.filter === "PRICELOWTOHIGH"
    );

    const priceHighToLow = props.filtersChecked.some(
      (data: any) => data.filter === "PRICEHIGHTOLOW"
    );

    const smallToLarge = props.filtersChecked.some(
      (data: any) => data.filter === "SMALLTOLARGE"
    );

    const largeToSmall = props.filtersChecked.some(
      (data: any) => data.filter === "LARGETOSMALL"
    );

    return {
      priceLowToHigh,
      priceHighToLow,
      smallToLarge,
      largeToSmall,
    };
  };

  useEffect(() => {
    const value = getValue();
    if (value.priceLowToHigh) {
      setValueSelected("PRICELOWTOHIGH");
    }

    if (value.priceHighToLow) {
      setValueSelected("PRICEHIGHTOLOW");
    }

    if (value.smallToLarge) {
      setValueSelected("SMALLTOLARGE");
    }

    if (value.largeToSmall) {
      setValueSelected("LARGETOSMALL");
    }

    //setValue();

    // eslint-disable-next-line
  }, [props.filtersChecked, props.openModal]);

  /*******************************CODE TO FILTERS************************ */
  // ref={wrapperRef}
  return (
    <>
      <div className="div-modal-filters">
        <div className="div-select-sort-by flex items-center">
        <h4 className="mr-2 text-16 font-bold text-secondary2">Sort By</h4>

          <select
            name="sortBy"
            id="sortBy"
            style={{ backgroundImage: `url(${background})` }}
            className="select-filter-sort-by"
            onChange={handleSelect}
            value={valueSelected}
          >
            {props.filtersContentful.priceLowToHigh && (
              <option value={props.filtersContentful.priceLowToHigh.value}>
                {props.filtersContentful.priceLowToHigh.title}
              </option>
            )}

            {props.filtersContentful.priceHighToLow && (
              <option value={props.filtersContentful.priceHighToLow.value}>
                {props.filtersContentful.priceHighToLow.title}{" "}
              </option>
            )}

            {props.filtersContentful.largeToSmall && (
              <option value={props.filtersContentful.largeToSmall.value}>
                {props.filtersContentful.largeToSmall.title}{" "}
              </option>
            )}

            {props.filtersContentful.smallToLarge && (
              <option value={props.filtersContentful.smallToLarge.value}>
                {props.filtersContentful.smallToLarge.title}{" "}
              </option>
            )}
          </select>
        </div>

        <div className="div-select-clear flex items-center">
          <p className="p-select-all cursor-pointer blue-button" onClick={selectAll}>
            Select All
          </p>
          <p className="cursor-pointer blue-button" onClick={clearAll}>
            Clear All
          </p>
        </div>

        <div>
          <h4 className="title-filter-by text-16 font-bold text-secondary2">
            Filter By: Unit Size
          </h4>
          <div className="div-unit-size-label mt-3 flex grid grid-cols-2 gap-y-5 gap-x-6 md:grid-cols-2">
            {props.filtersContentful.parking && (
              <label className="flex items-center text-16 text-greyHeavy">
                <input
                  onChange={handleCheck}
                  checked={filtersChecked.some((e) => e.filter === "PARKING")}
                  type="checkbox"
                  className="radio mr-5"
                  value={props.filtersContentful.parking.value}
                />
                {props.filtersContentful.parking.title}
              </label>
            )}

            {props.filtersContentful.small && (
              <label className="flex items-center text-16 text-greyHeavy">
                <input
                  onChange={handleCheck}
                  checked={filtersChecked.some((e) => e.filter === "SMALL")}
                  type="checkbox"
                  className="radio mr-5"
                  value={props.filtersContentful.small.value}
                />
                {props.filtersContentful.small.title}
              </label>
            )}

            {props.filtersContentful.medium && (
              <label className="flex items-center text-16 text-greyHeavy">
                <input
                  onChange={handleCheck}
                  checked={filtersChecked.some((e) => e.filter === "MEDIUM")}
                  type="checkbox"
                  className="radio mr-5"
                  value={props.filtersContentful.medium.value}
                />
                {props.filtersContentful.medium.title}
              </label>
            )}

            {props.filtersContentful.large && (
              <label className="flex items-center text-16 text-greyHeavy">
                <input
                  onChange={handleCheck}
                  checked={filtersChecked.some((e) => e.filter === "LARGE")}
                  type="checkbox"
                  className="radio mr-5"
                  value={props.filtersContentful.large.value}
                />
                {props.filtersContentful.large.title}
              </label>
            )}
          </div>
        </div>

        <div className="button-div-modal text-center">
          <button
            onClick={() => applyFilters()}
            className="bg-primary1 yellow-button py-3 px-10 text-secondary1"
          >
            Apply
          </button>
        </div>
      </div>
    </>
  );
};

export default FilterInformationModal;
