import { describe, expect, it, vi } from "vitest";
import { render, screen, fireEventAliased } from "../../../../utils/test-utils";
import { BrowserRouter as Router } from "react-router-dom";
import TermsModal from './index';

// Mock IntersectionObserver
class IntersectionObserver {
  observe = vi.fn();
  disconnect = vi.fn();
  unobserve = vi.fn();
}

Object.defineProperty(window, "IntersectionObserver", {
  writable: true,
  configurable: true,
  value: IntersectionObserver,
});

Object.defineProperty(global, "IntersectionObserver", {
  writable: true,
  configurable: true,
  value: IntersectionObserver,
});

const fakeData = {
  ctaTextWithAutoPay: "Keep Enroll with Auto  Pay",
  "ctaTextWithoutAutoPay": "Continue without enrolling in Auto Pay",
  description: {
    json: {
      data: {},
      nodeType: "document",
      content: [
        {
          content: [{ data: {}, marks: [], nodeType: "text", value: "Thank you for visiting the " }],
          data: {},
          nodeType: "paragraph"
        }
      ]
    },
    links: {}
  },
  title: "Payment Successful",
};

// Cache original functionality
const fetchFunction = {
  json: function () {
    return fakeData;
  },
};

const obj = {
  fetchData: async () => {
    return fetchFunction;
  }
}

describe("TermsModal component", () => {

  it("Verify if TermsModal component renders", async () => {
    render(
      <Router>
        <TermsModal open={true} />,
      </Router>
    );

    expect(screen.getByLabelText('payment-modal')).toBeInTheDocument();
  });


  it("Verify that, exists body", async () => {
    render(
      <Router>
        <TermsModal open={true} />,
      </Router>
    );

    const wrapperText = screen.getByLabelText("modal-body");
    expect(wrapperText.innerHTML).toBeDefined();
  });
});
