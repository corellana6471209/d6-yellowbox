import { Fragment, useState, useEffect } from "react";
import { Dialog, Transition } from "@headlessui/react";
import { INLINES } from "@contentful/rich-text-types";
import { documentToReactComponents } from "@contentful/rich-text-react-renderer";
import type { Hyperlink } from "@contentful/rich-text-types";
import { getAutoPayModal } from "./backend";
import Icon from "../../svg/Icon";

function AutoPayModal(props: any) {
  const { open, additionalClass, callback, message } = props;
  const [isOpen, setIsOpen] = useState(false);
  const [description, setDescription] = useState<any>(null);
  const [ctaTextWithAutoPay, setCtaTextWithAutoPay] = useState<any>(null);
  const [ctaTextWithoutAutoPay, setCtaTextWithoutAutoPay] = useState<any>(null);

  const renderOptions: any = {
    renderNode: {
      [INLINES.HYPERLINK]: ({ data }: Hyperlink, children: []) => {
        return (
          <a
            className="text-callout"
            href={data.uri}
            target={`${data.uri ? "_self" : "_blank"}`}
            rel={`${data.uri ? "" : "noopener noreferrer"}`}
          >
            {children}
          </a>
        );
      },
    },
  };

  useEffect(() => {
    async function fetchData() {
      const data = await getAutoPayModal("autoPayModal");
      setDescription(data.body);

      setCtaTextWithAutoPay(data.ctaTextWithAutoPay);
      setCtaTextWithoutAutoPay(data.ctaTextWithoutAutoPay);
      return data;
    }

    fetchData();
  }, []);

  useEffect(() => {
    setIsOpen(open);
  }, [open]);

  const handlerClose = (value: any) => {
    setIsOpen(false);
    callback(value);
  };

  const renderDescription = () => {
    return (
      <>
        {description?.content?.json?.content.map((item: any, index: number) => (
          <div key={index} className="mb-3">
            {documentToReactComponents(item, renderOptions)}
          </div>
        ))}
      </>
    );
  };

  return (
    <Transition.Root show={isOpen} as={Fragment}>
      <Dialog
        as="div"
        className="message-modal relative"
        onClose={() => handlerClose(true)}
        aria-label="payment-modal"
      >
        <Transition.Child
          as={Fragment}
          enter="ease-out duration-300"
          enterFrom="opacity-0"
          enterTo="opacity-100"
          leave="ease-in duration-200"
          leaveFrom="opacity-100"
          leaveTo="opacity-0"
        >
          <div className="fixed inset-0 bg-black bg-opacity-50" />
        </Transition.Child>

        <div className="fixed inset-0 z-10 overflow-y-auto">
          <div className="flex min-h-full items-center justify-center p-4">
            <Transition.Child
              as={Fragment}
              enter="ease-out duration-300"
              enterFrom="opacity-0 translate-y-4 sm:translate-y-0 sm:scale-95"
              enterTo="opacity-100 translate-y-0 sm:scale-100"
              leave="ease-in duration-200"
              leaveFrom="opacity-100 translate-y-0 sm:scale-100"
              leaveTo="opacity-0 translate-y-4 sm:translate-y-0 sm:scale-95"
            >
              <Dialog.Panel className="w-full transform bg-white align-middle shadow-xl transition-all md:max-w-[700px]">
                <div className="bg-white px-4 pt-3 pb-4 sm:px-6 sm:pb-4">
                  <div className="w-100 text-right">
                    <button
                      onClick={() => handlerClose(true)}
                      aria-label="modal-btn-close"
                    >
                      <Icon iconName="cross" />
                    </button>
                  </div>

                  <div className="sm:flex sm:items-start">
                    <div className="mt-3">
                      <div
                        className={`mt-2 max-h-[68vh] overflow-auto ${additionalClass}`}
                        aria-label="modal-body"
                      >
                        {message ? (
                          <p>{message}</p>
                        ) : (
                          <>{renderDescription()}</>
                        )}
                      </div>
                    </div>
                  </div>

                  <div
                    className="d-flex mt-8 justify-center"
                    onClick={() => handlerClose(true)}
                  >
                    <button className="button-sucesssful-modal block bg-primary1 yellow-button py-3 text-center text-16 text-secondary1 no-underline">
                      {ctaTextWithAutoPay}
                    </button>
                  </div>

                  <p
                    className="mt-8 mb-10 cursor-pointer text-center text-12 font-bold text-callout link sm:text-14 md:text-16"
                    onClick={() => handlerClose(false)}
                  >
                    {ctaTextWithoutAutoPay}
                  </p>
                  <div></div>
                </div>
              </Dialog.Panel>
            </Transition.Child>
          </div>
        </div>
      </Dialog>
    </Transition.Root>
  );
}

export default AutoPayModal;
