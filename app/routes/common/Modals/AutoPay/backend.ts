import fetchContentful from "../../../../utils/fetchContentful";

export async function getAutoPayModal(params: string) {
  const query = `
  {
    autoPayModalCollection(limit: 1, where: {id: "${params}"}) {
      items {
        title
        ctaTextWithAutoPay
        ctaTextWithoutAutoPay
        body {
          name
          content {
            json
            links {
              assets {
                block {
                  title
                  url
                  sys {
                    id
                  }
                }
              }
            }
          }
        }
      }
    }
  }
  
  `;

  const data = await fetchContentful(query);

  if (
    data.autoPayModalCollection &&
    data.autoPayModalCollection.items[0] != null
  )
    return data.autoPayModalCollection.items[0];
  else return null;
}
