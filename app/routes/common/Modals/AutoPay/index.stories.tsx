import { ComponentStory, ComponentMeta } from '@storybook/react';
import AutoPayModal from './index';

export default {
  title: 'components/core/AutoPayModal',
  component: AutoPayModal,
} as ComponentMeta<typeof AutoPayModal>;

const Template: ComponentStory<typeof AutoPayModal> = (args) => <AutoPayModal {...args} />;

export const Primary = Template.bind({});
Primary.args = {
  open: true,
  callback: () => { 
    console.log('----> finish') 
  }
};


