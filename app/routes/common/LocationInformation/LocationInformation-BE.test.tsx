import { describe, expect, it } from "vitest";
import { loadLocationContent } from "../../locations/backend";
import { locationInformation } from "../../../utils/fakedataToTests";

const slug = "panthersville-decature-self-storage-2";
const fetchFunction = {
  json: function () {
    return {
      data: {
        contentTypeLocationCollection: { ...locationInformation },
      },
      errors: null,
    };
  },
};
global.fetch = async () => {
  return fetchFunction;
};

describe("LocationInformation component BE", () => {
  it("Verify the slug of the component", async () => {
    const data = await loadLocationContent(slug);
    expect(data.contentTypeLocationCollection.slug).toBe(
      "panthersville-decature-self-storage-2"
    );
  });

  it("Verify the image information", async () => {
    const data = await loadLocationContent(slug);

    expect(data.contentTypeLocationCollection.locationImage.title).toBe(
      "Panthersville / Decatur Self Storage"
    );

    expect(data.contentTypeLocationCollection.locationImage.description).toBe(
      "Main image for Panthersville / Decatur Self Storage"
    );

    expect(data.contentTypeLocationCollection.locationImage.url).toBe(
      "https://images.ctfassets.net/rfh1327471ch/79zy3LJSjYVR9mZ2jYxlrq/fc6f075c536e5f07ebbc93207a595b9b/panthersville--decatur_af947f0321d307d8e398d398a5c8fb9dd1e50cb9.jpeg"
    );
  });

  it("Verify the address information", async () => {
    const data = await loadLocationContent(slug);

    expect(data.contentTypeLocationCollection.address.address1).toBe(
      "1234 Peachtree street NE"
    );

    expect(data.contentTypeLocationCollection.address.state.stateFullName).toBe(
      "Georgia"
    );
    expect(
      data.contentTypeLocationCollection.address?.state?.stateAbbreviation
    ).toBe("GA");

    expect(data.contentTypeLocationCollection.address.postalCode).toBe("30032");
  });
});
