import type { ComponentMeta } from "@storybook/react";
import LocationInformation from "./index";
import { BrowserRouter as Router } from "react-router-dom";

export default {
  title: "modules/LocationInformation",
  component: LocationInformation,
} as ComponentMeta<typeof LocationInformation>;

const Template = () => (
  <Router>
    <LocationInformation />
  </Router>
);

export const LocationListTypical = Template.bind({});
