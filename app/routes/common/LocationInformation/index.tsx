import { useState, useEffect } from "react";
import StarRatings from "react-star-ratings";
import useWindowDimensions from "../../../utils/hooks/useWindowDimensions";
// import { useNavigate } from "react-router-dom";
import type { Location } from "../../locations/$slug";
import useRating from "../../../utils/hooks/useRating";
import calculateDistance from "../../../utils/hooks/useDistance";
import { getAddressCoords } from "../../../utils/cookie";
import ChevronLeft from "../../common/svg/ChevronLeft";
import ChevronUp from "../../common/svg/ChevronUp";
import ChevronDown from "../../common/svg/ChevronDown";

/**
 *
 * @param {props} props props recieved
 * @returns {Function} main Search
 */
function LocationInformation(props: Location) {
  // const navigate = useNavigate();
  const coords = getAddressCoords();
  const previousRoute = document.referrer;
  const currentlyRoute = window.location.href;

  const {
    slug,
    locationName,
    locationImageCollection,
    address,
    phoneNumberNewCustomer,
    phoneNumberExistingCustomer,
    mondayOfficeHours,
    tuesdayOfficeHours,
    wednesdayOfficeHours,
    thursdayOfficeHours,
    fridayOfficeHours,
    saturdayOfficeHours,
    sundayOfficeHours,
    lunchHours,
    gateAccessHours,
    googleBusinessProfileId,
  } = props;
  const [showDetails, setShowDetails] = useState<boolean>(false);
  const { width } = useWindowDimensions();
  const distance = calculateDistance(
    null,
    null,
    address.coordinates.lat,
    address.coordinates.lon
  );

  // Get rating
  const rating = useRating(googleBusinessProfileId);
  useEffect(() => {
    if (width < 1024) {
      setShowDetails(false);
    } else {
      setShowDetails(true);
    }
  }, [width]);

  const locationDetails = (value: boolean) => {
    setShowDetails(value);
  };

  const goBack = (route: string) => {
    const verifyRoute = route.includes("locations");

    if (verifyRoute) {
      try {
        const routeSplitted = route.split("locations");
        // navigate(`/locations${routeSplitted[1]}`);
        window.location.href = `/locations${routeSplitted[1]}`;
      } catch (error) {
        // navigate("/");
        window.location.href = "/";
      }
    } else {
      // navigate("/");
      window.location.href = "/";
    }
  };

  const getDirection = () => {
    if (coords) {
      const dataCoords = JSON.parse(coords);
      window.open(
        `https://www.google.com/maps/dir/${dataCoords.lat},${dataCoords.lng}/${address.coordinates.lat},${address.coordinates.lon}/@${dataCoords.lat},${dataCoords.lng},13z`,
        "_blank"
      );
    } else {
      window.open(
        `https://www.google.com/maps/dir//${address.coordinates.lat},${address.coordinates.lon}/@${address.coordinates.lat},${address.coordinates.lon},14z/`,
        "_blank"
      );
    }
  };

  return (
    <div className="d-flex justify-center">
      <div className="div-location-information grid max-w-[1200px]">
        <div className="div-img-banner">
          {previousRoute && previousRoute !== currentlyRoute && (
            <div
              className="div-badge-img full-opacity absolute flex items-center bg-secondary5 p-3 opacity-80"
              onClick={() => goBack(previousRoute)}
            >
              <ChevronLeft size="30px" color="#FFFFFF" />
              <p className="text-sm ml-2 text-16 text-white">Back To Results</p>
            </div>
          )}
          {locationImageCollection.items[0] && (
            <img
              src={locationImageCollection.items[0].url}
              alt={locationImageCollection.items[0].description}
              title={locationImageCollection.items[0].title}
              className="img-location-information object-cover"
            />
          )}
        </div>

        <div className="div-body-text text-white">
          {locationName && (
            <h1 className="title-body-text font-serif text-28 md:text-32">
              {locationName}
            </h1>
          )}
          {typeof rating === "number" ? (
            <div className="location-star-rating">
              <StarRatings
                rating={rating}
                starRatedColor="#FFFFFF"
                starEmptyColor="rgba(255, 255, 255, 0.25)"
                numberOfStars={5}
                name="rating"
                starDimension="25px"
                starSpacing="0px"
              />
            </div>
          ) : (
            ""
          )}
          <div className="grid grid-cols-1 lg:grid-cols-2">
            {address && (
              <div className="div-text-address">
                <h4 className="text-16 md:text-20 md:leading-7">
                  {address.address1}
                </h4>
                <h4 className="mb-5 text-16 md:mb-0 md:text-20 md:leading-7">
                  {address.city}, {address?.state?.stateAbbreviation}{" "}
                  {address.postalCode}
                </h4>

                <div
                  className={`div-text-get-directions mb-5 h-7 md:mb-auto md:h-auto ${
                    showDetails && "margin-custom"
                  }`}
                >
                  {distance && (
                    <p className="font-bold md:mt-5 md:mb-5">
                      {distance ? `${Number(distance).toFixed(2)} mi` : ""}
                    </p>
                  )}
                  <p
                    className="text-direction link font-bold text-primary1"
                    onClick={() => getDirection()}
                  >
                    Get Directions
                  </p>
                </div>
              </div>
            )}

            {!showDetails && (
              <>
                <div
                  className="div-view-detail text-center"
                  onClick={() => locationDetails(true)}
                  data-testid="div-view-more-detail"
                >
                  <ChevronDown size="45px" color="#FFFFFF" />
                  <p className="text-12 text-white"> view more detail</p>
                </div>
              </>
            )}

            {showDetails && (
              <div className="div-text-customer">
                {phoneNumberNewCustomer && (
                  <div>
                    <p className="title-new-customer font-bold">New Customer</p>
                    <p className="number-new-customer mt-2 md:mt-2.5">
                      {width < 1024 ? (
                        <a href={`tel:${phoneNumberNewCustomer}`}>
                          {phoneNumberNewCustomer}
                        </a>
                      ) : (
                        phoneNumberNewCustomer
                      )}
                    </p>
                  </div>
                )}

                {phoneNumberExistingCustomer && (
                  <div>
                    <p className="title-current-customer mt-0 mt-3 font-bold">
                      Current Customer
                    </p>
                    <p className="number-current-customer mt-2 md:mt-2.5">
                      {width < 1024 ? (
                        <a href={`tel:${phoneNumberExistingCustomer}`}>
                          {phoneNumberExistingCustomer}
                        </a>
                      ) : (
                        phoneNumberExistingCustomer
                      )}
                    </p>
                  </div>
                )}
              </div>
            )}
          </div>
          {showDetails && (
            <>
              <div className="div-text-days">
                <p className="text-office-hours font-bold md:mb-2.5">
                  Office Hours
                </p>

                <div className="div-days">
                  {mondayOfficeHours && (
                    <p className="flex">
                      <span className="span-text-day">Mon.</span>
                      {mondayOfficeHours}
                    </p>
                  )}
                  {tuesdayOfficeHours && (
                    <p className="flex">
                      <span className="span-text-day">Tue.</span>
                      {tuesdayOfficeHours}
                    </p>
                  )}
                  {wednesdayOfficeHours && (
                    <p className="flex">
                      <span className="span-text-day">Wed.</span>
                      {wednesdayOfficeHours}
                    </p>
                  )}

                  {thursdayOfficeHours && (
                    <p className="flex">
                      <span className="span-text-day">Thu.</span>
                      {thursdayOfficeHours}
                    </p>
                  )}

                  {fridayOfficeHours && (
                    <p className="flex">
                      <span className="span-text-day">Fri.</span>
                      {fridayOfficeHours}
                    </p>
                  )}

                  {saturdayOfficeHours && (
                    <p className="flex">
                      <span className="span-text-day">Sat.</span>
                      {saturdayOfficeHours}
                    </p>
                  )}

                  {sundayOfficeHours && (
                    <p className="flex">
                      <span className="span-text-day">Sun.</span>
                      {sundayOfficeHours}
                    </p>
                  )}
                </div>
              </div>

              <div className="div-hours-information grid grid-cols-1 md:gap-10 lg:grid-cols-2">
                {lunchHours && (
                  <div className="div-lunch-hours">
                    <p className="font-bold">Lunch Hours (Closed)</p>
                    <p className="mt-2 md:mt-2.5"> {lunchHours}</p>
                  </div>
                )}

                {gateAccessHours && (
                  <div className="div-gate-access">
                    <p className="font-bold">Gate Access Hours</p>
                    <p className="mt-2 mb-10 md:mt-2.5 md:mb-auto">
                      {gateAccessHours}
                    </p>
                  </div>
                )}
              </div>
              <div
                data-testid="div-view-less-detail"
                className="div-view-detail text-center"
                onClick={() => locationDetails(false)}
              >
                <ChevronUp size="45px" color="#FFFFFF" />
                <p className="text-12 text-white"> view less detail</p>
              </div>
            </>
          )}
        </div>
      </div>
    </div>
  );
}

/**
 * Search.propTypes = {
  iconColor: PropTypes.string,
  overlayCloseBtn: PropTypes.func,
  name: PropTypes.string,
  show: PropTypes.bool,
  toggleNav: PropTypes.any,
};

Search.defaultProps = {
  iconColor: "#000",
  show: true,
};

 */
export default LocationInformation;
