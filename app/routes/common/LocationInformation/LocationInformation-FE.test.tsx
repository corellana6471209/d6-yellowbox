import React from "react";
import { describe, expect, it } from "vitest";
import LocationInformation from ".";
import { render, screen, userEvent } from "../../../utils/test-utils";
import { BrowserRouter as Router } from "react-router-dom";
import { locationInformation } from "../../../utils/fakedataToTests";

describe("Search component", () => {
  beforeEach(() => {
    const setState = vi.fn();
    const useStateSpy = vi.spyOn(React, "useState");
    useStateSpy.mockImplementation((initialValue) => [initialValue, setState]);
  });
  afterEach(() => {
    vi.clearAllMocks();
  });

  it("Verify if the component renders", () => {
    render(
      <Router>
        <LocationInformation {...locationInformation} />
      </Router>
    );
  });

  it("Verify if exists title of body text", async () => {
    render(
      <Router>
        <LocationInformation {...locationInformation} />
      </Router>
    );

    expect(
      await screen.findByText("Panthersville / Decatur Self Storage 2 (1)")
    ).toBeInTheDocument();
  });

  it("Verify if exists div with view more details text", async () => {
    render(
      <Router>
        <LocationInformation {...locationInformation} />
      </Router>
    );
    userEvent.click(screen.getByTestId("div-view-less-detail"));
    expect(await screen.findByText(/view more detail/i)).toBeInTheDocument();
  });

  it("Verify if exists div with view less details text", async () => {
    render(
      <Router>
        <LocationInformation {...locationInformation} />
      </Router>
    );
    expect(await screen.findByText(/view less detail/i)).toBeInTheDocument();
  });
});
