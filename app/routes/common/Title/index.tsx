import PropTypes from 'prop-types';

/**
 * Main app function
 * @param {props} props props recieved
 * @returns {function} main app
 */

const Title = (props: any) => {
  const { text } = props; 
  return (
    <h1 className='Title-root'>{text}</h1>
  );
};

Title.propTypes = {
  text: PropTypes.string.isRequired
};

Title.defaultProps = {
  text: '',
};

export default Title;