import { BLOCKS, INLINES } from "@contentful/rich-text-types";
import { documentToReactComponents } from "@contentful/rich-text-react-renderer";
import { ClassNames } from "@storybook/theming";

interface HyperLink {
  data: {
    uri?: string;
  };
}
interface Asset {
  content: [];
  data: {
    sys: {
      id: string;
      linkType: string;
      type: string;
    };
    target: any;
  };
  nodeType: string;
}

export default function RichTextComponent(props: any) {
  const { data, contentWrap, centerText } = props;
  let className;
  if (contentWrap !== false) {
    if (contentWrap === "small") {
      className = `${
        //px-5 mx-auto // max-w-screen-lg
        centerText ? "text-center" : ""
      }`;
    } else {
      className = `${
        //px-5 mx-auto // max-w-screen-xl
        centerText ? "text-center" : ""
      }`;
    }
  }
  const renderOptions: any = {
    renderNode: {
      [BLOCKS.EMBEDDED_ASSET]: (node: Asset) => {
        const img = data?.links?.assets.block.find(
          (i) => i.sys.id === node.data.target.sys.id
        );
        return <img src={img.url} alt={img.title} />;
      },
      [INLINES.HYPERLINK]: ({ data }: HyperLink, children: []) => {
        return (
          <a
            href={data.uri}
            target={`${data.uri ? "_blank" : "_self"}`}
            rel={`${data.uri ? "" : "noopener noreferrer"}`}
          >
            {children}
          </a>
        );
      },
    },
  };
  return (
    <div className={className}>
      {data?.json?.content.map((item, index: number) => (
        <div key={index}>{documentToReactComponents(item, renderOptions)}</div>
      ))}
    </div>
  );
}
