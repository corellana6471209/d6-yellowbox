import React, { useState, useEffect } from "react";
import { useActionData, useTransition, useSubmit } from "@remix-run/react";
import moment from "moment";
import ReactTooltip from "react-tooltip";

import Mastercard from "../../common/svg/Mastercard";
import Visa from "../../common/svg/Visa";
import AmericanExpress from "../../common/svg/AmericanExpress";
import Discover from "../../common/svg/Discover";
//import Spinner from "../../common/Spinner";
import MessageModal from "../../common/MessageModal";
import AutoPayModal from "../../common/Modals/AutoPay";
import PaymenSuccessfulModal from "../../common/Modals/PaymenSuccessful";
import InfoCircle from "../../common/svg/InfoCircle";

import { statesFilterSearch } from "../../../utils/fakedataToTests";
import { validationCard } from "../../../utils/validationCard";
import { getLabel } from "../../../utils/getLabel";
import { identify } from "../../../utils/identify";

import background from "../../../resources/dropdown.svg";

const PaymentUnitForm = (props: any) => {
  const actionData = useActionData();
  const transition = useTransition();
  const submit = useSubmit();
  const {
    unit,
    detailPaymentViewType,
    units,
    openDetailsPaymentSection,
    setNewUnits,
  } = props;

  const [errorToSendInformation, setErrorToSendInformation] = useState({
    nameOnCard: true,
    cardNumber: true,
    cw: true,
    expirationMonth: true,
    expirationYear: true,
    autoPay: false,
    autoPayFutureMonth:
      detailPaymentViewType === "paymentNormalAutoPay" ? false : true,
    fullName: true,
    addressOne: true,
    //addressTwo: true,
    city: true,
    state: true,
    zipCode: true,
    permitTransaction: true,
  });

  const [errorsValidation, setErrorsValidation] = useState({
    nameOnCard: false,
    cardNumber: false,
    cw: false,
    expirationMonth: false,
    expirationYear: false,
    autoPay: false,
    autoPayFutureMonth: false,
    fullName: false,
    addressOne: false,
    //addressTwo: false,
    city: false,
    state: false,
    zipCode: false,
    permitTransaction: false,
  });

  const [form, setForm] = useState({
    nameOnCard: "",
    cardNumber: "",
    cw: "",
    expirationMonth: "null",
    expirationYear: "null",
    autoPay: detailPaymentViewType === "paymentNormalAutoPay" ? true : false,
    autoPayFutureMonth: "null",
    fullName: "",
    addressOne: "",
    addressTwo: "",
    city: "",
    state: "null",
    zipCode: "",
    permitTransaction: false,
  });

  const isSubmiting = transition.state === "submitting";
  const [total, setTotal] = useState<any>(
    detailPaymentViewType === "paymentNormalAutoPay" ? unit.rent : unit.rent + 1
  );
  const actualYear = new Date().getFullYear();
  const [openModal, setOpenModal] = useState<any>(false);
  const [unitsEdited, setUnitsEdited] = useState<any>(units);
  const [unitSelectedEdited, setUnitSelectedEdited] = useState<any>(unit);
  const [changeInformation, setChangeInformation] = useState<any>(false);
  //const [showSpinner, setShowSpinner] = useState(true);
  const [labels, setLabels] = useState<any>({
    titleCreditCard: "Credit Card Details",
    subTitleCreditCard: "Pay now with credit card",
    allRequiredText: "All fields required",
    toolTipTextMyAccount:
      "Auto-Pay saves you time and money by automatically charging your card on file every month, so there are no late fees if you forget to pay. In addition, online payments made with Auto-Pay do not incur a convenience fee of $1.00 with each payment.",
    buttonPaymentMyAccount: "Process Payment",
  });

  const [numberMonth, setNumberMonth] = useState<any>(1);
  const [autoPayModal, setAutoPayModal] = useState<any>(false);
  const [successFulModal, setSuccessFulModal] = useState<any>(false);

  const [message, setMessage] = useState<any>(null);
  const [creditCardType, setCreditCardType] = useState<any>(null);

  /************TIME VARIABLES************ */
  const paidThruDateTime = moment(unit.paidThruDateTime).format(
    "MMMM DD, YYYY"
  );

  /************DYNAMICS SELECTS************ */

  useEffect(() => {
    async function fetchData() {
      //Labels
      const titleCreditCard = await getLabel("titleCreditCard");
      const subTitleCreditCard = await getLabel("subTitleCreditCard");
      const allRequiredText = await getLabel("allRequiredText");
      const toolTipTextMyAccount = await getLabel("toolTipTextMyAccount");
      const buttonPaymentMyAccount = await getLabel("buttonPaymentMyAccount");

      setLabels({
        titleCreditCard: titleCreditCard,
        subTitleCreditCard: subTitleCreditCard,
        allRequiredText: allRequiredText,
        toolTipTextMyAccount: toolTipTextMyAccount,
        buttonPaymentMyAccount: buttonPaymentMyAccount,
      });

      //setShowSpinner(false);
    }
    fetchData();
  }, []);

  //Adding the years dynamically
  const optionYearsSelect = () => {
    const options = [
      <option value="null" key={actualYear - 1}>
        Expiration Year
      </option>,
    ];

    for (let i = actualYear; i < actualYear + 10; i++) {
      options.push(
        <option value={i} key={i}>
          {i}
        </option>
      );
    }

    return options;
  };

  //Adding the states dynamically
  const statesSelect = () => {
    const options = [
      <option value="null" key="select your state">
        State/Region
      </option>,
    ];

    for (let i = 0; i < statesFilterSearch.length; i++) {
      options.push(
        <option
          value={statesFilterSearch[i].stateFullName}
          key={statesFilterSearch[i].stateFullName}
        >
          {statesFilterSearch[i].stateFullName}
        </option>
      );
    }

    return options;
  };

  //Adding next months to pay dinamically
  const autoPayFutureMonthSelect = (date: any) => {
    const options = [
      <option value="null" key={20}>
        Select month
      </option>,
    ];

    for (let i = 1; i < 11; i++) {
      options.push(
        <option value={i} key={i}>
          {moment(unit.paidThruDateTime).add(i, "M").format("MMMM, YYYY")}
        </option>
      );
    }

    return options;
  };

  /************DYNAMICS SELECTS************ */

  /************VERIFY FIELDS************ */

  const handleCheck = (evt: any) => {
    //Function to send the information
    if (evt.target.value === "permitTransaction") {
      if (evt.target.checked) {
        setErrorToSendInformation({
          ...errorToSendInformation,
          permitTransaction: false,
        });
      } else {
        setErrorToSendInformation({
          ...errorToSendInformation,
          permitTransaction: true,
        });
      }

      setForm({
        ...form,
        [evt.target.value]: evt.target.checked,
      });
    } else {
      if (evt.target.checked) {
        setForm({
          ...form,
          [evt.target.value]: evt.target.checked,
          autoPayFutureMonth: "null",
        });
        setErrorToSendInformation({
          ...errorToSendInformation,
          autoPayFutureMonth: false,
        });
        setTotal(total - 1);
      } else {
        setAutoPayModal(true);
      }
    }
  };

  const handleChange = (evt: any) => {
    //Only permit letter and numbers javascript input regex
    const re = /^[A-Za-z0-9\s]+$/; //Verify only permitted number, letter and spaces
    const value = evt.target.value;
    const name = evt.target.name;

    if (!re.test(value) && value !== "") {
      return;
    }

    if (evt.target.name === "autoPayFutureMonth") {
      if (value != "null") {
        setTotal(unit.rent * parseInt(value) + 1);
        setNumberMonth(parseInt(value));
      } else {
        setTotal(unit.rent + 1);
        setNumberMonth(1);
      }
    }

    if (evt.target.name === "cardNumber") {
      const determineCard = value.charAt(0);
      if (determineCard == 3) {
        setCreditCardType(7);
      } else if (determineCard == 4) {
        setCreditCardType(6);
      } else if (determineCard == 5) {
        setCreditCardType(5);
      } else if (determineCard == 6) {
        setCreditCardType(8);
      } else {
        setCreditCardType(null);
      }
    }

    setForm({
      ...form,
      [name]: value,
    });
  };

  const verifyData = async (evt: any) => {
    //Get the errors
    const errorsSelected = { ...errorToSendInformation };
    const errorsToValidate = { ...errorsValidation };

    const reAddress = /^[a-zA-Z0-9\s]{6,}$/; //At least 6 characters
    const value = evt.target.value;
    const name = evt.target.name;

    //Verify onBlur
    if (
      name !== "addressOne" &&
      name !== "addressTwo" &&
      value.replace(/ /g, "") === "" //Verify that the user doesnt enters blank spaces
    ) {
      setErrorToSendInformation({ ...errorsSelected, [name]: true });
      setErrorsValidation({ ...errorsToValidate, [name]: true });
    } else if (
      name === "addressOne" && // || name === "addressTwo"
      (value.replace(/ /g, "") === "" || !reAddress.test(value))
    ) {
      console.log("ERROR");
      setErrorToSendInformation({ ...errorsSelected, [name]: true });
      setErrorsValidation({ ...errorsToValidate, [name]: true });
    } else if (
      (name === "expirationMonth" ||
        name === "expirationYear" ||
        name === "autoPayFutureMonth" ||
        name === "state") &&
      value === "null"
    ) {
      setErrorToSendInformation({ ...errorsSelected, [name]: true });
      setErrorsValidation({ ...errorsToValidate, [name]: true });
    } else if (name === "cardNumber") {
      const determineCard = value.charAt(0);
      let cardType = "";
      if (determineCard == 3) {
        cardType = "7";
      } else if (determineCard == 4) {
        cardType = "6";
      } else if (determineCard == 5) {
        cardType = "5";
      } else if (determineCard == 6) {
        cardType = "8";
      }

      const validation = validationCard(value, cardType);

      if (!validation) {
        setErrorToSendInformation({ ...errorsSelected, [name]: true });
        setErrorsValidation({ ...errorsToValidate, [name]: true });
      } else {
        setErrorToSendInformation({ ...errorsSelected, [name]: false });
        setErrorsValidation({ ...errorsToValidate, [name]: false });
      }
    } else if (name === "cw") {
      //Verify onBlur the CVV
      const re = /^\d{3}$/;
      if (!re.test(value)) {
        setErrorToSendInformation({ ...errorsSelected, cw: true });
        setErrorsValidation({ ...errorsToValidate, [name]: true });
      } else {
        setErrorToSendInformation({ ...errorsSelected, [name]: false });
        setErrorsValidation({ ...errorsToValidate, [name]: false });
      }
    } else {
      setErrorToSendInformation({ ...errorsSelected, [name]: false });
      setErrorsValidation({ ...errorsToValidate, [name]: false });
    }
  };

  const verifyFields = () => {
    if (
      form.nameOnCard === "" ||
      form.cardNumber === "" ||
      form.cw === "" ||
      form.cw === "null" ||
      form.expirationMonth === "null" ||
      form.expirationYear === "null" ||
      form.fullName === "" ||
      form.addressOne === "" ||
      //form.addressTwo === "" ||
      form.city === "" ||
      form.city === "null" ||
      form.zipCode === "" ||
      form.permitTransaction === false
    ) {
      return true;
    }

    return false;
  };

  /************VERIFY FIELDS************ */

  /************CALLBACK AUTOPAYMODAL AND DETERMINE IF CAN SEND INFORMATION************ */
  const callBackAutoPayModal = (value: any) => {
    if (!value) {
      setErrorToSendInformation({
        ...errorToSendInformation,
        autoPayFutureMonth: true,
      });
      setForm({
        ...form,
        autoPayFutureMonth: "null",
        autoPay: value,
      });
      setTotal(unit.rent + 1);
    } else {
      setForm({
        ...form,
        autoPay: value,
      });
    }
    setAutoPayModal(!autoPayModal);
  };

  const callBackSuccessFulModal = () => {
    setNewUnits(unitsEdited, unitSelectedEdited);
    openDetailsPaymentSection(unitSelectedEdited);
  };

  useEffect(() => {
    if (actionData && actionData.showModal && changeInformation) {
      setOpenModal(actionData.showModal);
      setMessage(actionData.message);
      setChangeInformation(false);
    }

    if (
      actionData &&
      actionData.showSuccessFulModal &&
      actionData.units &&
      actionData.unit &&
      changeInformation
    ) {
      setChangeInformation(false);
      setUnitsEdited(actionData.units);
      setUnitSelectedEdited(actionData.unit);
      setSuccessFulModal(true);

      identify(
        window,
        `${actionData.unit.tenantId}${actionData.unit.locationId}`,
        {
          autopay: form.autoPay ? 1 : 0,
        }
      );
    }
  }, [actionData]);

  const sendInformation = () => {
    const formData = new FormData();
    formData.set("form", JSON.stringify(form));
    formData.set("errors", JSON.stringify(errorToSendInformation));
    formData.set("unit", JSON.stringify(unit));
    formData.set("units", JSON.stringify(units));
    formData.set("creditCardType", creditCardType);
    formData.set("total", total);
    formData.set("numberMonth", numberMonth);
    setChangeInformation(true);
    submit(formData, { method: "post", action: "/my-account?index" });
  };

  /************CALLBACK AUTOPAYMODAL AND DETERMINE IF CAN SEND INFORMATION************ */

  return (
    <section className="div-payment-unit-details">
      <ReactTooltip />
      <MessageModal
        open={openModal}
        message={message}
        title=" "
        ctaText="Close"
        callback={() => setOpenModal(!openModal)}
      />
      <AutoPayModal
        open={autoPayModal}
        callback={(value: any) => callBackAutoPayModal(value)}
      />

      <PaymenSuccessfulModal
        open={successFulModal}
        callback={() => callBackSuccessFulModal()}
      />

      {/**(isSubmiting || showSpinner) && <Spinner /> */}
      <div className="sm:pl-12 sm:pr-12">
        <p className="mt-16 mb-2 text-center text-24 font-bold">
          {labels.titleCreditCard}
        </p>

        <p className="mb-4 text-center text-16 text-greyHeavy">
          {" "}
          {labels.subTitleCreditCard}
        </p>

        <div className="div-icon-cards flex justify-center">
          <div
            className={`${
              creditCardType === 5 ? "div-cards-not-filter" : "div-cards"
            } mr-3`}
          >
            <Mastercard />
          </div>
          <div
            className={`${
              creditCardType === 6 ? "div-cards-not-filter" : "div-cards"
            } mr-3`}
          >
            <Visa />
          </div>
          <div
            className={`${
              creditCardType === 7 ? "div-cards-not-filter" : "div-cards"
            } mr-3`}
          >
            <AmericanExpress />
          </div>
          <div
            className={`${
              creditCardType === 8 ? "div-cards-not-filter" : "div-cards"
            }`}
          >
            <Discover />
          </div>
        </div>
        <div className="div-form-payment-details mt-3">
          <div className="relative">
            <span className="span-div-input absolute text-20 text-callout">
              *
            </span>
            <div className="div-input">
              <input
                disabled={isSubmiting}
                autoComplete="off"
                autoComplete="chrome-off"
                onChange={handleChange}
                onBlur={(e) => verifyData(e)}
                value={form.nameOnCard}
                name="nameOnCard"
                className="input w-100"
                type="text"
                placeholder="Name On Card"
              />
            </div>
            {errorsValidation && errorsValidation.nameOnCard && (
              <p className="message-error text-left">This name is required</p>
            )}
          </div>

          <div className="relative">
            <span className="span-div-input absolute text-20 text-callout">
              *
            </span>
            <div className="div-input">
              <input
                disabled={isSubmiting}
                autoComplete="off"
                autoComplete="chrome-off"
                onChange={handleChange}
                onBlur={(e) => verifyData(e)}
                value={form.cardNumber}
                name="cardNumber"
                className="input w-100"
                type="text"
                placeholder="Card Number"
              />
            </div>

            {errorsValidation && errorsValidation.cardNumber && (
              <p className="message-error text-left">This number is invalid</p>
            )}
          </div>

          <div className="relative">
            <span className="span-div-input absolute text-20 text-callout">
              *
            </span>

            <div className="div-input">
              <input
                disabled={isSubmiting}
                autoComplete="off"
                autoComplete="chrome-off"
                onChange={handleChange}
                onBlur={(e) => verifyData(e)}
                value={form.cw}
                name="cw"
                className="input w-100"
                type="text"
                placeholder="CW"
                maxLength={3}
              />
            </div>
            {errorsValidation && errorsValidation.cw && (
              <p className="message-error text-left">This cw is invalid</p>
            )}
          </div>

          <div className="grid grid-cols-1 gap-1">
            <div className="relative">
              <span className="span-div-input absolute mt-4 text-20 text-callout">
                *
              </span>

              <select
                disabled={isSubmiting}
                onChange={handleChange}
                onBlur={(e) => verifyData(e)}
                value={form.expirationMonth}
                style={{ backgroundImage: `url(${background})` }}
                name="expirationMonth"
                id="expirationMonth"
                className="select-filter-sort-by-payment w-100 mt-4"
              >
                <option value="">Expiration Month</option>,
                <option value="01">January</option>
                <option value="02">February</option>
                <option value="03">March</option>
                <option value="04">April</option>
                <option value="05">May</option>
                <option value="06">June</option>
                <option value="07">July</option>
                <option value="08">August</option>
                <option value="09">September</option>
                <option value="10">October</option>
                <option value="11">November</option>
                <option value="12">December</option>
              </select>
              {errorsValidation && errorsValidation.expirationMonth && (
                <p className="message-error text-left">The month is required</p>
              )}
            </div>

            <div className="relative">
              <span className="span-div-input absolute mt-4 text-20 text-callout">
                *
              </span>

              <select
                disabled={isSubmiting}
                autoComplete="off"
                onChange={handleChange}
                onBlur={(e) => verifyData(e)}
                value={form.expirationYear}
                name="expirationYear"
                id="expirationYear"
                className="select-filter-sort-by-payment w-100 mt-4"
                style={{ backgroundImage: `url(${background})` }}
              >
                {optionYearsSelect()}
              </select>
              {errorsValidation && errorsValidation.expirationYear && (
                <p className="message-error text-left">The year is required</p>
              )}
            </div>
          </div>

          <div className="mt-8 mb-10 flex">
            <label className="flex text-12 text-16 text-greyHeavy">
              <input
                disabled={isSubmiting}
                onChange={handleCheck}
                type="checkbox"
                className="radio mr-4"
                value="autoPay"
                name="autoPay"
                checked={form.autoPay}
              />
              Enroll in hassle-free Auto-Pay and save on convenience fees{" "}
            </label>

            <span
              className="ml-1 cursor-pointer"
              data-tip={labels.toolTipTextMyAccount}
              data-place="top"
              data-background-color="#00155F"
              data-text-color="white"
              data-class="custom-tooltip"
            >
              <InfoCircle size="17" color="#657AC1" />
            </span>
          </div>

          {!form.autoPay && (
            <>
              <p>
                You’re paid through{" "}
                <span className="font-bold text-secondary1">
                  {paidThruDateTime}
                </span>
                . Use the drop down menu below to make an advance payment.
              </p>

              <div className="relative grid grid-cols-1 gap-1">
                <span className="span-div-input absolute mt-4 text-20 text-callout">
                  *
                </span>

                <select
                  disabled={isSubmiting}
                  onChange={handleChange}
                  onBlur={(e) => verifyData(e)}
                  value={form.autoPayFutureMonth}
                  name="autoPayFutureMonth"
                  id="autoPayFutureMonth"
                  className="select-filter-sort-by-payment w-100 mt-4"
                  style={{ backgroundImage: `url(${background})` }}
                >
                  {autoPayFutureMonthSelect(paidThruDateTime)}
                </select>
                {errorsValidation && errorsValidation.autoPayFutureMonth && (
                  <p className="message-error text-left">
                    The month is required
                  </p>
                )}
              </div>
            </>
          )}

          <h2 className="mt-14 mb-6 text-center text-26 font-bold">
            Billing Information
          </h2>

          <div className="grid grid-cols-1 gap-1">
            <div className="d-flex items-center justify-end text-12 text-greyMedium">
              {labels.allRequiredText}
            </div>

            <div className="relative">
              <span className="span-div-input absolute mt-4 text-20 text-callout">
                *
              </span>

              <div className="div-input">
                <input
                  disabled={isSubmiting}
                  autoComplete="off"
                  autoComplete="chrome-off"
                  name="fullName"
                  value={form.fullName}
                  onChange={handleChange}
                  onBlur={(e) => verifyData(e)}
                  className="input w-100"
                  type="text"
                  placeholder="Full Name"
                />
              </div>

              {errorsValidation && errorsValidation.fullName && (
                <p className="message-error text-left">The name is required</p>
              )}
            </div>

            <div className="relative">
              <span className="span-div-input absolute mt-4 text-20 text-callout">
                *
              </span>

              <div className="div-input">
                <input
                  disabled={isSubmiting}
                  autoComplete="off"
                  autoComplete="chrome-off"
                  name="addressOne"
                  onChange={handleChange}
                  onBlur={(e) => verifyData(e)}
                  value={form.addressOne}
                  className="input w-100"
                  type="text"
                  placeholder="Address 1"
                />
              </div>

              {errorsValidation && errorsValidation.addressOne && (
                <p className="message-error text-left">
                  This address is invalid must be at least 6 characters
                </p>
              )}
            </div>

            <div>
              <div className="div-input">
                <input
                  disabled={isSubmiting}
                  autoComplete="off"
                  autoComplete="chrome-off"
                  name="addressTwo"
                  onChange={handleChange}
                  onBlur={(e) => verifyData(e)}
                  value={form.addressTwo}
                  className="input w-100"
                  type="text"
                  placeholder="Address 2"
                />
              </div>
              {/*errorsValidation && errorsValidation.addressTwo && (
                <p className="message-error text-left">
                  This address is invalid must be at least 6 characters
                </p>
              )*/}
            </div>

            <div className="relative">
              <span className="span-div-input absolute mt-4 text-20 text-callout">
                *
              </span>
              <select
                disabled={isSubmiting}
                autoComplete="off"
                onChange={handleChange}
                onBlur={(e) => verifyData(e)}
                value={form.state}
                name="state"
                id="state"
                className="select-filter-sort-by-payment w-100 mt-4"
                style={{ backgroundImage: `url(${background})` }}
              >
                {statesSelect()}
              </select>
              {errorsValidation && errorsValidation.state && (
                <p className="message-error text-left">The state is required</p>
              )}
            </div>

            <div className="relative">
              <span className="span-div-input absolute mt-4 text-20 text-callout">
                *
              </span>
              <div className="div-input">
                <input
                  disabled={isSubmiting}
                  autoComplete="off"
                  autoComplete="chrome-off"
                  name="zipCode"
                  onChange={handleChange}
                  onBlur={(e) => verifyData(e)}
                  value={form.zipCode}
                  className="input w-100"
                  type="text"
                  placeholder="Zipcode"
                />
              </div>
              {errorsValidation && errorsValidation.zipCode && (
                <p className="message-error text-left">
                  The zipcode is required
                </p>
              )}
            </div>

            <div className="relative">
              <span className="span-div-input absolute mt-4 text-20 text-callout">
                *
              </span>

              <div className="div-input">
                <input
                  disabled={isSubmiting}
                  autoComplete="off"
                  autoComplete="chrome-off"
                  name="city"
                  onChange={handleChange}
                  onBlur={(e) => verifyData(e)}
                  value={form.city}
                  className="input w-100"
                  type="text"
                  placeholder="City"
                />
              </div>
              {errorsValidation && errorsValidation.city && (
                <p className="message-error text-left">The city is required</p>
              )}
            </div>

            <p className="d-flex mt-10 justify-center align-middle text-12">
              <span className="mr-2 text-20 text-callout">*</span> Required
              Fields
            </p>

            <label className="mt-8 mb-14 flex items-center text-16 text-greyHeavy">
              <input
                disabled={isSubmiting}
                autoComplete="off"
                onChange={handleCheck}
                type="checkbox"
                className="radio mr-2"
                name="permitTransaction"
                value="permitTransaction"
                checked={form.permitTransaction}
              />
              <span className="mr-2 text-callout">*</span>I Authorize This
              Transaction
            </label>
          </div>
        </div>
      </div>
      <div className="sm:pl-12 sm:pr-12">
        {!form.autoPay && (
          <div className="d-flex mt-6 justify-between ">
            <p className="text-16 text-greyHeavy">Convenience Fee:</p>
            <p className="text-16 text-greyHeavy">$1.00</p>
          </div>
        )}
        <div className="d-flex mt-2 justify-between">
          <p className="text-20 text-secondary1">Total Due:</p>
          <p className="text-36 font-bold text-secondary2">
            ${total.toFixed(2)}
          </p>
        </div>
        <div className="div-button-unit-payment-bottom d-flex mt-10 justify-center">
          <button
            type="button"
            onClick={() => sendInformation()}
            disabled={verifyFields() || isSubmiting}
            className={`block ${
              verifyFields() || isSubmiting
                ? "button-disabled"
                : "yellow-button bg-primary1 text-secondary1"
            } py-3 text-center text-16  no-underline`}
          >
            {isSubmiting
              ? "Sending information"
              : labels.buttonPaymentMyAccount}
          </button>
        </div>
      </div>
    </section>
  );
};

export default PaymentUnitForm;
