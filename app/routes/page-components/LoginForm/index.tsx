import React, { useState, useEffect } from "react";

import {
  Form,
  Link,
  useActionData,
  useTransition,
  useLoaderData,
} from "@remix-run/react";

// Custom components
import Input from "../../common/Input";
import MessageModal from "../../common/MessageModal";
import Spinner from "../../common/Spinner";
import { analytics } from "../../../utils/analytics";
import { globalRedirect } from "../../../utils/globalRedirect";
import { identify } from "../../../utils/identify";

const LoginForm = () => {
  const actionData = useActionData();
  const transition = useTransition();
  const loaderData = useLoaderData();
  const isSubmiting = transition.state === "submitting";
  // const [openModal, setOpenModal] = useState<any>(false);
  const [disableButton, setDisableButton] = useState<any>(false);
  const [lockedOutModal, setLockedOutModal] = useState<boolean>(false);

  useEffect(() => {
    if (actionData && actionData.userIsLockedOut) {
      setLockedOutModal(true);
    } else if (actionData && actionData.redirect) {
      const locationId =
        actionData.data && actionData.data[0]
          ? actionData.data[0].sitelinkUserTenant.sitelinkLocationId
          : null;

      const tenantId =
        actionData.data && actionData.data[0]
          ? actionData.data[0].sitelinkUserTenant.sitelinkTenantId
          : null;

      analytics(window, "loggedIn", {
        email: actionData.email,
        locationID: locationId,
      });

      identify(window, `${tenantId}${locationId}`, {
        email: actionData.email,
        locationID: locationId,
      });

      globalRedirect(window, actionData.redirectTo);
    } else if (actionData && !actionData.redirectTo) {
      setDisableButton(false);
    }
  }, [actionData]);

  // To disable the button when I click on the login-button
  useEffect(() => {
    if (isSubmiting) {
      setDisableButton(true);
    }
  }, [isSubmiting]);

  const redirectToForgotPassword = () => {
    window.location.href = "/forgot-password";
  };

  return (
    <>
      {isSubmiting && <Spinner />}

      <MessageModal
        open={lockedOutModal}
        callback={() => {
          redirectToForgotPassword();
        }}
        message="Incorrect username or password. Your account has been locked for too many login attempts. Please try again in 15 minutes. You can also go to forget password to reactivate your account"
        title=" "
        ctaText="Go to Forgot Password"
      />

      <Form className="LoginForm-root" method="post" action="/login?index">
        <>
          <input type="hidden" name="slug" value={loaderData.slug} />
          <input
            type="hidden"
            name="reservation"
            value={loaderData.reservation}
          />
          <input type="hidden" name="islegacy" value={loaderData.islegacy} />
          <input
            type="hidden"
            name="redirectTo"
            value={loaderData.redirectTo ? loaderData.redirectTo : ""}
          />
        </>

        <div className="LoginForm-container">
          <div>
            <Input
              type="text"
              name="email"
              placeholder="Email Address"
              ariaLabel="email-input"
            />
            {actionData &&
              actionData.frontErrors &&
              actionData.frontErrors.email && (
                <p className="message-error">
                  This field must be example@example.com
                </p>
              )}
          </div>
          <div>
            <Input type="password" name="password" placeholder="Password" />
            {actionData &&
              actionData.frontErrors &&
              actionData.frontErrors.password && (
                <p className="message-error">This field is required</p>
              )}
            {actionData && actionData.serverError && (
              <p className="message-error">{actionData.serverError}</p>
            )}
          </div>
        </div>

        <div className="LoginForm-button-container">
          <button
            type="submit"
            className={`${
              disableButton
                ? "LoginForm-button-disabled"
                : "LoginForm-button yellow-button"
            }`}
            aria-label="submit-loginform"
            disabled={disableButton}
          >
            Login
          </button>
        </div>

        <div className="LoginForm-link-container">
          {loaderData &&
          loaderData.slug &&
          loaderData.reservation &&
          loaderData.islegacy ? (
            <Link
              to={`/forgot-password?slug=${loaderData.slug}&reservation=${loaderData.reservation}&islegacy=${loaderData.islegacy}`}
              className="LoginForm-link link"
            >
              Forgot Password
            </Link>
          ) : (
            <Link to="/forgot-password" className="LoginForm-link link">
              Forgot Password
            </Link>
          )}
        </div>
      </Form>
    </>
  );
};

export default LoginForm;
