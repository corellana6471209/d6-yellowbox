import { validateEmail } from '../../../utils/regex';

const validate = ({ email, password }) => {
  const emailValidation = validateEmail(email);
  const passwordValidation = password.length > 0;

  const frontErrors = {
    email: !emailValidation,
    password: !passwordValidation
  }

  return ({
    frontErrors,
    isValid: emailValidation && passwordValidation
  });
}

export default validate;