import React from "react";
import { vi, describe, it } from "vitest";
import { BrowserRouter as Router } from "react-router-dom";
import Icons from ".";
import { render, screen } from "../../../utils/test-utils";

const mockSetState = vi.fn();
vi.mock("react", () => ({
  useState: (initial) => [initial, mockSetState],
  useRef: (initial) => [initial, mockSetState],
  useEffect: (initial) => [initial, mockSetState],
}));


describe("Unit-MyAccount component", () => {
  it("Verify if the component renders", () => {
    render(
      <Router>
        <Icons />
      </Router>
    );
  });

  it("Verify if div-img exists", async () => {
    const { container } = render(<Icons

    />);
    expect(await container.getElementsByClassName("div-svg").length).toBe(3);
  });

});
