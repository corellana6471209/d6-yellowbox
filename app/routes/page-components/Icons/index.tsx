import { useState } from 'react';
import Bell from "../../common/svg/Bell";
import DoorOut from "../../common/svg/DoorOut";
import User from "../../common/svg/User";
import Lock from "../../common/svg/Lock";
import Spinner from "../../common/Spinner";


const Icons = () => {
  const [showSpinner, setShowSpinner] = useState(false);

  const redirectTo = (link: string) => {
    setShowSpinner(true)
    window.location.href = `${link}`;
  };


  const handleKeyPress = (e: any, link: string,) => {
    if (e.charCode == 13) {
      redirectTo(link)
      return;
    }


  };


  return (
    <section className="unit-my-account-icons mt-[60px] mb-[40px]">
      {showSpinner && <Spinner />}

      <div className="div-icons-svg">

        <div className="div-svg cursor-pointer" tabIndex={0} onClick={() => redirectTo('/contact-details')} onKeyPress={e => handleKeyPress(e, '/contact-details')}>
          <div>
            <User />
          </div>
          <p className="text-center text-12 sm:text-16">My profile</p>
        </div>



        <div className="div-svg cursor-pointer" onClick={() => redirectTo('/change-password')} tabIndex={0} onKeyPress={e => handleKeyPress(e, '/change-password')}>
          <div>
            <Lock />
          </div>
          <p className="text-center text-12 sm:text-16">Change Password</p>
        </div>


        <div className="div-svg cursor-pointer md:mt-0 mt-3" tabIndex={0} onClick={() => redirectTo('/contact-details')} onKeyPress={e => handleKeyPress(e, '/contact-details')}>
          <div>
            <Bell />
          </div>
          <p className="text-center text-12 sm:text-16">
            Notification Settings
          </p>
        </div>



     {/**
      *   <div className="div-svg" tabIndex={8}>
          <div>
            <DoorOut />
          </div>
          <p className="text-center text-12 sm:text-16">Vacate Unit</p>
        </div>
      * 
      */}
        

      </div>
    </section>
  );
};

export default Icons;
