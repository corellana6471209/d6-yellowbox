import React, { useState } from "react";

export default function Tagging(props: any) {
  const { pageDetails } = props;
  const [isHydrated] = useState(true);
  const [categories] = useState(pageDetails.categories);
  const [tags] = useState(pageDetails.tags);

  if (isHydrated && (categories || tags)) {
    return (
      <div className="blog-tagging pl-5 pr-5 d-flex justify-center mt-[70px]">
        <div className="grid grid-cols-1 gap-4 md:grid-cols-2 md:gap-10 max-w-[900px]">
          {categories && (
            <div>
              <p className="text-16 font-bold text-greyHeavy md:text-24">
                Tagged With:
              </p>

              <div>
                {categories.map((category: any, index: number) => (
                  <a
                    className="mr-2 text-16 text-callout link md:text-24"
                    href={`/blog?category=${encodeURIComponent(category)}`}
                    key={index}
                  >
                    {category}{index + 1 != categories.length ? "," : ""}
                  </a>
                ))}
              </div>
            </div>
          )}

          {tags && (
            <div>
              <p className="text-16 font-bold text-greyHeavy md:text-24">
                Filed Under:
              </p>
              <div>
                {tags.map((tag: any, index: number) => (
                  <a
                    className="mr-2 text-16 text-callout link md:text-24"
                    href={`/blog?tag=${encodeURIComponent(tag)}`}
                    key={index}
                  >
                    {tag}{index + 1 != tags.length ? "," : ""}
                  </a>
                ))}
              </div>
            </div>
          )}
        </div>
      </div>
    );
  } else {
    return <></>;
  }
}
