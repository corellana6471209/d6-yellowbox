import type { ComponentStory, ComponentMeta } from "@storybook/react";
import Tagging from "./index";

export default {
  title: "components/core/Tagging",
  component: Tagging,
} as ComponentMeta<typeof Tagging>;

const Template: ComponentStory<typeof Tagging> = (args) => (
  <Tagging />
  // {...args}
);

export const Primary = Template.bind({});
/**
 * Primary.args = {
  unit: fakeDataUnit,
  ctaText: "Save Now with No Obligation",
};
 */
