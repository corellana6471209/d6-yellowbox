import fetchCustomApi from "~/utils/fetchCustomApi";

export async function loadRichTextCollection(params: string) {
  const query = `
    query componentrichTextFieldCollection {
      componentrichTextFieldCollection(where: {sys: {id: "${params}"}}) {
        items {
          contentTitle
          showTitle
          richText {
            json
          }
        }
      }
    }`;

  var theData = await fetchCustomApi(`/api/content/common/contentful/${query}`);
  return theData;
}

export async function getRichTextWidthLinks(params: string) {
  const query = `{
    componentrichTextField(id: "${params}") {
      contentTitle
      centerText
      showTitle
      richText {
        json
        links {
          assets {
            block {
              title
              url
              sys {
                id
              }
            }
          }
        }
      }
    }
  }`;

  const { componentrichTextField } = await fetchCustomApi(
    `/api/content/common/contentful/${query}`
  );
  return componentrichTextField;
}
