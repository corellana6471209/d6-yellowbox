import { useState, useEffect } from "react";
import RichTextComponent from "../../common/RichTextComponent";
import { getRichTextWidthLinks } from "./backend";
import { RichTextState } from "./RichText.interface";

function RichText(params: any) {
  const [richTextState, setRichTextState] = useState<RichTextState>({
    title: "",
    centerText: false,
    data: undefined,
    isHydrated: false,
    showTitle: true,
  });

  async function fetchData(): Promise<void> {
    const { richText, contentTitle, centerText, showTitle } =
      await getRichTextWidthLinks(params.id);

    setRichTextState({
      centerText: centerText,
      title: contentTitle,
      isHydrated: true,
      data: richText,
      showTitle: showTitle,
    });
  }

  useEffect(() => {
    // load the data
    fetchData();
  }, [params.id]);

  const { isHydrated, title, centerText, data, showTitle } = richTextState;

  if (isHydrated)
    return (
      <div className="rich-text">
        <div className="small-wrapper mx-auto">
          {showTitle ? <div className="rich-text-title">{title}</div> : ""}

          <RichTextComponent data={data} centerText={centerText} />
        </div>
      </div>
    );
}

export default RichText;
