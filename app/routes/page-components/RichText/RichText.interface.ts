export type RichTextType = {
  json: {
    content: [];
    data: any;
    nodeType: string;
  };
  links: {
    assets: {
      block: any[];
    };
  };
};

export type RichTextState = {
  title: string;
  centerText: boolean;
  data?: RichTextType;
  isHydrated: boolean;
  showTitle: boolean;
};
