import React, { useState, useEffect } from "react";
import { getLabel } from "../../../utils/getLabel";

import {
  Form,
  useActionData,
  useTransition,
  useLoaderData,
} from "@remix-run/react";

import Title from "~/routes/common/Title";
import Input from "~/routes/common/Input";
import MessageModal from "../../common/MessageModal";
import Spinner from "../../common/Spinner";

const PasswordResetForm = () => {
  const [typePassword, setShowPassword] = useState<string>("password");

  const actionData = useActionData();
  const transition = useTransition();
  const loaderData = useLoaderData();
  const [openModal, setOpenModal] = useState<any>(true);
  const [message, setMessage] = useState<any>(
    "Your password reset email will be sent to the email address associated with your account. Once your password is reset you may log in."
  );
  const [labels, setLabels] = useState<any>({
    passwordText:
      "Create a password for your new account. Password must be 6 to 8 characters and/or numbers, and no special characters.",
  });

  const onClick = () => {
    if (typePassword === "password") return setShowPassword("text");
    setShowPassword("password");
  };

  const isSubmiting = transition.state === "submitting";

  useEffect(() => {
    async function fetchData() {
      const passwordText = await getLabel("passwordText");
      setLabels({
        passwordText: passwordText ? passwordText : labels.passwordText,
      });
    }
    fetchData();
  }, []);

  useEffect(() => {
    if (actionData && actionData.showModal) {
      setOpenModal(actionData.showModal);
      setMessage(actionData.message);
    }
  }, [actionData]);

  return (
    <>
      {isSubmiting && <Spinner />}

      <MessageModal
        open={openModal}
        callback={() => {
          setOpenModal(false);
        }}
        message={message}
        title=" "
        ctaText="Close"
      />

      <Form
        className="PasswordResetForm-container"
        method="post"
        action="/password-reset?index"
      >
        <Title text={"Password Reset"} />
        <div className="PasswordResetForm-inputs-container">
          <input type="hidden" name="slug" value={loaderData.slug} />
          <input
            type="hidden"
            name="reservation"
            value={loaderData.reservation}
          />
          <input type="hidden" name="islegacy" value={loaderData.islegacy} />
          <div>
            <Input
              type="text"
              name="email"
              placeholder="Email Address"
              ariaLabel="email-input"
            />
            {actionData &&
              actionData.frontErrors &&
              actionData.frontErrors.email && (
                <p className="message-error">
                  This field must be example@example.com
                </p>
              )}
          </div>
          <div>
            <Input
              type={typePassword}
              name="newPassword"
              placeholder="New Password"
              ariaLabel="new-password-input"
              iconName={typePassword === "password" ? "blindEye" : "eye"}
              onClick={onClick}
            />
            {actionData &&
              actionData.frontErrors &&
              actionData.frontErrors.password && (
                <p className="message-error">{labels.passwordText}</p>
              )}
          </div>
          <div>
            <Input
              type="text"
              name="verificationCode"
              placeholder="Verification Code"
              ariaLabel="verification-code-input"
            />
            {actionData &&
              actionData.frontErrors &&
              actionData.frontErrors.verificationCode && (
                <p className="message-error">This field must be filled</p>
              )}
          </div>
        </div>
        <div className="PasswordResetForm-button-container">
          <button
            type="submit"
            className="PasswordResetForm-button yellow-button"
            aria-label="submit-PasswordResetForm"
            disabled={isSubmiting}
          >
            Change Password
          </button>
        </div>
      </Form>
    </>
  );
};

export default PasswordResetForm;
