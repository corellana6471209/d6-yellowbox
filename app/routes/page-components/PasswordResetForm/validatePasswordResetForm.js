import { validateEmail, isAlphanumeric } from '../../../utils/regex';

const validate = ({ email, password, verificationCode }) => {
  const emailValidation = validateEmail(email);
  const passwordValidation = isAlphanumeric(password) && password.length >= 6 && password.length <= 8;
  const verificationCodeValidation = verificationCode.length > 0;

  const frontErrors = {
    email: !emailValidation,
    password: !passwordValidation,
    verificationCode: !verificationCodeValidation
  }

  return ({
    frontErrors,
    isValid: (emailValidation && passwordValidation && verificationCodeValidation)
  });
}

export default validate;