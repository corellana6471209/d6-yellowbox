import type { ComponentStory, ComponentMeta } from "@storybook/react";
import { BrowserRouter as Router } from "react-router-dom";

import NotFound from "./index";

export default {
  title: "components/core/NotFound",
  component: NotFound,
} as ComponentMeta<typeof NotFound>;

const Template: ComponentStory<typeof NotFound> = (args) => (
  <Router>
    <NotFound />
  </Router>
);

export const NotFoundComponent = Template.bind({});
NotFoundComponent.args = {};
