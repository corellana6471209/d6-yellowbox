import { describe, expect, it } from "vitest";
import { loadInteractiveMap } from "./backend";

const id = "1234";
const fetchFunction = {
  json: function () {
    return {
      data: {
        componentInteractiveMap: {
          items: [
            {
              interactiveMapText: "Wait for us",
              buttonText: "Sign up",
              showNewLetter: true,
            },
          ],
        },
      },
      errors: null,
    };
  },
};
global.fetch = async () => {
  return fetchFunction;
};

describe("Interactive map component BE", () => {
  it("Verify the interactiveMapText of the component", async () => {
    const data = await loadInteractiveMap(id);
    expect(data.componentInteractiveMap.items[0].interactiveMapText).toBe(
      "Wait for us"
    );
  });
  it("Verify the buttonText of the component", async () => {
    const data = await loadInteractiveMap(id);
    expect(data.componentInteractiveMap.items[0].buttonText).toBe("Sign up");
  });

  it("Verify the showNewLetter of the component", async () => {
    const data = await loadInteractiveMap(id);
    expect(data.componentInteractiveMap.items[0].interactiveMapText).toBe(true);
  });
});
