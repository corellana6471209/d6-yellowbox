import { describe, expect, it } from "vitest";
import { getStates } from "./backend";

const fetchFunction = {
  json: function () {
    return {
      data: [
        {
          sitelinkLocationID: "9001",
          locationName: "Test Location 1",
          stateCode: "FL",
        },
        {
          sitelinkLocationID: "9002",
          locationName: "Test Location 2",
          stateCode: "FL",
        },
        {
          sitelinkLocationID: "9003",
          locationName: "Test Location 3",
          stateCode: "FL",
        },
      ],
      errors: null,
    };
  },
};
global.fetch = async () => {
  return fetchFunction;
};

describe("LocationList prices component BE", () => {
  it("Verify the sitelinkLocationID of the component", async () => {
    const data = await getStates();
    expect(data[0].sitelinkLocationID).toBe("9001");
  });

  it("Verify the locationName of the component", async () => {
    const data = await getStates();
    expect(data[0].locationName).toBe("Test Location 1");
  });

  it("Verify the stateCode of the component", async () => {
    const data = await getStates();
    expect(data[0].stateCode).toBe("FL");
  });
});
