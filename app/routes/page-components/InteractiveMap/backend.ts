import fetchCustomApi from "../../../utils/fetchCustomApi";
import type { responseObject } from "~/routes/api/shared/prepareSimplyFetch";

export async function loadInteractiveMap(params: string) {
  const query = `
  query componentInteractiveMap {
    componentinteractiveMap(id: "${params}") {
      title,
      buttonText,
      showNewLetter,
      interactiveMapText{
          name
          content {
            json
            links {
              assets {
                block {
                  title
                  url
                  sys {
                    id
                  }
                }
              }
            }
        }
      }
    }
  }`;

  var theData = await fetchCustomApi(`/api/content/common/contentful/${query}`);
  return theData;
}

export async function getStates() {
  const data: responseObject = await fetchCustomApi(
    `/api/Inventory/getStatesWithLocations/locations`
  );
  if (data.err || !data) {
    //if you want details you have data.errText
    return "error";
  }
  return data?.data;
}
