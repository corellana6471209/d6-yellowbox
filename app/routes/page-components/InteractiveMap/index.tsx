import { useEffect, useState } from "react";

import Map from "../../common/svg/InteractiveMap";
import NewLetter from "../../common/NewLetter";
import { loadInteractiveMap, getStates } from "./backend";

function InteractiveMap(props: any) {
  const [enablePopUp, setEnablePopUp] = useState<any>(null);
  const [states, setStates] = useState([]);
  const [data, setData] = useState(null);
  const [showLetter, setShowLetter] = useState(true);

  const [isHydrated, setIsHydrated] = useState(false);

  useEffect(() => {
    async function fetchData() {
      // You can await here
      const response = await loadInteractiveMap(props.id);
      const locations = await getStates();

      setStates(locations);
      setData(response.componentinteractiveMap.interactiveMapText);
      setShowLetter(response.componentinteractiveMap.showNewLetter);
      setIsHydrated(true);
    }
    fetchData();
    // setIsHydrated(true);
  }, [props.id]);

  if (isHydrated) {
    return (
      <section className="div-not-locations-body">
        <div
          className={`mb-8 grid grid-cols-1 gap-4 ${
            showLetter ? "sm:grid-cols-5" : ""
          } sm:pl-10 sm:pr-10`}
        >
          <div
            className={`${
              showLetter ? "svg-big col-span-3" : "svg-small"
            } hidden sm:block`}
          >
            <Map
              states={states}
              setPopUp={(data: any) => {
                setEnablePopUp(data);
              }}
            />
          </div>

          {showLetter && <NewLetter data={data} />}
        </div>

        {/**Pop up */}
        <div
          className={`description ${enablePopUp ? "active" : ""}`}
          style={
            enablePopUp
              ? {
                  left: enablePopUp.x + enablePopUp.width,
                  top: enablePopUp.y - 7,
                  display: "block",
                }
              : {}
          }
        >
          {enablePopUp?.message}
        </div>

        {/**Pop up */}
      </section>
    );
  } else {
    return <div className="min-h-[500px]"></div>;
  }
}

export default InteractiveMap;
