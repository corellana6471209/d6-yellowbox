import { useState, useEffect } from "react";
import { Form, useActionData, useTransition } from "@remix-run/react";

import SubTitle from "~/routes/common/SubTitle";
import Input from "~/routes/common/Input";
import MessageModal from "../../common/MessageModal";
import Spinner from "../../common/Spinner";
import { getLabel } from "../../../utils/getLabel";

const ChangePasswordForm = () => {
  const actionData = useActionData();
  const transition = useTransition();

  const [typePassword, setShowPassword] = useState<string>("password");
  const [openModal, setOpenModal] = useState<boolean>(false);
  const [message, setMessage] = useState<string>("");
  const [redirect, setRedirect] = useState<boolean>(false);
  const [passwordText, setPasswordText] = useState(
    "Password must be between 6-8 characters and Alpha-Numeric. No special characters."
  );

  useEffect(() => {
    async function fetchData() {
      const passwordText = await getLabel("passwordText");

      if (passwordText) {
        setPasswordText(passwordText);
      }
    }
    fetchData();
  }, []);

  useEffect(() => {
    if (actionData && actionData.showModal) {
      setOpenModal(actionData.showModal);
      setMessage(actionData.message);
      setRedirect(actionData.redirect);
    }
  }, [actionData]);

  const onClick = () => {
    if (typePassword === "password") return setShowPassword("text");
    setShowPassword("password");
  };

  const callback = () => {
    setOpenModal(false);
    if (redirect) window.location.href = "/login";
  };

  const isSubmiting = transition.state === "submitting";

  return (
    <>
      {isSubmiting && <Spinner />}

      <MessageModal
        open={openModal}
        callback={() => callback()}
        message={message}
        title=" "
        ctaText="Close"
      />

      <Form
        className="ChangePasswordForm-root"
        method="post"
        action="/change-password?index"
      >
        <div className="ChangePasswordForm-container">
          <SubTitle text="Old Password" />
          <div>
            <Input
              type="password"
              name="oldPassword"
              placeholder="Old Password"
              ariaLabel="old-password-input"
            />
            {actionData?.frontErrors?.oldPassword && (
              <p className="message-error">This field is required</p>
            )}
          </div>
        </div>
        <div className="ChangePasswordForm-container">
          <SubTitle text="New Password" />
          <div>
            <Input
              type={typePassword}
              name="newPassword"
              placeholder="New Password"
              ariaLabel="new-password-input"
              iconName={typePassword === "password" ? "blindEye" : "eye"}
              onClick={onClick}
            />
            {actionData?.frontErrors?.newPassword && (
              <p className="message-error">{passwordText}</p>
            )}
          </div>
        </div>
        <div className="ChangePasswordForm-button-container">
          <button
            type="submit"
            className="ChangePasswordForm-button yellow-button"
            aria-label="submit-changepasswordform"
          >
            Update
          </button>
        </div>
      </Form>
    </>
  );
};

export default ChangePasswordForm;
