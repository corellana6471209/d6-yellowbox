import { isAlphanumeric } from '../../../utils/regex';

/**
 * const hasNumber = (value) => {
  return /\d/.test(value);
}

 * 
 */
//const hasCapitalLetter = (value) => {
  //return /[A-Z]/.test(value);
//}

const validate = ({ oldPassword, newPassword }) => {
  const oldPasswordValidation = oldPassword.length > 0;

  //&& hasCapitalLetter(newPassword)

  const newPasswordValidation = isAlphanumeric(newPassword) && (newPassword.length <= 8 && newPassword.length >= 6)

  const frontErrors = {
    newPassword: !newPasswordValidation,
    oldPassword: !oldPasswordValidation
  }

  return ({
    frontErrors,
    isValid: newPasswordValidation && oldPasswordValidation
  });
}

export default validate;