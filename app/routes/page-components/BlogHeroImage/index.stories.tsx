import type { ComponentStory, ComponentMeta } from "@storybook/react";
import { BrowserRouter as Router } from "react-router-dom";
import BlogHeroImage from "./index";

export default {
  title: "components/core/BlogHeroImage",
  component: BlogHeroImage,
} as ComponentMeta<typeof BlogHeroImage>;

const Template: ComponentStory<typeof BlogHeroImage> = (args) => (
  <Router>
    <BlogHeroImage />
  </Router>
  // {...args}
);

export const Primary = Template.bind({});
/**
 * Primary.args = {
  unit: fakeDataUnit,
  ctaText: "Save Now with No Obligation",
};
 */
