import fetchCustomApi from "~/utils/fetchCustomApi";

/**
 *
 * @param {string} componentBlogHeroId
 * @returns https://simplyselfstorage.atlassian.net/browse/YBP-928
 */
export const loadBlogArticle = async (componentBlogHeroId: string) => {
  const query = `
  query componentBlogCollection {
    componentblogHero(id: "${componentBlogHeroId}"){
      title
      blogArticleCollection{
        total
        items{
          sys {
            id
            publishedAt
          }
          title
          slug
          pageImage{
            url
          }
        }
        
      }
    }
  } `;
  var blogArticleData = await fetchCustomApi(
    `/api/content/common/contentful/${query}`
  );
  return blogArticleData;
};
