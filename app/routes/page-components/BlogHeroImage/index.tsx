import React from "react";
import { useState } from "react";

import moment from "moment";
import ChevronLeft from "../../common/svg/ChevronLeft";

export default function BlogHeroImage(props: any) {
  const { pageDetails } = props;
  const [imageBlogHero] = useState<any>(
    pageDetails?.pageImage?.url || "https://via.placeholder.com/1300x700"
  );
  const [title] = useState<any>(pageDetails?.title);

  const date = moment(pageDetails?.sys?.publishedAt).format("MMMM DD, YYYY");

  //infinite
  return (
    <section className="blog-hero-image-section mb-[60px] max-w-[1200px] mx-auto">
      {/*******************************************IMAGE******************************************** */}

      <div className="div-image-hero">
        <div
          className="div-badge-img absolute flex cursor-pointer items-center bg-tertiary1 p-2 opacity-80 full-opacity"

          onClick={() => (window.location.href = "/blog")}
        >
          <ChevronLeft size="30px" color="#FFFFFF" />
          <p className="text-sm ml-2 hidden text-white md:block">
            Back To Results
          </p>
          <p className="text-sm ml-2 text-white md:hidden">Back</p>
        </div>
        <div className="relative">
          <img src={imageBlogHero} alt="Blog" />
          <section className="section-hero-text w-100 absolute hidden md:block">
            <div className="div-image-hero-text ">
              {title && (
                <h1 className="font-serif text-36 font-semibold text-white">
                  {title}
                </h1>
              )}

              <div className="mt-2 mb-[3rem]">
                {/**<p className="text-24 text-white">{date}</p> */}
                <p className="text-24 font-bold text-primary1 no-underline">
                  by Simply
                </p>
              </div>
            </div>
          </section>
        </div>

        <div className="div-hero-text bg-tertiary1 p-5 md:hidden">
          {title && (
            <p className="font-serif text-24 font-semibold text-white">
              {title}
            </p>
          )}

          <div className="mt-2">
            {/**<p className="text-14 text-white">{date}</p> */}
            <p className="text-14 font-bold text-primary1 no-underline">
              by Simply
            </p>
          </div>
        </div>
      </div>

      {/*******************************************IMAGE******************************************** */}
    </section>
  );
  // } else {
  //  return <></>;
  // }
}
