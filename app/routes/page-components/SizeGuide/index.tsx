import { useEffect, useState } from "react";
import { getSizeGuide } from "./backend";
import RichTextComponent from "~/routes/common/RichTextComponent";
import Button from "../../common/Button";
import { Link } from "react-scroll";

export default function SizeGuide(props: any) {
  const [sizeGuide, setSizeGuide] = useState<any>(null);
  const [isHydrated, setIsHydrated] = useState(false);
  const [activeItem, setActiveSGItem] = useState(0);

  useEffect(() => {
    async function fetchData() {
      // You can await here
      const response = await getSizeGuide(props.id);
      setSizeGuide(response.componentsizeGuide);
      setIsHydrated(true);
    }
    fetchData();
  }, [props.id]);

  function changeActiveSG(index: number) {
    setActiveSGItem(index);
  }

  if (isHydrated) {
    return (
      <div className="size-guide">
        <div className="wide-wrapper">
          <h2 className="mb-5 text-center font-serif text-28 text-greyHeavy">
            {sizeGuide.title}
          </h2>

          <div className="mb-10 ml-auto mr-auto max-w-sm justify-start overflow-y-auto whitespace-nowrap md:max-w-3xl lg:max-w-5xl lg:justify-center xl:max-w-none">
            {sizeGuide.unitTypeCollection.items.map((unit: any, index: any) => (
              <p
                onClick={() => changeActiveSG(index)}
                key={index}
                className="mr-1 inline-block cursor-pointer bg-secondary4 p-2 font-bold text-secondary2"
              >
                {unit.tab ? unit.tab : ""}
              </p>
            ))}
          </div>

          <div className="size-guide--content lg:mx-20">
            {sizeGuide.unitTypeCollection.items.map((unit: any, index: any) => (
              <div
                key={index}
                className={`grid grid-cols-1 justify-start gap-4 transition-opacity duration-1000 ease-in-out lg:grid-cols-3 ${
                  activeItem == index
                    ? "opacity-1"
                    : "h-0 overflow-hidden opacity-0"
                }`}
              >
                <div className="lg:col-span-1">
                  <img
                    src={unit.zoomedImage.url}
                    alt={unit.zoomedImage.description}
                    className="w-9/12"
                  />
                </div>
                <div className="text-left lg:col-span-2">
                  <h3 className="mb-3 text-24 font-bold">{unit.name}</h3>
                  <div className="rt mb-10">
                    <RichTextComponent data={unit.description} />
                  </div>
                  {/* <Link
                    className="cursor-pointer bg-primary1 yellow-button py-3 px-10 text-secondary1 no-underline"
                    to={sizeGuide.buttonScrollId}
                    smooth={true}
                  >
                    {sizeGuide.buttonText
                      ? sizeGuide.buttonText
                      : "Explore Units Near Me"}
                  </Link> */}
                  <a
                    href="/"
                    className="cursor-pointer bg-primary1 yellow-button py-3 px-10 text-secondary1 no-underline"
                  >
                    {sizeGuide.buttonText
                      ? sizeGuide.buttonText
                      : "Explore Units Near Me"}
                  </a>
                </div>
              </div>
            ))}
          </div>
        </div>
      </div>
    );
  } else {
    return <div className="min-h-[500px]"></div>;
  }
}
