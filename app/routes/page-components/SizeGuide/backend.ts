import fetchCustomApi from "~/utils/fetchCustomApi";

export async function getSizeGuide(params: string) {
  const query = `
    query {
        componentsizeGuide(id: "${params}") {
            title
            buttonText
            buttonScrollId
            unitTypeCollection(limit: 25) {
                items {
                    __typename
                    ... on Entry {
                        sys {
                            id
                        }
                    }
                    name
                    tab
                    description{
                        json
                    }
                    zoomedImage{
                        url
                        description
                    }
                }
            }
        }
    }`;

  var theData = await fetchCustomApi(`/api/content/common/contentful/${query}`);
  return theData;
}
