import React from "react";

export default function OneTrustButton() {
  return (
    <div className="flex justify-center py-5">
      <button id="ot-sdk-btn" className="ot-sdk-show-settings">
        Do Not Sell My Personal Information
      </button>
    </div>
  );
}
