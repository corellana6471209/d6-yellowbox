import React, { useState, useEffect } from "react";
import MessageModal from "../../common/MessageModal";
import Spinner from "../../common/Spinner";

import {
  Form,
  useActionData,
  useTransition,
  useLoaderData,
} from "@remix-run/react";

import Title from "~/routes/common/Title";
import Input from "~/routes/common/Input";

const ForgotPasswordForm = () => {
  const actionData = useActionData();
  const transition = useTransition();
  const loaderData = useLoaderData();
  const [openModal, setOpenModal] = useState<any>(false);
  const [message, setMessage] = useState<any>(null);

  const isSubmiting = transition.state === "submitting";

  useEffect(() => {
    if (loaderData.islegacy === "true") {
      const dateToSave = JSON.stringify(loaderData);
      localStorage.setItem("redirectLogin", dateToSave);
    }
  }, [loaderData]);

  useEffect(() => {
    if (actionData && actionData.showModal) {
      setOpenModal(actionData.showModal);
      setMessage(actionData.message);
    }
  }, [actionData]);

  return (
    <>
      {isSubmiting && <Spinner />}

      <MessageModal
        open={openModal}
        callback={() => {
          setOpenModal(false);
        }}
        message={message}
        title=" "
        ctaText="Close"
      />

      <Form
        className="ForgotPasswordForm-container"
        method="post"
        action="/forgot-password?index"
      >
        <Title text={"Forgot Your Password?"} />
        <span className="ForgotPasswordForm-message">
          No problem. Please enter the email address associated with your
          account. We will send you an email for resetting your password.
        </span>
        <div className="ForgotPasswordForm-inputs-container">
          <input
            type="hidden"
            name="slug"
            value={loaderData.slug ? loaderData.slug : ""}
          />
          <input
            type="hidden"
            name="reservation"
            value={loaderData.reservation ? loaderData.reservation : ""}
          />
          <input
            type="hidden"
            name="islegacy"
            value={loaderData.islegacy ? loaderData.islegacy : ""}
          />

          <div>
            <Input
              type="text"
              name="email"
              placeholder="Email Address"
              ariaLabel="email-input"
            />
            {actionData &&
              actionData.frontErrors &&
              actionData.frontErrors.email && (
                <p className="message-error">
                  This field must be example@example.com
                </p>
              )}
          </div>
        </div>
        <div className="ForgotPasswordForm-button-container">
          <button
            type="submit"
            className="ForgotPasswordForm-button yellow-button"
            aria-label="submit-forgotPasswordForm"
            disabled={isSubmiting}
          >
            Send Email
          </button>
        </div>
      </Form>
    </>
  );
};

export default ForgotPasswordForm;
