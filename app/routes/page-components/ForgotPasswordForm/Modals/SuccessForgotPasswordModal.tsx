import Icon from "../../../common/svg/Icon";

const SuccessForgotPasswordModal = (props: any) => {
  const onClick = () => {
    console.log("cerrar");
  };

  return (
    <Icon
      iconName="cross"
      classIcon="SuccessForgotPasswordModal-icon"
      onClick={onClick}
    />
  );
};

export default SuccessForgotPasswordModal;
