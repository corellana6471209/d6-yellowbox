import fetchCustomApi from "~/utils/fetchCustomApi";


export async function getCalloutCta(params: string) {
  const query = `
    query componentCallOutCta {
      componentcalloutCta(id: "${params}") {
        title
        description
        ctaUrl
        ctaText      
    }
  }`;

  var theData = await fetchCustomApi(`/api/content/common/contentful/${query}`);
  return theData;
}
