import React from "react";
import { describe, expect, it } from "vitest";
import MovingSupplies from ".";
import { render, screen } from "../../../utils/test-utils";
import { BrowserRouter as Router } from "react-router-dom";
import { callOut } from "../../../utils/fakedataToTests";

describe("Search component", () => {
  it("Verify if the component renders", () => {
    render(
      <Router>
        <MovingSupplies callOut={callOut} />
      </Router>
    );
  });

  it("Verify if exists title", async () => {
    render(
      <Router>
        <MovingSupplies callOut={callOut} />
      </Router>
    );

    expect(await screen.findByText(callOut.title)).toBeInTheDocument();
  });

  it("Verify if exists description", async () => {
    render(
      <Router>
        <MovingSupplies callOut={callOut} />
      </Router>
    );

    expect(await screen.findByText(callOut.description)).toBeInTheDocument();
  });

  it("Verify if exists description", async () => {
    render(
      <Router>
        <MovingSupplies callOut={callOut} />
      </Router>
    );
    const link = screen.getByRole("link");
    expect(link).toBeInTheDocument();
  });
});
