import { useEffect, useState } from "react";
import Button from "../../common/Button";
import { getCalloutCta } from "./backend";

function CallOutCta(props: any) {
  const [callOut, setCallOut] = useState<any>(null);
  const [isHydrated, setIsHydrated] = useState(false);

  useEffect(() => {
    async function fetchData() {
      // You can await here
      const response = await getCalloutCta(props.id);
      setCallOut(response.componentcalloutCta);
      setIsHydrated(true);
    }
    fetchData();
    // setIsHydrated(true);
  }, [props.id]);

  if (isHydrated) {
    return (
      <div className="div-body-moving-supplies mt-4 bg-lightGold">
        <h3 className="title-state-list w-100 text-4xl pt-5 text-center font-serif text-blueDark">
          {callOut.title}
        </h3>

        <p className="text-description mb-8 mt-6 mb-10 text-center text-16 text-blueDark">
          {callOut.description}
        </p>

        <div className="div-button-congratulations mt-10 pb-6">
          <Button
            href={callOut.ctaUrl}
            target="_self"
            title={callOut.ctaText}
            buttonStyle="button-tertiary-inverted"
          />
        </div>
      </div>
    );
  } else {
    return ( <div className="min-h-[500px]"></div> );
  }
}

export default CallOutCta;
//<div className="div-reservations-unit">WORK IN PROGRESS</div>
