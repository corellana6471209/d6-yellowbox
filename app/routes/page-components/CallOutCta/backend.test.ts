import { describe, expect, it, vi } from "vitest";
import { getCalloutCta } from "./backend";

const id = "1234";

const obj = {
  getCalloutCta: () => "getCalloutCta",
};

vi.mock("./backend", () => ({
  getCalloutCta: vi.fn().mockImplementation(() => "getCalloutCta"),
}));

describe("fetching location data component unit test ", () => {
  it("LoadModuleTitle should be mock-getCalloutCta", () => {
    expect(vi.isMockFunction(getCalloutCta)).toBe(true);
  });

  it("Should be executed loadModuleTitle-getCalloutCta", async () => {
    const spy = vi.spyOn(obj, "getCalloutCta");

    const resp = await getCalloutCta(id);
    expect(spy.getMockName()).toEqual("getCalloutCta");
    expect(resp).toBe("getCalloutCta");
  });
});
