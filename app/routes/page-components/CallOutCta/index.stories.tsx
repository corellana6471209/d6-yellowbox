import type { ComponentStory, ComponentMeta } from "@storybook/react";
import { BrowserRouter as Router } from "react-router-dom";

import CallOutCta from ".";

export default {
  title: "components/core/CallOutCta",
  component: CallOutCta,
} as ComponentMeta<typeof CallOutCta>;

const Template: ComponentStory<typeof CallOutCta> = (args) => (
  <Router>
    <CallOutCta {...args} />
  </Router>
);

export const CallOutCtaComponent = Template.bind({});
CallOutCtaComponent.args = { id: "42rNmfCl4eynBECSWjNrmE" };
