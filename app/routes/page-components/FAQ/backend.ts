import fetchCustomApi from "~/utils/fetchCustomApi";


export let loadFAQ = async (id: string) => {
  // console.log("RichText::loadRichText params", heroId);
  const query = `
      query {
        componentfaq(id: "${id}") {
          sys {
            id
          }
          title
          faqCollection(limit: 25) {
            items {
              __typename
              ... on Entry {
                sys {
                  id
                }
              }
              question
              answer{
                json
              }
            }
          }
        }
      }
      `;
  var theData = await fetchCustomApi(`/api/content/common/contentful/${query}`);
  return theData;
};
