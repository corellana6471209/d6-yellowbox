import { useEffect, useState } from "react";
import { loadFAQ } from "./backend";
import Search from "../../common/Search";
import { getLabel } from "../../../utils/getLabel";
import RichTextComponent from "~/routes/common/RichTextComponent";
import { renderToStaticMarkup } from 'react-dom/server';

// define module shape
interface Module {
  title: string;
  faqCollection: {
    items: {};
  };
}

let isHydrating = true;

export default function HeroWithSearch(props: any) {
  // defines the initial state
  let [isHydrated, setIsHydrated] = useState(!isHydrating);
  const [module, setModule] = useState<Module | null>(null);
  const [openItem, setOpenItem] = useState(null);
  //   const [config, setConfig] = useState<any>(null);
  useEffect(() => {
    async function fetchData() {
      // You can await here
      const response = await loadFAQ(props.id);
      setModule(response.componentfaq);
      return response.componentfaq;
    }
    const incomingModule = props?.id;
    if (incomingModule) {
      isHydrating = false;
      // load the data
      fetchData();
      setIsHydrated(true);
    }
  }, [props.id]);

  function faqItemSetter(item: any) {
    setOpenItem(openItem === item ? null : item);
  }

  function handleKeyPress(e: any, item: any) {
    var key = e.key;
    if (key === "Enter") {
      setOpenItem(openItem === item ? null : item);
    }
  }

  const RTFAnswer = (data: any) => {
    return renderToStaticMarkup(<RichTextComponent contentWrap={false} data={data} />)
  }

  const Schema = () => {
    if (module) {
      try {
        return (
          <script
            type="application/ld+json"
            dangerouslySetInnerHTML={{
              __html: `
              {
                "@context": "https://schema.org",
                "@type": "FAQPage",
                "mainEntity": ${JSON.stringify(module.faqCollection.items.map((faq: any, index: any) => {
                  return ({
                    "@type": "Question",
                    "name": faq.question,
                    "acceptedAnswer": {
                      "@type": "Answer",
                      "text": RTFAnswer(faq.answer)
                    }
                  })
                }
                ))}
              }
            `,
            }}
          />
        )
      } catch (error) {
        console.log(error);
        return null;
      }
      
    }
    return null;
  }

  if (isHydrated && module) {
    return (
      <div className="faq my-5 bg-lightGold p-10">
        <div className="wide-wrapper">
          <h2 className="mb-5 text-center font-serif text-28 text-greyHeavy">
            {module.title}
          </h2>
          <Schema />
          {module.faqCollection.items.map((faq: any, index: any) => (
            <div
              key={index}
              className="faq__item mb-[10px] cursor-pointer"
              onClick={(e) => {
                faqItemSetter(faq);
              }}
              onKeyPress={(e) => {
                handleKeyPress(e, faq);
              }}
              tabIndex={0}
            >
              <div className="bg-primary2 p-5">
                <div className="mx-auto flex max-w-screen-xl content-start items-start justify-start">
                  <div className="q-decorator mr-4 flex items-start font-serif text-30 text-secondary1">
                    <span className="mt-[-6px]">Q</span>
                  </div>
                  <h3 className="text-20 font-bold text-secondary1">
                    {faq.question}
                  </h3>
                </div>
              </div>
              {openItem?.sys?.id === faq.sys?.id && (
                <div className="cursor-default p-5">
                  <div className="mx-auto max-w-screen-xl text-greyHeavy">
                    <RichTextComponent contentWrap={false} data={faq.answer} />
                  </div>
                </div>
              )}
            </div>
          ))}
        </div>
      </div>
    );
  } else {
    return <div className="min-h-[500px]"></div>;
  }
}
