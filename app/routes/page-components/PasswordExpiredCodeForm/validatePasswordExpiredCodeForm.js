import { validateEmail } from '../../../utils/regex';

const validate = ({ email }) => {
  const emailValidation = validateEmail(email);

  const frontErrors = {
    email: !emailValidation,
  }

  return ({
    frontErrors,
    isValid: emailValidation
  });
}

export default validate;