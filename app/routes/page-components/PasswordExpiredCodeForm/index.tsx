import React, { useState, useEffect } from "react";
import MessageModal from "../../common/MessageModal";
import Spinner from "../../common/Spinner";
import { getRTF } from "~/utils/getRtf";

import {
  Form,
  useActionData,
  useTransition,
  useLoaderData,
} from "@remix-run/react";

import Title from "~/routes/common/Title";
import Input from "~/routes/common/Input";

//import SuccessForgotPasswordModal from "./Modals/SuccessForgotPasswordModal";

const PasswordExpiredCodeForm = () => {
  const actionData = useActionData();
  const transition = useTransition();
  const loaderData = useLoaderData();
  const [openModal, setOpenModal] = useState<any>(false);
  const [message, setMessage] = useState<any>(null);
  const [text, setText] = useState<any>({});

  const [titleRTF, setTitleRTF] = useState<string>('');
  const [subTitleRTF, setSubTitleRTF] = useState<string>('');
  const [paragraph1RTF, setParagraph1RTF] = useState<string>('');
  const [paragraph2RTF, setParagraph2RTF] = useState<string>('');

  const isSubmiting = transition.state === "submitting";

  useEffect(() => {
    if (loaderData.islegacy === "true") {
      const dateToSave = JSON.stringify(loaderData);
      localStorage.setItem("redirectLogin", dateToSave);
    }
  }, [loaderData]);

  useEffect(() => {
    if (actionData && actionData.showModal) {
      setOpenModal(actionData.showModal);
      setMessage(actionData.message)
    }
  }, [actionData]);

  useEffect(() => {
    fetchData();
  }, []);

  const fetchData = async () => {
    const title = await getRTF('reset-link-text-4');
    const subTitle = await getRTF('reset-link-text-1');
    const paragraph1 = await getRTF('reset-link-text-2');
    const paragraph2 = await getRTF('reset-link-text-3');

    setTitleRTF(title);
    setSubTitleRTF(subTitle);
    setParagraph1RTF(paragraph1);
    setParagraph2RTF(paragraph2);
  }

  return (
    <>
      {isSubmiting && <Spinner />}

      <MessageModal
        open={openModal}
        callback={() => {
          setOpenModal(false);
        }}
        message={message}
        title=" "
        ctaText="Close"
      />

      <Form
        className="PasswordExpiredCodeForm-container"
        method="post"
        action="/password-expired-code?index"
      >
        <Title text={titleRTF} />
        <div className='PasswordExpiredCodeForm-message-container'>
          <span className="PasswordExpiredCodeForm-error-message">
            {subTitleRTF}
          </span>
          <span className="PasswordExpiredCodeForm-message">
            {paragraph1RTF}
          </span>
          <span className="PasswordExpiredCodeForm-message">
            {paragraph2RTF}
          </span>
        </div>
        <div className="PasswordExpiredCodeForm-inputs-container">
          <input type="hidden" name="slug" value={loaderData.slug} />
          <input
            type="hidden"
            name="reservation"
            value={loaderData.reservation}
          />
          <input type="hidden" name="islegacy" value={loaderData.islegacy} />

          <div>
            <Input
              type="text"
              name="email"
              placeholder="Email Address"
              ariaLabel="email-input"
            />
            {actionData &&
              actionData.frontErrors &&
              actionData.frontErrors.email && (
                <p className="message-error">
                  This field must be example@example.com
                </p>
              )}
          </div>
        </div>
        <div className="PasswordExpiredCodeForm-button-container">
          <button
            type="submit"
            className="PasswordExpiredCodeForm-button yellow-button"
            aria-label="submit-PasswordExpiredCodeForm"
            disabled={isSubmiting}
          >
            Send Email
          </button>
        </div>
      </Form>
    </>
  );
};

export default PasswordExpiredCodeForm;