import fetchCustomApi from "~/utils/fetchCustomApi";

export let loadStates = async (): Promise<{ name: string; link: string }[]> => {
  const query = `
    query stateCollection {
      stateCollection(order: stateFullName_ASC) {
        items {
          stateFullName
          stateAbbreviation
        }
      }
    }`;

  const { stateCollection } = await fetchCustomApi(`/api/content/common/contentful/${query}`);
  // convert the return value to needed model
  if (!stateCollection) return [];
  return handleResponse(stateCollection.items);
};

export const handleResponse = (items: any[]) => {
  let dataResponse = [];
  for (let i = 0; i < items.length; i++) {
    dataResponse[i] = {
      name: items[i].stateFullName + " Self Storage",
      link: `/locations/state/${items[i].stateAbbreviation}`,
    };
  }
  return dataResponse;
};

export let loadModuleTitle = async (
  componentId = { componentId }
): Promise<{ title: string }> => {
  const query = `
    query componentstateList {
      componentstateList(id: "${componentId}") {
        title
      }
    }`;

  var theData = await fetchCustomApi(`/api/content/common/contentful/${query}`);
  return theData.componentstateList.title;
};
