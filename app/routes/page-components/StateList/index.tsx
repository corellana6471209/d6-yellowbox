import StateListItem from "./StateListItem";
import { useState, useEffect } from "react";
import React from "react";
import { loadModuleTitle, loadStates } from "./backend";

let isHydrating = true;

/**
 * StateList function
 * @return {component} with data
 *
 */
interface Module {
  title: string;
  states: { name: string; link: string }[];
}

const StateList = function StateList(props: any) {
  const { id } = props;
  let [isHydrated, setIsHydrated] = React.useState(!isHydrating);
  // defines the initial state
  const [module, setModule] = useState<Module | null>(null);

  useEffect(() => {
    async function fetchData() {
      // You can await here
      const statesTitle = await loadModuleTitle(props.id);
      const statesList = await loadStates();
      const moduleResponse = {
        title: statesTitle,
        states: statesList,
      };
      setModule(moduleResponse);
      return moduleResponse;
    }
    const incomingModule = props?.module;
    if (!incomingModule) {
      isHydrating = false;
      // load the data
      fetchData();
      setIsHydrated(true);
    } else {
      isHydrating = false;
      setModule(incomingModule);
      setIsHydrated(true);
    }
  }, [props.id]);

  if (isHydrated && module) {
    return (
      <div className="div-state-list bg-lightGold">
        <div className="mx-auto max-w-[1200px]">
          <h1 className="title-state-list w-100 text-4xl text-center font-serif text-greyHeavy">
            {module.title}
          </h1>

          <div
            className="grid grid-cols-2 content-center gap-x-[50px] gap-y-[30px] max-sm:p-4 md:grid-cols-3 xl:grid-cols-4 xl:gap-x-[100px]"
            id="list-item"
          >
            {module.states.map((item) => {
              return (
                <StateListItem
                  key={item.name}
                  name={item.name}
                  link={item.link}
                />
              );
            })}
          </div>
        </div>

        {/* <div className="div-state-list-item">
          {module.states.map((item) => (
            <>
              <StateListItem
                key={item.name}
                name={item.name}
                link={item.link}
              />
            </>
          ))}
        </div> */}
      </div>
    );
  } else {
    return ( <div className="min-h-[500px]"></div> );
  }
};

export default StateList;
