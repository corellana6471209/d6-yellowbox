import { describe, expect, it, vi } from "vitest";
import { loadStates, loadModuleTitle } from './backend';
import { states, statesFilterSearch } from '../../../utils/fakedataToTests';

const obj = {
  loadStates: () => states,
  loadModuleTitle: () => "Locations By State",
}

vi.mock('./backend', () => ({
  loadStates: vi.fn().mockImplementation(() => states), //return what you want
  loadModuleTitle: vi.fn().mockImplementation(() => "Locations By State"),
}));

describe("StateList component unit test", () => {
  it('loadStates should be mock', () =>{
    expect(vi.isMockFunction(loadStates)).toBe(true)
  });

  it('Should be executed loadStates', async () => {
    const spy = vi.spyOn(obj, 'loadStates');
    const resp = await loadStates();
    expect(spy.getMockName()).toEqual('loadStates');
    expect(resp.length).toBeGreaterThan(0);
  });

  it('Should be executed loadStates first item should be the first of fakeData', async () => {
    const resp = await loadStates();
    expect(resp[0].name).toBe(states[0].name);
  });

  it('LoadModuleTitle should be mock', () =>{
    expect(vi.isMockFunction(loadModuleTitle)).toBe(true)
  });

  it('Should be executed loadModuleTitle', async () => {
    const spy = vi.spyOn(obj, 'loadModuleTitle');
    const resp = await loadModuleTitle();
    expect(spy.getMockName()).toEqual('loadModuleTitle');
    expect(resp).toBe("Locations By State");
  });
});
