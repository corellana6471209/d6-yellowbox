// import Link from "../../../common/Link";

// import { useNavigate } from "react-router-dom";
import { getLocations } from "../../../../routes/page-components/LocationMapList/backend";
import { getCoords } from "../../../common/Search/backend";

/**
 * StateList function
 * @return {component} with data
 *
 */
interface StateListItemProps {
  name: string;
  link: string;
}

const StateListItem = function GetStateListItem({
  name,
  link,
}: StateListItemProps) {
  // const navigate = useNavigate();


  return (
    <div className="state-list-item">
      <a
        href={link}
        className="stateListLink cursor-pointer text-blueDark sm:font-size[16px] link"
        style={{textDecoration:"none"}}
      >
        {name}
      </a>
    </div>
  );
};

export default StateListItem;
