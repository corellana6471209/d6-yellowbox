import type { Meta } from "@storybook/react";
import { Key } from "react";
import { states } from "../../../../utils/fakedataToTests";
import StateListItem from ".";

export default {
  title: "modules/StateListItem",
  component: StateListItem,
  name: "",
  link: "",
} as Meta;

/**
 * @param {object} args to the component
 * @returns {component} with data.
 */

const Template = (args) => <StateListItem {...args} />;

export const Primary = Template.bind({});

Primary.args = {
  name: "Georgia Self Storage",
  link: "#",
};
