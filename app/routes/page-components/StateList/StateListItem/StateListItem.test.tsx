import { describe, expect, it } from "vitest";
import { render, screen } from "../../../../utils/test-utils";
import { BrowserRouter as Router } from "react-router-dom";
import StateListItem from ".";
import { states } from "../../../../utils/fakedataToTests";


describe("StateListItem component unit test", () => {
  it("verify the render the StateListItem component", () => {
    render(
      <Router>
        <StateListItem {...states[0]} />,
      </Router>
    );
  });

  it("Verify that the link exist with the correct name", async () => {
    render(
      <Router>
        <StateListItem {...states[0]} />,
      </Router>
    );

    const link = screen.getByRole("link", {
      name: /Georgia Self Storage/i,
    });
    expect(link).toBeInTheDocument();
  });
});
