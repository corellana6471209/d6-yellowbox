import type { ComponentMeta } from '@storybook/react';
import { states } from '../../../utils/fakedataToTests';
import StateList from './index';

export default {
  title: 'modules/StateList',
    component: StateList,
  } as ComponentMeta<typeof StateList>;

/**
 * @param {object} args to the component
 * @returns {component} with data.
 */

const Template = args => <StateList {...args} />;

export const Primary = Template.bind({});
Primary.args = {
    id: "abc123",
    module: {
      states: states,
      title: "Locations By State",
    },
  };
