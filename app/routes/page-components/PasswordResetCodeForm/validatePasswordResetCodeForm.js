import { isAlphanumeric } from '../../../utils/regex';

/**
const hasNumber = (value) => {
  return /\d/.test(value);
}

*/

/**
const hasCapitalLetter = (value) => {
  return /[A-Z]/.test(value);
}
*/


const validate = ({ password, resetKey }) => {
  const passwordValidation = isAlphanumeric(password) && password.length >= 6 & password.length <= 8;
  const resetKeyValidation = resetKey.length > 0;

  const frontErrors = {
    password: !passwordValidation,
    resetKey: !resetKeyValidation
  }

  return ({
    frontErrors,
    isValid: (passwordValidation && resetKeyValidation)
  });
}

export default validate;