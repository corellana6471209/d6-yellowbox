import React, { useEffect, useState } from "react";

import {
  Form,
  useActionData,
  useLoaderData,
  useTransition,
} from "@remix-run/react";

import Title from "~/routes/common/Title";
import Input from "~/routes/common/Input";
import MessageModal from "../../common/MessageModal";
import Spinner from "../../common/Spinner";
import { getLabel } from "../../../utils/getLabel";

const PasswordResetCodeForm = () => {
  const [typePassword, setShowPassword] = useState<string>("password");
  const [redirectLogin, setRedirectLogin] = useState<any>(null);

  const actionData = useActionData();
  const transition = useTransition();
  const loaderData = useLoaderData();
  const [openModal, setOpenModal] = useState<any>(false);
  const [message, setMessage] = useState<any>(null);
  const [labels, setLabels] = useState<any>({
    passwordText:
      "Create a password for your new account. Password must be 6 to 8 characters and/or numbers, and no special characters.",
  });

  const onClick = () => {
    if (typePassword === "password") return setShowPassword("text");
    setShowPassword("password");
  };

  const isSubmiting = transition.state === "submitting";

  useEffect(() => {
    const redirectLogin = localStorage.getItem("redirectLogin");

    if (redirectLogin) {
      setRedirectLogin(JSON.parse(redirectLogin));
    }

    async function fetchData() {
      const passwordText = await getLabel("passwordText");

      setLabels({
        passwordText: passwordText ? passwordText : labels.passwordText,
      });
    }

    fetchData();
  }, []);

  useEffect(() => {
    if (actionData && actionData.showModal) {
      setOpenModal(actionData.showModal);
      setMessage(actionData.message);
    }
  }, [actionData]);

  return (
    <>
      {isSubmiting && <Spinner />}

      <MessageModal
        open={openModal}
        callback={() => {
          setOpenModal(false);
        }}
        message={message}
        title=" "
        ctaText="Close"
      />
      <Form
        className="PasswordResetCodeForm-container"
        method="post"
        action="/password-reset-code?index"
      >
        <Title text={"Password Reset"} />
        <div className="PasswordResetCodeForm-inputs-container">
          {redirectLogin && (
            <>
              <input type="hidden" name="slug" value={redirectLogin.slug} />
              <input
                type="hidden"
                name="reservation"
                value={redirectLogin.reservation}
              />
              <input
                type="hidden"
                name="islegacy"
                value={redirectLogin.islegacy}
              />
            </>
          )}

          <div>
            <Input
              type={typePassword}
              name="newPassword"
              placeholder="New Password"
              ariaLabel="new-password-input"
              iconName={typePassword === "password" ? "blindEye" : "eye"}
              onClick={onClick}
            />
            {actionData &&
              actionData.frontErrors &&
              actionData.frontErrors.password && (
                <p className="message-error">{labels.passwordText}</p>
              )}
          </div>
          <input name="resetKey" value={loaderData.resetKey} type="hidden" />
        </div>
        <div className="PasswordResetCodeForm-button-container">
          <button
            type="submit"
            className="PasswordResetCodeForm-button yellow-button"
            aria-label="submit-PasswordResetCodeForm"
            disabled={isSubmiting}
          >
            Change Password
          </button>
        </div>
      </Form>
    </>
  );
};

export default PasswordResetCodeForm;
