import fetchCustomApi from "~/utils/fetchCustomApi";

/**
 * 
 * @param {string} componentblockQuoteId 
 * @returns https://simplyselfstorage.atlassian.net/browse/YBP-937
 * @testing componentblockQuoteId: "01osSSHPQWMzk5GEWcCBBN"
 */
export const loadBlockQuote = async (componentblockQuoteId: string) => {
    const query = `
  query componentMarkDownCollection {
    componentmarkDownEditor(id: "${componentblockQuoteId}") {
      content
    }
  }`;
    var componentblockQuoteData = await fetchCustomApi(`/api/content/common/contentful/${query}`);
    return componentblockQuoteData;
};