import React, { useState } from "react";
import { useEffect } from "react";
import { loadBlockQuote } from "./backend";
import { marked } from "marked";
import * as DOMPurify from "dompurify";

let isHydrating = true;

export default function MarkDown(props: any) {
  const { id, body } = props;
  const [isHydrated, setIsHydrated] = useState(!isHydrating);
  const [text, setText] = useState<any>(null);

  useEffect(() => {
    if (body) {
      setText(DOMPurify.sanitize(marked.parse(body)));
      setIsHydrated(true);
    } else {
      async function fetchData() {
        const componentResponse = await loadBlockQuote(id);
        setText(
          DOMPurify.sanitize(
            marked.parse(componentResponse.componentmarkDownEditor.content)
          )
        );
        setIsHydrated(true);
      }
      fetchData();
    }
  }, []);

  if (isHydrated) {
    return (
      <div
        className={`markdown-information-wrapper small-wrapper pl-5 pr-5 md:pl-0 md:pr-0 ${
          body ? "text-mark-down" : `blog-mark-down`
        }`}
      >
        <div dangerouslySetInnerHTML={{ __html: text }}></div>
      </div>
    );
  } else {
    return <></>;
  }
}
