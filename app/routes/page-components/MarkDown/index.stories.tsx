import type { ComponentStory, ComponentMeta } from "@storybook/react";
import MarkDown from "./index";

export default {
  title: "components/core/MarkDown",
  component: MarkDown,
} as ComponentMeta<typeof MarkDown>;

const Template: ComponentStory<typeof MarkDown> = (args) => (
  <MarkDown />
  // {...args}
);

export const Primary = Template.bind({});
/**
 * Primary.args = {
  unit: fakeDataUnit,
  ctaText: "Save Now with No Obligation",
};
 */