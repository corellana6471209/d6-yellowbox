import React from "react";
import { describe, expect, it } from "vitest";
import Congratulations from ".";
import { render, screen } from "../../../utils/test-utils";
import { BrowserRouter as Router } from "react-router-dom";
import { callOutIcon } from "../../../utils/fakedataToTests";

describe("Search component", () => {
  it("Verify if the component renders", () => {
    render(
      <Router>
        <Congratulations callOutIcon={callOutIcon} />
      </Router>
    );
  });

  it("Verify if exists title", async () => {
    render(
      <Router>
        <Congratulations callOutIcon={callOutIcon} />
      </Router>
    );

    expect(await screen.findByText(callOutIcon.title)).toBeInTheDocument();
  });

  it("Verify if exists description", async () => {
    render(
      <Router>
        <Congratulations callOutIcon={callOutIcon} />
      </Router>
    );

    expect(
      await screen.findByText(callOutIcon.description)
    ).toBeInTheDocument();
  });

  it("Verify if exists description", async () => {
    render(
      <Router>
        <Congratulations callOutIcon={callOutIcon} />
      </Router>
    );
    const link = screen.getByRole("link");
    expect(link).toBeInTheDocument();
  });
});
