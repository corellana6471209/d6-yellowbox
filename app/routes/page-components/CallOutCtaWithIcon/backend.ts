import fetchCustomApi from "~/utils/fetchCustomApi";

export async function getCalloutCtaWithIcon(params: string) {
  const query = `
    query componentCallCtaWithIcon {
      componentcalloutCtaWithIcon(id: "${params}") {
        title
        icon {
          title
          description
          contentType
          fileName
          size
          url
          width
          height
        }
        description
        ctaUrl
        ctaText      
    }
  }`;

  var theData = await fetchCustomApi(`/api/content/common/contentful/${query}`);
  return theData;
}
