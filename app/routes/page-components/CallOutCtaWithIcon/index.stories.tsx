import type { ComponentStory, ComponentMeta } from "@storybook/react";
import { BrowserRouter as Router } from "react-router-dom";

import CallOutCtaWithIcon from ".";

export default {
  title: "components/core/CallOutCtaWithIcon",
  component: CallOutCtaWithIcon,
} as ComponentMeta<typeof CallOutCtaWithIcon>;

const Template: ComponentStory<typeof CallOutCtaWithIcon> = (args) => (
  <Router>
    <CallOutCtaWithIcon {...args} />
  </Router>
);

export const CallOutCtaWithIconComponent = Template.bind({});
CallOutCtaWithIconComponent.args = { id: "4r4zQiHF6me90nyfio25ds" };
