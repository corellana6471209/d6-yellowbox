import { describe, expect, it, vi } from "vitest";
import { getCalloutCtaWithIcon } from "./backend";

const id = "1234";
const obj = {
  getCalloutCtaWithIcon: () => "getCalloutCtaWithIcon",
};

vi.mock("./backend", () => ({
  getCalloutCtaWithIcon: vi
    .fn()
    .mockImplementation(() => "getCalloutCtaWithIcon"),
}));

describe("fetching location data component unit test ", () => {
  it("LoadModuleTitle should be mock-getCalloutCtaWithIcon", () => {
    expect(vi.isMockFunction(getCalloutCtaWithIcon)).toBe(true);
  });

  it("Should be executed loadModuleTitle-getCalloutCtaWithIcon", async () => {
    const spy = vi.spyOn(obj, "getCalloutCtaWithIcon");

    const resp = await getCalloutCtaWithIcon(id);
    expect(spy.getMockName()).toEqual("getCalloutCtaWithIcon");
    expect(resp).toBe("getCalloutCtaWithIcon");
  });
});
