import { useEffect, useState } from "react";
import Button from "../../common/Button";
import { getCalloutCtaWithIcon } from "./backend";

function CallOutCtaWithIcon(props: any) {
  const [callOutIcon, setCallOutIcon] = useState<any>(null);
  const [isHydrated, setIsHydrated] = useState(false);

  useEffect(() => {
    async function fetchData() {
      // You can await here
      const response = await getCalloutCtaWithIcon(props.id);
      setCallOutIcon(response.componentcalloutCtaWithIcon);
      setIsHydrated(true);
    }
    fetchData();
    // setIsHydrated(true);
  }, [props.id]);

  if (isHydrated) {
    return (
      <div className="div-body-congratulations">
        <div className="div-icon d-flex mt-9 justify-center">
          <img src={callOutIcon.icon?.url} alt="Icon" />
        </div>
        <h1 className="div-paragraph mt-2 text-center text-secondary2">
          {callOutIcon.title}
        </h1>

        <p className="text-description mb-8 mt-6 mb-10 text-center text-16 text-greyHeavy">
          {callOutIcon.description}
        </p>

        <div className="div-button-congratulations mt-10 mb-16">
          <Button
            href={callOutIcon.ctaUrl}
            target="_self"
            title={callOutIcon.ctaText}
            buttonStyle="button-primary-expanded"
          />
        </div>
      </div>
    );
  } else {
    return <></>;
  }
}

export default CallOutCtaWithIcon;
//<div className="div-reservations-unit">WORK IN PROGRESS</div>
