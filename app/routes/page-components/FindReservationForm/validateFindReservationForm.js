import { validateEmail } from '../../../utils/regex';

const validate = ({ emailAddress }) => {

  const emailAddressValidation = validateEmail(emailAddress);

  const frontErrors = {
    emailAddress: !emailAddressValidation
  }

  return ({
    frontErrors,
    isValid: emailAddressValidation
  });
}

export default validate;