import fetchContentful from "../../../utils/fetchContentful";

export async function getNoReservationText(){
  const query = `
  query {
    # add your query
    componentrichTextFieldCollection(where : {contentTitle : "no-reservation-found"}){
      items{
        contentTitle,
        richText{
          json
        }      
      }
    }
  }
  `;
  const data = await fetchContentful(query);
  if (data.componentrichTextFieldCollection?.items[0]?.richText?.json?.content[0]?.content[0]?.value) {
    return (data.componentrichTextFieldCollection?.items[0].richText?.json?.content[0]?.content[0].value)
  }
  return '';
}