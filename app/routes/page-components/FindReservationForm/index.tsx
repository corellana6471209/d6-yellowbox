import React, { useState, useEffect } from "react";
import Spinner from "../../common/Spinner";

import {
  Form,
  useActionData,
  useTransition
} from "@remix-run/react";

import Title from "~/routes/common/Title";
import Input from "~/routes/common/Input";
import ReservationCard from "./UnitReservationCard";
import MessageModal from "../../common/MessageModal";
import { getRTF } from "~/utils/getRtf";

const FindReservationForm = () => {
  const [reservation, setReservation] = useState<any>({ reservations: [], showReservations: false });
  const [showNotFoundModal, setShowNotFoundModal] = useState<boolean>(false);
  const [showNotLoggedModal, setShowNotLoggedModal] = useState<boolean>(false);
  const [showReservationFoundModal, setShowReservationFoundModal] = useState<boolean>(false);
  const [showOneFoundModal, setShowOneFoundModal] = useState<boolean>(false);
  const [noFoundMessage, setNoFoundMessage] = useState<string>('');
  const [foundMessage, setFoundMessage] = useState<string>('');
  const [descriptionMessage, setDescriptionMessage] = useState<string>('');

  const actionData = useActionData();
  const transition = useTransition();

  const isSubmiting = transition.state === "submitting";

  useEffect(() => {
    if (actionData?.reservations && actionData?.reservations.length > 1) {
      setShowReservationFoundModal(true);
      setReservation({ reservations: actionData.reservations, showReservations: true })
    }
    else if (actionData?.reservations && actionData?.reservations.length === 1) {
      setShowOneFoundModal(true);
      const reservation = actionData?.reservations[0];
      window.location.href = `/locations/make-payment/${reservation.locationId}/${reservation.waitingId}`;
    }

    if (actionData?.errorModals) {
      setShowNotFoundModal(actionData.errorModals.showNotFoundModal);
      setShowNotLoggedModal(actionData.errorModals.showNotLoggedModal);
    }
  }, [actionData])

  useEffect(() => {
    fetchData();
  }, []);

  const fetchData = async () => {
    const notFoundMessage = await getRTF("no-reservation-found");
    const foundMessage = await getRTF("reservation-found");
    const descriptionMessage = await getRTF("looking-for-reservation");
    setNoFoundMessage(notFoundMessage);
    setFoundMessage(foundMessage);
    setDescriptionMessage(descriptionMessage);
  }

  const redirectToLogin = () => window.location.href = '/login?redirectTo=reservation';

  return (
    <>
      {isSubmiting && <Spinner />}

      <MessageModal
        open={showNotFoundModal}
        callback={() => setShowNotFoundModal(false)}
        message={noFoundMessage}
        title=" "
        ctaText="Okay"
      />

      <MessageModal
        open={showNotLoggedModal}
        callback={redirectToLogin}
        message="We see you have an existing Simply SS account. Please log in to complete your rental."
        title=" "
        ctaText="Go to Login"
      />

      <MessageModal
        open={showReservationFoundModal}
        callback={() => setShowReservationFoundModal(false)}
        message={foundMessage}
        title=" "
        ctaText="Close"
      />

      <MessageModal
        open={showOneFoundModal}
        callback={() => setShowOneFoundModal(false)}
        message="We found one reservation with this email address. You will be redirected to the Rental Form"
        title=" "
        ctaText="Close"
      />

      {reservation.showReservations ?
        <div className="FindReservationForm-reservations">
          <div className="FindReservationForm-title">
            <Title text='Your Reservations' />
          </div>
          <div className="FindReservationForm-reservation-list">
            {reservation.reservations.map((data: any) => <ReservationCard key={data.id} data={data} />)}
          </div>
        </div>
        :
        <Form
          className="FindReservationForm-container"
          method="post"
          action="/reservation?index"
        >
          <div className="FindReservationForm-inputs-container">
            <span>{descriptionMessage}</span>
            <div>
              <Input
                type="text"
                name="email"
                placeholder="Email Address"
                ariaLabel="email-input"
              />
              {actionData?.frontErrors?.emailAddress && (
                <p className="message-error">
                  This field must be example@example.com
                </p>
              )}
            </div>
          </div>
          <div className="FindReservationForm-button-container">
            <button
              type="submit"
              className="FindReservationForm-button yellow-button"
              aria-label="submit-findReservationForm"
              disabled={isSubmiting}
            >
              Find Reservation
            </button>
          </div>
        </Form>
      }
    </>
  );
};

export default FindReservationForm;
