import { useMediaQuery } from "react-responsive";

//Custom Components
import { formatDateLong } from "../../../../utils/formatDate";

const UnitReservationCard = (props: any) => {
  const isMobile = useMediaQuery({ query: "(max-width: 991px)" });
  const { createdDateTime, locationId, waitingId, location, unit, image } = props.data;
  // Location
  const { address } = location;
  const { city, state } = address;

  // Unit
  const { length, width, unitTypeName} = unit;

  const onClick = () => window.location.href = `/locations/make-payment/${locationId}/${waitingId}`;

  const renderImage = () => {
    if (image?.image?.unitTypeImage?.url) return image.image.unitTypeImage.url;
    return "https://via.placeholder.com/186";
  }

  return (
    <div className="unit-card__wrapper">
      <span className="UnitReservationCard-date-title">Reservation Date: <span className="UnitReservationCard-date">{formatDateLong(createdDateTime)}</span></span>
      <div className="UnitReservationCard-root">
        <div className="UnitReservationCard-unit">
          <img
            src={renderImage()}
            height={isMobile ? 102 : 175}
            width={isMobile ? 128 : 186}
          />
          <div className="UnitReservationCard-information">
            <span className="UnitReservationCard-location">{`${city}, ${state.stateAbbreviation}`}</span>
            <span className="UnitReservationCard-dimensions">{width}’ X {length}’</span>
            <span className="UnitReservationCard-unit-type">{unitTypeName}</span>
          </div>
        </div>
        {!isMobile && <button className="UnitReservationCard-button" onClick={onClick}>View Reservation</button>}
      </div>
      {isMobile && <button className="UnitReservationCard-button" onClick={onClick}>View Reservation</button>}
    </div>
  );
};

export default UnitReservationCard;
