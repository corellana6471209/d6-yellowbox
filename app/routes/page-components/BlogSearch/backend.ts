import fetchContentful from "~/utils/fetchContentful";
import fetchCustomApi from "~/utils/fetchCustomApi";

/**
 * 
 * @param {string} componentBlogSearchId 
 * @returns https://simplyselfstorage.atlassian.net/browse/YBP-937
 * @testing componentblockQuoteId: "01osSSHPQWMzk5GEWcCBBN"
 */
export const loadBlogSearch = async (componentBlogSearchId: string) => {
  const query = `
query componentBlogSearchCollection {
  componentblogSearch(id: "${componentBlogSearchId}") {
    title
    textOfTheInput
    textOfTheButton
  }
}`;
  var componentBlogSearchData = await fetchCustomApi(`/api/content/common/contentful/${query}`);
  return componentBlogSearchData;
};



export const loadBlogArticles = async () => {
  const query = `
  query {
    pageCollection(where: {type: "Blog"}) {
      items {
        type
        visibility
       
        title
        slug
        seoTitle
        categories
        tags
        seoDescription
        seoKeywords
        pageImage {
          title
          description
          contentType
          fileName
          size
          url
          width
          height
        }
        sys {
          id
          publishedAt
        }
      }
    }
  }
  `;

  const data = await fetchContentful(query);

  if (data.pageCollection && data.pageCollection != null) {
    const blogs = data.pageCollection?.items;
    return blogs;
  } else {
    return null;
  }
};

/**
 *
 * @param {string} componentBlogSearch
 * @returns https://simplyselfstorage.atlassian.net/browse/YBP-928
 */
export const componentBlogSearch = async (searchParam: string) => {
  const query = `
  query{
    pageCollection(
      where: {
      AND: [
          {
              OR: [
                  { title_contains: ${searchParam} },
                  { seoDescription_contains: ${searchParam} },
                  { seoTitle_contains: ${searchParam} },
                  { seoKeywords_contains: ${searchParam} },
              ],
          },
          { visibility: "Public" },
          {type: "Blog"},
      ] 
      }
    ){
      items {
        type
        title
        slug
        seoTitle
        categories
        tags
        seoDescription
        seoKeywords
        pageImage {
          title
          description
          contentType
          fileName
          size
          url
          width
          height
        }
        sys {
          id
          publishedAt
        }
      }
    }
  }
  `;
  return await fetchContentful(query);
};
