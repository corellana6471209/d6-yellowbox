import React from "react";
import { useEffect, useState } from "react";
import queryString from "query-string";

import Spinner from "../../common/Spinner";

import { loadBlogArticles, loadBlogSearch } from "./backend";
import BlogPreview from "../../common/BlogPreview";

//const pageCount = Math.ceil(listItems.length / paginationLimit);
export default function BlogHero(props: any) {
  const { pageDetails, id } = props;
  const { categories } = pageDetails;
  const [isHydrated, setIsHydrated] = useState(false);
  const [blogHeroInformation, setBlogHeroInformation] = useState<any>(null);
  const [blogHeroPosts, setBlogHeroPosts] = useState<any>(null);
  const [blogHeroPostsToFilter, setBlogHeroPostsToFilter] = useState<any>(null);
  const [categorySelected, setCategorySelected] = useState("ALL");
  const [value, setValue] = useState("");
  const { category, tag } = queryString.parse(location.search);

  //Variables to paginate
  const [pageCount, setPageCount] = useState(1);
  const [pageSelected, setPageSelected] = useState(1);

  useEffect(() => {
    async function fetchData() {
      const componentResponse = await loadBlogArticles();
      const componentData = await loadBlogSearch(id);

      if (componentResponse && componentResponse.length > 0) {
        const sortedPosts = componentResponse
          .filter((item: any) => item !== null)
          .sort((first: any, second: any) => {
            let a = new Date(second.sys.publishedAt).getTime();
            let b = new Date(first.sys.publishedAt).getTime();

            return a - b;
          });

        if (category) {
          // Conditional if Exists one parameter on the url
          const blogsFiltered = [];
          for (let i = 0; i < sortedPosts.length; i++) {
            if (sortedPosts[i].categories) {
              const match = sortedPosts[i].categories
                .filter((item: any) => item !== null)
                .find(
                  (item: any) => item.toLowerCase() === category.toLowerCase()
                );

              if (match !== undefined) {
                blogsFiltered.push(sortedPosts[i]);
              }
            }
          }
          setCategorySelected(category || "ALL");
          setBlogHeroPostsToFilter(blogsFiltered);
          setPageCount(Math.ceil(blogsFiltered.length / 8));
        } else if (tag) {
          // Conditional if Exists one parameter on the url
          const blogsFiltered = [];
          for (let i = 0; i < sortedPosts.length; i++) {
            if (sortedPosts[i].tags) {
              const match = sortedPosts[i].tags
                .filter((item: any) => item !== null)
                .find((item: any) => item.toLowerCase() === tag.toLowerCase());

              if (match !== undefined) {
                blogsFiltered.push(sortedPosts[i]);
              }
            }
          }
          setCategorySelected("");
          setBlogHeroPostsToFilter(blogsFiltered);
          setPageCount(Math.ceil(blogsFiltered.length / 8));
        } else {
          setPageCount(Math.ceil(sortedPosts.length / 8));
          setBlogHeroPostsToFilter([...sortedPosts]);
        }

        setBlogHeroPosts(sortedPosts);
        setBlogHeroInformation(componentData.componentblogSearch);
      }

      setIsHydrated(true);
    }
    fetchData();
  }, []);

  //Adding the locks dinamically
  const pagination = (number: any, pageSelected: number) => {
    const options = [];

    for (let i = 1; i <= number; i++) {
      options.push(
        <span
          onClick={() => setPageSelected(i)}
          className={`mr-3 cursor-pointer text-16 text-tertiary1 md:text-24 ${
            i == pageSelected && "font-bold underline"
          }`}
          key={i}
        >
          {i}
        </span>
      );
    }

    return options;
  };

  /******************FILTER BY FORM*************************** */

  const handleChange = (evt: any) => {
    const value = evt.target.value;
    setValue(value);
  };

  const filterByCategories = (value: any, blog: any) => {
    if (blog.categories) {
      const match = blog.categories
        .filter((item: any) => item !== null)
        .find((item: any) => item.toLowerCase() === value);

      if (match !== undefined) {
        return true;
      } else {
        return false;
      }
    }

    return false;
  };

  const filterByTags = (value: any, blog: any) => {
    if (blog.tags) {
      const match = blog.tags
        .filter((item: any) => item !== null)
        .find((item: any) => item.toLowerCase() === value);

      if (match !== undefined) {
        return true;
      } else {
        return false;
      }
    }

    return false;
  };

  const sendFilters = (event: any, blogs: any) => {
    event.preventDefault();
    if (value) {
      const blogsFiltered = blogs.filter((blog: any) => {
        if (
          blog.title?.toLowerCase().includes(value.toLowerCase()) ||
          blog.seoDescription?.toLowerCase().includes(value.toLowerCase()) ||
          filterByCategories(value.toLowerCase(), blog) ||
          filterByTags(value.toLowerCase(), blog)
        ) {
          return true;
        }
        return false;
      });

      setCategorySelected("");
      //set 1 the page selected
      setPageSelected(1);
      //set new page count
      setPageCount(Math.ceil(blogsFiltered.length / 8));
      setBlogHeroPostsToFilter(blogsFiltered);
    } else {
      setValue("");
      selectCategory("ALL", blogHeroPosts);
    }
  };

  /******************FILTER BY FORM*************************** */

  const selectCategory = (category: string, blogs: any) => {
    setValue("");
    if (category === "ALL") {
      setCategorySelected(category);
      //set 1 the page selected
      setPageSelected(1);
      //set new page count
      setPageCount(Math.ceil(blogHeroPosts.length / 8));

      setBlogHeroPostsToFilter(blogHeroPosts);
    } else {
      setCategorySelected(category);
      const blogsFiltered = [];

      for (let i = 0; i < blogs.length; i++) {
        if (blogs[i].categories) {
          const match = blogs[i].categories.find(
            (item: any) => item === category
          );

          if (match !== undefined) {
            blogsFiltered.push(blogs[i]);
          }
        }
      }
      //set 1 the page selected
      setPageSelected(1);
      //set new page count
      setPageCount(Math.ceil(blogsFiltered.length / 8));

      setBlogHeroPostsToFilter(blogsFiltered);
    }
  };

  if (isHydrated) {
    if (blogHeroPosts && blogHeroInformation) {
      return (
        <section className="blog-hero-section">
          {/*******************************************SEARCHER******************************************** */}

          <div className="pl-5 pr-5 lg:pl-0 lg:pr-0">
            <h1 className="mt-7 mb-7 text-center font-serif text-28 text-greyHeavy md:text-36">
              {blogHeroInformation.title}
            </h1>

            <div className="div-searcher-tags max-w-[1200px] mx-auto">
              <div
                aria-label="form-search"
                className="form-search d-flex justify-center"
              >
                <form>
                  <div className="form-group">
                    <input
                      className="form-search__input"
                      name="search"
                      placeholder={
                        blogHeroInformation.textOfTheInput || "Search for Blog"
                      }
                      onChange={handleChange}
                      value={value}
                      autoComplete="off"
                    />
                  </div>

                  <button
                    className="btn-tertiary1 mr-1 text-white green-button"
                    onClick={(e) => sendFilters(e, blogHeroPosts)}
                  >
                    {blogHeroInformation.textOfTheButton || "Search"}
                  </button>
                </form>

                {/**  <p className="message-error text-left">{messageError}</p> */}
              </div>

              {/*******************************************SEARCHER******************************************** */}

              {/*******************************************TAGS ENTRIES******************************************** */}

              <section className="d-flex justify-center">
                <div className="tags-categories d-flex mt-8">
                  <span
                    onClick={() => selectCategory("ALL", blogHeroPosts)}
                    className={`mr-1 cursor-pointer px-4 py-2 text-16 font-bold text-white md:text-24 ${
                      categorySelected === "ALL"
                        ? "bg-tertiary1"
                        : "bg-tertiary2"
                    }`}
                  >
                    All
                  </span>
                  {categories.map((category: any, index: number) => (
                    <span
                      onClick={() => selectCategory(category, blogHeroPosts)}
                      className={`mr-1 cursor-pointer px-4 py-2 text-16 font-bold text-white md:text-24 ${
                        categorySelected === category
                          ? "bg-tertiary1"
                          : "bg-tertiary2"
                      }`}
                      key={index}
                    >
                      {category}
                    </span>
                  ))}
                </div>
              </section>

              {/*******************************************TAGS ENTRIES******************************************** */}

              {/*******************************************BLOGS ENTRIES******************************************** */}

              <section className="section-blog-entries">
                {blogHeroPostsToFilter && blogHeroPostsToFilter.length > 0 ? (
                  <>
                    {" "}
                    <div className="mt-14 grid grid-cols-1 gap-4 md:grid-cols-2 md:gap-10 lg:grid-cols-3">
                      {blogHeroPostsToFilter
                        .slice((pageSelected - 1) * 8, pageSelected * 8)
                        .map((blog: any, index: number) => (
                          <BlogPreview
                            blogPage={true}
                            maxlines={3}
                            key={index}
                            module={{
                              ...blog,
                              image: blog.pageImage,
                              summary: blog.seoDescription,
                              date: blog?.sys?.publishedAt,
                            }}
                          />
                        ))}
                    </div>
                    <div className="d-flex justify-center">
                      {pagination(pageCount, pageSelected)}
                    </div>
                  </>
                ) : (
                  <h1 className="mt-10 text-center">No blogs content</h1>
                )}
              </section>

              {/*******************************************BLOGS ENTRIES******************************************** */}
            </div>
          </div>
        </section>
      );
    } else {
      return <div className="min-h-[500px]"></div>;
    }
  } else {
    return <Spinner />;
  }
}
