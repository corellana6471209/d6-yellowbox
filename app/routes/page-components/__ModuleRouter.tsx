import RichText from "./RichText";
import BlockQuote from "./BlockQuote";
import MarkDown from "./MarkDown";
import BlogHero from "./BlogHero";
import BlogSearch from "./BlogSearch";
import BlogHeroImage from "./BlogHeroImage";
import Tagging from "./Tagging";

import HeroWithSearch from "./HeroWithSearch";
import LocationsMapListing from "./LocationMapList/index";
import StateList from "./StateList";
import BlogTeaser from "./BlogTeaser";
import LocationList from "./LocationList";
import InteractiveMap from "./InteractiveMap";
import CallOutCtaWithIcon from "./CallOutCtaWithIcon";
import CallOutCta from "./CallOutCta";
import FAQ from "./FAQ";
import SizeGuide from "./SizeGuide";
import OneTrustButton from "./OneTrustButton";
import OneTrustIFrame from "./OneTrustIFrame";
import VideoEmbed from "./VideoEmbed";

export type ContextType = {
  componentId: string;
};
interface AppProps {
  components: any;
  pageDetails: any;
  slug: string;
  token: string;
}
interface ComponentShape {
  __typename: string;
  sys: {
    id: string;
  };
}

function Component(props: AppProps) {
  const { components, pageDetails, slug, token } = props;

  if (slug === "success-rental" && token) {
    window.location.href = "/my-account";
  }

  if (!components) {
    return <div key="components-router-loading"></div>;
  }

  //console.log("components", components);
  //console.log("pageDetails", pageDetails);
  //console.log("slug", slug);

  return components.map((item: ComponentShape, key: number) => {
    switch (item?.__typename) {
      case "ComponentrichTextField":
        return <RichText key={item.sys.id} id={item.sys.id} />;
      case "ComponentblogHero":
        return (
          <BlogHero
            key={item.sys.id}
            id={item.sys.id}
            pageDetails={pageDetails}
          />
        );

      case "ComponentblogSearch":
        return (
          <BlogSearch
            key={item.sys.id}
            id={item.sys.id}
            pageDetails={pageDetails}
          />
        );

      case "HeroBlogImage":
        return (
          <BlogHeroImage
            key={item.sys.id}
            id={item.sys.id}
            pageDetails={pageDetails}
          />
        );

      case "ComponentcallOutBlog":
        return <BlockQuote key={item.sys.id} id={item.sys.id} />;

      case "Componenttagging":
        return (
          <Tagging
            key={item.sys.id}
            id={item.sys.id}
            pageDetails={pageDetails}
          />
        );

      case "ComponentmarkDownEditor":
        return <MarkDown key={item.sys.id} id={item.sys.id} slug={pageDetails.slug}/>;
      case "ComponentinteractiveMap":
        return <InteractiveMap key={item.sys.id} id={item.sys.id} />;
      case "ComponentcalloutCtaWithIcon":
        return <CallOutCtaWithIcon key={item.sys.id} id={item.sys.id} />;
      case "ComponentcalloutCta":
        return <CallOutCta key={item.sys.id} id={item.sys.id} />;
      case "ComponentlocationList":
        return <LocationList key={item.sys.id} id={item.sys.id} />;
      case "ComponentheroWithSearch":
        return <HeroWithSearch key={item.sys.id} id={item.sys.id} />;
      case "LocationsMapListing":
        return (
          <LocationsMapListing
            type="search"
            key={item.sys.id}
            id={item.sys.id}
            param="null"
          />
        );
      case "ComponentstateList":
        return <StateList key={item.sys.id} id={item.sys.id} />;
      case "ComponentblogTeaser":
        return <BlogTeaser key={item.sys.id} id={item.sys.id} />;
      case "Componentfaq":
        return <FAQ key={item.sys.id} id={item.sys.id} />;
      case "ComponentsizeGuide":
        return <SizeGuide key={item.sys.id} id={item.sys.id} />;
      case "ComponentoneTrustButton":
        return <OneTrustButton key={item.sys.id} id={item.sys.id} />;
      case "ComponentoneTrustIFrame":
        return <OneTrustIFrame key={item.sys.id} id={item.sys.id} />;
      case "ComponentvideoEmbed":
        return <VideoEmbed key={item.sys.id} id={item.sys.id} />;
      default:
        return <p key={key}></p>;
    }
  });
}

export default Component;
