import StarRatings from "react-star-ratings";
import Button from "../../../common/Button";
import type { ListItems } from "../index";
import useWindowDimensions from "../../../../utils/hooks/useWindowDimensions";
import useRating from "../../../../utils/hooks/useRating";

/**
 * List Item for locations listing
 * @param {object} props content to be displayed
 * @returns {string} returns component
 */
function LocationListItem(props: ListItems) {
  const {
    price,
    address,
    locationName,
    slug,
    buttonLabel,
    distance,
    salesFlag,
    locationImageCollection,
    googleBusinessProfileId,
    index,
  } = props;

  const addressString = `${address?.address1} ${address?.city}, ${
    address?.state ? address?.state?.stateAbbreviation : ""
  }${address?.postalCode ? ` ${address?.postalCode}` : ""}`;

  // go get rating
  const rating = useRating(googleBusinessProfileId);
  return (
    <div>
      {useWindowDimensions().width > 1024 && (
        <div className="@screens-md:invisible @screens-lg:invisible @screens-xl:invisible">
          <div className="mb-[15px] flex border-b-4 border-secondary2 pt-[15px] pb-[15px]">
            <div style={{ width: "180px" }}>
              {" "}
              {locationImageCollection.items[0] && (
                <img
                  src={locationImageCollection.items[0].url}
                  alt={locationImageCollection.items[0].title}
                  className="h-[118px] w-[144px]"
                />
              )}
            </div>
            <div className="w-1/3">
              <h5 className="font-serif text-20 text-secondary2">
                {locationName}
              </h5>
              <p className="bottom-0 py-1 text-16 font-bold text-callout">
                {distance ? `${distance} mi` : ""}
              </p>
              <p className="bottom-0 text-16 font-normal text-greyHeavy">
                {addressString}
              </p>
            </div>
            <div className="w-1/2">
              {typeof rating === "number" ? (
                <div className="star-rating-wrap flex justify-end">
                  <StarRatings
                    rating={rating}
                    starRatedColor="#5F7BC6"
                    starEmptyColor="#EAEBEF"
                    numberOfStars={5}
                    name="rating"
                    starDimension="25px"
                    starSpacing="0px"
                  />
                </div>
              ) : (
                ""
              )}

              <div className="flex" style={{ float: "right" }}>
                <div
                  className="text-right text-16 font-bold"
                  style={{ textAlign: "right", marginRight: "28px" }}
                >
                  <p>
                    Starting <br />
                    From
                  </p>
                </div>
                <div
                  className="w-1/2 text-right text-36 font-bold text-callout"
                  style={{ textAlign: "right" }}
                >
                  <span>${price}</span>
                  <sup className="font-extralight">*</sup>
                </div>
              </div>

              <div className="float-right mt-[10px] w-auto">
                {slug && (
                  <Button
                    href={`/locations/${slug}`}
                    target="_self"
                    title={buttonLabel ? buttonLabel : "Explore Units"}
                    buttonStyle="button-primary"
                  />
                )}
              </div>
            </div>
          </div>
        </div>
      )}

      {useWindowDimensions().width < 1024 && (
        <div className="@screens-sm:invisible @screens-xs:invisible mb-[15px] border-b-4 border-secondary2 pt-[15px] pb-[15px]">
          <div style={{ marginLeft: "10px" }}>
            <div className="flex" style={{ gap: "20px" }}>
              <div>
                <div>
                  <img
                    src={locationImageCollection.items[0].url}
                    alt={locationImageCollection.items[0].title}
                    className="h-[78px] w-[124px]"
                  />
                </div>

                <div style={{ marginTop: "10px" }}>
                  {typeof rating === "number" ? (
                    <div className="star-rating-wrap flex justify-end">
                      <StarRatings
                        rating={rating}
                        starRatedColor="#5F7BC6"
                        starEmptyColor="#EAEBEF"
                        numberOfStars={5}
                        name="rating"
                        starDimension="25px"
                        starSpacing="0px"
                      />
                    </div>
                  ) : (
                    ""
                  )}
                </div>
              </div>

              <div className="w-1/3">
                <h5
                  className="font-serif text-20 text-secondary2"
                  style={{ lineHeight: "15px" }}
                >
                  {locationName}
                </h5>
                <p className="bottom-0 py-1 text-16 font-bold text-callout">
                  {distance ? `${distance} mi` : ""}
                </p>
                <p className="bottom-0 text-16 font-normal text-greyHeavy">
                  {addressString}
                </p>
              </div>
            </div>

            <div style={{ display: "flex", marginTop: "15px" }}>
              <div className="flex" style={{ float: "left" }}>
                <div
                  className="text-right text-16 font-bold"
                  style={{ textAlign: "right", marginRight: "28px" }}
                >
                  <p>
                    Starting <br />
                    From
                  </p>
                </div>
                <div
                  className="w-1/2 text-right text-36 font-bold text-callout"
                  style={{ textAlign: "right" }}
                >
                  <span>${price}</span>
                  <sup className="font-extralight">*</sup>
                </div>
              </div>

              <div className="float-right mt-[10px] ml-[10px] w-auto">
                {slug && (
                  <Button
                    href={`/locations/${slug}`}
                    target="_self"
                    title={buttonLabel ? buttonLabel : "Explore Units"}
                    buttonStyle="button-primary"
                  />
                )}
              </div>
            </div>
          </div>
        </div>
      )}

      {/* End of code */}
      {/* <div
        className="LocationMapList-list-item m-4 mb-6 border-b-4 border-secondary2 pb-3 lg:m-0 lg:mb-6"
        id={`location-item-${index}`}
      > */}

      {/* {salesFlag && (
        <div className="lli-promo-banner mb-2 flex items-center justify-between bg-secondary1 py-1 px-2">
          <div className="star">
            <MdStar />
          </div>
          <p className="text-center font-bold text-white">
            Savings Available at this Location
          </p>
          <div className="star">
            <MdStar />
          </div>
        </div>
      )} */}

      {/* <div className="grid grid-cols-3 gap-4">
          <div className="col-span-1">
            {locationImageCollection.items[0] && (
              <img
                src={locationImageCollection.items[0].url}
                alt={locationImageCollection.items[0].title}
                className="w-full object-cover lg:h-full"
              />
            )}
            {useWindowDimensions().width < 1024 && typeof rating === "number" ? (
              <div className="star-rating-wrap flex justify-end">
                <StarRatings
                  rating={rating}
                  starRatedColor="#5F7BC6"
                  starEmptyColor="#EAEBEF"
                  numberOfStars={5}
                  name="rating"
                  starDimension="18px"
                  starSpacing="0px"
                />
              </div>
            ) : ''}
          </div>
          <div className="col-span-2">
            <div className="grid grid-cols-2 gap-4">
              <div className="col-span-2 lg:col-span-1">
                <h5 className="font-serif text-20 text-secondary2">
                  {locationName}
                </h5>
                <p className="py-1 text-16 font-bold text-callout">
                  {distance ? `${distance} mi` : ""}
                </p>
                <p className="text-16 font-normal text-greyHeavy">
                  {addressString}
                </p>
              </div>
              {useWindowDimensions().width >= 1024 && (
                <div className="col-span-1 flex flex-col justify-between pr-2">
                  {typeof rating === "number" ? (
                    <div className="star-rating-wrap flex justify-end">
                      <StarRatings
                        rating={rating}
                        starRatedColor="#5F7BC6"
                        starEmptyColor="#EAEBEF"
                        numberOfStars={5}
                        name="rating"
                        starDimension="25px"
                        starSpacing="0px"
                      />
                    </div>
                  ) : ''}

                  <div className="grid grid-cols-2 gap-2 pb-1">
                    {price && (
                      <>
                        <div className="col-span-1 pt-2 text-right text-16 font-bold">
                          <p>
                            Starting <br />
                            From
                          </p>
                        </div>
                        <div className="col-span-1 text-right text-24 font-bold text-callout xl:text-36">
                          <span>${price}</span>
                          <sup className="font-extralight">*</sup>
                        </div>
                      </>
                    )}
                  </div>
                  <div className="grid">
                    {slug && (
                      <Button
                        href={`/locations/${slug}`}
                        target="_self"
                        title={buttonLabel ? buttonLabel : "Explore Units"}
                        buttonStyle="button-primary-expanded"
                      />
                    )}
                  </div>
                </div>
              )}
            </div>
          </div>
        </div> */}
      {/* {useWindowDimensions().width < 1024 && (
          <div className="lg:mt-4 grid grid-cols-4 gap-2 p-4 pb-1">
            {price && (
              <>
                <div className="col-span-1 pt-2 text-right text-16 font-bold">
                  <p>
                    Starting <br />
                    From
                  </p>
                </div>
                <div className="col-span-1 text-right text-24 font-bold text-callout xl:text-36">
                  <span>${price}</span>
                  <sup className="font-extralight">*</sup>
                </div>
              </>
            )}
            <div className="col-span-2 text-right font-bold text-callout">
              {slug && (
                <Button
                  href={`/locations/${slug}`}
                  target="_self"
                  title={buttonLabel ? buttonLabel : "Explore Units"}
                  buttonStyle="button-primary-expanded"
                />
              )}
            </div>
          </div>
        )} */}
    </div>
    // </div >
  );
}

export default LocationListItem;
