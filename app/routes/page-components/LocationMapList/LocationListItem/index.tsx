import StarRatings from "react-star-ratings";
// import { MdStar } from "react-icons/md";
import Button from "../../../common/Button";
import type { ListItems } from "../index";
import useWindowDimensions from "~/utils/hooks/useWindowDimensions";
import useRating from "~/utils/hooks/useRating";

/**
 * List Item for locations listing
 * @param {object} props content to be displayed
 * @returns {string} returns component
 */
function LocationListItem(props: ListItems) {
  const {
    price,
    address,
    locationName,
    buttonLink,
    distance,
    salesFlag,
    locationImage,
    googleBusinessProfileId,
    index,
  } = props;
  const addressString = `${address?.address1} ${address?.city}, ${address?.state.stateAbbreviation}`;

  // go get rating
  const rating = useRating(googleBusinessProfileId);

  return (
    <div
      className="LocationMapList-list-item m-4 mb-6 border-b-4 border-secondary2 pb-3 lg:m-0 lg:mb-6"
      id={`location-item-${index}`}
    >
      {salesFlag && (
        <div className="lli-promo-banner mb-2 flex items-center justify-between bg-secondary1 py-1 px-2">
          <div className="star">{/* <MdStar /> */}</div>
          <p className="text-center font-bold text-white">
            Savings Available at this Location
          </p>
          <div className="star">{/* <MdStar /> */}</div>
        </div>
      )}
      <div className="grid grid-cols-3 gap-[20px]">
        <div className="col-span-1">
          <img
            src={locationImage.url}
            alt={locationImage.title}
            className="m-[20px] w-full object-cover lg:h-full"
          />
          {useWindowDimensions().width < 1024 && typeof rating === "number" ? (
            <>
              {rating ? (
                <div className="star-rating-wrap flex justify-end">
                  <StarRatings
                    rating={rating}
                    starRatedColor="#5F7BC6"
                    starEmptyColor="#EAEBEF"
                    numberOfStars={5}
                    name="rating"
                    starDimension="21px"
                    starSpacing="0px"
                  />
                </div>
              ) : (
                ""
              )}
            </>
          ) : (
            ""
          )}
        </div>
        <div className="col-span-2">
          <div className="grid grid-cols-2 gap-4">
            <div className="col-span-2 lg:col-span-1">
              <h5 className="mb-[5px] font-serif text-20 text-secondary2">
                {locationName}
              </h5>
              <p className="mt-[15px] mb-[15px] py-1 text-16 font-bold text-callout">
                {distance ? `${distance} mi` : ""}
              </p>
              <p className="mb-[20px] text-16 font-normal text-greyHeavy">
                {addressString}
              </p>
            </div>
            {useWindowDimensions().width >= 1024 &&
            typeof rating === "number" ? (
              <div className="col-span-1 flex flex-col justify-between">
                {rating ? (
                  <div className="star-rating-wrap flex justify-end">
                    <StarRatings
                      rating={rating}
                      starRatedColor="#5F7BC6"
                      starEmptyColor="#EAEBEF"
                      numberOfStars={5}
                      name="rating"
                      starDimension="25px"
                      starSpacing="0px"
                    />
                  </div>
                ) : (
                  ""
                )}

                <div className="grid grid-cols-2 gap-2 pb-1  mb-[10px]">
                  <div className="col-span-1 pt-2 text-right text-16 font-bold starting-from">
                    <p>
                      Starting <br />
                      From
                    </p>
                  </div>
                  <div className="col-span-1 ml-[20px] text-right text-36 font-bold text-callout">
                    {price}
                    <sup className="font-extralight">*</sup>
                  </div>
                </div>
                <div className="grid">
                  <Button
                    href={buttonLink}
                    target="_self"
                    title="Explore Units"
                    buttonStyle="button-primary-expanded"
                  />
                </div>
              </div>
            ) : (
              ""
            )}
          </div>
        </div>
      </div>
      {useWindowDimensions().width < 1024 && (
        <div className="mt-4 grid grid-cols-4 gap-2 p-4 pb-1">
          <div className="col-span-1 pt-2 text-right text-16 font-bold">
            <p>
              Starting <br />
              From
            </p>
          </div>
          <div className="col-span-1 text-right text-36 font-bold text-callout">
            {price}
            <sup className="font-extralight">*</sup>
          </div>
          <div className="col-span-2 text-right font-bold text-callout">
            <Button
              href={buttonLink}
              target="_self"
              title="Explore Units"
              buttonStyle="button-primary-expanded"
            />
          </div>
        </div>
      )}
    </div>
  );
}

export default LocationListItem;
