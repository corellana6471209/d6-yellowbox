import { render, screen, waitFor } from "@testing-library/react";
import { rest } from "msw";
import { setupServer } from "msw/node";
import "@testing-library/jest-dom";
import LocationListItem from "./__index";

const args = {
  price: "$78",
  locationName: "Simply Woodstock",
  buttonLink: "#something",
  distance: 5,
  salesFlag: true,
  address: {
    address1: "1234 Applejack lane",
    address2: "",
    city: "Woodstock",
    coordinates: {
      lat: 1,
      lon: 1,
    },
    postalCode: 12345,
    state: {
      stateFullName: "Georgia",
      stateAbbreviation: "GA",
    },
  },
  locationImage: {
    title: "This is an image alt text",
    url: "https://picsum.photos/30/30",
  },
  googleBusinessProfileId: "ChIJN1t_tDeuEmsRUsoyG83frY4",
  index: 1,
};

const server = setupServer(
  rest.get(
    "https://maps.googleapis.com/maps/api/place/details/json",
    (req, res, ctx) => {
      return res(
        ctx.json({
          result: {
            rating: 4,
          },
        })
      );
    }
  )
);

beforeAll(() => server.listen());
afterEach(() => server.resetHandlers());
afterAll(() => server.close());

describe("LocationListItem component unit test", () => {
  test("verify rendering of the LocationListItem component", async () => {
    render(<LocationListItem {...args} />);
  });

  test("Verify LocationListItem Rating", async () => {
    render(<LocationListItem {...args} />);
    const rating = await waitFor(() => screen.getByTitle(/4 Stars/));
    expect(rating).toBeInTheDocument();
  });

  test("Verify LocationListItem Address", async () => {
    render(<LocationListItem {...args} />);
    const address = await waitFor(() =>
      screen.getByText(/1234 Applejack lane Woodstock, GA/)
    );
    expect(address).toBeInTheDocument();
  });

  test("Verify LocationListItem Location", async () => {
    render(<LocationListItem {...args} />);
    const location = await waitFor(() => screen.getByText(/Simply Woodstock/));
    expect(location).toBeInTheDocument();
  });

  test("Verify LocationListItem Distance", async () => {
    render(<LocationListItem {...args} />);
    const distance = await waitFor(() => screen.getByText(/5 mi/));
    expect(distance).toBeInTheDocument();
  });
});
