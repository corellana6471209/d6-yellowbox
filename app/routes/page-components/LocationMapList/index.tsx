import React, { useEffect, useState, useMemo } from "react";
import queryString from "query-string";
import { GoogleMap, Marker, InfoWindow } from "@react-google-maps/api";
import LocationListItem from "./LocationListItem/__index";
import Filter from "../../common/svg/Filter";
import CloseIcon from "../../common/svg/Close";
import LoaderList from "~/routes/common/LoaderList";
import {
  getLocations,
  getLocationsByState,
  getPrices,
  getUnits,
} from "../../../routes/page-components/LocationMapList/backend";
import { getCoords } from "../../../routes/common/Search/backend";
import FilterModal from "../../common/Modals/Filters";
import Delete from "../../common/svg/Delete";

import getModalFilters from "../../../utils/getFilters";
import useWindowDimensions from "../../../utils/hooks/useWindowDimensions";
import sortLocations from "../../../utils/hooks/useSortLocations";
import determineCategory from "../../../utils/hooks/useCategory";
import calculateDistance from "../../../utils/hooks/useDistance";

import pin from "../../../resources/pin.png";
import pinHover from "../../../resources/pinHover.png";
import mapStyles from "../../../resources/mapStyles.json";
import MapInfoBox from "./MapInfoBox/__index";
//import { useSearchParams } from "react-router-dom";
import { getLabel } from "../../../utils/getLabel";
import getSetting from "../../../utils/getSetting";
//import MessageModal from "~/routes/common/MessageModal";

/**
 * interface Page {
  disclaimerText: string;
  viewLocationButtonLabel: string;
  viewMoreLinkText: string;
  markerBoxButtonLabel: string;
}

 */

let isHydrating = true;

export type ListItems = {
  price: string;
  address: {
    address1: string;
    address2: string | null;
    city: string;
    coordinates: {
      lat: number;
      lon: number;
    };
    postalCode: number;
    state: {
      stateFullName: string;
      stateAbbreviation: string;
    };
  };
  locationName: string;
  slug: string;
  buttonLabel: string;
  distance: number;
  salesFlag: boolean;
  locationImageCollection: {
    items: [
      {
        title: string;
        url: string;
      }
    ];
  };
  googleBusinessProfileId: string;
  index: number;
  locationId: number;
};
interface Props {
  id: string | null;
  type: string;
  param: string;
}

interface Coords {
  lat: number;
  lng: number;
  status: string;
}
/**
 *
 * @param {object} props of page component
 * @returns {string} returns component
 */
export default function LocationMapList(props: Props) {
  let [isHydrated, setIsHydrated] = React.useState(!isHydrating);
  let searchParam: string | any[];

  // component props / state
  const { type, param } = props;
  if (type === "search") {
    const { s } = queryString.parse(location.search);
    searchParam = s ? s : "";
  } else {
    searchParam = param;
  }

  const { width } = useWindowDimensions();
  const [locations, setLocations] = useState<ListItems[]>([]);
  const [labels, setLabels] = useState<any>(null);
  const [currentTab, setTab] = useState("map");
  const [sidebarTop, setSidebarTop] = useState(1);
  const [coords, setCoords] = useState({ status: "ZERO_RESULTS" } as Coords);
  const [infoBox, setMarkerInfoBox] = useState(null);
  const [hover] = useState(false);
  const [marker] = useState(null);
  const [openModal, setOpenModal] = useState(false);
  const [radius, setRadius] = useState<number>(25);
  const defaultItemsToShow: number = width < 1024 ? 4 : 10;
  const [itemsToShow, setItemsToShow] = useState<number>(
    labels !== null ? labels.mapLocationsToShow : defaultItemsToShow
  );

  const [isFetchingLocations, setFetchingStatus] = useState(true);

  /************************FILTERS*********************** */
  const [locationsWithOutFilters, setLocationsWithOutFilters] = useState<
    ListItems[]
  >([]);
  const [filtersChecked, setFiltersChecked] = useState<Array<any>>([
    { filter: "DISTANCE" },
  ]);
  const [filtersContentful, setFiltersContentful] = useState<any>({
    priceLowToHigh: null,
    distance: null,
    parking: null,
    small: null,
    medium: null,
    large: null,
  });

  /************************FILTERS*********************** */
  useEffect(() => {
    async function fetchData() {
      const filters = await getModalFilters("location");
      setFiltersContentful({
        priceLowToHigh: filters.find((e: any) => e.name === "priceLowToHigh"),
        distance: filters.find((e: any) => e.name === "distance"),
        parking: filters.find((e: any) => e.name === "parking"),
        small: filters.find((e: any) => e.name === "small"),
        medium: filters.find((e: any) => e.name === "medium"),
        large: filters.find((e: any) => e.name === "large"),
      });
    }

    fetchData();
  }, []);

  /************************FILTERS*********************** */

  // ================================
  // Get Component Data
  // ================================

  useEffect(() => {
    async function fetchData() {
      const filters = await getModalFilters("location");
      setFiltersContentful({
        priceLowToHigh: filters.find((e: any) => e.name === "priceLowToHigh"),
        distance: filters.find((e: any) => e.name === "distance"),
        parking: filters.find((e: any) => e.name === "parking"),
        small: filters.find((e: any) => e.name === "small"),
        medium: filters.find((e: any) => e.name === "medium"),
        large: filters.find((e: any) => e.name === "large"),
      });
    }

    fetchData();
  }, []);

  const assignFilters = (filters: Array<any>) => {
    setFiltersChecked(filters);

    if (filters && filters.length > 0) {
      const data = sortLocations(filters, locationsWithOutFilters);
      setLocations(data);
    } else {
      setLocations(locationsWithOutFilters);
    }
  };

  // Function to delete the filters
  // Add/Remove checked item from list
  const deleteFilter = (filter: any) => {
    let updatedList: any[] = [...filtersChecked];
    updatedList.splice(
      filtersChecked
        .map(function (e: any) {
          return e.filter;
        })
        .indexOf(filter),
      1
    );

    if (updatedList && updatedList.length > 0) {
      setFiltersChecked(updatedList);
      assignFilters(updatedList);
    } else {
      setFiltersChecked([{ filter: "DISTANCE" }]);
      assignFilters([{ filter: "DISTANCE" }]);
    }
  };
  /************************FILTERS*********************** */

  // ================================
  // Tabs
  // ================================
  function handleTab(tab: string) {
    return (event: React.MouseEvent) => {
      setTab(tab);
      event.preventDefault();
    };
  }

  // ================================
  // Get Coordinates
  // ================================
  async function fetchCoords() {
    if (searchParam) {
      let searchQuery =
        type === "search" ? searchParam : `${searchParam}, United States`;
      const response = await getCoords(String(searchQuery));
      if (response.status === "OK") {
        return response.results[0].geometry.location;
      } else {
        return response;
      }
    }
  }

  useEffect(() => {
    async function getCoords() {
      let coords = await fetchCoords();
      if (String(searchParam)) {
        setCoords(coords);
      }
      return;
    }
    getCoords();
  }, []);

  // ================================
  // Get Locations
  // ================================
  useMemo(() => {
    async function fetchData() {
      let loc: any[] = [];
      let radius = 25 / 0.62137;
      const mapMarkerBoxButtonLabel = await getLabel("mapMarkerBoxButtonLabel");
      const mapLocationsToShow = await getSetting("mapLocationsToShow");
      const mapViewLocationButtonLabel = await getLabel(
        "mapViewLocationButtonLabel"
      );
      const mapViewMoreLinkText = await getLabel("mapViewMoreLinkText");
      const disclaimerLabel = await getLabel("disclaimerLabel");

      setLabels({
        mapMarkerBoxButtonLabel: mapMarkerBoxButtonLabel,
        mapViewLocationButtonLabel: mapViewLocationButtonLabel,
        mapViewMoreLinkText: mapViewMoreLinkText,
        disclaimerLabel: disclaimerLabel,
      });
      if (mapLocationsToShow) {
        setItemsToShow(mapLocationsToShow);
      }

      // Try catch is used because on this code it is very easy to get an error
      try {
        if (coords?.status !== "ZERO_RESULTS") {
          do {
            let l =
              type != "state"
                ? await getLocations(coords, radius, String(searchParam))
                : await getLocationsByState(String(searchParam));
            loc = l.contentTypeLocationCollection.items;
            radius = radius + 25 / 0.62137;
            setRadius(radius);
          } while (loc.length === 0 && radius <= 250 / 0.62137);

          // add distance to array of locations
          if (coords?.status !== "ZERO_RESULTS") {
            for (let i = 0; i < loc.length; i++) {
              loc[i].distance = calculateDistance(
                coords?.lat,
                coords?.lng,
                loc[i].address.coordinates.lat,
                loc[i].address.coordinates.lon,
                true
              );
            }

            if (type === "search") {
              // sort locations by distance
              loc.sort(function (a: ListItems, b: ListItems) {
                return a.distance - b.distance;
              });
            } else {
              // sort locations by name
              loc.sort(function (a: ListItems, b: ListItems) {
                if (a.locationName < b.locationName) {
                  return -1;
                }
                if (a.locationName > b.locationName) {
                  return 1;
                }
                return 0;
              });
            }

            let locationsIDs: any[] | string = [];
            for (let i = 0; i < loc.length; i++) {
              locationsIDs.push(loc[i].locationId);
            }

            locationsIDs = locationsIDs.join(",");
            const units = await getUnits(locationsIDs);

            for await (const result of loc) {
              const unitsFiltered = units.filter(
                (data: any) => data.sitelinkLocationId == result.locationId
              );
              const categories = determineCategory(unitsFiltered);
              result.categories = categories;
            }

            /**********GET UNITS CATEGORY************* */

            /**********GET UNITS CATEGORY************* */

            const prices = await getPrices(locationsIDs);

            for (let i = 0; i < loc.length; i++) {
              const priceFiltered = prices.filter(
                (data: any) => data.locationId == loc[i].locationId
              );
              if (
                priceFiltered[0] &&
                (priceFiltered[0].lowestPrice ||
                  priceFiltered[0].lowestPrice === 0)
              ) {
                loc[i].price = priceFiltered[0].lowestPrice;
              }
            }

            setLocationsWithOutFilters([...loc]);
          }

          setFetchingStatus(false);
          setLocations(loc);
        }
      } catch (error) {
        // navigate("/no-locations");
        setLocationsWithOutFilters([...loc]);
        setFetchingStatus(false);
        setLocations(loc);
      }
      return loc;
    }
    // load the data
    fetchData();
    setIsHydrated(true);
  }, [coords]);

  // ================================
  // Sort Locations by distance
  // ================================

  // ================================
  // Sticky
  // ================================
  useEffect(() => {
    if (isHydrated) {
      const sidebarEl = document!
        .querySelector<HTMLInputElement>(".LocationMapList-map")!
        .getBoundingClientRect();
      // setSidebarWidth(sidebarEl.width);
      setSidebarTop(sidebarEl.top);
    }
  }, []);

  useEffect(() => {
    if (!sidebarTop) return;

    window.addEventListener("scroll", isSticky);
    return () => {
      window.removeEventListener("scroll", isSticky);
    };
  }, [sidebarTop]);

  const isSticky = () => {
    const sidebarEl = document.querySelector(".LocationMapList-map");
    const scrollTop = window.scrollY;
    if (scrollTop >= sidebarTop - 10) {
      sidebarEl?.classList.add("sticky", "top-1");
    } else {
      sidebarEl?.classList.remove("sticky", "top-1");
    }
  };

  // ================================
  // Map stuff
  // ================================
  let containerStyle = {
    width: "100%",
    height: "100%",
  };

  let options = {
    styles: mapStyles,
    streetViewControl: false,
    fullscreenControl: false,
    mapTypeControl: false,
  };

  let zoom: number = 4;

  let center = {
    lat: 34.74865,
    lng: -98.2322,
  };

  if (coords?.status !== "ZERO_RESULTS") {
    // Change some stuff if we have coordinates
    center = {
      lat: coords?.lat,
      lng: coords?.lng,
    };

    // ** NOTE **
    // sometimes google cant find a coords based on search query
    // but contentful can find locations based on search query
    // in this case we are still zooming way out since there are
    // locations to show but cant center
    if (type === "search") {
      if (radius === 25) {
        zoom = 11;
      } else if (radius <= 50) {
        zoom = 10;
      } else if (radius <= 100) {
        zoom = 9;
      } else if (radius <= 150) {
        zoom = 8;
      } else if (radius <= 200) {
        zoom = 7;
      } else if (radius <= 250) {
        zoom = 6;
      } else {
        zoom = 9;
      }
    }
    if (type === "state") {
      zoom = 6;
    } else {
      zoom = 7;
    }
  }

  // const infoOptions = { closeBoxURL: "", enableEventPropagation: true };

  const handleMarkerClick = (ib: any, index: number) => {
    if (width < 1024) {
      const setInfo = ib === infoBox ? null : ib;
      setMarkerInfoBox(setInfo);
    } else {
      //let element = document.getElementById(`location-item-${index}`);
      //element?.scrollIntoView({ behavior: "smooth" });
      const setInfo = ib === infoBox ? null : ib;
      setMarkerInfoBox(setInfo);
    }
  };

  // ================================
  // Labels
  // ================================
  let disclaimerLabel: string,
    markerButtonLabel: string,
    viewLocationLabel: string | null,
    viewMoreLinkTextLabel: string;
  viewMoreLinkTextLabel =
    labels !== null ? labels.mapViewMoreLinkText : "View More";
  viewLocationLabel =
    labels !== null ? labels.mapViewLocationButtonLabel : "View Units";
  markerButtonLabel =
    labels !== null ? labels.mapMarkerBoxButtonLabel : "Select";
  disclaimerLabel =
    labels !== null
      ? labels.disclaimerLabel
      : "Other fees may apply upon payment processing.";

  if (isHydrated && locations && typeof document !== "undefined") {
    return (
      <div className="mx-auto max-w-screen-xl md:px-5">
        <h1 className="location-in text-center font-normal">
          Locations {String(searchParam) ? `in ` : ""}
          {String(searchParam) ? (
            <span className="mt-[20px] capitalize text-secondary2">{`"${String(
              searchParam || ""
            )}"`}</span>
          ) : (
            ""
          )}
        </h1>
        <h2
          className={`${
            width > 479 && "pb-8"
          } result-count text-center text-24 font-bold ${
            locations?.length < 1 ? "opacity-0" : ""
          }`}
        >
          {`${locations?.length} Result${locations?.length > 1 ? "s" : ""}`}
        </h2>

        {!isFetchingLocations && (
          <div className="div-filters-selected  flex flex-wrap">
            {filtersChecked && filtersChecked.length > 0 && (
              <>
                {filtersChecked.map((filter: any) => (
                  <span
                    className="flex text-12"
                    key={filter.filter}
                    onClick={() => deleteFilter(filter.filter)}
                  >
                    {filter.filter}
                    <div className="mt-px">
                      <Delete size="15px" color="#D6643F" />
                    </div>
                  </span>
                ))}
              </>
            )}
          </div>
        )}

        <div className="div-tab-filter">
          <ul className="LocationMapList-map-tabs space-between grid grid-cols-12 bg-secondary4 p-0 lg:grid-cols-12">
            {/* <li className="hidden lg:col-span-1 lg:block"></li> */}
            <li className="LocationMapList-map-tabs-map tab-title  col-span-5 h-[50px] list-none text-20 lg:col-span-6">
              <button
                className={`d-flex h-[50px] w-full items-center justify-center py-4 font-bold lg:cursor-auto
                            ${
                              currentTab === "map"
                                ? "bg-secondary2 text-secondary4 lg:bg-secondary4 lg:text-secondary2"
                                : "bg-secondary4 text-secondary2"
                            }
                        `}
                onClick={handleTab("map")}
              >
                Map View
              </button>
            </li>
            <li className="LocationMapList-map-tabs-list tab-title col-span-5 h-[50px] list-none lg:col-span-5">
              <button
                className={`d-flex h-[50px] w-full items-center justify-center py-4 text-20 font-bold lg:cursor-auto
                            ${
                              currentTab === "list"
                                ? "bg-secondary2 text-secondary4 lg:bg-secondary4 lg:text-secondary2"
                                : "bg-secondary4 text-secondary2"
                            }`}
                onClick={handleTab("list")}
              >
                List View
              </button>
            </li>

            {!isFetchingLocations && (
              <li className="col-span-2 list-none lg:col-span-1">
                <div className="button-wrap flex justify-end bg-secondary4">
                  <button onClick={() => setOpenModal(!openModal)}>
                    {openModal ? <CloseIcon /> : <Filter size={50} />}
                  </button>
                </div>
              </li>
            )}
          </ul>

          {openModal && (
            <FilterModal
              filtersChecked={filtersChecked}
              filtersContentful={filtersContentful}
              setOpenModal={() => setOpenModal(false)}
              setFiltersChecked={(filters: any) => assignFilters(filters)}
            />
          )}
        </div>
        <div className="relative grid grid-cols-12 lg:gap-8">
          {openModal && <div className="background-modal-absolute"></div>}

          <div
            className={`LocationMapList-map col-span-12 lg:col-span-6  ${
              currentTab === "map" ? "activeTab" : ""
            }`}
          >
            <GoogleMap
              mapContainerStyle={containerStyle}
              zoom={zoom}
              options={options}
              center={center}
            >
              {locations.map((item: ListItems, index: number) => (
                <Marker
                  key={index}
                  position={{
                    lat: item.address.coordinates.lat,
                    lng: item.address.coordinates.lon,
                  }}
                  icon={hover && marker === item ? pinHover : pin}
                  label={{
                    text: item.price ? `$${item.price}*` : `$0*`,
                    className: "marker-label",
                  }}
                  onClick={() => handleMarkerClick(item, index)}
                  clickable
                >
                  {infoBox && infoBox === item && (
                    <InfoWindow
                      onCloseClick={() => {
                        handleMarkerClick(item, index);
                      }}
                      position={{
                        lat: item.address.coordinates.lat + 0.019,
                        lng: item.address.coordinates.lon,
                      }}
                    >
                      <MapInfoBox
                        address={item.address}
                        key={index}
                        locationName={item.locationName}
                        price={`${item.price}`}
                        locationImageCollection={item.locationImageCollection}
                        googleBusinessProfileId={item.googleBusinessProfileId}
                        index={index}
                        buttonLabel={markerButtonLabel}
                        slug={item.slug}
                      />
                    </InfoWindow>
                  )}
                </Marker>
              ))}
            </GoogleMap>
          </div>
          <div
            className={`LocationMapList-list col-span-12 lg:col-span-6 ${
              currentTab === "list" ? "activeTab" : ""
            }`}
          >
            {!isFetchingLocations && locations.length < 1 && (
              <h3 className="mt-10 text-center text-secondary1">
                Oops! Looks like there are no locations that meet your search
                criteria.
              </h3>
            )}
            {console.log("locations", locations)}
            {isFetchingLocations && <LoaderList num={4} />}
            {/* Mobile list  */}
            {width < 1024
              ? locations
                  .slice(0, itemsToShow)
                  .map((item: ListItems, index: number) => (
                    <LocationListItem
                      address={item.address}
                      key={index}
                      locationName={item.locationName}
                      slug={item.slug}
                      buttonLabel={viewLocationLabel}
                      distance={item.distance}
                      salesFlag
                      price={item.price}
                      locationImageCollection={item.locationImageCollection}
                      googleBusinessProfileId={item.googleBusinessProfileId}
                      index={index}
                      locationId={item.locationId}
                    />
                  ))
              : ""}
            {/* Search page desktop list  */}
            {width >= 1024 && type === "search"
              ? locations.map((item: ListItems, index: number) => (
                  <LocationListItem
                    address={item.address}
                    key={index}
                    locationName={item.locationName}
                    slug={item.slug}
                    buttonLabel={viewLocationLabel}
                    distance={item.distance}
                    salesFlag
                    price={item.price}
                    locationImageCollection={item.locationImageCollection}
                    googleBusinessProfileId={item.googleBusinessProfileId}
                    index={index}
                    locationId={item.locationId}
                  />
                ))
              : ""}
            {/* State page desktop list  */}
            {width >= 1024 && type !== "search"
              ? locations
                  .slice(0, itemsToShow)
                  .map((item: ListItems, index: number) => (
                    <LocationListItem
                      address={item.address}
                      key={index}
                      locationName={item.locationName}
                      slug={item.slug}
                      buttonLabel={viewLocationLabel}
                      distance={item.distance}
                      salesFlag
                      price={item.price}
                      locationImageCollection={item.locationImageCollection}
                      googleBusinessProfileId={item.googleBusinessProfileId}
                      index={index}
                      locationId={item.locationId}
                    />
                  ))
              : ""}
            {/* State page load more button  */}

            {itemsToShow < locations.length &&
              type !== "search" &&
              width >= 1024 && (
                <a
                  href="#"
                  className="link block text-center text-16 font-bold text-callout"
                  onClick={(e) => {
                    e.preventDefault();
                    setItemsToShow(itemsToShow + 4);
                  }}
                >
                  {viewMoreLinkTextLabel}
                </a>
              )}
          </div>
        </div>
        <p className="my-8 text-center text-12">
          <span className="text-callout">*</span>
          {disclaimerLabel}
        </p>

        {itemsToShow < locations.length &&
          currentTab === "list" &&
          width <= 1024 && (
            <a
              href="#"
              className="link block text-center text-16 font-bold text-callout"
              onClick={(e) => {
                e.preventDefault();
                setItemsToShow(itemsToShow + 4);
              }}
            >
              {viewMoreLinkTextLabel}
            </a>
          )}
      </div>
    );
  } else {
    return <p>Map not hydrated</p>;
  }
}
