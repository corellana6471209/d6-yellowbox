import { describe, expect, it } from "vitest";
import {getInsuranceCoverage } from "./backend";

const fetchFunction = {
  json: function () {
    return {
      data: {
        insuranceCoverageDetails:null,
        rt:{returnCode: 1, returnMessage: '', id: 'RT1', rowOrder: 0, hasChanges: null},
      }
    }
  }
};
global.fetch = async () => {
  return fetchFunction;
};

const locationId = 9001;
const insuranceCoverageDetails = null;
const returnCode = 1;
const hasChanges = null


describe("getPostIteamsByLocationId BE", () => {
  it("Verify insuranceCoverageDetails", async () => {
    const data = await getInsuranceCoverage(locationId);
    expect(data.insuranceCoverageDetails).toBe(insuranceCoverageDetails);
  });
  it("Verify returnCode", async () => {
    const data = await getInsuranceCoverage(locationId);
    expect(data.rt.returnCode).toBe(returnCode);
  });
  it("Verify hasChanges", async () => {
    const data = await getInsuranceCoverage(locationId);
    expect(data.rt.hasChanges).toBe(hasChanges);
  });
});
