/**
 * function used in multiple query fetch - fetching location data
 * @return {JSON} fetched data
 */

import type { responseObject } from "~/routes/api/shared/prepareSimplyFetch";
import fetchCustomApi from "../../../utils/fetchCustomApi";
import fetchContentful from "../../../utils/fetchContentful";

export let loadLocationList = async (id: string) => {
  const query = `
   query componentHeroCollection {
     componentlocationList(id: "${id}") {
       title
       feeText
       numberOfRegisters
     }
   }
       `;
  var theData = await fetchContentful(query);
  return theData;
};

export async function getPrices(locations: any) {
  const data: responseObject = await fetchCustomApi(
    `/api/Inventory/LowestUnitTypePriceForLocationList/${locations}`
  );
  if (data.err) {
    //if you want details you have data.errText
    return "error";
  }
  return data.data;
}

export const getUnits = async (locations: any) => {
  const data: responseObject = await fetchCustomApi(
    `/api/Inventory/UnitTypeInventoryForMultipleLocations/${locations}`
  );
  if (data.err) {
    //if you want details you have data.errText
    //alert("something went wrong");
  }

  return data.data;
};

export async function getComponentData(id: string) {
  const query = `
     query {
       locationsMapListing(id: "${id}") {
         disclaimerText
         viewMoreLinkText
         viewLocationButtonLabel
         markerBoxButtonLabel
       }
     }`;
  const response = await fetchContentful(query);
  return response.locationsMapListing;
}
//export async function getComponentData(id: string) {
//  const query = `
//    query {
//      locationsMapListing(id: "${id}") {
//        disclaimerText
//        viewMoreLinkText
//        viewLocationButtonLabel
//        markerBoxButtonLabel
//      }
//    }`;
//  const response = await fetchContentful(query);
//  return response.locationsMapListing;
//}

/**
 *
 * @param locationId
 * @returns "insuranceCoverageDetails": {true|false|null} Object
 */
export const getInsuranceCoverage = async (locationId: number) => {
  // this fetch custom api using this file
  // simpleStorage/app/routes/api/Rental/insuranceCoverageRetrieve/$locationId.ts
  const data: responseObject = await fetchCustomApi(
    `/api/Rental/InsuranceCoverageRetrieve/${locationId}`
  );
  if (data.err) {
    //if you want details you have data.errText
    return "error";

    //alert("something went wrong -- getInsuranceCoverage");
  }

  return data.data;
};

/**
 * @param {number} locationId
 */

export const getPosItemsByLocationId = async (locationId: number) => {
  const data: responseObject = await fetchCustomApi(
    `/api/Payment/POSItemsRetrieve/${locationId}`
  );
  if (data.err) {
    //if you want details you have data.errText
    //alert("something went wrong -- getItemsByLocationId");
    return "error";
  }

  return data.data;
};

/**
 * YBP-797
 * @param {any} information
 * @returns
 */
export const unitsInformationByUnitID = async (information: any) => {
  const data: responseObject = await fetchCustomApi(
    `/api/Inventory/UnitsInformationByUnitID/${information}`
  );
  if (data?.err || !data) {
    return null;
  }
  return data.data;
};

/**
 * YBP-797
 * @param {any} information
 * @returns
 */
export const LedgersByTenantId = async (information: any) => {
  const data: responseObject = await fetchCustomApi(
    `/api/Reservation/LedgersByTenantId/${information}`
  );
  if (data.err || !data) {
    console.log(data.errText);
  }
  return data.data;
};

export async function getLocationsByState( searchParam?: string ) {
  let sp = "";
  if (searchParam) {
    // if search parama exists
    sp = `{address: {stateAbbreviation_contains: "${searchParam}"}}`;
  }

  let query = `
           {
               contentTypeLocationCollection(
                 limit: 25,
                 where: {AND: [{OR: [
                     ${sp !== "" ? sp : ""}
                   ]}
                     {enabled: true}
                   ]}
                 ) {
                 items {
                   locationName
                   slug
                   locationId
                   requireLock
                   locationImageCollection {
                    items {
                      title
                      description
                      contentType
                      fileName
                      size
                      url
                      width
                      height
                    }
                  }
                   siteLinkLocationId
                   googleBusinessProfileId
                   address {
                     address1
                     address2
                     city
                     state {
                       stateFullName
                       stateAbbreviation
                     }
                     postalCode
                     coordinates {
                       lat
                       lon
                     }
                   }
                   phoneNumberNewCustomer
                   phoneNumberExistingCustomer
                   mondayOfficeHours
                   tuesdayOfficeHours
                   wednesdayOfficeHours
                   thursdayOfficeHours
                   fridayOfficeHours
                   saturdayOfficeHours
                   sundayOfficeHours
                   amenitiesCollection(limit: 10) {
                    items {
                      name
                      amenityId
                      description
                    }
                  }
                   aboutOurFacility
                   storeEmailAddress
                   holdReservationTime
                 }
               }
             }
         `;
  // if there are no searh params, dont even bother querying
  //if (sp !== "") {
  var theData = await fetchContentful(query);
  return theData;
  //}
}

export async function getLocations(
  coordinates: any,
  radius: number = 25 / 0.62137,
  searchParam?: string
) {
  let coords = "";
  let sp = "";
  if (coordinates.lat) {
    // if coordinates exist
    coords = `
       {address: 
         {coordinates_within_circle: 
           {
             lat: ${coordinates.lat},
             lon: ${coordinates.lng},
             radius: ${radius}
           }
         }
       },`;
  }

  if (searchParam) {
    // if search parama exists
    sp = `
         {locationName_contains: "${searchParam}"},
         {address: {city_contains: "${searchParam}"}},
         {address: {postalCode_contains: "${searchParam}"}},
         `;
  }

  let query = `
           {
               contentTypeLocationCollection(
                 limit: 25,
                 where: {AND: [{OR: [
                     ${coords !== "" ? coords : ""}
                     ${sp !== "" ? sp : ""}
                   ]}
                     {enabled: true}
                   ]}
                 ) {
                 items {
                   locationName
                   slug
                   locationId
                   requireLock
                   locationImageCollection {
                    items {
                      title
                      description
                      contentType
                      fileName
                      size
                      url
                      width
                      height
                    }
                  }
                   siteLinkLocationId
                   googleBusinessProfileId
                   address {
                     address1
                     address2
                     city
                     state {
                       stateFullName
                       stateAbbreviation
                     }
                     postalCode
                     coordinates {
                       lat
                       lon
                     }
                   }
                   phoneNumberNewCustomer
                   phoneNumberExistingCustomer
                   mondayOfficeHours
                   tuesdayOfficeHours
                   wednesdayOfficeHours
                   thursdayOfficeHours
                   fridayOfficeHours
                   saturdayOfficeHours
                   sundayOfficeHours
                   amenitiesCollection(limit: 10) {
                    items {
                      name
                      amenityId
                      description
                    }
                  }
                   aboutOurFacility
                   storeEmailAddress
                   holdReservationTime
                 }
               }
             }
         `;
  // if there are no searh params, dont even bother querying
  //if (sp !== "") {
  var theData = await fetchContentful(query);
  return theData;
  //}
}
