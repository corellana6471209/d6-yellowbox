import StarRatings from "react-star-ratings";
import Button from "../../../../routes/common/Button/";
import useRating from "../../../../utils/hooks/useRating";
import { Link } from "react-scroll";
import useWindowDimensions from "../../../../utils/hooks/useWindowDimensions";

interface InfoItemsProps {
  price: string;
  locationName: string;
  address: {
    address1: string;
    address2: string | null;
    city: string;
    coordinates: {
      lat: number;
      lon: number;
    };
    postalCode: number;
    state: {
      stateFullName: string;
      stateAbbreviation: string;
    };
  };
  locationImageCollection: {
    items: [
      {
        title: string;
        url: string;
      }
    ];
  };
  googleBusinessProfileId: string;
  index: number;
  buttonLabel: string;
  slug: string;
}

export default function MapInfoBox(props: InfoItemsProps) {
  const {
    price,
    address,
    locationName,
    locationImageCollection,
    googleBusinessProfileId,
    index,
    buttonLabel,
    slug,
  } = props;
  const addressString = `${address?.address1} ${address?.city}, ${
    address?.state?.stateAbbreviation
  }${address?.postalCode ? ` ${address?.postalCode}` : ""}`;
  const { width } = useWindowDimensions();

  const rating = useRating(googleBusinessProfileId);
  return (
    <div className="LocationMapList-list-item--info-window m-2.5">
      <div className="grid grid-cols-1 gap-2.5 md:grid-cols-3 md:gap-4">
        <div className="col-span-1 max-h-32 grid-flow-col overflow-hidden">
          {locationImageCollection.items[0] && (
            <img
              src={locationImageCollection.items[0].url}
              alt={locationImageCollection.items[0].title}
              className="image-item--info-window"
            />
          )}
        </div>
        <div className="div-body-location-dialog col-span-1 md:col-span-2">
          <div className="grid grid-cols-2 gap-4">
            <div className="col-span-1">
              <h5 className="font-serif text-16 leading-5 text-secondary2 md:text-24">
                {locationName}
              </h5>
              <>
                {typeof rating === "number" ? (
                  <div className="star-rating-wrap flex justify-start">
                    <StarRatings
                      rating={rating}
                      starRatedColor="#5F7BC6"
                      starEmptyColor="#EAEBEF"
                      numberOfStars={5}
                      name="rating"
                      starDimension={width > 768 ? "25px" : "17px"}
                      starSpacing="0px"
                    />
                  </div>
                ) : (
                  ""
                )}
              </>

              <p className="text-address-item mt-5 font-sans text-12 font-normal text-greyHeavy md:text-16">
                {addressString}
              </p>
            </div>
            <div className="col-span-1 grid md:justify-end">
              {price && (
                <>
                  <p className="text-right font-sans text-12 font-bold md:text-20">
                    Starting From
                  </p>
                  <div className="div-text-price text-right text-36 font-bold text-callout">
                    <span className="font-sans">${price}</span>
                    <sup className="font-extralight">*</sup>
                  </div>
                </>
              )}

              <Button
                href={`/locations/${slug}`}
                target="_self"
                title={buttonLabel}
                buttonStyle="button-primary-expanded"
                additionalClass="font-medium"
              />
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}
