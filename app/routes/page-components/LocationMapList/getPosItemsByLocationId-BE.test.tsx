import { describe, expect, it } from "vitest";
import {getPosItemsByLocationId } from "./backend";

const fetchFunction = {
  json: function () {
    return {
      data: {
        posItems:[
          {
            inStock:22,
            corpCategory:null,
            id:"Table1",
            rowOrder:0,
            chargeDescriptionId:21264
          }
        ],
        rt:null,
      }
    }
  }
};
global.fetch = async () => {
  return fetchFunction;
};

const locationId = 9001;
const id = "Table1";
const chargeDescriptionId = 21264


describe("getPostIteamsByLocationId BE", () => {
  it("Verify stock", async () => {
    const data = await getPosItemsByLocationId(locationId);
    expect(data.posItems[0].inStock).toBe(22);
  });
  it("Verify Id", async () => {
    const data = await getPosItemsByLocationId(locationId);
    expect(data.posItems[0].id).toBe(id);
  });
  it("Verify chargeDescriptionId", async () => {
    const data = await getPosItemsByLocationId(locationId);
    expect(data.posItems[0].chargeDescriptionId).toBe(chargeDescriptionId);
  });
});
