import React from "react";
import { useEffect, useState } from "react";
import { loadBlockQuote } from "./backend";

let isHydrating = true;

export default function BlockQuote(props: any) {
  const { id } = props;
  let [isHydrated, setIsHydrated] = React.useState(!isHydrating);
  const [value, setValue] = useState("");

  useEffect(() => {
    async function fetchData() {
      console.log(id);
      const componentResponse = await loadBlockQuote(id);
      setValue(componentResponse.componentcallOutBlog.body);
      setIsHydrated(true);
    }
    fetchData();
  }, []);

  if (isHydrated && value) {
    // console.log(`HeroWithSearch data:`, module);
    return (
      <div className="blog-quote mt-8 mb-8 pl-5 pr-5 d-flex justify-center">
        <p className="blog-quote-text text-16 font-bold text-greyHeavy md:text-20 max-w-[900px]">
          {value}
        </p>
      </div>
    );
  } else {
    return ( <div className="min-h-[500px]"></div> );
  }
}
