import fetchCustomApi from "~/utils/fetchCustomApi";

/**
 * 
 * @param {string} componentblockQuoteId 
 * @returns https://simplyselfstorage.atlassian.net/browse/YBP-937
 * @testing componentblockQuoteId: "01osSSHPQWMzk5GEWcCBBN"
 */
export const loadBlockQuote = async (componentblockQuoteId: string) => {
    const query = `
  query componentCallOutCollection {
    componentcallOutBlog(id: "${componentblockQuoteId}") {
      title
      body
    }
  }`;
    var componentblockQuoteData = await fetchCustomApi(`/api/content/common/contentful/${query}`);
    return componentblockQuoteData;
};