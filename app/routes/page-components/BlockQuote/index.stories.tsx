import type { ComponentStory, ComponentMeta } from "@storybook/react";
import BlockQuote from "./index";

export default {
  title: "components/core/BlockQuote",
  component: BlockQuote,
} as ComponentMeta<typeof BlockQuote>;

const Template: ComponentStory<typeof BlockQuote> = (args) => (
  <BlockQuote />
  // {...args}
);

export const Primary = Template.bind({});
/**
 * Primary.args = {
  unit: fakeDataUnit,
  ctaText: "Save Now with No Obligation",
};
 */
