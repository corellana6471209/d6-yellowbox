import React from "react";
import { useEffect } from "react";
import BlogPreview, { convertFromContentful } from "../../common/BlogPreview";
import {
  loadBlogTeaserContent,
  loadBlogPreviews,
  loadBlogPreviewsByCategory,
  loadBlogPreviewsById,
} from "./backend";

// define module shape
interface Module {
  title: string;
  viewMoreCtaText: string;
  numberOfBlogs: number;
  blogs: BlogPreview.propTypes[];
  maxLines: number;
}

let isHydrating = true;

export default function BlogTeaser(props: any) {
  const { id } = props;
  let [isHydrated, setIsHydrated] = React.useState(!isHydrating);

  // defines the initial state
  const [module, setModule] = React.useState<Module | null>(null);

  useEffect(() => {
    async function fetchData() {
      // You can await here
      const componentResponse = await loadBlogTeaserContent(props.id);
      let teasersResponse = null;
      if (
        componentResponse.componentblogTeaser.populationStyle ==
        "Specify Category"
      ) {
        teasersResponse = await loadBlogPreviewsByCategory(
          componentResponse.componentblogTeaser.category,
          componentResponse.componentblogTeaser.numberOfBlogs
        );

        const convertedBlogs = convertFromContentful(teasersResponse, null);
        componentResponse.componentblogTeaser.blogs = convertedBlogs;
      } else if (
        componentResponse.componentblogTeaser.populationStyle == "Specify Blogs"
      ) {
        let blogIds: Array<string> = [],
          contentfulBlogItems =
            componentResponse.componentblogTeaser.blogsCollection.items;
        for (
          let blogCounter = 0;
          blogCounter < contentfulBlogItems.length;
          blogCounter++
        ) {
          blogIds.push(contentfulBlogItems[blogCounter].sys.id);
        }
        teasersResponse = await loadBlogPreviewsById(
          componentResponse.componentblogTeaser.numberOfBlogs,
          blogIds
        );
        const convertedBlogs = convertFromContentful(teasersResponse, blogIds);
        componentResponse.componentblogTeaser.blogs = convertedBlogs;
      } /* DEFAULT Latest Blogs */ else {
        teasersResponse = await loadBlogPreviews(
          componentResponse.componentblogTeaser.numberOfBlogs
        );
        const convertedBlogs = convertFromContentful(teasersResponse, null);
        componentResponse.componentblogTeaser.blogs = convertedBlogs;
      }
      setModule(componentResponse.componentblogTeaser);
    }
    const incomingModule = props?.module;
    if (!incomingModule) {
      isHydrating = false;
      // load the data
      fetchData();
      setIsHydrated(true);
    } else {
      isHydrating = false;
      setModule(incomingModule);
      setIsHydrated(true);
    }
  }, [props.id]);

  if (isHydrated && module) {
    // console.log(`HeroWithSearch data:`, module);
    return (
      <div className="blog-teaser bg-tertiary3">
        <div className="content-wrapper wide-wrapper"> {/**px-[20px] */}
          <h1 className="blog-teaser-title pb-[20px] pt-[30px] text-center font-serif text-28 leading-[35px]  md:pt-[60px] md:pb-[40px] md:text-36">
            {module.title}
          </h1>
          <div className="grid justify-between sm:grid-cols-1 md:grid-cols-2 md:gap-10 lg:grid-cols-3 xl:grid-cols-3">
            {module.blogs.slice(0, module.numberOfBlogs).map((item, index) => (
              <BlogPreview maxlines={module.maxLines} key={index} {...item} />
            ))}
          </div>
          <div className="blog-teaser-view-more text-center">
            <a
              className="mt-10 mb-10 inline-block text-22 font-bold text-callout link"
              href="/blog"
            >
              {module.viewMoreCtaText}
            </a>
          </div>
        </div>
      </div>
    );
  } else {
    return <></>;
  }
}
