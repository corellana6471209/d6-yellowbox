import fetchCustomApi from "~/utils/fetchCustomApi";

export let loadBlogTeaserContent = async (componentId: string) => {
  // console.log("BlogTeaser::loadBlogTeaserContent params: ", componentId);
  const query = `
      query componentblogTeaser {
        componentblogTeaser(id: "${componentId}") {
          title
          numberOfBlogs
          viewMoreCtaText
          populationStyle
          category
          maxLines
    			blogsCollection {
            items {
              sys {
                id
              }
            }
              }
          }
      }`;
  // console.log(`BlogTeaser::loadBlogTeaserContent query:\n`,query);
  var theData = await fetchCustomApi(`/api/content/common/contentful/${query}`);
  return theData;
};

export let loadBlogPreviewsByCategory = async (
  categoryName: string,
  numberOfBlogs: number
) => {
  const query = `
    query BlogTeasers {
      pageCollection(limit: ${numberOfBlogs}, where: {
        AND: [
          {categories_contains_some: "${categoryName}"},
          {type_contains: "blog"}
        ],
      }, order: sys_publishedAt_DESC) {
        items {
          sys {
            id
            firstPublishedAt
          }
          title
          slug
          seoDescription
          pageImage {
            title
            description
            contentType
            fileName
            size
            url
            width
            height
          }
        }
      }
    }      `;
  var theData = await fetchCustomApi(`/api/content/common/contentful/${query}`);
  // console.log(`BlogTeaser::loadBlogPreviewsByCategory query:\n`,query);
  return theData;
};

export let loadBlogPreviewsById = async (
  numberOfBlogs: number,
  blogIds: string[]
) => {
  const blogIdsString = '"' + blogIds.join('","') + '"';
  const query = `
    query BlogTeasers {
      pageCollection(limit: ${numberOfBlogs}, where: {
        AND: [
          {sys: { id_in: [${blogIdsString}]}},
          {type_contains: "blog"}
        ],
      }) {
        items {
          sys {
            id
            firstPublishedAt
          }
          title
          slug
          seoDescription
          pageImage {
            title
            description
            contentType
            fileName
            size
            url
            width
            height
          }
        }
      }
    }      `;
  // console.log(`BlogTeaser::loadBlogPreviewsById query:\n`,query);
  var theData = await fetchCustomApi(`/api/content/common/contentful/${query}`);
  return theData;
};

export let loadBlogPreviews = async (numberOfBlogs: number) => {
  const query = `
    query BlogTeasers {
      pageCollection(limit: ${numberOfBlogs}, where: {type_contains: "blog"}, order: sys_publishedAt_DESC) {
        items {
          sys {
            id
            firstPublishedAt
          }
          title
          slug
          seoDescription
          pageImage {
            title
            description
            contentType
            fileName
            size
            url
            width
            height
          }
        }
      }
    }      `;
  // console.log(`BlogTeaser::loadBlogPreviews query:\n`,query);
  var theData = await fetchCustomApi(`/api/content/common/contentful/${query}`);
  return theData;
};
