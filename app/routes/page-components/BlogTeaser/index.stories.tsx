import type { ComponentMeta } from '@storybook/react';
import testImage from '/mocks/test-assets/Comp_Teaser.png'
import * as PreviewStories from "../../common/BlogPreview/index.stories"

import BlogTeaser from './index';
import BlogPreview from '~/routes/common/BlogPreview';

export default {
    title: 'modules/BlogTeaser',
    component: BlogTeaser,
} as ComponentMeta<typeof BlogTeaser>;

const Template = args => <BlogTeaser {...args} />;

export const BlogTeaserTypical = Template.bind({});
BlogTeaserTypical.args = {
    id: "hero-test",
    module: {
        title: "Test Title for Blog Teaser",
        viewMoreCtaText: "View More Test",
        numberOfBlogs: 3,
        blogs: [
          { ...PreviewStories.Primary.args },
          { ...PreviewStories.Secondary.args },
          { ...PreviewStories.Tertiary.args },
          { ...PreviewStories.Quaternary.args }
        ]
    }
};

export const BlogTeaserWithTwoBlogs = Template.bind({});
BlogTeaserWithTwoBlogs.args = {
    id: "blog-test",
    module: {
        title: "Test Title for Blog Teaser",
        viewMoreCtaText: "View More Test",
        numberOfBlogs: 2,
        blogs: [
          { ...PreviewStories.Primary.args },
          { ...PreviewStories.Secondary.args },
          { ...PreviewStories.Tertiary.args },
          { ...PreviewStories.Quaternary.args }
        ]
    }
};

export const BlogTeaserWithOneBlog = Template.bind({});
BlogTeaserWithOneBlog.args = {
    id: "blog-test",
    module: {
        title: "Test Title for Blog Teaser",
        viewMoreCtaText: "View More Test",
        numberOfBlogs: 1,
        blogs: [
          { ...PreviewStories.Primary.args },
          { ...PreviewStories.Secondary.args },
          { ...PreviewStories.Tertiary.args },
          { ...PreviewStories.Quaternary.args }
        ]
    }
};

export const BlogTeaserTryFour = Template.bind({});
BlogTeaserTryFour.args = {
    id: "blog-test",
    module: {
        title: "Test Title for Blog Teaser",
        viewMoreCtaText: "View More Test",
        numberOfBlogs: 4,
        blogs: [
          { ...PreviewStories.Primary.args },
          { ...PreviewStories.Secondary.args },
          { ...PreviewStories.Tertiary.args },
          { ...PreviewStories.Quaternary.args }
        ]
    }
};
