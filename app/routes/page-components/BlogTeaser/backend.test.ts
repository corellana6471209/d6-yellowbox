import { describe, expect, it, vi } from "vitest";
import { loadBlogPreviews, loadBlogPreviewsByCategory, loadBlogPreviewsById, loadBlogTeaserContent } from './backend';
import { render, screen } from '@testing-library/react';


const obj = {
    loadBlogTeaserContent: () => "loadBlogTeaserContent",
    loadBlogPreviewsByCategory: () => "loadBlogPreviewsByCategory",
    loadBlogPreviewsById: () => "loadBlogPreviewsById",
    loadBlogPreviews: () => "loadBlogPreviews",

}

vi.mock('./backend', () => ({
    loadBlogTeaserContent: vi.fn().mockImplementation(() => "loadBlogTeaserContent"),
    loadBlogPreviewsByCategory: vi.fn().mockImplementation(() => "loadBlogPreviewsByCategory"),
    loadBlogPreviewsById: vi.fn().mockImplementation(() => "loadBlogPreviewsById"),
    loadBlogPreviews: vi.fn().mockImplementation(() => "loadBlogPreviews"),
}));

describe("fetching location data component unit test", () => {
 

    it('loadBlogTeaserContent should be mock', () => {
        expect(vi.isMockFunction(loadBlogTeaserContent)).toBe(true)
    });

    it('Should be executed loadBlogTeaserContent', async () => {
        const spy = vi.spyOn(obj, 'loadBlogTeaserContent');
        const resp = await loadBlogTeaserContent("test");
        expect(spy.getMockName()).toEqual('loadBlogTeaserContent');
        expect(resp).toBe("loadBlogTeaserContent");
    });

    it('loadBlogPreviewsByCategory should be mock', () => {
        expect(vi.isMockFunction(loadBlogPreviewsByCategory)).toBe(true)
    });

    it('Should be executed loadBlogPreviewsByCategory', async () => {
        const spy = vi.spyOn(obj, 'loadBlogPreviewsByCategory');
        const resp = await loadBlogPreviewsByCategory("test",0);
        expect(spy.getMockName()).toEqual('loadBlogPreviewsByCategory');
        expect(resp).toBe("loadBlogPreviewsByCategory");
    });



    it('loadBlogPreviewsById should be mock', () => {
        expect(vi.isMockFunction(loadBlogPreviewsById)).toBe(true)
    });

    it('Should be executed loadBlogPreviewsById', async () => {
        const spy = vi.spyOn(obj, 'loadBlogPreviewsById');
        const resp = await loadBlogPreviewsById(0, [""]);
        expect(spy.getMockName()).toEqual('loadBlogPreviewsById');
        expect(resp).toBe("loadBlogPreviewsById");
    });



    it('loadBlogPreviews should be mock', () => {
        expect(vi.isMockFunction(loadBlogPreviews)).toBe(true)
    });

    it('Should be executed loadBlogPreviews', async () => {
        const spy = vi.spyOn(obj, 'loadBlogPreviews');
        const resp = await loadBlogPreviews(0);
        expect(spy.getMockName()).toEqual('loadBlogPreviews');
        expect(resp).toBe("loadBlogPreviews");
    });
});