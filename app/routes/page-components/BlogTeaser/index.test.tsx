import { vi, describe, expect, it } from "vitest";
import React from "react";
import BlogTeaser from ".";
import { render, screen } from "../../../utils/test-utils";
import { loadBlogTeaserContent } from "./backend";

vi.mock("../../common/BlogPreview", () => ({
  convertFromContentful: vi.fn(),
}));

// Cache original functionality
const fetchFunction = {
  json: function () {
    return {
      data: {
        componentblogTeaser: {
          componentblogTeaser: {
            blogs: [
              {
                module: {
                  date: "2022-08-14T14:15:33.993Z",
                  id: "231MoEFaH8PsYVWTUsG23w",
                  image: {
                    alt: "Main image for Panthersville / Decatur Self Storage",
                    url: "https://images.ctfassets.net/rfh1327471ch/79zy3LJSjYVR9mZ2jYxlrq/fc6f075c536e5f07ebbc93207a595b9b/panthersville--decatur_af947f0321d307d8e398d398a5c8fb9dd1e50cb9.jpeg",
                  },
                  summary:
                    "Number Three Blog Title for the SEO Description Field",
                  title: "Number Three Blog Title",
                },
              },
            ],
            numberOfBlogs: 3,
            title: "Blog Teaser Title",
            viewMoreCtaText: "View All Blogs",
          },
        },
      },
      errors: null,
    };
  },
};
global.fetch = async () => {
  return fetchFunction;
};

vi.mock("../../common/BlogPreview", () => {
  return {
    default: "",
  };
});

describe("BlogTeaser component FE", () => {
  beforeEach(() => {
    const setState = vi.fn();
    const useStateSpy = vi.spyOn(React, "useState");
    useStateSpy.mockImplementation(() => [
      {
        blogs: [],
        numberOfBlogs: 3,
        title: "Blog Teaser Title",
        viewMoreCtaText: "View All Blogs",
      },
      setState,
    ]);
  });
  afterEach(() => {
    vi.clearAllMocks();
  });

  it("Verify if the BlogTeaser component renders", () => {
    render(<BlogTeaser id={"avV4ys0j25ZoSXnAjlRtK"} />);
  });

  it("Verify if the component has correct view more text", async () => {
    render(<BlogTeaser id={"avV4ys0j25ZoSXnAjlRtK"} />);
    expect(await screen.findByText("View All Blogs")).toBeInTheDocument();
  });

  it("Verify number of blogs", async () => {
    const data = await loadBlogTeaserContent("id");
    expect(data.componentblogTeaser.componentblogTeaser.numberOfBlogs).toBe(3);
  });
});
