import React, { useState, useEffect } from "react";
import { useSubmit, useTransition, useActionData } from "@remix-run/react";
import moment from "moment";
import PaymentUnitForm from "../../../routes/page-components/PaymentUnitForm";
import { sizeCategory } from "../../../utils/fakedataToTests";
import { identify } from "../../../utils/identify";

import AutoPayModal from "../../common/Modals/AutoPay";
import Spinner from "../../common/Spinner";
import MessageModal from "../../common/MessageModal";
import ChevronRight from "../../common/svg/ChevronRight";
import ChevronLeft from "../../common/svg/ChevronLeft";
import ChevronDown from "../../common/svg/ChevronDown";

const UnitPayment = (props: any) => {
  const submit = useSubmit();
  const transition = useTransition();
  const actionData = useActionData();
  const isSubmiting = transition.state === "submitting";
  const [paymentDetails, setPaymentDetails] = useState<any>(false);
  const [openAutoPayModal, setOpenAutoPayModal] = useState<any>(false);
  const [paymentForm, setPaymentForm] = useState<any>(false);
  const [changeInformation, setChangeInformation] = useState<any>(false);
  const [openModal, setOpenModal] = useState<any>(false);
  const [message, setMessage] = useState<any>(null);

  const [paymentTitle, setPaymentFormTitle] = useState<any>("Make A Payment");
  const {
    setShowBackButton,
    showBackButton,
    detailPaymentViewType,
    openDetailsPaymentSection,
    unit,
    units,
    setNewUnits,
  } = props;

  const sizeCategoryText =
    unit && unit.category != null ? `${sizeCategory[unit.category]}", "` : "";
  const description = `${sizeCategoryText} ${
    unit.unitTypeName ? unit.unitTypeName : ""
  }`;

  /************TIME VARIABLES************ */

  const startOfMonth = moment(unit.paidThruDateTime)
    .startOf("month")
    .format("MMMM DD, YYYY");
  const paidThruDateTime = moment(unit.paidThruDateTime).format(
    "MMMM DD, YYYY"
  );
  /************TIME VARIABLES************ */

  const showPaymentDetails = () => {
    setPaymentDetails(!paymentDetails);
  };

  useEffect(() => {
    if (
      detailPaymentViewType === "paymentNormal" ||
      detailPaymentViewType === "paymentNormalAutoPay"
    ) {
      showPaymentForm("Make A Payment", detailPaymentViewType);
      window.scrollTo({ top: 0, behavior: "smooth" });
    }
    // eslint-disable-next-line
  }, [detailPaymentViewType]);

  const showPaymentForm = (
    title: string = "Make A Payment",
    view: String = "paymentSection"
  ) => {
    setPaymentFormTitle(title);
    openDetailsPaymentSection(view, unit);
    setPaymentForm(!paymentForm);
    setShowBackButton(!showBackButton);
  };

  const redirectTo = (type: string = "detailsSection", unit: any) => {
    if (type === "detailsSection") {
      setShowBackButton(true);
    }
    openDetailsPaymentSection(type, unit);
  };

  const handleKeyPress = (e: any, type: string = "detailsSection") => {
    if (type === "detailsSection") {
      setShowBackButton(true);
    }
    openDetailsPaymentSection(type, unit);
  };

  const callBackAutoPayModal = (value: any) => {
    if (!value) {
      setChangeInformation(true);
      const formData = new FormData();
      formData.set("unit", JSON.stringify(unit));
      formData.set("units", JSON.stringify(units));
      formData.set("disableAutoPay", "true");
      submit(formData, { method: "post", action: "/my-account?index" });
    }

    setOpenAutoPayModal(false);
  };

  useEffect(() => {
    if (actionData && actionData.showModal && changeInformation) {
      setOpenModal(actionData.showModal);
      setMessage(actionData.message);
      setChangeInformation(false);
    }

    if (
      actionData &&
      actionData.showSuccessFulModal &&
      actionData.units &&
      actionData.unit &&
      changeInformation
    ) {
      identify(
        window,
        `${actionData.unit.tenantId}${actionData.unit.locationId}`,
        {
          autopay: 0,
        }
      );

      setChangeInformation(false);
      setNewUnits(actionData.units, actionData.unit);
    }
  }, [actionData]);

  return (
    <section className="unit-my-account-unit-payment">
      <MessageModal
        open={openModal}
        message={message}
        title=" "
        ctaText="Close"
        callback={() => setOpenModal(!openModal)}
      />

      <AutoPayModal
        open={openAutoPayModal}
        callback={(value: any) => callBackAutoPayModal(value)}
      />

      {/**isSubmiting && <Spinner /> */}

      <div className="grid grid-cols-1">
        {paymentForm && (
          <div onClick={() => showPaymentForm()}>
            <p className="d-flex mt-5 mb-5 cursor-pointer align-middle text-16 font-bold text-secondary1">
              <ChevronLeft size="25px" color="#00155F" />
              Back
            </p>
            <h1 className="mb-5 text-center font-serif text-36 text-greyHeavy">
              {paymentTitle}
            </h1>
          </div>
        )}

        <section className="div-section-blocks-unit-payment relative">
          <div className="sm:pl-12 sm:pr-12 md:pl-0 md:pr-0">
            <p className="text-40 font-bold text-secondary2">
              Unit {unit.unitName}
            </p>
            <p className="text-16 font-bold">
              {unit.width}’ X {unit.length}’
            </p>
            <div className="flex items-center justify-between">
              <p className="text-12 font-bold">{description}</p>
              {unit && unit.address && (
                <p className="text-16 font-bold sm:text-24">
                  {unit.address.city}, {unit.address?.state?.stateAbbreviation}
                </p>
              )}
            </div>

            {unit && unit.address && (
              <div className="div-section-blocks-unit-payment-direction">
                <p className="mt-1 text-16 text-greyHeavy sm:text-24">
                  {unit.address.address1}
                </p>
                <p className="text-16 text-greyHeavy sm:text-24">
                  {unit.address.city}, {unit.address?.state?.stateAbbreviation}{" "}
                  {unit.address.postalCode}
                </p>
              </div>
            )}
          </div>

          {paymentForm ? (
            <PaymentUnitForm
              showPaymentForm={() => redirectTo("detailsSection", unit)}
              unit={unit}
              setNewUnits={(units: any, unit: any) => setNewUnits(units, unit)}
              detailPaymentViewType={detailPaymentViewType}
              openDetailsPaymentSection={(unitSelected: any) =>
                redirectTo("detailsSection", unitSelected)
              }
              units={units}
            />
          ) : (
            <section className="section-payment sm:pl-12 sm:pr-12 md:pl-0 md:pr-0">
              <div className="div-additional-information mt-8">
                <p className="text-16 text-greyHeavy sm:text-20">
                  Auto Pay:
                  <span className="text-20 font-bold text-black">
                    {unit.autoBillType ? " ON" : " OFF"}
                  </span>
                </p>

                {unit.autoBillType ? (
                  <p
                    className="link mt-8 mb-10 cursor-pointer text-16 font-bold text-callout sm:text-24"
                    onClick={() => setOpenAutoPayModal(true)}
                  >
                    Click to modify Auto Pay
                  </p>
                ) : (
                  ""
                )}

                <div className="div-button-to-pay mt-10">
                  {!unit.autoBillType ? (
                    <>
                      <p className="mb-10 text-14 font-bold text-greyHeavy sm:text-20">
                        Ready to setup Auto Pay? It's the simplest way to make
                        sure you never miss a payment.
                      </p>

                      <div className="div-button-unit-payment d-flex mt-3 mb-3 justify-center">
                        <button
                          className="yellow-button block bg-primary1 py-3 text-center text-16 text-secondary1 no-underline"
                          tabIndex={1}
                          onClick={() =>
                            redirectTo("paymentNormalAutoPay", unit)
                          }
                          onKeyPress={(e) =>
                            handleKeyPress(e, "paymentNormalAutoPay")
                          }
                        >
                          Set Up Auto Pay
                        </button>
                      </div>

                      <p className="mt-10 mb-10 text-14 text-greyHeavy sm:text-20">
                        Please note: Setting up Auto Pay for your rental account
                        requires a payment to be processed at the time of setup.
                        Any future Auto Pay payments will process on the
                        agreed-upon contract date.
                      </p>
                    </>
                  ) : (
                    ""
                  )}

                  <p className="text-20">Paid Through Date:</p>
                </div>
              </div>

              <div className="div-additional-information-payment mt-1 cursor-pointer">
                <div
                  className="d-flex justify-between"
                  onClick={() => showPaymentDetails()}
                >
                  <p className="text-20 font-bold text-secondary1">
                    {paidThruDateTime}
                  </p>
                  <div>
                    {paymentDetails ? (
                      <ChevronDown size="25px" color="#00155F" />
                    ) : (
                      <ChevronRight size="25px" color="#00155F" />
                    )}
                  </div>
                </div>

                {paymentDetails ? (
                  <>
                    <div className="d-flex mt-6 justify-between">
                      <p className="text-12 text-greyHeavy">
                        Next Payment Amount Due:
                      </p>
                      <p className="text-28 font-bold text-greyHeavy">
                        ${unit.rent.toFixed(2)}
                      </p>
                    </div>

                    {!unit.autoBillType && (
                      <div className="div-button-to-pay mt-8">
                        <p className="text-left text-16 text-greyHeavy sm:text-center">
                          Set up Auto Pay or make a secure one-time payment by
                          selecting the button below.
                        </p>

                        <div className="div-button-unit-payment-bottom d-flex mt-3 mb-3 mb-20	justify-center">
                          <button
                            className="yellow-button block bg-primary1 py-3 text-center text-16 text-secondary1 no-underline"
                            tabIndex={2}
                            onClick={() => redirectTo("paymentNormal", unit)}
                            onKeyPress={(e) =>
                              handleKeyPress(e, "paymentNormal")
                            }
                          >
                            Make A One-Time Payment
                          </button>
                        </div>
                      </div>
                    )}
                  </>
                ) : (
                  ""
                )}
              </div>
            </section>
          )}
        </section>
      </div>
    </section>
  );
};

export default UnitPayment;
