import type { ComponentStory, ComponentMeta } from "@storybook/react";
import { BrowserRouter as Router } from "react-router-dom";
import BlogHero from "./index";

export default {
  title: "components/core/BlogHero",
  component: BlogHero,
} as ComponentMeta<typeof BlogHero>;

const Template: ComponentStory<typeof BlogHero> = (args) => (
  <Router>
    <BlogHero />
  </Router>
  // {...args}
);

export const Primary = Template.bind({});
/**
 * Primary.args = {
  unit: fakeDataUnit,
  ctaText: "Save Now with No Obligation",
};
 */
