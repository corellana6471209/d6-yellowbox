import React from "react";
import { useEffect, useState } from "react";
import Carousel from "react-multi-carousel";
import Spinner from "../../common/Spinner";

import { loadBlogArticle } from "./backend";

const responsive = {
  desktop: {
    breakpoint: { max: 6000, min: 1024 },
    items: 1,
  },

  tablet: {
    breakpoint: { max: 1024, min: 464 },
    items: 1,
  },
  mobile: {
    breakpoint: { max: 464, min: 0 },
    items: 1,
  },
};

//const pageCount = Math.ceil(listItems.length / paginationLimit);
export default function BlogHero(props: any) {
  const { id } = props;
  const [isHydrated, setIsHydrated] = useState(false);
  const [blogHeroInformation, setBlogHeroInformation] = useState<any>(null);
  const [blogHeroPosts, setBlogHeroPosts] = useState<any>(null);

  //Variables to paginate

  useEffect(() => {
    async function fetchData() {
      const componentResponse = await loadBlogArticle(id);

      if (componentResponse.componentblogHero?.blogArticleCollection?.total) {
        const blogs =
          componentResponse.componentblogHero.blogArticleCollection.items;

        const sortedPosts = blogs
          .filter((item: any) => item !== null)
          .sort((first: any, second: any) => {
            let a = new Date(second.sys.publishedAt).getTime();
            let b = new Date(first.sys.publishedAt).getTime();

            return a - b;
          });

        setBlogHeroPosts(sortedPosts);
        setBlogHeroInformation(componentResponse.componentblogHero);
      }

      setIsHydrated(true);
    }
    fetchData();
  }, []);

  if (isHydrated) {
    if (blogHeroPosts && blogHeroInformation) {
      return (
        <section className="blog-hero-section max-w-[1200px] mx-auto">
          {/*******************************************CAROUSEL******************************************** */}

          <Carousel responsive={responsive} showDots arrows={false}>
            {blogHeroPosts
              .slice(0, blogHeroInformation.numberOfPostsOnCarousel)
              .map((blog: any, index: number) => (
                <div className="div-image-carousel relative" key={index}>
                  <img
                    src={
                      blog.pageImage.url ||
                      `https://via.placeholder.com/1300x700`
                    }
                    alt="Blog"
                  />
                  <div className="div-image-carousel-text">
                    {blog.title && (
                      <p className="mb-1 font-serif text-24 font-semibold text-white md:mb-5 md:text-36">
                        {blog.title}
                      </p>
                    )}

                    <a
                      className="text-14 font-bold text-primary1 no-underline md:text-24 link"
                      href={`/blog/${blog.slug}`}
                    >
                      Read More
                    </a>
                  </div>
                </div>
              ))}
          </Carousel>

          {/*******************************************CAROUSEL******************************************** */}
        </section>
      );
    } else {
      return <div className="min-h-[500px]"></div>;
    }
  } else {
    return <Spinner />;
  }
}
