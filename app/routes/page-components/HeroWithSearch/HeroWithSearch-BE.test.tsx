import { describe, expect, it } from "vitest";
import { loadHeroWithSearch } from "./backend";

const heroId = "someString";
const fetchFunction = {
  json: function () {
    return {
      data: {
        componentHeroCollection: {
          componentheroWithSearch: {
            componentName: "Home page component",
            heroImage: {
              description: "Image",
              url: "https://images.ctfassets.net/rfh1327471ch/6xd4I8TX097heV11eeu15M/8fbca13c71c6b2083285b88b6450da5b/home-page-image.jpg",
            },
          },
        },
      },
      errors: null,
    };
  },
};
global.fetch = async () => {
  return fetchFunction;
};

describe("HeroWithSearch component BE", () => {
  it("Verify the name of the component", async () => {
    const data = await loadHeroWithSearch(heroId);
    expect(
      data.componentHeroCollection.componentheroWithSearch.componentName
    ).toBe("Home page component");
  });

  it("Verify the image information", async () => {
    const data = await loadHeroWithSearch(heroId);
    expect(
      data.componentHeroCollection.componentheroWithSearch.heroImage.description
    ).toBe("Image");

    expect(
      data.componentHeroCollection.componentheroWithSearch.heroImage.url
    ).toBe(
      "https://images.ctfassets.net/rfh1327471ch/6xd4I8TX097heV11eeu15M/8fbca13c71c6b2083285b88b6450da5b/home-page-image.jpg"
    );
  });
});
