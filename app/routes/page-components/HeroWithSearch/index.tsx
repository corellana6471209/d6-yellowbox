import { useEffect, useState } from "react";
import { loadHeroWithSearch } from "./backend";
import Search from "../../common/Search";
import { getLabel } from "../../../utils/getLabel";

// define module shape
interface Module {
  componentName: string;
  searchActionUrl: string;
  searchButtonText: string;
  searchHelperText: string;
  showAdvancedSearchLink: string;
  showSearch: boolean;
  heroImage: {
    url: string;
    description: string;
    height: string;
    width: string;
  };
}

let isHydrating = true;

export default function HeroWithSearch(props: any) {
  // defines the initial state
  let [isHydrated, setIsHydrated] = useState(!isHydrating);
  const [module, setModule] = useState<Module | null>(null);
  const [config, setConfig] = useState<any>(null);

  useEffect(() => {
    async function fetchData() {
      // You can await here
      const response = await loadHeroWithSearch(props.id);
      const helperText = await getLabel("searchHelperText");
      const buttonText = await getLabel("searchButtonText");
      setConfig({ searchHelperText: helperText, searchButtonText: buttonText });
      setModule(response.componentheroWithSearch);
      return response.componentheroWithSearch;
    }
    const incomingModule = props?.module;
    if (!incomingModule) {
      isHydrating = false;
      // load the data
      fetchData();
      setIsHydrated(true);
    } else {
      isHydrating = false;
      setModule(incomingModule);
      setIsHydrated(true);
    }
  }, [props.id]);

  if (isHydrated && module) {
    // console.log(`HeroWithSearch data:`, module);
    return (
      <div className="div-hero-page">
        <div className="div-title-hero-page pl-5 pr-5 md:pl-0 md:pr-0">
          <h1 className="text-left text-white xs:text-36 lg:text-48">
            {module.componentName}
          </h1>
          {module.showSearch && <Search config={config} />}
        </div>
        <img
          src={module.heroImage.url}
          alt={module.heroImage.description}
          // height={module.heroImage.height}
          // width={module.heroImage.width}
        />
      </div>
    );
  } else {
    return (
      <div className="div-hero-page min-h-[1000px]">
        <div className="div-title-hero-page">
          <h1 className="pl-5 pr-5 text-left text-white xs:text-36 pd:ml-0 pd:mr-0 lg:text-48"></h1>
        </div>
      </div>
    );
  }
}
