import { describe, expect, it, vi } from "vitest";
import { loadHeroWithSearch } from './backend';
import { render, screen } from '@testing-library/react';
import { loadBlogTeaserContent } from "../BlogTeaser/backend";


const obj = {
    loadHeroWithSearch: () => "loadHeroWithSearch",
}

vi.mock('./backend', () => ({
    loadHeroWithSearch: vi.fn().mockImplementation(() => "loadHeroWithSearch"),
  
}));

describe("fetching location data component unit test", () => {
 

    it('loadHeroWithSearch should be mock', () => {
        expect(vi.isMockFunction(loadHeroWithSearch)).toBe(true)
    });

    it('Should be executed loadHeroWithSearch', async () => {
        const spy = vi.spyOn(obj, 'loadHeroWithSearch');
        const resp = await loadHeroWithSearch("test");
        expect(spy.getMockName()).toEqual('loadHeroWithSearch');
        expect(resp).toBe("loadHeroWithSearch");
    });

});