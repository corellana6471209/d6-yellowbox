import { vi, describe, expect, it } from "vitest";
import React from "react";
import HeroWithSearch from ".";
import { render, screen } from "../../../utils/test-utils";

// Cache original functionality
const fetchFunction = {
  json: function () {
    return {
      data: {
        componentHeroCollection: {
          componentheroWithSearch: {
            componentName: "Home page component",
          },
        },
      },
      errors: null,
    };
  },
};
global.fetch = async () => {
  return fetchFunction;
};

describe("HeroWithSearch component FE", () => {
  afterEach(() => {
    vi.clearAllMocks();
  });

  it("Verify if the component renders", () => {
    render(<HeroWithSearch />);
  });
});
