import fetchCustomApi from "~/utils/fetchCustomApi";

export let loadHeroWithSearch = async (heroId: string) => {
  const query = `
      query componentHeroCollection {
          componentheroWithSearch(id: "${heroId}") {
            sys {
              id
            }
            componentName
            heroImage {
              title
              description
              contentType
              fileName
              size
              url
              width
              height
            }
            showSearch
            searchHelperText
            showAdvancedSearchLink
            searchButtonText
            searchActionUrl
          }
        }
      `;
  var theData = await fetchCustomApi(`/api/content/common/contentful/${query}`);
  return theData;
};
