import type { ComponentMeta } from '@storybook/react';
import heroImage from '/mocks/test-assets/home-page-image.jpeg'
import { withRouter } from 'storybook-addon-react-router-v6';
import HeroWithSearch from './index';

export default {
    title: 'modules/HeroWithSearch',
    component: HeroWithSearch,
    decorators: [withRouter],
} as ComponentMeta<typeof HeroWithSearch>;

const Template = args => <HeroWithSearch {...args} />;

export const HeroWithSearchModule = Template.bind({});
HeroWithSearchModule.args = {
    id: "hero-test",
    module: {
        componentName: "This is the component Name",
        searchActionUrl: "#",
        searchButtonText: "Find Locations",
        searchHelperText: "Helper Text",
        showAdvancedSearchLink: "false",
        showSearch: true,
        heroImage: {
          url: heroImage,
          description: 'image description',
        }
    }
};
