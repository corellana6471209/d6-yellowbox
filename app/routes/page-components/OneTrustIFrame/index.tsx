import React from "react";

export default function OneTrustIFrame() {
  return (
    <div className="flex w-full justify-center py-5">
      <iframe
        src="https://privacyportal-cdn.onetrust.com/dsarwebform/0949be23-d5a3-47b0-a8a5-8e3a5d23622a/e4db7014-dcf8-4e9a-af42-9c92e372587c.html"
        // style={{ width: "100%", height: "100%", border: "none" }}
        className="wide-wrapper min-h-screen border-2 border-black"
      ></iframe>
    </div>
  );
}
