import { describe, expect, it } from "vitest";
import { getLocations } from "../LocationMapList/backend";

const coords = { lat: 5, lng: 4 };
const radius = 25;
const fetchFunction = {
  json: function () {
    return {
      data: {
        contentTypeLocationCollection: {
          items: [
            {
              locationName: "WoodStack GA",
              locationImage: {
                title: "Woodstack Image",
                url: "https://images.ctfassets.net/rfh1327471ch/6xd4I8TX097heV11eeu15M/8fbca13c71c6b2083285b88b6450da5b/home-page-image.jpg",
              },
            },
          ],
        },
      },
      errors: null,
    };
  },
};
global.fetch = async () => {
  return fetchFunction;
};

describe("LocationList component BE", () => {
  it("Verify the name of the component", async () => {
    const data = await getLocations(coords, radius);
    expect(data.contentTypeLocationCollection.items[0].locationName).toBe(
      "WoodStack GA"
    );
  });

  it("Verify the image information", async () => {
    const data = await getLocations(coords, radius);
    expect(
      data.contentTypeLocationCollection.items[0].locationImage.title
    ).toBe("Woodstack Image");

    expect(data.contentTypeLocationCollection.items[0].locationImage.url).toBe(
      "https://images.ctfassets.net/rfh1327471ch/6xd4I8TX097heV11eeu15M/8fbca13c71c6b2083285b88b6450da5b/home-page-image.jpg"
    );
  });
});
