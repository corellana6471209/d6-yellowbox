import LocationListImage from "../../../common/LocationList/Image";
import TextLocationList from "../../../common/LocationList/Text";

/**
 * LocationListItem function
 * @param {string} image to the component
 * @param {any} address to the component
 * @param {number} stars to the component
 * @param {number} distance to the component
 * @param {string} direction to the component
 * @param {number} price to the component
 * @return {component} with data
 *
 */

export interface LocationListItemProps {
  id: number;
  locationName: string;
  image: string;
  address: any;
  googleBusinessProfileId: string;
  distance: string;
  price: number;
  link: string;
}

const LocationListItem = function LocationListItem({
  image,
  locationName,
  address,
  googleBusinessProfileId,
  distance,
  price,
  link,
}: // , link
LocationListItemProps) {
  return (
    <div className="location-list-item">
      <LocationListImage image={image} />

      <div className="location-list-item-body-text mt-2 mb-2 pl-3 pr-3 sm:pl-5 sm:pr-5">
        <TextLocationList
          address={address}
          locationName={locationName}
          googleBusinessProfileId={googleBusinessProfileId}
          distance={distance}
          price={price}
          link={link}
        />
      </div>
    </div>
  );
};

export default LocationListItem;
