import { describe, expect, it } from "vitest";
import { render, screen } from "../../../../utils/test-utils";
import { fakeDataBadge } from "../../../../utils/fakedataToTests";

import LocationListItem from ".";

describe("LocationListItem component unit test", () => {
  it("verify the render the LocationListItem component", () => {
    render(<LocationListItem {...fakeDataBadge} />);
  });

  describe("StateList component unit test every body text element", () => {
    it("Verify the title", async () => {
      render(<LocationListItem {...fakeDataBadge} />);
      expect(await screen.getAllByText(/Georgia, GA/i)[0]).toBeInTheDocument();
    });

    it("Verify if star element exist", async () => {
      const { container } = render(<LocationListItem {...fakeDataBadge} />);

      // Stars
      expect(
        await container.getElementsByClassName("star-ratings").length
      ).toBe(1);
    });

    it("Verify the distance", async () => {
      render(<LocationListItem {...fakeDataBadge} />);
      expect(await screen.findByText(/4.3 mi/i)).toBeInTheDocument();
    });

    it("Verify the direction", async () => {
      render(<LocationListItem {...fakeDataBadge} />);
      expect(
        await screen.findByText(/123 H. F. Shepherd Drive/i)
      ).toBeInTheDocument();
    });

    it("Verify the postal", async () => {
      render(<LocationListItem {...fakeDataBadge} />);
      expect(await screen.findByText(/Georgia, GA 30034/i)).toBeInTheDocument();
    });

    it("Verify the text price", async () => {
      render(<LocationListItem {...fakeDataBadge} />);
      expect(await screen.findByText(/Starting From/i)).toBeInTheDocument();
    });

    it("Verify the price", async () => {
      render(<LocationListItem {...fakeDataBadge} />);
      expect(await screen.findByText(/\$67\*/i)).toBeInTheDocument();
    });
  });
});
