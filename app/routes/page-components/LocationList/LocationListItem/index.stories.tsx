import type { ComponentStory, ComponentMeta } from '@storybook/react';
import LocationListItem from '.';
import testImage from '/mocks/test-assets/Comp_Teaser.png'


export default {
  title: 'components/core/LocationListItem',
  component: LocationListItem,
} as ComponentMeta<typeof LocationListItem>;

const Template: ComponentStory<typeof LocationListItem> = (args) => <LocationListItem {...args} />;

export const Primary = Template.bind({});
Primary.args = {
    id: 1,
    promotion: true,
    image: testImage,
    site: "Woodstock, GA",
    rating: 90,
    distance: 4.3,
    direction: "140 Emma Lane",
    postal: "Woodstock, GA 30189",
    price: 67,
    link: "https://www.google.com"
  };

  export const Secondary = Template.bind({});
  Secondary.args = {
    promotion: false,
    image: testImage,
    site: "Woodstock, GA",
    rating: 90,
    distance: 4.3,
    direction: "140 Emma Lane",
    postal: "Woodstock, GA 30189",
    price: 67
  };

  export const Tertiary = Template.bind({});
  Tertiary.args = {
    promotion: true,
    image: testImage,
    site: "Woodstock, GA",
    rating: 90,
    distance: 4.3,
    direction: "140 Emma Lane",
    postal: "Woodstock, GA 30189",
    price: 67
  };

  export const Quaternary = Template.bind({});
  Quaternary.args = {
    promotion: true,
    image: testImage,
    site: "Woodstock, GA",
    rating: 90,
    distance: 4.3,
    direction: "140 Emma Lane",
    postal: "Woodstock, GA 30189",
    price: 67
  };