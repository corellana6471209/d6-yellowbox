import React, { useState } from "react";
import LocationListItem from "./LocationListItem";
import { PageDetailsContext } from "../../../utils/context/PageDeatilsContext";
import { getAddressCoords } from "../../../utils/cookie";

import LoaderGrid from "~/routes/common/LoaderGrid";
//import { loaderLocationsNearBy } from "./backend";
import { useContext, useEffect } from "react";

import calculateDistance from "../../../utils/hooks/useDistance";

import {
  getLocations,
  getPrices,
  loadLocationList,
} from "../../../routes/page-components/LocationMapList/backend";
/**
 * LocationList function
 * @return {component} with data
 *
 */

const LocationList = function LocationList(props: any) {
  const [locations, setLocations] = useState<any>([]);
  const [isHydrated, setIsHydrated] = useState(false);
  const [itemsToShow, setItemsToShow] = useState<number>(4);
  const [config, setConfig] = useState<any>(null);
  const coords = getAddressCoords();
  const pageDetails = useContext(PageDetailsContext);

  useEffect(() => {
    if (pageDetails?.address?.address && !coords) {
      async function fetchData() {
        setIsHydrated(false);
        await getLocationsData(
          pageDetails?.address?.address,
          config?.numberOfRegisters
        );

        setIsHydrated(true);
      }

      fetchData();
    }

    if (pageDetails?.address?.address && coords) {
      async function fetchData() {
        setIsHydrated(false);
        await getLocationsData(coords, config?.numberOfRegisters);

        setIsHydrated(true);
      }
      fetchData();
    }
  }, [pageDetails?.address.address, config?.numberOfRegisters, coords]);

  useEffect(() => {
    async function fetchData() {
      const response = await loadLocationList(props.id);

      setConfig({
        title: response.componentlocationList?.title,
        feeText: response.componentlocationList?.feeText,
      });
      setIsHydrated(true);
    }
    fetchData();
  }, []);

  const getLocationsData = async (
    coords: any,
    numberOfRegisters: number = 4
  ) => {
    let loc: any[] = [];
    let radius = 25 / 0.62137;
    const dataCoords = JSON.parse(coords);

    // Try catch is used because on this code it is very easy to get an error
    try {
      do {
        let l = await getLocations(dataCoords, radius);
        loc = l.contentTypeLocationCollection.items;
        radius = radius + 25 / 0.62137;
      } while (loc.length === 0 && radius <= 250 / 0.62137);
      // add distance to array of locations
      for (let i = 0; i < loc.length; i++) {
        loc[i].distance = calculateDistance(
          null,
          null,
          loc[i].address.coordinates.lat,
          loc[i].address.coordinates.lon
        );
      }

      if (loc && loc.length > 0) {
        let locationsIDs: any[] | string = [];
        for (let i = 0; i < loc.length; i++) {
          locationsIDs.push(loc[i].locationId);
        }
        locationsIDs = locationsIDs.join(",");

        const prices = await getPrices(locationsIDs);
        for (let i = 0; i < loc.length; i++) {
          const priceFiltered = prices.filter(
            (data: any) => data.locationId == loc[i].locationId
          );
          if (priceFiltered[0] && priceFiltered[0].lowestPrice) {
            loc[i].price = priceFiltered[0].lowestPrice;
          }
        }
        // sort locations by distance
        loc.sort(function (a, b) {
          return a.distance - b.distance;
        });
      }

      setLocations(loc);
    } catch (error) {
      setLocations(loc.slice(0, numberOfRegisters));
    }
  };

  if (!isHydrated) {
    return (
      <>
        <LoaderGrid num={4} />
      </>
    );
  } else {
    if (locations && locations.length > 0) {
      return (
        <div className="div-location-list" id={props.id}>
          <div className="wide-wrapper">
            {locations.length > 0 && (
              <h1 className="title-location-list text-center font-serif text-24 text-greyHeavy md:text-28 lg:text-36">
                {config.title}
              </h1>
            )}

            <div className="grid grid-cols-1 gap-4 md:gap-10 lg:grid-cols-2">
              {locations
                .slice(0, itemsToShow)
                .map((location: any, index: any) => (
                  <>
                    <LocationListItem
                      id={location.locationId}
                      key={index}
                      locationName={location.locationName}
                      image={
                        location.locationImageCollection.items[0]
                          ? location.locationImageCollection.items[0].url
                          : ""
                      }
                      address={location.address}
                      googleBusinessProfileId={location.googleBusinessProfileId}
                      distance={location.distance}
                      price={location.price}
                      link={location.slug}
                    />
                  </>
                ))}
            </div>

            <div className="location-important-message mt-2 text-center sm:mt-6">
              <p className="text-10 text-greyHeavy md:text-12">
                <span className="relative text-20 text-callout">*</span>
                {config.feeText}
              </p>

              {itemsToShow < locations.length && (
                <div
                  className="mt-3 mb-3 md:mt-7 md:mb-7"
                  onClick={(e) => {
                    setItemsToShow(itemsToShow + 4);
                  }}
                >
                  <p className="cursor-pointer text-16 font-bold text-callout link md:text-24">
                    View More Locations
                  </p>
                </div>
              )}
            </div>
          </div>
        </div>
      );
    }
  }
};
export default LocationList;
