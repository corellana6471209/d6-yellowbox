import { describe, expect, it } from "vitest";
import { getPrices } from "../LocationMapList/backend";

const locationId = 5;
const fetchFunction = {
  json: function () {
    return {
      data: [26],
      errors: null,
    };
  },
};
global.fetch = async () => {
  return fetchFunction;
};

describe("LocationList prices component BE", () => {
  it("Verify the name of the component", async () => {
    const data = await getPrices(locationId);
    expect(data.data[0]).toBe(26);
  });
});
