import type { ComponentMeta, ComponentStory } from '@storybook/react';
import LocationList from './index';
import * as LocationListItemStories from './LocationListItem/index.stories';

export default {
    title: 'modules/LocationList',
    component: LocationList,
} as ComponentMeta<typeof LocationList>;

//const Template: ComponentStory<typeof LocationList> = (args) => <LocationList {...args} />;
const Template = args => <LocationList {...args} />;

export const LocationListTypical = Template.bind({});
LocationListTypical.args = {
    id: "hero-test",
    locations: [
        { ...LocationListItemStories.Primary.args },
        { ...LocationListItemStories.Secondary.args },
        { ...LocationListItemStories.Tertiary.args },
        { ...LocationListItemStories.Quaternary.args }
    ]
};