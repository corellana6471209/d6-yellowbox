import { json } from "@remix-run/node";
import fetchCustomApi from "~/utils/fetchCustomApi";

export const loaderLocationsNearBy = async (origin) => {
  // const client = new Client({});
  const service = new google.maps.DistanceMatrixService();

  const query = `
  {
      contentTypeLocationCollection(where: {AND: [{locationName_contains: "${origin}"}, {enabled: true}]}) {
        items {
          locationId
          locationImageCollection {
            items {
              title
              description
              contentType
              fileName
              size
              url
              width
              height
            }
          }
          
          address{
            city
            address1
            address2
            stateAbbreviation
            postalCode
            coordinates{
              lat
              lon
            }
          }
        }
      }
    }
  `;

  var pois = await fetchCustomApi(`/api/content/common/contentful/${query}`);

  //transform to Gmaps LatLng
  const nearPois = pois.contentTypeLocationCollection.items.map((poi) => {
    return new google.maps.LatLng(poi.address.coordinates);
  });

  //information to request the information from GMapsAPI
  const request = {
    origins: [origin],
    destinations: nearPois,
    //TBD: How to calculare the route correctly?
    //might be different distances between straight line or using GMaps DistanceMatrix
    //walking? Driving?
    travelMode: google.maps.TravelMode.DRIVING,
    //Miles in the US?
    unitSystem: google.maps.UnitSystem.IMPERIAL,
  };
  //after resolve execute callback (orderList)
  service.getDistanceMatrix(request, orderList);

  const poisDistance: any[] = [];
  let sortedPois = [];

  //to calculate from nearest to farthest locations nearby
  function orderList(response, status) {
    if (status == "OK") {
      var origins = response.originAddresses;
      var destinations = response.destinationAddresses;
      //from the origin ...
      for (var i = 0; i < origins.length; i++) {
        var results = response.rows[i].elements;
        //get the destination info
        for (var j = 0; j < results.length; j++) {
          var element = results[j];
          var distance = element.distance.value;
          poisDistance.push(distance);
        }
      }
    }
  }
  //Sorted ascending by distance(calculate by meters, by expresed by miles)
  sortedPois = poisDistance.sort((a, b) => a - b);
  return json({ sortedPois });
};

// const fake = [
//   {
//     id: 1,
//     promotion: true,
//     image: "https://via.placeholder.com/700x400",
//     site: "Woodstock, GA",
//     rating: 90,
//     distance: 4.3,
//     direction: "140 Emma Lane",
//     postal: "Woodstock, GA 30189",
//     price: 67,
//     link: "https://via.placeholder.com/700x400",
//   },

//   {
//     id: 2,
//     promotion: false,
//     image: "https://via.placeholder.com/700x400",
//     site: "Woodstock, GA",
//     rating: 90,
//     distance: 4.3,
//     direction: "140 Emma Lane",
//     postal: "Woodstock, GA 30189",
//     price: 67,
//     link: "https://via.placeholder.com/700x400",
//   },
// ];
