import { vi, describe, it } from "vitest";
import LocationList from ".";
import { render } from "../../../utils/test-utils";

vi.mock("./LocationListItem", () => {
  return {
    default: "location-list-component-string",
  };
});

describe("LocationList component FE", () => {
  it("Verify if the component renders", () => {
    render(<LocationList />);
  });
});
