import { sizeCategory } from "../../../utils/fakedataToTests";
import { getAddressCoords } from "../../../utils/cookie";
import formatNumberMyAccount from "../../../utils/formatNumberMyAccount";

const UnitDetails = (props: any) => {
  const { unit, openDetailsPaymentSection } = props;
  const coords = getAddressCoords();
  const phone = formatNumberMyAccount(unit.phoneNumberNewCustomer || null);

  const sizeCategoryText =
    unit && unit.category != null ? `${sizeCategory[unit.category]}", "` : "";
  const description = `${sizeCategoryText} ${
    unit.unitTypeName ? unit.unitTypeName : ""
  }`;

  const getDirection = () => {
    if (coords) {
      const dataCoords = JSON.parse(coords);
      window.open(
        `https://www.google.com/maps/dir/${dataCoords.lat},${dataCoords.lng}/${unit.address.coordinates.lat},${unit.address.coordinates.lon}/@${dataCoords.lat},${dataCoords.lng},13z`,
        "_blank"
      );
    } else {
      window.open(
        `https://www.google.com/maps/dir//${unit.address.coordinates.lat},${unit.address.coordinates.lon}/@${unit.address.coordinates.lat},${unit.address.coordinates.lon},14z/`,
        "_blank"
      );
    }
  };

  const redirectTo = (type: string = "detailsSection") => {
    openDetailsPaymentSection(type, unit);
  };

  const handleKeyPress = (e: any, type: string = "detailsSection") => {
    if (e.charCode == 13 && type != "direction") {
      openDetailsPaymentSection(type, unit);
      return;
    }

    if (e.charCode == 13 && type === "direction") {
      getDirection();
    }
  };

  return (
    <section className="unit-my-account-unit-details">
      <div className="grid grid-cols-1">
        <section className="div-section-blocks-unit-details relative mb-[40px]">
          <p className="text-40 font-bold text-secondary2">
            Unit {unit.unitName}
          </p>
          <p className="text-16 font-bold">
            {unit.width}’ X {unit.length}’
          </p>
          <div className="flex items-center justify-between">
            <p className="text-12 font-bold">{description}</p>

            {unit && unit.address && (
              <p className="text-16 font-bold sm:text-24">
                {unit.address.city}, {unit.address?.state?.stateAbbreviation}
              </p>
            )}
          </div>

          {unit.address && (
            <div className="div-section-blocks-unit-details-direction">
              <p className="mt-1 text-16 text-greyHeavy sm:text-24">
                {unit.address.address1}
              </p>
              <p className="text-16 text-greyHeavy sm:text-24">
                {unit.address.city}, {unit.address?.state?.stateAbbreviation}{" "}
                {unit.address.postalCode}
              </p>

              <p
                className="link mt-3 cursor-pointer text-16 font-bold text-callout sm:text-24"
                tabIndex={1}
                onClick={() => getDirection()}
                onKeyPress={(e) => handleKeyPress(e, "direction")}
              >
                Directions
              </p>
            </div>
          )}

          <div className="div-additional-information mt-8">
            <p className="text-28 font-bold">{unit.accessCode}</p>
            <p className="text-12 text-greyHeavy sm:text-20">Gate Code</p>
          </div>

          {phone && (
            <div className="div-additional-information mt-8">
              <div className="d-flex">
                <span className="span-code-number mr-1 text-12 font-bold sm:text-14">
                  ({phone.code})
                </span>
                <p className="text-20 font-bold sm:text-28">{phone.number}</p>
              </div>
              <p className="text-12 text-greyHeavy sm:text-20">Phone Number</p>
            </div>
          )}

          <div
            className={`${
              !unit.autoBillType && "div-additional-information"
            } mt-8`}
          >
            <p className="text-16 text-greyHeavy sm:text-20">
              Auto Pay:{" "}
              <span className="text-20 font-bold text-black">
                {unit.autoBillType ? "ON" : "OFF"}
              </span>
            </p>

            {!unit.autoBillType ? (
              <div className="div-button-to-pay mt-10">
                <p className="mb-10 text-14 font-bold text-greyHeavy sm:text-20">
                  Ready to setup Auto Pay? It's the simplest way to make sure
                  you never miss a payment.
                </p>

                <div className="div-button-unit-details d-flex mt-3 mb-3 justify-center	">
                  <button
                    className="yellow-button block bg-primary1 py-3 text-center text-secondary1 no-underline"
                    tabIndex={2}
                    onClick={() => redirectTo("paymentNormalAutoPay")}
                    onKeyPress={(e) =>
                      handleKeyPress(e, "paymentNormalAutoPay")
                    }
                  >
                    Set Up Auto Pay
                  </button>
                </div>

                <p className="mt-10 mb-10 text-14 text-greyHeavy sm:text-20">
                  Please note: Setting up Auto Pay for your rental account
                  requires a payment to be processed at the time of setup. Any
                  future Auto Pay payments will process on the agreed-upon
                  contract date.
                </p>
              </div>
            ) : (
              ""
            )}
          </div>
        </section>
        {/******************************************* */}

        {/******************************************* */}
      </div>
    </section>
  );
};

export default UnitDetails;
