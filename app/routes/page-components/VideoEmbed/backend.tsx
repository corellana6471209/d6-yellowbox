import fetchCustomApi from "~/utils/fetchCustomApi";

export async function getVideoEmbed(params: string) {
  const query = `
    query {
        componentvideoEmbed(id: "${params}") {
            title
            embedCode
        }
    }`;

  var theData = await fetchCustomApi(`/api/content/common/contentful/${query}`);
  return theData;
}
