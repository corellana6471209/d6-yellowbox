import { useEffect, useState } from "react";
import { getVideoEmbed } from "./backend";

export default function VideoEmbed(props: any) {
  const [videoEmbed, setVideoEmbed] = useState<any>(null);
  const [isHydrated, setIsHydrated] = useState(false);

  
  useEffect(() => {
    async function fetchData() {
      // You can await here
      const response = await getVideoEmbed(props.id);
      setVideoEmbed(response.componentvideoEmbed);
      setIsHydrated(true);
    }
    fetchData();
  }, [props.id]);

  const Schema = () => {
    if (videoEmbed) {
      return (
        <script
          type="application/ld+json"
          dangerouslySetInnerHTML={{
            __html: `
            {
              "@context": "https://schema.org",
              "@type": "VideoObject",
              "name": "${videoEmbed.title}",
              "description": "${videoEmbed.title}"
            }
          `,
          }}
        />
      )
    }
    return null;
  }

  if (isHydrated) {
    return (
      <>
        <Schema />
        <div className="video-embed mx-auto mt-10 mb-[30px] lg:mb-10 lg:mt-[70px]">
          <h1 className="mb-[30px] text-center font-serif text-28 lg:mb-10 lg:text-36">
            {videoEmbed.title}
          </h1>
          <div className="video-embed--wrapper d-flex justify-center">
            <div
              dangerouslySetInnerHTML={{
                __html: `
                ${videoEmbed.embedCode}
                `,
              }}
            />
          </div>
        </div>
      </>
    );
  } else {
    return <></>;
  }
}
