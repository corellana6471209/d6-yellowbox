import React from "react";
import { vi, describe, it } from "vitest";
import { BrowserRouter as Router } from "react-router-dom";
import Unit from ".";
import { render, screen } from "../../../utils/test-utils";
import { unitInformationMyAccount } from "../../../utils/fakedataToTests";

const mockSetState = vi.fn();
vi.mock("react", () => ({
  useState: (initial) => [initial, mockSetState],
  useRef: (initial) => [initial, mockSetState],
  useEffect: (initial) => [initial, mockSetState],
}));


describe("Unit-MyAccount component", () => {
  it("Verify if the component renders", () => {
    render(
      <Router>
        <Unit
          unit={unitInformationMyAccount}
        />
      </Router>
    );
  });

  it("Verify if div-img exists", async () => {
    const { container } = render(<Unit
      unit={unitInformationMyAccount}
    />);
    expect(await container.getElementsByClassName("div-svg").length).toBe(3);
  });


  it("Verify if divider exists", async () => {
    const { container } = render(<Unit
      unit={unitInformationMyAccount}
    />);
    expect(await container.getElementsByClassName("divider").length).toBe(1);
  });


  it("Verify if phoneNumber exists", async () => {
    render(
      <Unit
        unit={unitInformationMyAccount}
      />
    );

    expect(
      await screen.findByText('Phone Number:')
    ).toBeInTheDocument();
  });
});
