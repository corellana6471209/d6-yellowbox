import Map from "~/resources/map.png";
import Door from "~/resources/door.png";
import Card from "~/resources/card.png";
import { useMediaQuery } from "react-responsive";

import { sizeCategory } from "../../../utils/fakedataToTests";
import formatNumberMyAccount from "../../../utils/formatNumberMyAccount";
import { getAddressCoords } from "../../../utils/cookie";

const Unit = (props: any) => {
  const isMobile = useMediaQuery({ query: "(max-width: 768px)" });
  const coords = getAddressCoords();

  const { unit, openDetailsPaymentSection } = props;

  const phone = formatNumberMyAccount(unit.phoneNumberNewCustomer || null);

  const sizeCategoryText =
    unit && unit.category != null ? `${sizeCategory[unit.category]}", "` : "";
  const description = `${sizeCategoryText} ${
    unit.unitTypeName ? unit.unitTypeName : ""
  }`;

  const getDirection = () => {
    if (coords) {
      const dataCoords = JSON.parse(coords);
      window.open(
        `https://www.google.com/maps/dir/${dataCoords.lat},${dataCoords.lng}/${unit.address.coordinates.lat},${unit.address.coordinates.lon}/@${dataCoords.lat},${dataCoords.lng},13z`,
        "_blank"
      );
    } else {
      window.open(
        `https://www.google.com/maps/dir//${unit.address.coordinates.lat},${unit.address.coordinates.lon}/@${unit.address.coordinates.lat},${unit.address.coordinates.lon},14z/`,
        "_blank"
      );
    }
  };

  const redirectTo = (type: string = "detailsSection") => {
    openDetailsPaymentSection(type, unit);
  };

  const handleKeyPress = (e: any, type: string = "detailsSection") => {
    if (e.charCode == 13 && type != "direction") {
      openDetailsPaymentSection(type, unit);
      return;
    }

    if (e.charCode == 13 && type === "direction") {
      getDirection();
    }
  };

  return (
    <section className="unit-my-account mt-8 bg-primary3">
      <div className="grid grid-cols-1 md:grid-cols-2">
        <section className="div-section-blocks relative">
          <p className="text-40 font-bold text-secondary2">
            Unit {unit.unitName}
          </p>
          <div>
            <p className="font-bold">
              {unit.width}’ X {unit.length}’
            </p>
            <div className="flex items-center justify-between">
              <p className="text-12 font-bold">{description}</p>
              <p className="unit-location-name font-bold">
                {unit.locationName}
              </p>
            </div>
          </div>

          <div className="mt-7 flex justify-between">
            <div
              className="div-svg cursor-pointer"
              tabIndex={0}
              onClick={() => redirectTo()}
              onKeyPress={(e) => handleKeyPress(e, "detailsSection")}
            >
              <div className="d-flex justify-center">
                <img src={Door} width="80" alt="Manage My Unit" />
              </div>
              <p className="text-center text-12">Manage My Unit</p>
            </div>

            {unit && unit.address && (
              <div
                className="div-svg cursor-pointer"
                tabIndex={0}
                onClick={() => getDirection()}
                onKeyPress={(e) => handleKeyPress(e, "direction")}
              >
                <img src={Map} width="80" alt="Directions" />
                <p className="text-center text-12">Directions</p>
              </div>
            )}

            {!unit.autoBillType && (
              <div
                className="div-svg cursor-pointer"
                tabIndex={0}
                onClick={() => redirectTo("paymentNormal")}
                onKeyPress={(e) => handleKeyPress(e, "paymentNormal")}
              >
                <img src={Card} width="80" alt="Make Payment" />
                <p className="text-center text-12">Make Payment</p>
              </div>
            )}
          </div>

          <span className="divider"></span>
        </section>
        {/******************************************* */}

        {/******************************************* */}

        <section>
          <div className="div-section-blocks pb-0 md:pb-10">
            {isMobile && !unit.autoBillType ? (
              <div className="div-button mb-10">
                <p className="mb-10 text-center text-12 font-bold">
                  Ready to setup Auto Pay? It's the simplest way to make sure
                  you never miss a payment.
                </p>
                <div
                  className="div-button-auto-pay mt-3 mb-10"
                  tabIndex={0}
                  onClick={() => redirectTo("paymentNormalAutoPay")}
                  onKeyPress={(e) => handleKeyPress(e, "paymentNormalAutoPay")}
                >
                  <button className="yellow-button block bg-primary1 py-3 text-center text-secondary1 no-underline">
                    Set Up Auto Pay
                  </button>
                </div>

                <p className="text-center text-12">
                  Please note: Setting up Auto Pay for your rental account
                  requires a payment to be processed at the time of setup. Any
                  future Auto Pay payments will process on the agreed-upon
                  contract date.
                </p>
              </div>
            ) : (
              ""
            )}

            <div className="flex items-center justify-between">
              <div>
                <p>Gate code:</p>
                <p className="text-20 font-bold sm:text-28">
                  {unit.accessCode}
                </p>
              </div>

              {phone && (
                <div className="div-information">
                  <p>Phone Number:</p>
                  <div className="d-flex">
                    <span className="text-12 font-bold sm:text-14">
                      ({phone.code})
                    </span>
                    <p className="text-20 font-bold sm:text-28">
                      {phone.number}
                    </p>
                  </div>
                </div>
              )}
            </div>

            <div className="mt-5 mb-16 flex items-center justify-between">
              <p className="text-12 sm:text-16">
                Auto Pay:{" "}
                <span className="font-bold">
                  {unit.autoBillType ? "ON" : "OFF"}
                </span>
              </p>
              {unit && unit.address && (
                <p className="text-12 sm:text-16">
                  Location:{" "}
                  <span className="font-bold">
                    {unit.address.city},{" "}
                    {unit.address?.state?.stateAbbreviation}
                  </span>
                </p>
              )}
            </div>
          </div>

          {!isMobile && !unit.autoBillType ? (
            <div className="div-button mb-8 px-10 pt-10">
              <p className="mb-10 text-center text-12 font-bold">
                Ready to setup Auto Pay? It's the simplest way to make sure you
                never miss a payment.
              </p>
              <div
                className="div-button-auto-pay mt-3 mb-10"
                tabIndex={0}
                onClick={() => redirectTo("paymentNormalAutoPay")}
                onKeyPress={(e) => handleKeyPress(e, "paymentNormalAutoPay")}
              >
                <button className="yellow-button block bg-primary1 py-3 text-center text-secondary1 no-underline">
                  Set Up Auto Pay
                </button>
              </div>

              <p className="text-center text-12">
                Please note: Setting up Auto Pay for your rental account
                requires a payment to be processed at the time of setup. Any
                future Auto Pay payments will process on the agreed-upon
                contract date.
              </p>
            </div>
          ) : (
            ""
          )}
        </section>
      </div>
    </section>
  );
};

export default Unit;
