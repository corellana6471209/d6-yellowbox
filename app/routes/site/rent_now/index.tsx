import { LoaderArgs } from "@remix-run/server-runtime";
import { redirect } from "@remix-run/node";

export async function loader({ request }: LoaderArgs) {
  const url = new URL(request.url);
  const reservationId = url.searchParams.get("reservation_id");
  const locationId = url.searchParams.get("location_table_id");
  if (reservationId && locationId) return redirect(`/locations/make-payment/${locationId}/${reservationId}`);
  return redirect("/");
}