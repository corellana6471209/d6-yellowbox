import type { LoaderFunction } from "@remix-run/node";
import { prepareFetch } from "~/routes/api/shared/prepareSimplyFetch";

export const loader: LoaderFunction = async ({ params }) => {  
  // here is where the real fetch is happening
  
  const locationId = params.locationId;
  const data = await prepareFetch("GET",
    `/Payment/PaymentTypesRetrieve?locationId=${locationId}`
  );
  return data;
};

//why make two request?
// first request in client side, asking to the server side make the real fetch 
// this $locationId is server side
// the api folder is server side and every folder is the path 