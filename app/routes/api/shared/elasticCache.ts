import { LoaderFunction } from "@remix-run/server-runtime";
import { getUserIdentificator } from "../../../utils/cookie";
const Memcached = require("memcached");

var memcached: any = 0;
var timeInSeconds: number = 0;
function start(): LoaderFunction {
  try {
    const url = process.env.MEMCACHE_URL || "localhost:11211";
    const TIMEOUT = process.env.MEMCACHE_TIMEOUT || "10000";
    const IDLE = process.env.MEMCACHE_TIMEOUT || "10000";
    const defaultValue = "30";
    const minutes = parseInt(process.env.SESSION_TTL_MINUTES || defaultValue);
    const minute = 60;
    timeInSeconds = minute * minutes;
    const memcached = new Memcached(url, {
      idle: 1000,
      timeout: parseInt(TIMEOUT),
      poolSize: 2,
      retries: 2,
      failures: 2,
      retry: 5000,
    });
    if (memcached != undefined) {
      memcached.on("failure", function (details: any) {
        console.error(
          "Server " +
            details.server +
            "went down due to: " +
            details.messages.join("")
        );
      });
      memcached.on("reconnecting", function (details: any) {
        console.debug(
          "Total downtime caused by server " +
            details.server +
            " :" +
            details.totalDownTime +
            "ms"
        );
      });
    }
    return memcached;
  } catch (error) {
    console.log("error start", error);
  }
  return memcached;
}
function getToken(request: Request) {
  const cookie = request.headers.get("Cookie");
  return getUserIdentificator(cookie || " ");
}
export async function setCacheValueByKey(
  key: string,
  value: any,
  request: Request | null,
  time: number = timeInSeconds
): Promise<LoaderFunction> {
  memcached = start();
  try {
    const token = request && request.headers ? getToken(request) : "";
    const fullKey = `${key}-${token}`;
    return new Promise((resolve, reject) => {
      memcached.set(
        fullKey,
        value,
        timeInSeconds,
        function (err: any, data: any) {
          if (err) {
            console.error("Error in Memcache SET", err);
            reject(new Error(err));
          }
          console.log("Memcache SET data", data);
          console.log("Memcache SET key", fullKey);
          resolve(data);
        }
      );
    });
  } finally {
    memcached.end();
  }
}

export async function getCacheValueByKey(
  key: string,
  request: Request | null
): Promise<LoaderFunction> {
  memcached = start();
  try {
    const token = request && request.headers ? getToken(request) : "";
    const fullKey = `${key}-${token}`;
    return new Promise((resolve, reject) => {
      memcached.get(fullKey, function (err: any, data: any) {
        if (err) {
          console.error("Error in Memcache GET", err);
          reject(new Error(err));
        }
        console.log("Memcache GET data", data);
        console.log("Memcache GET key", fullKey);
        resolve(data);
      });
    });
  } finally {
    memcached.end();
  }
}
