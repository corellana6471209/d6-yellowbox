// import getSecrets from "./awsSecrets";

export interface responseObject {
  err: boolean;
  errText?: string;
  data?: any;
}
function isJSON(str) {
    try {
        return (JSON.parse(str) && !!str);
    } catch (e) {
        return false;
    }
}
export const prepareFetch = async (
  method: string,
  url: string,
  body: any = null,
  toJson: any = false
) => {
  try {
    // const gateway = await getSecrets("GateWayApiKey");
    const gateway = process.env.API_GATEWAY_KEY || "notDefined";
    const token = gateway || process.env.SIMPLY_AUTH || "notDefined";
    const simplyUrl = process.env.BACK_END_API_URL || "notDefined";
    const myHeaders = new Headers();

    myHeaders.append("SimplyAuth", token);
    myHeaders.append("Content-Type", "application/json");

    const requestOptions = {
      method: method,
      headers: myHeaders,
      body: body,
    };
    const response: responseObject = {
      err: false,
    };

    const fetchResponse = await fetch(simplyUrl + url, requestOptions)
      .then((response) => ( ( response.status < 300 ) ? response.text() : response.json()))
      .then((result) => {
        return result;
      })
      .catch((error: Error) => {
        response.err = true;
        response.errText = error.message;
      });

    response.data = isJSON(fetchResponse) ? JSON.parse(fetchResponse.toString()) : fetchResponse;
    return response;
  } catch (err) {
    console.log("err", err);
  }

  console.log("fetched");
};
