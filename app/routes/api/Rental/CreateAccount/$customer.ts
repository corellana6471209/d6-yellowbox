import type { LoaderFunction } from "@remix-run/node";
import { prepareFetch } from "~/routes/api/shared/prepareSimplyFetch";

export const loader: LoaderFunction = async ({ params }) => {  
  //REMOVE ME
  const information = params.information || "";
  if (!information) {
    return null;
  }
  const informationArray = information.split(",");
  
    const url = `/Authentication/CreateAccount`
    const method = "POST"

    //looking forward to get the params
    const locationId = params.locationId;

  const data = await prepareFetch(method,url, JSON.stringify({
    emailAddress: `${informationArray[0]}`,
    password: `${informationArray[1]}`,
    phoneNumber: `${informationArray[2]}`,
    firstName: `${informationArray[3]}`,
    lastName: `${informationArray[4]}`,
  }));
  return data;
};

//why make two request?
// first request in client side, asking to the server side make the real fetch 
// this $locationId is server side
// the api folder is server side and every folder is the path 