import type { LoaderFunction } from "@remix-run/node";
import { prepareFetch } from "~/routes/api/shared/prepareSimplyFetch";

export const loader: LoaderFunction = async ({ params }) => {
    // Looking foward to get the params
    const { emailAddress } = params

    const url = `/Authentication/IsLegacyUser?emailAddress=${emailAddress}`
    const method = "GET"

    const data = await prepareFetch(method, url);
    return data;
};