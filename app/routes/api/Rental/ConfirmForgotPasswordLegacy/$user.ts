import type { LoaderFunction } from "@remix-run/node";
import { prepareFetch } from "~/routes/api/shared/prepareSimplyFetch";

export const loader: LoaderFunction = async ({ params }) => {
    const information = params.information || "";
    if (!information) {
        return null;
    }
    const informationArray = information.split(",");

    const url = `/Authentication/ConfirmForgotPasswordLegacy`
    const method = "POST"

    const data = await prepareFetch(method, url, JSON.stringify({
        resetGuid: `${informationArray[0]}`,
        newPassword: `${informationArray[1]}`,
    }));
    return data;
};