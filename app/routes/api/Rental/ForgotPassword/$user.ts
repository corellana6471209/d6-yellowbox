import type { LoaderFunction } from "@remix-run/node";
import { prepareFetch } from "~/routes/api/shared/prepareSimplyFetch";

export const loader: LoaderFunction = async ({ params }) => {
    const { emailAddress, isLegacy } = params
    const information = params.information || "";
    if (!information) {
        return null;
    }
    const informationArray = information.split(",");

    const url = `/Authentication/ForgotPassword?emailAddress=${emailAddress}&isLegacy=${isLegacy}`
    const method = "POST"

    //looking forward to get the params
    const locationId = params.locationId;

    const data = await prepareFetch(method, url, JSON.stringify({
        emailAddress: `${informationArray[0]}`,
        isLegacy: `${informationArray[1]}`,

    }));
    return data;
};