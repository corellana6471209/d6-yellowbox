import type { LoaderFunction } from "@remix-run/node";
import { prepareFetch } from "../../shared/prepareSimplyFetch";

export const loader: LoaderFunction = async ({ params }) => {
  //not try to make the request if there is no locations values
  const information = params.information || "";
  if (!information) {
    return null;
  }
  const informationArray = information.split(",");

  const url = "/Rental/MoveInCostRetrieveWithDiscountReservation";

  let body = {};

  if (informationArray[3] == "null" || !informationArray[3]) {
    body = JSON.stringify({
      locationId: `${informationArray[0]}`,
      unitId: `${informationArray[1]}`,
      moveInDate: `${informationArray[2]}`,
      concessionPlanId: `${informationArray[4]}`,
      reservationId: `${informationArray[5]}`,
    });
  } else {
    body = JSON.stringify({
      locationId: `${informationArray[0]}`,
      unitId: `${informationArray[1]}`,
      moveInDate: `${informationArray[2]}`,
      insuranceCoverageId: `${informationArray[3]}`,
      concessionPlanId: `${informationArray[4]}`,
      reservationId: `${informationArray[5]}`,
    });
  }



  const value = await prepareFetch("POST", url, body);

  // await stupidAwait();
  return value;
};

//to make this request fail any time just turn on stupid await
