import type { LoaderFunction } from "@remix-run/node";
import { prepareFetch } from "~/routes/api/shared/prepareSimplyFetch";

export const loader: LoaderFunction = async ({ params }) => {
    const information = params.information || "";
    if (!information) {
        return null;
    }
    const informationArray = information.split(",");

    const url = `/Authentication/LoginTenant`
    const method = "POST"

    //looking forward to get the params
    const locationId = params.locationId;

    const data = await prepareFetch(method, url, JSON.stringify({
        EmailAddress: `${informationArray[0]}`,
        Password: `${informationArray[1]}`,
        IsLegacyUser: `${informationArray[2]}`,
    }));
    return data;
};