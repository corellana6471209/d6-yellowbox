import type { LoaderFunction } from "@remix-run/node";
import { prepareFetch } from "~/routes/api/shared/prepareSimplyFetch";

export const loader: LoaderFunction = async ({ params }) => {
  const { EmailAddress } = params;
  if (!EmailAddress) {
    return null;
  }
  const url = `/CustomerProfile/GetTenantByEmail?emailAddress=${EmailAddress}`
  const method = "GET"

  // here is where the real fetch is happening


  const data = await prepareFetch(method, url);
  return data;
};