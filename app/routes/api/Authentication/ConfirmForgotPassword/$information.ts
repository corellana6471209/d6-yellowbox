import { prepareFetch } from "~/routes/api/shared/prepareSimplyFetch";
import type { LoaderFunction } from "@remix-run/node";

export const loader: LoaderFunction = async (params: any) => {
    if (!params || !params.emailAddress || !params.newPassword || !params.passwordConfirmationCode) {
        return null;
    }

    const url = `/Authentication/ConfirmForgotPassword`
    const method = "POST"


    const data = await prepareFetch(method, url, JSON.stringify({
        emailAddress: params.emailAddress,
        newPassword: params.newPassword,
        passwordConfirmationCode: params.passwordConfirmationCode,
    }));

    return data;
};