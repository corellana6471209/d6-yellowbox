import type { LoaderFunction } from "@remix-run/node";
import { prepareFetch } from "~/routes/api/shared/prepareSimplyFetch";

export const loader: LoaderFunction = async ({ params }) => {
  const customer = params.customer || "";
  if (!customer) {
    return null;
  }

  const url = `/Authentication/CreateAccount`;
  const method = "POST";

  const data = await prepareFetch(method, url, JSON.stringify(customer), false);

  return data;
};
