import type { LoaderFunction } from "@remix-run/node";
import { prepareFetch } from "~/routes/api/shared/prepareSimplyFetch";

export const loader: LoaderFunction = async ({ params }) => {
  const information = params.information || "";

  
  if (!information) {
    return null;
  }


  const url = `/CustomerProfile/TenantBillingInfoUpdate`;
  const method = "POST";

  const data = await prepareFetch(
    method,
    url,
    JSON.stringify(information),
    false
  );
  return data;
};
