import type { LoaderFunction } from "@remix-run/node";
import { prepareFetch } from "~/routes/api/shared/prepareSimplyFetch";

export const loader: LoaderFunction = async ({ params }) => {
  const { locationId, waitingListId } = params;
  if (!locationId || !waitingListId) {
    return null;
  }
  const url = `/Authentication/IsExistingUser?locationId=${locationId}&reservationId=${waitingListId}`
  const method = "GET"

  // here is where the real fetch is happening


  const data = await prepareFetch(method, url);
  return data;
};