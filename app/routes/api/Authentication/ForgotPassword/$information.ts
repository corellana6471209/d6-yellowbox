import { prepareFetch } from "~/routes/api/shared/prepareSimplyFetch";
import type { LoaderFunction } from "@remix-run/node";

export const loader: LoaderFunction = async (params: any) => {
  if (!params || !params.emailAddress) {
    return null;
  }

  const url = `/Authentication/ForgotPassword?emailAddress=${params.emailAddress}&isLegacy=${params.isLegacy}`;
  const method = "POST";

  const data = await prepareFetch(method, url);

  return data;
};
