import type { LoaderFunction } from "@remix-run/node";
import { prepareFetch } from "~/routes/api/shared/prepareSimplyFetch";

export const loader: LoaderFunction = async ({ params }) => {
  // Looking foward to get the params
  const emailAddress = params.emailAddress || "";
  if (!emailAddress) {
    return null;
  }


  const url = `/Authentication/IsLegacyUser?emailAddress=${encodeURIComponent(
    emailAddress
  )}`;
  const method = "GET";

  const data = await prepareFetch(method, url);
  return data;
};
