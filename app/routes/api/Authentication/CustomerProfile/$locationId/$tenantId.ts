import type { LoaderFunction } from "@remix-run/node";
import { prepareFetch } from "~/routes/api/shared/prepareSimplyFetch";

export const loader: LoaderFunction = async ({ params }) => {
  const { locationId, tenantId } = params;
  if (!locationId || !tenantId) {
    return null;
  }
  const url = `/CustomerProfile/TenantInfoByTenantId?locationId=${locationId}&tenantId=${tenantId}`
  const method = "GET"

  // here is where the real fetch is happening


  const data = await prepareFetch(method, url);
  return data;
};