import { prepareFetch } from "~/routes/api/shared/prepareSimplyFetch";
import type { LoaderFunction } from "@remix-run/node";

export const loader: LoaderFunction =  async (params: any) => {
    if (!params || !params.newPassword || !params.resetGuid) {
        return null;
    }

    const url = `/Authentication/ConfirmForgotPasswordLegacy`
    const method = "POST"

    const data = await prepareFetch(method, url, JSON.stringify({
        resetGuid: params.resetGuid,
        newPassword: params.newPassword,
    }));

    return data;
};