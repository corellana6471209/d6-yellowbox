import { prepareFetch } from "~/routes/api/shared/prepareSimplyFetch";
import type { LoaderFunction } from "@remix-run/node";

export const loader: LoaderFunction = async (params: any) => {
    if (!params || !params.resetGuid) {
        return null;
    }

    const { resetGuid } = params;
    const url = `/Authentication/IsResetGuidExpired?guid=${resetGuid}`
    const method = "GET"

    const data = await prepareFetch(method, url);
    return data;
};