import { handlePageData } from "~/models/component.server";

export function loader({ params }) {
  // get slug from params
  const slug = params["*"];
  if (slug && slug.startsWith("_static")) {
    throw new Response("Not Found", {
      status: 404,
    });
  }
  // homepage doesnt have a slug so we have to get it another way
  let w = `{title: "Home Page"}`;
  if (params["*"]) {
    w = `{slug: "${slug}"}`;
  }
  const page = handlePageData(w);
  return page;
}
