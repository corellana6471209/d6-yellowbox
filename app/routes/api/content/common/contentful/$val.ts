import type { LoaderFunction } from "@remix-run/node";
import fetchContentful from "~/utils/fetchContentful";

export let loadContenfulFromAPI = async (query:string) => {
  var theData = await fetchContentful(query);
  return theData;
};


export const loader: LoaderFunction = async ({ params }) => {
  const query = params.val;
  var theData = await loadContenfulFromAPI(query);
  return theData;
};
