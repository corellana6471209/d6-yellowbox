import type { LoaderFunction } from "@remix-run/node";
import contentfulKeys from "~/utils/config";

export const loader: LoaderFunction = async ({ params }) => {
  const val = params.val;
  const coords = await fetch(
    `https://maps.googleapis.com/maps/api/place/details/json?placeid=${val}&key=${contentfulKeys.googleApiKey}`
  )
    .then((response: any) => {
      return response;
    })
    .catch(() => {
      return null;
    });

  return coords;
};
