import type { LoaderFunction } from "@remix-run/node";
import axios from "axios";
import contentfulKeys from "~/utils/config";

export const loader: LoaderFunction = async ({ params }) => {
  const val = params.val;
  const coords = await axios
    .get(
      `https://maps.googleapis.com/maps/api/geocode/json?address=${val}&key=${contentfulKeys.googleApiKey}`
    )
    .then((response: any) => {
      return response.data;
    })
    .catch(() => {
      return null;
    });

  return coords;
};
