import type { LoaderFunction } from "@remix-run/node";
import axios from "axios";
import contentfulKeys from "~/utils/config";


export const loader: LoaderFunction = async ({ params }) => {
  const val = params.val;
  const autocomplete = await axios.get(
    `https://maps.googleapis.com/maps/api/place/autocomplete/json?input=${val}&key=${contentfulKeys.googleApiKey}&components=country:us`
  )
    .then((theResponse: any) => {
      return theResponse.data;
    })
    .catch((error) => {
      console.log("Error returned from Google Maps API Service Call: ",error)
      return error;
    });
  // console.log("Returned value from Google Maps API Service Call: ", autocomplete);
  return autocomplete;
};
