import type { LoaderFunction } from "@remix-run/node";
import { prepareFetch } from "../shared/prepareSimplyFetch";

export const loader: LoaderFunction = async (params: any) => {
  if (!params || !params.locationid || !params.tenantid) {
    return null;
  }

  const url = `/Reservation/reservationlistbytenantid?locationid=${params.locationid}&tenantid=${params.tenantid}`;
  const value = await prepareFetch("GET", url, null);

    // await stupidAwait();
  return value;
};

//to make this request fail any time just turn on stupid await
//if you want to make the request fail but no retrying  just change the url for a wrong one
