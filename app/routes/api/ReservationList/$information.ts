import type { LoaderFunction } from "@remix-run/node";
import { prepareFetch } from "../shared/prepareSimplyFetch";

export const loader: LoaderFunction = async ({ params }) => {
  //not try to make the request if there is no locations values
  const information = params.information || "";
  if (!information) {
    return null;
  }
  const informationArray = information.split(",");

  const url = `/Reservation/reservationlist?locationid=${informationArray[0]}&waitingListId=${informationArray[1]}&version=${informationArray[2]}`;
  const value = await prepareFetch("GET", url, null);

  // await stupidAwait();
  return value;
};

//to make this request fail any time just turn on stupid await
//if you want to make the request fail but no retrying  just change the url for a wrong one
