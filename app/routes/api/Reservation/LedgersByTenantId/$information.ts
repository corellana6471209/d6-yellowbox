import type { LoaderFunction } from "@remix-run/node";
import { prepareFetch } from "~/routes/api/shared/prepareSimplyFetch";

export const loader: LoaderFunction = async ({ params }) => {
    const information = params.information || "";

    if (!information) {
        return null;
    }

    const informationArray = information.split(",");
    
    const url = `/Payment/LedgersByTenantId?locationid=${informationArray[0]}&tenantid=${informationArray[1]}`;
    const method = "GET";

    const data = await prepareFetch(
        method,
        url
    );
    return data;
};