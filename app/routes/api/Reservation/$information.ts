import type { LoaderFunction } from "@remix-run/node";
import { prepareFetch } from "../shared/prepareSimplyFetch";

export const loader: LoaderFunction = async ({ params }) => {
  //not try to make the request if there is no locations values
  const information = params.information;
  if (!information) {
    return null;
  }

  const url = "/Reservation/MakeReservation";
  const value = await prepareFetch("POST", url, JSON.stringify(information));

  // await stupidAwait();
  return value;
};

//to make this request fail any time just turn on stupid await
//if you want to make the request fail but no retrying  just change the url for a wrong one
