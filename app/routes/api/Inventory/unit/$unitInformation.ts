import type { LoaderFunction } from "@remix-run/node";
import { prepareFetch } from "../../shared/prepareSimplyFetch";

export const loader: LoaderFunction = async ({ params }) => {
  const location = params.unitInformation;
  const url =
    "/Inventory/UnitsInformationLocalByLocation?locationid=" + location;
  const value = await prepareFetch("GET", url, null);
  return value;
};
