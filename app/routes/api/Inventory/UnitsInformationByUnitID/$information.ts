import type { LoaderFunction } from "@remix-run/node";
import { prepareFetch } from "~/routes/api/shared/prepareSimplyFetch";

interface payload {
  locationid: string;
  unitid: string;
}
export const loader: LoaderFunction = async ({ params }) => {
  const information = params.information || "";
  let payload: payload = {
    locationid: "0",
    unitid: "0",
  };
  if (params.information) {
    const informationArray = information.split(",");
    payload.locationid = informationArray[0];
    payload.unitid = informationArray[1];
  } else if (params.locationid && params.unitid) {
    payload.locationid = params.locationid;
    payload.unitid = params.unitid;
  } else {
    return null;
  }

  const url = `/Inventory/UnitsInformationByUnitID?locationid=${payload.locationid}&unitid=${payload.unitid}`;
  const method = "GET";

  const data = await prepareFetch(method, url);
  return data;
};
