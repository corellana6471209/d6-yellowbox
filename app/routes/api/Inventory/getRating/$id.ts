import type { LoaderFunction } from "@remix-run/node";
const axios = require("axios").default;

export const loader: LoaderFunction = async ({ params }) => {
  //not try to make the request if there is no locations values
  const id = params.id || "";
  if (!id) {
    return null;
  }
  const rating = await axios
    .get(
      `https://maps.googleapis.com/maps/api/place/details/json?placeid=${id}&key=AIzaSyCpbHnhHkqRiS6jS7aU0kRge3C47ESjxws`
    )
    .then((response: any) => {
      if (response.data.status !== "INVALID_REQUEST") {
        return response.data.result.rating;
      }
    })
    .catch(function (error: any) {
      console.log("ERROR: ", error);
    });
  // await stupidAwait();
  return rating ? rating : null;
};
