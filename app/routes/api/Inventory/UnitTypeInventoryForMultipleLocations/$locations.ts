import type { LoaderFunction } from "@remix-run/node";
import { prepareFetch } from "../../shared/prepareSimplyFetch";

// async function stupidAwait() {
//   return new Promise((resolve, reject) => {
//     for (var i = 0; i < 999999999; i++) {
//       console.log("jaja");
//     }
//     resolve(true);
//   });
// }
export const loader: LoaderFunction = async ({ params }) => {
  //not try to make the request if there is no locations values
  const locations = params.locations || "";
  if (!locations) {
    return null;
  }
  const locationsArray = locations.split(",");
  const url = "/Inventory/UnitTypeInventoryForMultipleLocations";
  const value = await prepareFetch("POST", url, JSON.stringify(locationsArray));
  // await stupidAwait();
  return value;
};

//to make this request fail any time just turn on stupid await
//if you want to make the request fail but no retrying  just change the url for a wrong one
