import type { LoaderFunction } from "@remix-run/node";
import { prepareFetch } from "../../shared/prepareSimplyFetch";

export const loader: LoaderFunction = async () => {
  const url = "/Inventory/AllLocationsWithState";
  const value = await prepareFetch("GET", url, null);
  return value;
};
