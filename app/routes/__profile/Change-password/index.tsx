import type { ActionFunction, LoaderArgs } from "@remix-run/node";
import { getCacheValueByKey } from "~/routes/api/shared/elasticCache";
import { prepareFetch } from "~/routes/api/shared/prepareSimplyFetch";

import ChangePasswordForm from '../../page-components/ChangePasswordForm';
import validateChangePasswordForm from "../../page-components/ChangePasswordForm/validateChangePasswordForm";

export async function loader({ params, request }: LoaderArgs) {
  const token = await getCacheValueByKey("accessToken", request);
  return {token};
}

export const action: ActionFunction = async ({ request }) => {
  const body = await request.formData();
  const oldPassword = body.get("oldPassword") as string;
  const newPassword = body.get("newPassword") as string;
  const errors = validateChangePasswordForm({ oldPassword, newPassword });
  
  if (!errors.isValid) return { frontErrors: errors.frontErrors };

  const emailAddress = await getCacheValueByKey("userEmail", request);
  const idToken = await getCacheValueByKey("idToken", request);  
  const refreshToken = await getCacheValueByKey("refreshToken", request);
  const accessToken = await getCacheValueByKey("accessToken", request);
  const expirationTime = "2023-08-04T21:43:23.979Z";
  const issuedTime = "2023-08-04T20:43:23.979Z";
  const payload = {
    "emailAddress": emailAddress,
    "newPassword": newPassword,
    "oldPassword": oldPassword,
    "userSession": {
      "idToken": idToken,
      "accessToken": accessToken,
      "refreshToken": refreshToken,
      "expirationTime": expirationTime,
      "issuedTime": issuedTime,
    }
  }
  const loged = await prepareFetch('POST', '/Authentication/ChangePassword', JSON.stringify(payload));
  if (loged?.err) return { showModal: true, redirect: false, message: 'Your password change has failed. Please try again. Contact the Call Center at (877) 786-7343 if you continue to have issues.' };
  return { showModal: true, redirect: true, message: 'You successfully changed your password. Log in using your new password'};
};

const BreadcrumbList = () => {
  try {
    if (window) {
      const domain = window.location.hostname
      return (
        <script
          type="application/ld+json"
          dangerouslySetInnerHTML={{
            __html: `
            {
              "@context": "https://schema.org",
              "@type": "BreadcrumbList",
              "itemListElement": [{
                "@type": "ListItem",
                "position": 1,
                "name": "Home",
                "item": "https://${domain}/"
              },{
                "@type": "ListItem",
                "position": 2,
                "name": "Change Password",
                "item": "https://${domain}/change-password/"
              }]
            }
          `,
          }}
        />
      )
    }
    return null;
  } catch (error) {
    return null
  }
}

const ChangePassword = () => {
  return (
    <>
      <BreadcrumbList />
      <ChangePasswordForm />
    </>
  );
};

export default ChangePassword;