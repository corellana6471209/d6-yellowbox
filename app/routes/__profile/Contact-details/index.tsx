import React, { useState, useEffect } from "react";
import { getLabel } from "../../../utils/getLabel";

const BreadcrumbList = () => {
  try {
    if (window) {
      const domain = window.location.hostname
      return (
        <script
          type="application/ld+json"
          dangerouslySetInnerHTML={{
            __html: `
            {
              "@context": "https://schema.org",
              "@type": "BreadcrumbList",
              "itemListElement": [{
                "@type": "ListItem",
                "position": 1,
                "name": "Home",
                "item": "https://${domain}/"
              },{
                "@type": "ListItem",
                "position": 2,
                "name": "Contact details",
                "item": "https://${domain}/contact-details/"
              }]
            }
          `,
          }}
        />
      )
    }
    return null;
  } catch (error) {
    return null
  }
}

const ContactDetails = () => {
  const [text, setText] = useState<any>(
    "Manage contact details is coming soon. Until then, please email us at digital@simplyss.com and request a specific change."
  );

  useEffect(() => {
    async function fetchData() {
      const contactDetails = await getLabel("contactDetails");
      setText(contactDetails ? contactDetails : text);
    }
    fetchData();
  }, []);

  return (
    <>
      <BreadcrumbList />
      <div className="d-flex contact-details-tabs-page justify-center text-24">
        <span className="max-w-[900px]">{text}</span>
      </div>
    </>
  );
};

export default ContactDetails;
