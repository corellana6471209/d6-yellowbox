import { redirect } from "@remix-run/node";
import type { ActionFunction, LoaderFunction } from "@remix-run/node";

import PasswordResetCodeForm from "../page-components/PasswordResetCodeForm";
import * as checkResetCode from "../api/Authentication/CheckResetCode/$information";
import * as passwordResetCode from "../api/Authentication/ConfirmForgotPasswordLegacy/$information";
import validatePasswordResetCodeForm from "../page-components/PasswordResetCodeForm/validatePasswordResetCodeForm";

import Footer from "~/routes/common/Footer";
import Header from "~/routes/common/Header";

export const loader: LoaderFunction = async ({ request }) => {
  const url = new URL(request.url);
  const resetKey = url.searchParams.get("ResetKey") as string;

  const params: any = {
    resetGuid: resetKey,
  };
  const response = await checkResetCode.loader(params);

  if (response.data.isExpired) return redirect('/password-expired-code');
  return { resetKey };
};

export const action: ActionFunction = async ({ request }) => {
  const body = await request.formData();
  const newPassword = body.get("newPassword") as string;
  const resetKey = body.get("resetKey") as string;

  const slug = body.get("slug") as string;
  const reservation = body.get("reservation") as string;
  const islegacy = body.get("islegacy") as string;

  const errors = validatePasswordResetCodeForm({
    password: newPassword,
    resetKey,
  });

  if (!errors.isValid) return { frontErrors: errors.frontErrors };

  const params: any = { newPassword, resetGuid: resetKey };
  const response = await passwordResetCode.loader(params);

  if (!response || response.err) {
    return { showModal: true, message: "The reset key is invalid, please get another key." };
  }

  if (response.errText && response.errText.includes("Reset key expired")) {
    return {
      showModal: true,
      message: "Reset key expired.",
    };
  }

  if (response.errText && response.errText.includes("No reset r")) {
    return {
      showModal: true,
      message: "The reset key is invalid, please get another key.",
    };
  }

  //if (response?.err) return { serverError: response?.errText };

  if (response.data && response.data.message === "Service Unavailable") {
    return { showModal: true };
  } else {
    if (slug && reservation) {
      return redirect(
        `/login?slug=${slug}&reservation=${reservation}&islegacy=${
          islegacy === "true" ? true : false
        }`
      );
    } else {
      return redirect("/login");
    }
  }
};

const PasswordReset = () => {
  return (
    <>
      <Header />
      <PasswordResetCodeForm />
      <Footer />
    </>
  );
};

export default PasswordReset;
