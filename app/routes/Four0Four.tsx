import Button from "~/routes/common/Button";
import Header from "~/routes/common/Header";
import Footer from "~/routes/common/Footer";
import FourOFour from "~/resources/404-desktop.png";

export default function Four0Four() {
  return (
    <>
      <Header />
      <div className="mx-auto max-w-[1200px]">
        <div className="mt-10 mb-5 flex justify-center">
          <img src={FourOFour} />
        </div>
        <div className="mb-10 flex justify-center">
          <Button href="/" title="Return Home" buttonStyle="button-primary" />
        </div>
      </div>
      <Footer />
    </>
  );
}
