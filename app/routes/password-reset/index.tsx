import { redirect } from "@remix-run/node";
import { LoaderArgs } from "@remix-run/server-runtime";

import type { ActionFunction } from "@remix-run/node";
import PasswordResetForm from "../page-components/PasswordResetForm";
import * as passwordReset from "../api/Authentication/ConfirmForgotPassword/$information";
import validatePasswordResetForm from "../page-components/PasswordResetForm/validatePasswordResetForm";

import Footer from "~/routes/common/Footer";
import Header from "~/routes/common/Header";

export async function loader({ request }: LoaderArgs) {
  const url = new URL(request.url);
  const slug = url.searchParams.get("slug");
  const reservation = url.searchParams.get("reservation");
  const islegacy = url.searchParams.get("islegacy");

  return { slug, reservation, islegacy };
}

export const action: ActionFunction = async ({ request }) => {
  const body = await request.formData();
  const emailAddress = body.get("email") as string;
  const newPassword = body.get("newPassword") as string;
  const verificationCode = body.get("verificationCode") as string;

  const slug = body.get("slug") as string;
  const reservation = body.get("reservation") as string;
  const islegacy = body.get("islegacy") as string;

  const errors = validatePasswordResetForm({
    email: emailAddress,
    password: newPassword,
    verificationCode,
  });
  if (!errors.isValid) return { frontErrors: errors.frontErrors };

  const params: any = {
    emailAddress: emailAddress,
    newPassword: newPassword,
    passwordConfirmationCode: verificationCode,
  };

  const response = await passwordReset.loader(params);


  if (response.errText && response.errText.includes("Invalid co")) {
    return {
      showModal: true,
      message: "Invalid code provided, please request a code again.",
    };
  }

  if (response.errText && response.errText.includes("Invalid ve")) {
    return {
      showModal: true,
      message: "Invalid verification code provided, please try again.",
    };
  }

  if (response.errText && response.errText.includes("Invalid ve")) {
    return {
      showModal: true,
      message: "Invalid verification code provided, please try again.",
    };
  }
  if (response.errText && response.errText.includes("Tenant wit")) {
    return {
      showModal: true,
      message:
        "Tenant with this email address does not have an existing or past rental",
    };
  }

  if (
    (response.data && response.data.message === "Service Unavailable") ||
    (response?.err &&
      !response.errText.includes("Unexpected end of JSON input"))
  ) {
    return { showModal: true, message: 'Invalid verification code provided, please try again' };
  } else {
    if (slug && reservation) {
      return redirect(
        `/login?slug=${slug}&reservation=${reservation}&islegacy=${
          islegacy === "true" ? true : false
        }`
      );
    } else {
      return redirect("/login");
    }
  }

  //if (response?.err) return { serverError: response?.errText };

  return "";
};

const PasswordReset = () => {
  return (
    <>
      <Header />
      <PasswordResetForm />
      <Footer />
    </>
  );
};

export default PasswordReset;
