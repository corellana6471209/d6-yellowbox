import { redirect } from "@remix-run/node";
import type { ActionFunction } from "@remix-run/node";
import { LoaderArgs } from "@remix-run/server-runtime";

import ForgotPasswordForm from "../page-components/ForgotPasswordForm";
import * as forgotPassword from "../api/Authentication/ForgotPassword/$information";
import * as isLegacy from "../api/Authentication/IsLegacy/$emailAddress";
import validateForgotPasswordForm from "../page-components/ForgotPasswordForm/validateForgotPasswordForm";
import { getCacheValueByKey } from "../api/shared/elasticCache";
import Footer from "~/routes/common/Footer";
import Header from "~/routes/common/Header";

export async function loader({ params, request }: LoaderArgs) {
  const token = await getCacheValueByKey("accessToken", request);
  const user = await getCacheValueByKey("tenantInfo", request);

  const url = new URL(request.url);
  const slug = url.searchParams.get("slug");
  const reservation = url.searchParams.get("reservation");
  const islegacy = url.searchParams.get("islegacy");

  return { token,user, slug, reservation, islegacy };
}

export const action: ActionFunction = async ({ request }) => {
  const body = await request.formData();
  const emailAddress = body.get("email") as string;

  const slug = body.get("slug") as string;
  const reservation = body.get("reservation") as string;
  const islegacy = body.get("islegacy") as string;
  const errors = validateForgotPasswordForm({ email: emailAddress });
  if (!errors.isValid) return { frontErrors: errors.frontErrors };

  const params: any = {
    emailAddress: encodeURIComponent(emailAddress)
  };

  if (!islegacy) {
    const data: any = { params: { emailAddress: emailAddress } };
    const legacyData = await isLegacy.loader(data);

    if (!legacyData || legacyData.err || !legacyData?.data.isLegacyUser) {
      params.isLegacy = false;
    } else {
      params.isLegacy = true;
    }
  } else {
    if (islegacy && islegacy !== "false") {
      params.isLegacy = true;
    } else {
      params.isLegacy = false;
    }
  }

  const response = await forgotPassword.loader(params);

  //Verify error by text, the API is returning me plain/text

  if (response.err || (response.data && response.data === 'User does not exist in Cognito')) {
    return { showModal: true, message: "Your email address information does not match our records. Try reentering your information again." };
  }

  if (response.data && response.data.message === "Service Unavailable") {
    return { showModal: true };
  } else {
    if (slug && reservation) {
      //Add new variable to isLegacy = true, You have been received one link to complete the process.

      if (params.isLegacy) {
        return {
          showModal: true,
          message:
            "Your password reset email will be sent to the email address associated with your account. Once your password is reset you may log in.",
        };
      } else {
        return redirect(
          `/password-reset?slug=${slug}&reservation=${reservation}&islegacy=${false}`
        );
      }
    } else {
      if (params.isLegacy) return ({
        showModal: true,
        message: "Your password reset email will be sent to the email address associated with your account. Once your password is reset you may log in."
      });
      else {
        return redirect("/password-reset");
      }
    }
  }
};

const ForgotPassword = () => {
  return (
    <>
      <Header />
      <ForgotPasswordForm />
      <Footer />
    </>
  );
};

export default ForgotPassword;
