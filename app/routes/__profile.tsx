import { useState, useEffect } from "react";
import { Outlet } from "@remix-run/react";
import { useLoaderData } from "@remix-run/react";
import Header from "./common/Header";
import Footer from "./common/Footer";
import Tabs from "./common/Tabs";
import Tab from "./common/Tabs/Tab";
import Title from "./common/Title";
import Spinner from "./common/Spinner";
import { getCacheValueByKey } from "./api/shared/elasticCache";
import { LoaderArgs } from "@remix-run/server-runtime";

export async function loader({ params, request }: LoaderArgs) {
  const token = await getCacheValueByKey("accessToken", request);
  const user = await getCacheValueByKey("tenantInfo", request);
  return { user, token };
}

const Profile = () => {
  const inf = useLoaderData();
  const [verifyingToken, setVerifyingToken] = useState(true);

  useEffect(() => {
    if (!inf.token) {
      window.location.href = "/login";
      return;
    } else {
      setVerifyingToken(false);
    }
  }, []);

  if (verifyingToken) {
    return <Spinner />;
  } else {
    return (
      <>
        <Header />
        <div className="my-profile-tabs">
          <div style={{ marginTop: "20px", marginBottom: "30px" }}>
            <Title text="My Profile" />
          </div>
          <Tabs>
            <Tab title="Contact Details" to="/contact-details" />
            <Tab title="Change Password" to="/change-password" />
          </Tabs>
        </div>
        <Outlet />
        <Footer />
      </>
    );
  }
};

export default Profile;
