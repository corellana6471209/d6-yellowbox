import type { LoaderFunction } from "@remix-run/node";
import { useState, useEffect } from "react";

// Custom compontents
import Header from "../common/Header";
import Footer from "../common/Footer";
import { setCacheValueByKey } from "../api/shared/elasticCache";

export const loader: LoaderFunction = async ({ request }) => {
  await setCacheValueByKey("accessToken", null, request);
  await setCacheValueByKey("units", null, request);
  await setCacheValueByKey("unit", null, request);
  return null;
};

const Logout = () => {
  const [timer, setTimer] = useState<number>(5);

  useEffect(() => {
    if (timer > 0) countDown();
    else window.location.href = "/";
  }, [timer]);

  const countDown = () => setTimeout(() => setTimer(timer - 1), 1000);

  return (
    <>
      <Header />
      <div
        style={{
          height: "500px",
          display: "flex",
          flexDirection: "column",
          justifyContent: "center",
          alignItems: "center",
        }}
      >
        <h3>Logged out!</h3>
        <h3>Redirecting in... {timer}</h3>
      </div>
      <Footer />
    </>
  );
};

export default Logout;
