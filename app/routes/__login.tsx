import {  useEffect } from "react";
import { Outlet } from "@remix-run/react";

import Header from "./common/Header";
import Footer from "./common/Footer";
import Tabs from "./common/Tabs";
import Tab from "./common/Tabs/Tab";

import { setCookie, deleteCookie } from "../utils/cookie";
import { getLabel } from "../utils/getLabel";

import contentfulKeys from "../utils/config";

const Login = () => {
  useEffect(() => {
    async function fetchCookie() {
      if (window && window.location.pathname !== "/") {
        deleteCookie(contentfulKeys.constants.lastPage);
        const days = await getLabel("cookieTimeDays");
        setCookie(
          contentfulKeys.constants.lastPage,
          window.location.href,
          days ? parseInt(days) : 30
        );
      }
    }

    fetchCookie();
  }, []);

  return (
    <>
      <Header />
      <div className='mx-auto lg:w-[1200px]'>
        <Tabs>
          <Tab title="Login" to="/login" />
          <Tab title="Find Reservation" to="/reservation" />
        </Tabs>
        <Outlet />
      </div>
      <Footer />
    </>
  );
};

export default Login;
