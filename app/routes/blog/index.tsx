import type { LoaderArgs } from "@remix-run/server-runtime";
import { handlePageData } from "../../models/component.server";
import { getCacheValueByKey } from "../api/shared/elasticCache";
//import { run,read } from "../api/shared/elasticCache";
export * from "./$";
export { default } from "./$";
import type { MetaFunction } from "@remix-run/node";

export async function loader({ params, request }: LoaderArgs) {
  const token = await getCacheValueByKey("accessToken", request);
  const user = await getCacheValueByKey("tenantInfo", request);
  const slug = `{slug: "blog"}`;
  const page = await handlePageData(slug);
  page.token = token;
  page.user = user;
  return page;
}

export const meta: MetaFunction = ({ data }) => {
  if (data?.pageCollection?.items?.length > 0) {
    return {
      title: `${data.pageCollection.items[0].seoTitle}`,
      description: `${data.pageCollection.items[0].seoDescription}`,
      robots: `${data.pageCollection.items[0].robotsMetaSettings}`,
      "og:image": `${
        data.pageCollection.items[0].pageImage
          ? data.pageCollection.items[0].pageImage.url
          : ""
      }`,
    };
  } else {
    return {};
  }
};
