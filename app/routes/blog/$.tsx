import { useState } from "react";
import { handlePageData, handleRedirectData } from "~/models/component.server";
import type { LoaderArgs } from "@remix-run/server-runtime";
import Page from "../common/Page";
import { redirect } from "@remix-run/node";

// import { json } from "@remix-run/node"; // or cloudflare/deno
import { useLoaderData, useLocation } from "@remix-run/react";
import { getCacheValueByKey } from "../api/shared/elasticCache";
import Four0Four from "../Four0Four";
import { promiseHash } from "remix-utils";
import { PageDetailsContext } from "../../utils/context/PageDeatilsContext";
import type { MetaFunction } from "@remix-run/node";
import { getCacheValueByKey } from "../api/shared/elasticCache";

export const meta: MetaFunction = ({ data }) => {
  if (data?.pageCollection?.items?.length > 0) {
    const param = data.pageCollection.items[0];
    const location = useLocation();
    const domain = "simplyss.com";
    return {
      title: param?.seoTitle,
      description: param?.seoDescription,
      robots: param?.robotsMetaSettings,
      datePublished: param?.sys.publishedAt,
      headline: param?.seoTitle,
      image: param?.pageImage?.url,

      // Facebook
      "og:image": param?.pageImage?.url,
      "og:url": `${domain}${location.pathname}`,
      "og:type": param?.type,
      "og:title": param?.seoTitle,
      "og:description": param?.seoDescription,

      // Twitter
      "twitter:card": param?.pageImage?.description,
      "twitter:domain": domain,
      "twitter:url": `${domain}${location.pathname}`,
      "twitter:title": param?.seoTitle,
      "twitter:description": param?.seoDescription,
      "twitter:image": param?.pageImage?.url,
    };
  } else {
    return {};
  }
};
export async function loader({ params, request }: LoaderArgs) {
  // get slug from params
  const slug = params["*"];
  const BlogPath = `/blog/${params["*"]}`;
  const user = await getCacheValueByKey("tenantInfo", request);
  const token = await getCacheValueByKey("accessToken", request);

  const sitelinkUserAndTenantInfo = getCacheValueByKey(
    "sitelinkUserAndTenantInfo",
    request
  );
  if (slug && slug.startsWith("_static")) {
    throw new Response("Not Found", {
      status: 404,
    });
  }
  // homepage doesnt have a slug so we have to get it another way
  let w = `{title: "Blog Page"}`;
  if (params["*"]) {
    w = `{slug_contains: "${slug}"}`;
  }

  let n = `{title: "Blog Page"}`;
  if (params["*"]) {
    n = `{slug_contains: "${BlogPath}"}`;
  }

  const page = await handlePageData(w);
  if (
    !page ||
    !page.pageCollection ||
    !page.pageCollection.items ||
    page.pageCollection.items.length == 0
  ) {
    const redirectData = await handleRedirectData(n);
    if (redirectData?.redirectCollection?.items.length === 0) {
      // return redirect("/_static/404.html", 404);
      page.token = token;
      page.slug = slug;
      page.sitelinkUserAndTenantInfo = sitelinkUserAndTenantInfo;
      return promiseHash(page);
    } else {
      return redirect(
        redirectData.redirectCollection.items[0].newPath,
        redirectData.redirectCollection.items[0].redirectCode
      );
    }
  } else {
    page.token = token;
    page.slug = slug;
    page.sitelinkUserAndTenantInfo = sitelinkUserAndTenantInfo;
    page.user = user;
    return promiseHash(page);
  }
  // if (!page || !page.pageCollection || !page.pageCollection.items) {
  //   throw new Response("Not Found", {
  //     status: 404,
  //   });
  // }
  // page.user = user;
  // page.token = token;
  // return promiseHash(page);
}

export default function Index() {
  const pageData = useLoaderData();

  const components =
    pageData?.pageCollection?.items[0]?.componentsCollection?.items;
  const title = pageData?.pageCollection?.items[0]?.title;
  const categories = pageData?.pageCollection?.items[0]?.categories;
  const pageImage = pageData?.pageCollection?.items[0]?.pageImage;

  const tags = pageData?.pageCollection?.items[0]?.tags;
  const publishedAt = pageData?.pageCollection?.items[0]?.sys.publishedAt;
  const displayBlogHero = pageData?.pageCollection?.items[0]?.displayBlogHero;
  const slug = pageData?.pageCollection?.items[0]?.slug;
  const token = pageData?.token;

  const pageName = title?.toLowerCase()?.replace(/ /g, "-");
  const displayHeaderAndFooter =
    pageData?.pageCollection?.items[0]?.displayHeaderAndFooter;
  const [address, setAddress] = useState(null);
  //   return 404 if page doesnt exist
  if (pageData?.pageCollection?.items?.length < 1) {
    return <Four0Four />;
  }

  const pageDetails = {
    title,
    slug: pageName,
    categories: categories,
    showBlogHero: displayBlogHero,
    publishedAt: publishedAt,
    pageImage: pageImage,
    tags: tags,
    displayHeader: displayHeaderAndFooter ? displayHeaderAndFooter : false,
    address: {
      address,
      setAddress,
    },
  };

  const Schema = () => {
    if (pageData?.pageCollection?.items[0]) {
      try {
        const param = pageData?.pageCollection?.items[0];
        const domain = window.location.hostname;
        return (
          <script
            type="application/ld+json"
            dangerouslySetInnerHTML={{
              __html: `
              {
                "@context": "https://schema.org",
                "@graph": [
                    {
                        "@type": "BlogPosting",
                        "@id": "https://${domain}/blog/${param.slug}",
                        "name": "${param.title}",
                        "headline": "${param.seoDescription}",
                        "author": {
                            "@id": "https://${domain}/blog/author/etucker/#author"
                        },
                        "publisher": {
                            "@id": "https://${domain}/blog/#organization"
                        },
                        "image": {
                            "@type": "ImageObject",
                            "url": "${param.pageImage.url}",
                            "width": ${param.pageImage.width},
                            "height": ${param.pageImage.height}
                        },
                        "datePublished": "${param.publishDate}",
                        "dateModified": "${param.sys.publishedAt}",
                        "inLanguage": "en-US",
                        "mainEntityOfPage": {
                            "@id": "https://${domain}/blog/${param.slug}"
                        },
                        "isPartOf": {
                            "@id": "https://${domain}/blog/${param.slug}"
                        },
                        "articleSection": "${param.categories.map(
                          (cat: any, index: any) => {
                            return `${cat} `;
                          }
                        )}"
                    }
                ]
            }
            `,
            }}
          />
        );
      } catch (error) {
        return null;
      }
    }
    return null;
  };

  const BreadcrumbList = () => {
    try {
      if (window) {
        const domain = window.location.hostname;
        return (
          <script
            type="application/ld+json"
            dangerouslySetInnerHTML={{
              __html: `
              {
                "@context": "https://schema.org",
                "@type": "BreadcrumbList",
                "itemListElement": [{
                  "@type": "ListItem",
                  "position": 1,
                  "name": "Home",
                  "item": "https://${domain}/"
                },{
                  "@type": "ListItem",
                  "position": 2,
                  "name": "Blog List",
                  "item": "https://${domain}/blog/"
                },{
                  "@type": "ListItem",
                  "position": 3,
                  "name": "Blog"
                }]
              }
            `,
            }}
          />
        );
      }
      return null;
    } catch (error) {
      return null;
    }
  };

  return (
    <PageDetailsContext.Provider value={pageDetails}>
      <main className={pageName}>
        <Schema />
        <BreadcrumbList />
        <div className="content-wrapper mx-auto">
          <Page components={components} slug={slug} token={token} />
        </div>
      </main>
    </PageDetailsContext.Provider>
  );
}
