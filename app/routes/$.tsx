import { useState } from "react";
import type { MetaFunction } from "@remix-run/node";
import { useLoaderData } from "@remix-run/react";
import { handlePageData, handleRedirectData } from "~/models/component.server";
import type { DynamicLinksFunction } from "remix-utils";
import Four0Four from "./Four0Four";
import Page from "./common/Page";
import type { LoaderArgs } from "@remix-run/server-runtime";
import { redirect } from "@remix-run/node";

import { promiseHash } from "remix-utils";
import { PageDetailsContext } from "~/utils/context/PageDeatilsContext";
import { getCacheValueByKey } from "./api/shared/elasticCache";

export const meta: MetaFunction = ({ data }) => {
  if (data?.pageCollection?.items?.length > 0) {
    return {
      title: `${data.pageCollection.items[0].seoTitle}`,
      description: `${data.pageCollection.items[0].seoDescription}`,
      robots: `${data.pageCollection.items[0].robotsMetaSettings}`,
      "og:image": `${
        data.pageCollection.items[0].pageImage
          ? data.pageCollection.items[0].pageImage.url
          : ""
      }`,
    };
  } else {
    return {};
  }
};

// let dynamicLinks: DynamicLinksFunction = ({ data }) => {
//   if (data.pageCollection.items.length > 0) {
//     // if (!data.user) return [];
//     return [
//       {
//         rel: "canonical",
//         href: data.pageCollection.items[0].canonicalUrl
//           ? data.pageCollection.items[0].canonicalUrl
//           : "",
//       },
//     ];
//   }
// };

// export let handle = { dynamicLinks };

export async function loader({ params, request }: LoaderArgs) {
  const token = await getCacheValueByKey("accessToken", request);
  const sitelinkUserAndTenantInfo = getCacheValueByKey(
    "sitelinkUserAndTenantInfo",
    request
  );
  const user = await getCacheValueByKey("tenantInfo", request);

  // get slug from params
  const slug = params["*"];
  if (slug && (slug.startsWith("_static") || slug.startsWith("favicon.ico"))) {
    throw new Response("Not Found", {
      status: 404,
    });
  }
  // homepage doesnt have a slug so we have to get it another way
  let w = `{title: "Home Page"}`;
  if (params["*"]) {
    w = `{slug_contains: "${slug}"}`;
  }

  let n = `{title: "Home Page"}`;
  if (params["*"]) {
    n = `{slug_contains: "/${slug}"}`;
  }

  const page = await handlePageData(w);
  if (
    !page ||
    !page.pageCollection ||
    !page.pageCollection.items ||
    page.pageCollection.items.length == 0
  ) {
    const redirectData = await handleRedirectData(n);
    if (redirectData?.redirectCollection?.items.length === 0) {
      // return redirect("/_static/404.html", 404);
      page.token = token;
      page.slug = slug;
      page.sitelinkUserAndTenantInfo = sitelinkUserAndTenantInfo;
      return promiseHash(page);
    } else {
      return redirect(
        redirectData.redirectCollection.items[0].newPath,
        redirectData.redirectCollection.items[0].redirectCode
      );
    }
  } else {
    page.token = token;
    page.slug = slug;
    page.sitelinkUserAndTenantInfo = sitelinkUserAndTenantInfo;
    page.user = user;
    return promiseHash(page);
  }
}

export default function Index() {
  const pageData = useLoaderData();
  const components =
    pageData?.pageCollection?.items[0]?.componentsCollection?.items;
  const slug = pageData?.pageCollection?.items[0]?.slug;
  const token = pageData?.token;

  const title = pageData?.pageCollection?.items[0]?.title;
  const pageName = title?.toLowerCase()?.replace(/ /g, "-");
  const displayHeaderAndFooter =
    pageData?.pageCollection?.items[0]?.displayHeaderAndFooter;
  const displayBlogHero = pageData?.pageCollection?.items[0]?.displayBlogHero;
  const pageCategories = pageData?.pageCollection?.items[0]?.categories;
  const pagePublishDate = pageData?.pageCollection?.items[0]?.sys?.publishedAt;

  const [address, setAddress] = useState(null);

  const pageDetails = {
    title,
    slug: pageName,
    displayHeader: displayHeaderAndFooter ? displayHeaderAndFooter : false,
    address: {
      address,
      setAddress,
    },
    showBlogHero: displayBlogHero,
    pageCategories: pageCategories,
    publishedAt: pagePublishDate,
  };

  //   return 404 if page doesnt exist
  if (pageData?.pageCollection?.items?.length < 1) {
    return <Four0Four />;
  }

  return pageDetails ? (
    <PageDetailsContext.Provider value={pageDetails}>
      <main className={pageName}>
        <Page components={components} slug={slug} token={token} />
      </main>
    </PageDetailsContext.Provider>
  ) : null;
}
