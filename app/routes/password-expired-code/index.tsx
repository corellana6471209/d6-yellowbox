import { redirect } from "@remix-run/node";
import type { ActionFunction } from "@remix-run/node";
import { LoaderArgs } from "@remix-run/server-runtime";

import PasswordExpiredCodeForm from '../page-components/PasswordExpiredCodeForm';
import * as forgotPassword from "../api/Authentication/ForgotPassword/$information";
import * as isLegacy from "../api/Authentication/IsLegacy/$emailAddress";
import validatePasswordExpiredCodeForm from "../page-components/PasswordExpiredCodeForm/validatePasswordExpiredCodeForm";
import { getCacheValueByKey } from "../api/shared/elasticCache";
import Footer from "~/routes/common/Footer";
import Header from "~/routes/common/Header";


export async function loader({ params, request }: LoaderArgs) {
  const token = await getCacheValueByKey("accessToken", request);
  const user = await getCacheValueByKey("tenantInfo", request);

  const url = new URL(request.url);
  const slug = url.searchParams.get("slug");
  const reservation = url.searchParams.get("reservation");
  const islegacy = url.searchParams.get("islegacy");

  return { token,user, slug, reservation, islegacy };
}

export const action: ActionFunction = async ({ request }) => {
  const body = await request.formData();
  const emailAddress = body.get("email") as string;

  const slug = body.get("slug") as string;
  const reservation = body.get("reservation") as string;
  const islegacy = body.get("islegacy") as string;

  const errors = validatePasswordExpiredCodeForm({ email: emailAddress });
  if (!errors.isValid) return { frontErrors: errors.frontErrors };

  const params: any = {
    emailAddress: encodeURIComponent(emailAddress)
  };

  if (!islegacy) {
    const data: any = { params: { emailAddress: emailAddress } };
    const legacyData = await isLegacy.loader(data);

    if (!legacyData || legacyData.err || !legacyData?.data.isLegacyUser) {
      params.isLegacy = false;
    } else {
      params.isLegacy = true;
    }
  } else {
    if (islegacy && islegacy !== "false") {
      params.isLegacy = true;
    } else {
      params.isLegacy = false;
    }
  }

  const response = await forgotPassword.loader(params);

  if (!response) {
    return { showModal: true };
  }

  //Verify error by text, the API is returning me plain/text

  if (
    response.errText &&
    (response.errText.includes("User does") ||
      (response.errText && response.errText.includes("Exception")))
  ) {
    return { showModal: true, message: "This user does not exists." };
  }

  if (response.data && response.data.message === "Service Unavailable") {
    return { showModal: true };
  } else {
    if (slug && reservation) {
      //Add new variable to isLegacy = true, You have been received one link to complete the process.

      if (params.isLegacy) {
        return {
          showModal: true,
          message:
            "You have been received one link in your email to complete the process.",
        };
      } else {
        return redirect(
          `/password-reset?slug=${slug}&reservation=${reservation}&islegacy=${false}`
        );
      }
    } else {
      if (params.isLegacy) return ({
        showModal: true,
        message: "You have been received one link in your email to complete the process."
      });
      else {
        return redirect("/password-reset");
      }
    }
  }
};

const PasswordExpiredCode = () => {
  return (
    <>
      <Header />
      <PasswordExpiredCodeForm />
      <Footer />
    </>
  );
};

export default PasswordExpiredCode;