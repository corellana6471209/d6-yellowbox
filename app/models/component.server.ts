import fetchContentful from "../utils/fetchContentful";
// initial page data fetch handler
export async function handlePageData(params: string) {
  const query = `
  query PageCollection {
      pageCollection(where: ${params}) {
          items {
              type
              visibility
              displayHeaderAndFooter
              displayBlogHero
              title
              slug
              seoTitle
              categories
              tags
              seoDescription
              seoKeywords
              canonicalUrl
              robotsMetaSettings
              excludePostFromRelatedList
              publishDate
              pageImage {
                title
                description
                contentType
                fileName
                size
                url
                width
                height
              }
              sys {
                id
                publishedAt
              }
          componentsCollection(limit: 25) {
              items {
                  __typename
                      ... on Entry {
                          sys{
                              id
                              publishedAt
                          }
                      }
                  }
              }
          }
      }
  }
  `;
  return await fetchContentful(query);
}

export async function handleNavigationData(params: string) {
  const query = `
  query NavigationCollection {
    navigationCollection(where: ${params}) {
      items {
        pageLinksCollection(limit: 10) {
          items {
            __typename
            ... on ExternalLink {
              linkText
              externalLink
            }
            ... on PageLink {
              linkText
              pageLink {
                slug
              }
              linkIcon {
                title
                description
                contentType
                fileName
                size
                url
                width
                height
              }
            }
          }
        }
      }
    }
  }
  `;
  return await fetchContentful(query);
}

export async function handleRedirectData(params: string) {
  const query = `
  query RedirectCollection {
      redirectCollection(where: ${params}) {
          items {
            slug,
            newPath,
            redirectCode
          }
      }
  }
  `;
  return await fetchContentful(query, true);
}
