/**
 * getApiPath
 * @param {string} name of cookie
 * @param {string} value value of cookie
 * @param {number} days value of cookie
 * @returns {url} url base for api fetching
 */
export function setCookie(name: string, value: any, days: any = null) {
  var expires = "";
  if (days) {
    var date = new Date();
    date.setTime(date.getTime() + days * 24 * 60 * 60 * 1000);
    expires = "; expires=" + date.toUTCString();
  } else {
    expires = "; expires=Thu, 01-Jan-70 00:00:01 GMT;";
  }
  document.cookie = name + "=" + (value || "") + expires + "; path=/";
}

export function deleteCookie(name: string) {
  document.cookie = name + "=; Path=/; Expires=Thu, 01 Jan 1970 00:00:01 GMT;";
}

/**
 * getApiPath
 * @param {string} name of cookie
 
 * @returns {cookie} cookie
 */
export function getUserIdentificator(string = "") {
  if (string) {
    const cookie = (string.match(
      /^(?:.*;)?\s*simplyUser\s*=\s*([^;]+)(?:.*)?$/
    ) || [, null])[1];
    return cookie;
  }
  const cookie = (document.cookie.match(
    /^(?:.*;)?\s*simplyUser\s*=\s*([^;]+)(?:.*)?$/
  ) || [, null])[1];
  return cookie;
}
export function getAddress() {
  const cookie = (document.cookie.match(
    /^(?:.*;)?\s*address\s*=\s*([^;]+)(?:.*)?$/
  ) || [, null])[1];
  return cookie;
}

export function getFirstRender() {
  const cookie = (document.cookie.match(
    /^(?:.*;)?\s*firstRender\s*=\s*([^;]+)(?:.*)?$/
  ) || [, null])[1];
  return cookie;
}

export function getAddressCoords() {
  const cookie = (document.cookie.match(
    /^(?:.*;)?\s*coords\s*=\s*([^;]+)(?:.*)?$/
  ) || [, null])[1];
  return cookie;
}

export function getLastPage() {
  const cookie = (document.cookie.match(
    /^(?:.*;)?\s*lastPage\s*=\s*([^;]+)(?:.*)?$/
  ) || [, null])[1];
  return cookie;
}

export function getRedirectVariables() {
  const cookie = (document.cookie.match(
    /^(?:.*;)?\s*redirectVariables\s*=\s*([^;]+)(?:.*)?$/
  ) || [, null])[1];
  return cookie;
}
