import { bool } from "aws-sdk/clients/signer";
import { createContext } from "react";

interface PageDetails {
  title: string;
  slug: string;
  categories: any;
  tags: any;
  pageImage: any;
  displayHeader: boolean;
  showBlogHero: bool;
  address: {
    address: string;
    setAddress: any;
  };
}

export const PageDetailsContext = createContext<PageDetails | null>(null);
