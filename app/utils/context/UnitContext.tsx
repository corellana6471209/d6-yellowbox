import { createContext } from "react";

interface UnitDetails {
  qtyUnitUrgencyMsg: number;
  unitUrgencyMsg: string;
}

export const UnitDetailsContext = createContext<UnitDetails | null>(null);
