import { prepareFetch } from "~/routes/api/shared/prepareSimplyFetch";

export interface loginForm {
    "EmailAddress":string;
    "Password":string;
    "IsLegacyUser"?:boolean;

}
export const login = async (form : loginForm) => {
    const loged = await prepareFetch('POST','/Authentication/LoginTenant',JSON.stringify(form));
    return loged;
}
export const getTenantInfo = async (email : string) => {
    const info = await prepareFetch('GET','/CustomerProfile/GetTenantByEmail?emailAddress='+email);
    return info;
}
//const form = {
  //  "EmailAddress":"jhan0416+1113@gmail.com",
  //  "Password":"Test123!!"
  //}
  //const loged = await login(form);
  //const cognitoUserSecret = loged.cognitoUserSecret?.cognitoAuthResponse;  
  //console.log('cognitoUserSecret',cognitoUserSecret);

  // use of login function
  // TODO save to session 