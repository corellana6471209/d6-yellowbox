import { describe, expect, it, vi } from "vitest";
import  {getSetting}  from './getSetting';
import { render, screen } from '@testing-library/react';

const params = "someString";
const fetchFunction = {
  json: function () {
    return {
      data: {
        genericMessageCollection: {
          items: [
            {
              value: "Internal Error"
            }
          ]
        },
      },
      errors: null,
    };
  },
};



const obj = {

  getSetting: () => "getSetting",
}

vi.mock('./getSetting', () => ({
 //return what you want
  getSetting: vi.fn().mockImplementation(() => "getSetting"),
}));



describe("getMessage component unit test", () => {

  it('loadStates should be mock', () => {
    expect(vi.isMockFunction(getSetting)).toBe(true)
  });


  it('Should be executed getSetting', async () => {
    const spy = vi.spyOn(obj, 'getSetting');
    const resp = await getSetting("");
    expect(spy.getMockName()).toEqual('getSetting');
    expect(resp.length).toBeGreaterThan(0);
  });


});
