import contentfulKeys from "./config";
import { setCookie, getAddress } from "./cookie";
import axios from "axios";

/**
 * getApiPath
 * @returns {object} geoLocation`s object
 */

export const getGeoLocation = async (firstRender: boolean = false) => {
  const cookie = getAddress();
  if (!cookie) {
    if (confirm("SimplySS wants to know your location.")) {
      getGeoLocationInformation();
    } else {
      if (firstRender) {
        setCookie(contentfulKeys.constants.firstRenderCookie, true, 7); // This is for verify if the message appear in the beggining of system
      }
    }
  }
};

// handle success case
const onSuccess = async (position: any) => {
  const { latitude, longitude } = position.coords;
  // Make a request for a user with a given ID
  await axios
    .get(
      `https://maps.googleapis.com/maps/api/geocode/json?latlng=${latitude},${longitude}&key=${contentfulKeys.googleApiKey}`
    )
    .then(function (response: any) {
      // handle success

      try {
        const result = response.data?.results[0]?.address_components.filter(
          (result: any) => result.types[0] === "postal_code"
        );
        if (result && result.length > 0) {
          const zipCode = result[0].long_name;
          setCookie(contentfulKeys.constants.addressCookie, zipCode, 7);
        } else {
          setCookie(contentfulKeys.constants.addressCookie, null, 7);
        }
      } catch (error) {
        setCookie(contentfulKeys.constants.addressCookie, null, 7);
      }
    })
    .catch(function (error: any) {
      // handle error
      setCookie(contentfulKeys.constants.addressCookie, null, 7);
    });
};

export const getGeoLocationInformation = async () => {
  await axios
    .post(
      `https://www.googleapis.com/geolocation/v1/geolocate?key=${contentfulKeys.googleApiKey}`
    )
    .then(async (response: any) => {
      if (response.data && response.data.location) {
        setCookie(
          contentfulKeys.constants.coords,
          JSON.stringify(response.data.location),
          7
        );
        await onSuccess({
          coords: {
            latitude: response.data.location.lat,
            longitude: response.data.location.lng,
          },
        });
      } else {
        setCookie(contentfulKeys.constants.addressCookie, null, 7);
      }
    })
    .catch(() => {
      console.error("Error retrieving GeoLocation Info from Google APIs");
      setCookie(contentfulKeys.constants.addressCookie, null, 7);
    });
};
