const maxRetries = 3;
export default async function fetchCustomApi(
  query: string,
  currentRetry: number = 0,
  toJson: any = true
): Promise<any> {
  if (currentRetry === maxRetries) {
    return {
      err: true,
    };
  }
  let responseData = {};
  const fetchRequest = await fetch(query)
    .then((response) => toJson ? response.json() : response.text())
    .then((response) => {
      return response;
    })
    .catch((error) => {
      //should retry due to unexpected error and not being handled
      return fetchCustomApi(query, currentRetry + 1);
    });

  return (responseData = fetchRequest);
}
