interface Asset {
  content: [];
  data: {
    sys: {
      id: string;
      linkType: string;
      type: string;
    };
    target: any;
  };
  nodeType: string;
}

export default Asset;