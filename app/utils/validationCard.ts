export const validationCard = (value: string, card: string) => {
  const mastercardReg =
    /^5[1-5][0-9]{14}$|^2(?:2(?:2[1-9]|[3-9][0-9])|[3-6][0-9][0-9]|7(?:[01][0-9]|20))[0-9]{12}$/;
  const visaReg = /^4[0-9]{12}(?:[0-9]{3})?$/;
  const americaReg = /^3[47][0-9]{13}$/;
  const discoverReg =
    /^65[4-9][0-9]{13}|64[4-9][0-9]{13}|6011[0-9]{12}|(622(?:12[6-9]|1[3-9][0-9]|[2-8][0-9][0-9]|9[01][0-9]|92[0-5])[0-9]{10})$/;

  if (card === "5") {
    if (value.match(mastercardReg)) {
      return true;
    } else {
      return false;
    }
  } else if (card === "6") {
    if (value.match(visaReg)) {
      return true;
    } else {
      return false;
    }
  } else if (card === "7") {
    if (value.match(americaReg)) {
      return true;
    } else {
      return false;
    }
  } else if (card === "8") {
    if (value.match(discoverReg)) {
      return true;
    } else {
      return false;
    }
  } else {
    return false;
  }
};
