import { month } from "./fakedataToTests";

export const dateInformation = (createReservationDate: any = null) => {
  let date = new Date(),
    y = date.getFullYear(),
    m = date.getMonth() + 1;
  const firstDay = new Date(y, m, 1).getDate();
  const lastDay = new Date(y, m, 0).getDate();

  let dateFormattedFirstDay = `${y}-${addZero(m)}-${addZero(firstDay)}`;
  let dateFormattedLastDay = `${y}-${addZero(m)}-${addZero(lastDay)}`;

  //Format createdDate
  let enterDay = null;
  let dateFormatted = null;
  let dateFormattedReversed = null;

  if (createReservationDate) {
    enterDay = new Date(createReservationDate);
    enterDay.setDate(enterDay.getDate()); //the same day i reserved and rented

    let day: any = addZero(enterDay.getDate());
    let month: any = addZero(enterDay.getMonth() + 1);

    dateFormatted = `${enterDay.getFullYear()}-${month}-${day}`;

    dateFormattedReversed = `${day}/${month}/${enterDay.getFullYear().toString().slice(2)}`;

  }

  let monthName = month[date.getMonth()];

  return { dateFormattedFirstDay, dateFormattedLastDay, dateFormatted,dateFormattedReversed,monthName };
};

//-----------------Functions

const addZero = (number: number) => {
  if (number < 10) {
    return "0" + number;
  }

  return number;
};
