export default function sortLocations(
  filters: Array<any>,
  originalLocations: Array<any>
) {
  let locationsSorted = [...originalLocations];

  //This code is to get all the original locations if one filter return empty

  //Verify sort by price
  const price = filters.some((data) => data.filter === "PRICELOWTOHIGH");
  const distance = filters.some((data) => data.filter === "DISTANCE");

  if (price) {
    const freeLocations = locationsSorted.filter((data) => !data.price);

    const filteringFreeLocations = locationsSorted
      .filter((data) => data.price)
      .sort(function (a: any, b: any) {
        return a.price - b.price;
      });

    locationsSorted = filteringFreeLocations.concat(freeLocations);
  } else if (distance) {
    locationsSorted = locationsSorted.sort(function (a: any, b: any) {
      return a.distance - b.distance;
    });
  }

  //Verify filter by parking,small,medium or large

  let dynamicFilters = {
    parking: (item: any) => item.categories.parking === true,
    small: (item: any) => item.categories.small === true,
    medium: (item: any) => item.categories.medium === true,
    large: (item: any) => item.categories.large === true,
  };

  let selected: any = [];

  //Determine what filters exists
  const parking = filters.some((data: any) => data.filter === "PARKING");
  const small = filters.some((data: any) => data.filter === "SMALL");
  const medium = filters.some((data: any) => data.filter === "MEDIUM");
  const large = filters.some((data: any) => data.filter === "LARGE");

  if (parking) selected.push(dynamicFilters.parking);
  if (small) selected.push(dynamicFilters.small);
  if (medium) selected.push(dynamicFilters.medium);
  if (large) selected.push(dynamicFilters.large);

  //Applyng the dinamic filter

  if (selected && selected.length > 0) {
    const r = locationsSorted.filter((item: any) =>
      selected.every((f: any) => f(item))
    );

    return r;
  } else {
    return locationsSorted;
  }
}
