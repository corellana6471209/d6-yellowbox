import { useState, useEffect } from "react";
const axios = require("axios").default;

/**
 * useRating function
 * @return {string} Google Place ID
 *
 */
export default function useRating(googleBusinessProfileId: string) {
  const [rating, setRating] = useState(0);
  /**
   * Get Google place rating
   */

  const getRating = async () => {
    if (!googleBusinessProfileId) {
      return null;
    }

    // go get rating
    await axios
      .get(`/api/inventory/getRating/${googleBusinessProfileId}`)
      .then((response: any) => {
        if (response.data !== null) {
          setRating(response.data);
        }
      })
      .catch(function (error: any) {
        return error;
      });
  };

  useEffect(() => {
    getRating();
  }, []);

  return rating;
}
