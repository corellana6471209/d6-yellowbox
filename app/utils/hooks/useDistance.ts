import { getAddressCoords } from "../cookie";

export default function calculateDistance(
  latLocation: any,
  lngLocation: any,
  latAddress: any = null,
  lngAddress: any = null,
  validation: boolean = false
) {
  const coords = getAddressCoords();

  if (coords) {
    const dataCoords = JSON.parse(coords);

    let radlat1 = (Math.PI * dataCoords.lat) / 180;
    let radlat2 = (Math.PI * Number(latAddress)) / 180;
    let theta = dataCoords.lng - Number(lngAddress);
    let radtheta = (Math.PI * theta) / 180;
    let dist =
      Math.sin(radlat1) * Math.sin(radlat2) +
      Math.cos(radlat1) * Math.cos(radlat2) * Math.cos(radtheta);
    dist = Math.acos(dist);
    dist = (dist * 180) / Math.PI;
    dist = dist * 60 * 1.1515;

    return Number(dist).toFixed(2);
  } else {
    if (validation) {
      let radlat1 = (Math.PI * latLocation) / 180;
      let radlat2 = (Math.PI * latAddress) / 180;
      let theta = lngLocation - lngAddress;
      let radtheta = (Math.PI * theta) / 180;
      let dist =
        Math.sin(radlat1) * Math.sin(radlat2) +
        Math.cos(radlat1) * Math.cos(radlat2) * Math.cos(radtheta);
      dist = Math.acos(dist);
      dist = (dist * 180) / Math.PI;
      dist = dist * 60 * 1.1515;

      return Number(dist).toFixed(2);
    } else {
      return null;
    }
  }
}
