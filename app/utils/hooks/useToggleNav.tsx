import { useState } from "react";

export default function useToggleNav(initialOptions: any = {}) {
  const [options, setOptions] = useState(initialOptions);
  function toggleNav(name: string) {
    const newState = Object.keys(options).reduce((accumulator, key) => {
      return key == name
        ? { ...accumulator, [key]: !options[key] }
        : { ...accumulator, [key]: false };
    }, {});

    setOptions(newState);
  }

  function resetNav() {
    setOptions(initialOptions);
  }

  return {
    ...options,
    toggleNav,
    resetNav,
  };
}
