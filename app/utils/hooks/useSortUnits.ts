export default function sortUnits(
  filters: Array<any>,
  originalUnits: Array<any>
) {
  let unitsSorted = [...originalUnits];

  //This code is to get all the original units if one filter return empty

  //Verify sort  checked.filter((data) => data.filter !== valueSelected)
  const priceLowHigh = filters.some((data) => data.filter === "PRICELOWTOHIGH");
  const priceHighLow = filters.some((data) => data.filter === "PRICEHIGHTOLOW");
  const smallLarge = filters.some((data) => data.filter === "SMALLTOLARGE");
  const largeSmall = filters.some((data) => data.filter === "LARGETOSMALL");

  //This is to add on the bottom on the array if the user get smallLarge/LargeSmall filter
  const parkingUnits = unitsSorted.filter((data) => data.category === 0);

  if (priceLowHigh) {
    unitsSorted = unitsSorted.sort(function (a: any, b: any) {
      return a.pushRate - b.pushRate;
    });
  } else if (priceHighLow) {
    unitsSorted = unitsSorted.sort(function (a: any, b: any) {
      return b.pushRate - a.pushRate;
    });
  } else if (smallLarge) {
    const filteringParking = unitsSorted
      .filter((data) => data.category !== 0)
      .sort(function (a: any, b: any) {
        //Filtering the parking category
        return a.category - b.category;
      });

    unitsSorted = filteringParking.concat(parkingUnits);
  } else if (largeSmall) {
    const filteringParking = unitsSorted
      .filter((data) => data.category !== 0)
      .sort(function (a: any, b: any) {
        //Filtering the parking category
        return b.category - a.category;
      });

    unitsSorted = filteringParking.concat(parkingUnits);
  }
  //Verify filter by parking,small,medium or large

  //Determine what filters exists
  const parking = filters.some((data: any) => data.filter === "PARKING");
  const small = filters.some((data: any) => data.filter === "SMALL");
  const medium = filters.some((data: any) => data.filter === "MEDIUM");
  const large = filters.some((data: any) => data.filter === "LARGE");

  //Applyng the dinamic filter

  if (parking || small || medium || large) {
    const r = unitsSorted.filter((item: any) => {
      return (
        (parking && item.category === 0) ||
        (small && item.category === 1) ||
        (medium && item.category === 2) ||
        (large && item.category === 3)
      );
    });
    return r;
  } else {
    return unitsSorted;
  }
}
