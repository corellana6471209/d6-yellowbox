export default function determineCategory(units: Array<any>) {
  // Determine Parking category = 0
  // Determine small category = 1
  // Determine medium category = 2
  // Determine large category = 3

  let parking = false;
  let small = false;
  let medium = false;
  let large = false;

  parking = units.some((data) => data.category === 0);
  small = units.some((data) => data.category === 1);
  medium = units.some((data) => data.category === 2);
  large = units.some((data) => data.category === 3);

  return {
    parking,
    small,
    medium,
    large,
  };
}
