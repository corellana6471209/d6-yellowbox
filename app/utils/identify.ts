export const identify = (window: any, typeAnalytics: string, data: any) => {
  if (window && window !== "undefined") {
    window.analytics.identify(typeAnalytics, data);
  } else {
    return null;
  }
};
