import contentfulKeys from "./config";
import getContentfulURL from "./getPath";
import {
  setCacheValueByKey,
  getCacheValueByKey,
} from "../routes/api/shared/elasticCache";

export default async function fetchContentful(
  query: string,
  skipCache: any | boolean = false
) {
  function createKeyName(inputKeyValue: string) {
    var newKeyName = inputKeyValue.replace(/\n|\r|\s/g, "");
    return newKeyName.slice(0, 249);
  }

  var cache = undefined;

  if (!skipCache && typeof window === "undefined") { // we're on the server
    try {
      cache = await getCacheValueByKey(createKeyName(query), null);
    } catch (error) {
      cache = undefined;
    }
  } else {
    // We're in the client
    cache = undefined;
  }

  var theData = null;
  if (cache == undefined || !cache) {
    theData = await fetch(getContentfulURL(), {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
        // Authenticate the request
        Authorization: `Bearer ${contentfulKeys.apiKey}`,
      },
      // send the GraphQL query
      body: JSON.stringify({ query }),
    })
      .then((response) => response.json())
      .then(({ data, errors }) => {
        if (errors) {
          //console.error("utils/fetchContentful: ",errors);
        }
        if (data) {
          //console.log("utils/fetchContentful success: ",data);
        }
        return data;
      });
      if (!skipCache && typeof window === "undefined") { // we're on the server
        try {
          await setCacheValueByKey(createKeyName(query),theData, null);
        } catch(error) {
          cache = undefined;
        }
      } 
      //console.log("utils/fetchContentful: ",theData)
    } else {
      theData = cache;
    }
  return theData;
}
