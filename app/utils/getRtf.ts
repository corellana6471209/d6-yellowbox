import fetchContentful from "./fetchContentful";


export async function getRTF(title: string) {
    const query = `
  query {
    componentrichTextFieldCollection(where : {contentTitle : "${title}"}){
      items{
        contentTitle,
        richText{
          json
        }      
      }
    }
  }
  `;
    const data = await fetchContentful(query);
    if (data.componentrichTextFieldCollection?.items[0]?.richText?.json?.content[0]?.content[0]?.value) {
        return (data.componentrichTextFieldCollection?.items[0].richText?.json?.content[0]?.content[0].value)
    }
    return '';
}