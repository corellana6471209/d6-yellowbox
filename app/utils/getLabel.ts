import fetchContentful from "./fetchContentful";

export async function getLabel(name: string) {
  const query = `query {
    globalLabelsCollection(where: {name: "${name}"}) {
      items {
        name
        value
      }
    }
  }`;

  const { globalLabelsCollection } = await fetchContentful(query);
  return globalLabelsCollection?.items[0]?.value;
}

export async function getLabels(names: Array<string>) {
  const query = `query {
    globalLabelsCollection(where: {name_in: ["${names.join("\", \"")}"]}) {
      items {
        name
        value
      }
    }
  }`;

  const { globalLabelsCollection } = await fetchContentful(query);
  return globalLabelsCollection?.items;
}
