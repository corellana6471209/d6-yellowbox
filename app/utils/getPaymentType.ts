export const getPaymentType = (card: string, paymentsArray: any = []) => {
  if (card === "5") {
    return paymentsArray.filter(
      (payment: any) => payment.paymentCategory === "MC"
    );
  } else if (card === "6") {
    return paymentsArray.filter(
      (payment: any) => payment.paymentCategory === "VISA"
    );
  } else if (card === "7") {
    return paymentsArray.filter(
      (payment: any) => payment.paymentCategory === "AMEX"
    );
  } else if (card === "8") {
    return paymentsArray.filter(
      (payment: any) => payment.paymentCategory === "DISC"
    );
  } else {
    return [];
  }
};
