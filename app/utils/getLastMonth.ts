import { month } from "./fakedataToTests";

export const getLastMonth = () => {
  var now = new Date();

  for (var i = 0; i < 12; i++) {
    var future = new Date(now.getFullYear(), now.getMonth() + i, 1);
    var monthName = month[future.getMonth()];
    var year = future.getFullYear();
    console.log(monthName, year);
  }
};
