export interface hashMap{ 
    [x: string]: any; 
}
export const createHashTablebyParam = (arrayToIterate : Array<hashMap>,key : string)=>{
    const hashMap: hashMap = {};
    if (arrayToIterate && arrayToIterate.length) {
      for(const element of arrayToIterate){
        if (element[key]) {
          const temporalKey = element[key];
          hashMap[temporalKey] = element;
        }
      }
    } 
    return hashMap;
  }
