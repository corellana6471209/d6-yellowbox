import { useEffect } from 'react';
import PropTypes from "prop-types";

const ScriptInjector = (props: any) => {
  const { url, apiKey, id, dataDomainScript } = props;

  useEffect(() => {
		const existingScript = document.getElementById(id);

		if (!existingScript) {
			const thirdPartyScript = document.createElement('script');
			thirdPartyScript.src = url + '?key=' + apiKey;
			thirdPartyScript.id = id;
      if (dataDomainScript) thirdPartyScript.setAttribute('data-domain-script', dataDomainScript);
			document.body.appendChild(thirdPartyScript);
		}
	});

	return null;
}

ScriptInjector.propTypes = {
  url: PropTypes.string.isRequired,
  id: PropTypes.string.isRequired,
  apyKey: PropTypes.string,
  dataDomainScript: PropTypes.string
};

ScriptInjector.defaultProps = {
  apyKey: '',
  dataDomainScript: ''
};

export default ScriptInjector;

// Source: https://www.thepaulcushing.com/react-and-third-party-scripts/