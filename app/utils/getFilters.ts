import fetchContentful from "./fetchContentful";

export default async function getModalFilters(page: string = "location") {
  const query = `query {
    pageFiltersCollection(where: {page: "${page}"}) {
      items {
        filtersCollection {
          items {
            name,
            title,
            value,
          }
        }
      }
    }
  }
  `;

  const { pageFiltersCollection } = await fetchContentful(query);

  return pageFiltersCollection?.items[0]?.filtersCollection?.items;
}
