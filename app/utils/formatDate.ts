export const formatDate = (date: string) => {
  let objectDate = new Date(date);
  let day: any = objectDate.getDate();

  if (day < 10) {
    day = "0" + day;
  }

  let month = objectDate.getMonth();

  let monthArray = [
    "Jan",
    "Feb",
    "Mar",
    "Apr",
    "May",
    "June",
    "July",
    "Aug",
    "Sept",
    "Oct",
    "Nov",
    "Dec",
  ];

  let year = objectDate.getFullYear();

  let dateFormatted = `${monthArray[month]}, ${day}  ${year}`;

  return dateFormatted;
};

export const formatDateLong = (date: string) => {
  let objectDate = new Date(date);
  let day: any = objectDate.getDate();

  if (day < 10) {
    day = "0" + day;
  }

  let month = objectDate.getMonth();

  let monthArray = [
    'January',
    'February',
    'March',
    'April',
    'May',
    'June',
    'July',
    'August',
    'September',
    'October',
    'November',
    'December'
  ];

  let year = objectDate.getFullYear();

  let dateFormatted = `${monthArray[month]} ${day}, ${year}`;

  return dateFormatted;
};