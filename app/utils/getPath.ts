import { globalState } from "../root";
import contentfulKeys from "./config";

export default function getContentfulURL() {
  let theEnvironment = globalState.apiPath
    ? globalState.apiPath
    : process.env.ENVIRONMENT;
  let contentfulEnvironment = "";
  if (theEnvironment == "development") {
    contentfulEnvironment = "development";
  } else if (theEnvironment == "test" || theEnvironment == "quality") {
    contentfulEnvironment = "quality";
  } else if (theEnvironment == "staging" || theEnvironment == "stage") {
    contentfulEnvironment = "stage";
  } else if (theEnvironment == "production") {
    contentfulEnvironment = "master";
  } else {
    contentfulEnvironment = "master";
  }
  let theURL = `https://graphql.contentful.com/content/v1/spaces/${contentfulKeys.spaceId}/environments/${contentfulEnvironment}`;
  // console.log(`theURL: `,theURL);
  return theURL;
}
