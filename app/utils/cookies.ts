export class cookieService {
    defaultExpirationDays = 7;
    day = 24 * 60 * 60 * 1000;
    getCookie(name : string) : string|null {
        const nameLenPlus = (name.length + 1);
	return document.cookie
		.split(';')
		.map(c => c.trim())
		.filter(cookie => {
			return cookie.substring(0, nameLenPlus) === `${name}=`;
		})
		.map(cookie => {
			return decodeURIComponent(cookie.substring(nameLenPlus));
		})[0] || null;
    }
    setCookie(name: string, val: string) {
        const date = new Date();
        const value = val;    
        date.setTime( date.getTime() + (this.defaultExpirationDays * this.day) );
        document.cookie = name+"="+value+"; expires="+date.toUTCString()+"; path=/";
    }
    deleteCookie(name: string) {
        const date = new Date();
        date.setTime(date.getTime() + (-1 * this.day));
        document.cookie = name+"=; expires="+date.toUTCString()+"; path=/";
    }
}