import fetchContentful from "./fetchContentful";

export default async function getSetting(name: string) {
  const query = `query {
    generalSettingsCollection(where: {name: "${name}"}) {
      items {
        name
        value
      }
    }
  }`;

  const { generalSettingsCollection } = await fetchContentful(query);
  return generalSettingsCollection?.items[0]?.value;
}
