export const validateUSPhone = (number: string) => {
  const re = RegExp(/^\([0-9]{3}\) [0-9]{3}-[0-9]{4}$/);
  return re.test(number);
};

export const validateEmail = (number: string) => {
  const re = RegExp(
    /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
  );
  return re.test(number);
};

export const isAlphanumeric = (value: string) => {
  return /^[a-zA-Z0-9]*$/.test(value);
}