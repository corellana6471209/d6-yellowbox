export const analytics = (window: any, typeAnalytics: string, data: any) => {
  if (window && window !== 'undefined') {
    if (typeAnalytics === "reservationCompleted") {
      window.analytics.ready(() => {
        window.analytics.track("Reservation Created", data);
      });
    } else if (typeAnalytics === "rentalCompleted") {
      window.analytics.track("Rental Completed", data);
    } else if (typeAnalytics === "loggedIn") {
      window.analytics.track("Logged In", data);
    } else if (typeAnalytics === "propertyViewed") {
      window.analytics.track("Property Viewed", data);
    } else if (typeAnalytics === "unitSelected") {
      window.analytics.track("Unit Selected", data);
    } else if (typeAnalytics === "rentalStarted") {
      window.analytics.track("Rental Started", data);
    } else {
      return null;
    }
  } else {
    return null;
  }
};
