export default function formatNumberMyAccount(value: any) {
  // if input value is falsy eg if the user deletes the input, then just return
  if (!value) return value;

  // clean the input for any non-digit values.
  const phoneNumber = value.replace(/[^\d]/g, "");

  return { code: phoneNumber.slice(0, 3), number: `${phoneNumber.slice(3, 6)}-${phoneNumber.slice(6, 10)}` }


}
