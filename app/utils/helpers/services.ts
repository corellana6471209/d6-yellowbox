import fetchContentful from "../fetchContentful";



export interface LocationSlug {
  contentTypeLocationCollection: {
    items: [{
      slug: string,
      sys: { publishedAt: string }
    }]
  }
}
/**
 * 
 * Return the slugs of all blog entries to populate the sitemap.xml
 * @returns {Promise} - Promise with blog entries
 */
export async function getLocationEntriesSlug(): Promise<LocationSlug> {
    const query = `query {
      contentTypeLocationCollection {
        items {
          slug
          sys {
            publishedAt
          }
        }
      }
    }
    `;
    return await fetchContentful(query);
}

export interface StateSlug {
  stateCollection: {
    items: [{
      stateFullName: string,
      sys: { publishedAt: string }
    }]
  }
}
/**
 * 
 * Return the slugs of all blog entries to populate the sitemap.xml
 * @returns {Promise} - Promise with blog entries
 */
export async function getStateEntriesSlug(): Promise<StateSlug> {
    const query = `query {
      stateCollection
      {
        items {
          stateFullName
          sys {
            publishedAt
          }
        }
      }
    }
    `;
    return await fetchContentful(query);
}

export interface PageSlug {
    pageCollection: {
        items: [
            {
                slug: string,
                sys: { publishedAt: string }
            }];
    }
}
/**
 *  
 * Return the slugs of all page entries to populate the sitemap.xml
 * @returns {Promise} - Promise with page entries
 */

export async function getPageEntriesSlug(): Promise<PageSlug> {
    const query = `query {
        pageCollection {
          items {
            slug
            sys{
              publishedAt
            }
          }
        }
      }
    `;
    return await fetchContentful(query);
}

export interface Slug {
    slug: string;
    lastmod: string;
}


export const getSlugs = async () => {
    /** PAGES **/
    const slugs: any = []
    const pages: PageSlug = await getPageEntriesSlug();
    pages.pageCollection.items.map((item: any) => {
        slugs.push(
            {
                slug: `${item.slug?item.slug:""}`,
                lastmod: `${item.sys.publishedAt}`
            });
    });
    /** BLOGS **/
    const locations:LocationSlug = await getLocationEntriesSlug();
    locations.contentTypeLocationCollection.items.map((item: any) => {
        slugs.push(
            {
                slug: `locations/${item.slug}`,
                lastmod: `${item.sys.publishedAt}`
            });
    });

    /** STATES **/
    const states:StateSlug = await getStateEntriesSlug();
    states.stateCollection.items.map((item: any) => {
      slugs.push(
            {
                slug: `locations/state/${item.stateFullName}`,
                lastmod: `${item.sys.publishedAt}`
            });
    });
    return slugs;
};
