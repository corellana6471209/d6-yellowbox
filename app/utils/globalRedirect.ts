export const globalRedirect = (window: any, redirectTo: string) => {
  if (window && window !== "undefined") {
    setTimeout(() => {
      window.location.href = redirectTo;
    }, 1000);
  } else {
    return null;
  }
};
