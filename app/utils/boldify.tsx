import React from "react";

const regexEscape = (str: string) =>
  str.replace(/[.*+\-?^${}()|[\]\\]/, "\\$&");

export const boldify = (searchStr: string, targetStr: string) => {
  return targetStr
    .split(new RegExp(`(${regexEscape(searchStr)})`, "i"))
    .map((part, idx) =>
      idx === 1 ? (
        <strong key={idx}>{part}</strong>
      ) : (
        <React.Fragment key={idx}>{part}</React.Fragment>
      )
    );
};
