import { useLoaderData } from "@remix-run/react";
import type {
  LinksFunction,
  LoaderFunction,
  MetaFunction,
} from "@remix-run/node";
import { json } from "@remix-run/node";
import {
  Links,
  LiveReload,
  Meta,
  Outlet,
  Scripts,
  ScrollRestoration,
} from "@remix-run/react";
import React from "react";
import { ExternalScripts, DynamicLinks } from "remix-utils";

import tailwindStylesheetUrl from "./styles/tailwind.css";
import globalStylesheetUrl from "./styles/global.css";
import header from "./styles/header.css";
import globalComponentsStyles from "./styles/page-components.css";
import locationStyles from "./styles/locations.css";
import myAccountStyles from "./styles/my-account.css";

import carouselStyles from "react-multi-carousel/lib/styles.css";

import favicon from "~/resources/favicon.ico";
import ScriptInjector from "./utils/ScriptInjector";

// import { composeUploadHandlers } from "@remix-run/server-runtime/dist/formData";
// import { getUser } from "./session.server";

interface GlobalStateLoaderData {
  userLoggedIn: boolean;
  apiPath: string | undefined;
  setGoogleApiKey: object;
}

export const links: LinksFunction = () => {
  return [
    { rel: "stylesheet", href: carouselStyles },
    { rel: "stylesheet", href: tailwindStylesheetUrl },
    { rel: "stylesheet", href: globalStylesheetUrl },
    { rel: "stylesheet", href: header },
    { rel: "stylesheet", href: globalComponentsStyles },
    { rel: "stylesheet", href: locationStyles },
    { rel: "stylesheet", href: myAccountStyles },

    { rel: "stylesheet", href: "https://use.typekit.net/etu3tii.css" },
    // NOTE: Architect deploys the public directory to /_static/
    { rel: "icon", href: favicon },
  ];
};

export const meta: MetaFunction = ({ data, params }) => {
  // fallback
  return {
    title: "Simply Self Storage",
    description: `Simply Self Storage`,
    viewport: "width=device-width,initial-scale=1,viewport-fit=cover",
  };
};

// type LoaderData = {
//   user: Awaited<ReturnType<typeof getUser>>;
// };

export const loader: LoaderFunction = async ({ request }) => {
  // we will revist this when we are looking at user stuff
  // return json<LoaderData>({
  //   user: await getUser(request),
  // });
  // TODO: Get actual user data
  return json<GlobalStateLoaderData>({
    userLoggedIn: false,
    apiPath: process.env.ENVIRONMENT,
    setGoogleApiKey: () => {},
  });
};

// todo add a real error message
export function ErrorBoundary({ error }) {
  if (
    error != "Error: NetworkError when attempting to fetch resource." &&
    error != "Error: Load failed"
  ) {
    return (
      <html>
        <head>
          <title>Oh no!</title>
          <Meta />
          <Links />
        </head>
        <body>
          <h1 className="pt-10 text-center">Something went wrong</h1>
          <p className="text-center">
            Try refreshing or navigating to another page.
          </p>
          <Scripts />
        </body>
      </html>
    );
  } else {
    return <html></html>;
  }
}

export default function App() {
  const globalStateLoader = useLoaderData();
  if (typeof window === "undefined") {
    globalStateLoader.apiPath = process.env.ENVIRONMENT;
  }
  globalState = globalStateLoader;

  return (
    <html lang="en">
      <head>
        {/* <!-- OneTrust Cookies Consent Notice start --> */}
        {globalStateLoader.apiPath !== "production" && (
          <ScriptInjector
            id="cookielaw-test"
            url="https://cdn.cookielaw.org/scripttemplates/otSDKStub.js"
            dataDomainScript="1de1c656-1822-4295-be26-fbd21ebea8e5-test"
          />
        )}

        {globalStateLoader.apiPath == "production" && (
          <ScriptInjector
            id="cookielaw-test"
            url="https://cdn.cookielaw.org/scripttemplates/otSDKStub.js"
            dataDomainScript="1de1c656-1822-4295-be26-fbd21ebea8e5"
          />
        )}

        <script
          type="text/javascript"
          dangerouslySetInnerHTML={{
            __html: `function OptanonWrapper(){};`,
          }}
        />

        {globalStateLoader.apiPath == "production" && (
          <script
            dangerouslySetInnerHTML={{
              __html: `
              !function(){var analytics=window.analytics=window.analytics||[];if(!analytics.initialize)if(analytics.invoked)window.console&&console.error&&console.error("Segment snippet included twice.");else{analytics.invoked=!0;analytics.methods=["trackSubmit","trackClick","trackLink","trackForm","pageview","identify","reset","group","track","ready","alias","debug","page","once","off","on","addSourceMiddleware","addIntegrationMiddleware","setAnonymousId","addDestinationMiddleware"];analytics.factory=function(e){return function(){var t=Array.prototype.slice.call(arguments);t.unshift(e);analytics.push(t);return analytics}};for(var e=0;e<analytics.methods.length;e++){var key=analytics.methods[e];analytics[key]=analytics.factory(key)}analytics.load=function(key,e){var t=document.createElement("script");t.type="text/javascript";t.async=!0;t.src="https://cdn.segment.com/analytics.js/v1/" + key + "/analytics.min.js";var n=document.getElementsByTagName("script")[0];n.parentNode.insertBefore(t,n);analytics._loadOptions=e};analytics._writeKey="bv7niEEfePkQHyjBdFU2cogZnMyG6jF1";;analytics.SNIPPET_VERSION="4.15.3";
              analytics.load("HroDFxINPabhmRtTf3SBkGH3ogJYk6CQ");
              analytics.page();
              }}();`,
            }}
          />
        )}

        {globalStateLoader.apiPath !== "production" && (
          <script
            dangerouslySetInnerHTML={{
              __html: `
              !function(){var analytics=window.analytics=window.analytics||[];if(!analytics.initialize)if(analytics.invoked)window.console&&console.error&&console.error("Segment snippet included twice.");else{analytics.invoked=!0;analytics.methods=["trackSubmit","trackClick","trackLink","trackForm","pageview","identify","reset","group","track","ready","alias","debug","page","once","off","on","addSourceMiddleware","addIntegrationMiddleware","setAnonymousId","addDestinationMiddleware"];analytics.factory=function(e){return function(){var t=Array.prototype.slice.call(arguments);t.unshift(e);analytics.push(t);return analytics}};for(var e=0;e<analytics.methods.length;e++){var key=analytics.methods[e];analytics[key]=analytics.factory(key)}analytics.load=function(key,e){var t=document.createElement("script");t.type="text/javascript";t.async=!0;t.src="https://cdn.segment.com/analytics.js/v1/" + key + "/analytics.min.js";var n=document.getElementsByTagName("script")[0];n.parentNode.insertBefore(t,n);analytics._loadOptions=e};analytics._writeKey="bv7niEEfePkQHyjBdFU2cogZnMyG6jF1";;analytics.SNIPPET_VERSION="4.15.3";
              analytics.load("bv7niEEfePkQHyjBdFU2cogZnMyG6jF1");
              analytics.page();
              }}();`,
            }}
          />
        )}
        {/* <script type="text/javascript">
          function OptanonWrapper() {console.log("loaded")}
        </script> */}

        {/* <!-- OneTrust Cookies Consent Notice end --> */}
        <meta httpEquiv="Content-Type" charSet="UTF-8" />
        <Meta />
        <DynamicLinks />
        <Links />
        <script src="https://maps.google.com/maps/api/js?key=AIzaSyCpbHnhHkqRiS6jS7aU0kRge3C47ESjxws&libraries=places" />
        <script src="https://cdn.jsdelivr.net/npm/feather-icons/dist/feather.min.js"></script>

        {globalStateLoader.apiPath !== "production" && (
          <script
            dangerouslySetInnerHTML={{
              __html: `
              (function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src='https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);})(window,document,'script','dataLayer','GTM-MW8LQNM');;
              `,
            }}
          />
        )}

        {globalStateLoader.apiPath == "production" && (
          <script
            dangerouslySetInnerHTML={{
              __html: `
              (function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src='https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);})(window,document,'script','dataLayer','GTM-5TSMMGN');
              `,
            }}
          />
        )}

        <script
          type="application/ld+json"
          dangerouslySetInnerHTML={{
            __html: `
          {
            "@context": "https://schema.org",
            "@type": "Organization",
            "name": "Simply Self Storage",
            "legalName" : "Simply Self Storage Management, LLC",
            "url": "https://www.simplyss.com/",
            "logo": "https://images.ctfassets.net/rfh1327471ch/x3KcoIuO1ZYduJ2zATaro/961b0d67def09ac3c64faa1773365ac1/simply-self-storage_logo.svg",
            "foundingDate": "2003",
            "foundingLocation": "Orlando, FL",
            "address": {
                "@type": "PostalAddress",
                "streetAddress": "4901 Vineland Rd, Suite 350",
                "addressLocality": "Orlando, FL",
                "postalCode": "32811"
            },
            "description": "With convenient locations throughout the US and millions of square feet of storage, we've got your space. Simply Self Storage offers clean, safe, and stress-free options with the features and flexibility you need.",
            "sameas" : [
                "https://www.facebook.com/SimplySelfStorage",
                "https://twitter.com/StoreWithSimply",
                "https://www.instagram.com/storewithsimply/",
                "https://www.youtube.com/channel/UCWBoyBlciNRbT_oAZTvVjJw/featured",
                "https://www.pinterest.com/simplyselfstorage",
                "https://www.linkedin.com/company/simply-self-storage",
                "https://en.wikipedia.org/wiki/Simply_Self_Storage"
            ]
          }
          `,
          }}
        />
      </head>
      <body className="relative h-full">
        {globalStateLoader.apiPath !== "production" && (
          <noscript>
            <iframe
              src="https://www.googletagmanager.com/ns.html?id=GTM-MW8LQNM"
              height="0"
              width="0"
              style={{ display: "none", visibility: "hidden" }}
            ></iframe>
          </noscript>
        )}

        {globalStateLoader.apiPath == "production" && (
          <noscript>
            <iframe
              src="https://www.googletagmanager.com/ns.html?id=GTM-5TSMMGN"
              height="0"
              width="0"
              style={{ display: "none", visibility: "hidden" }}
            ></iframe>
          </noscript>
        )}

        <ExternalScripts />
        <Outlet />
        <ScrollRestoration />
        <Scripts />
        <LiveReload />
      </body>
    </html>
  );
}

// TODO: Get actual user data
export var globalState = {
  userLoggedIn: false,
  apiPath: "",
  setGoogleApiKey: () => {},
};

export const globalStateContext = React.createContext(globalState);
