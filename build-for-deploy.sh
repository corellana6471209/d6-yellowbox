#!/usr/bin/env bash
npm run build
mkdir dist
scp -r server package.json dist
cd dist
npm install --production
rm package-lock.json
zip -r ../Remix.zip node_modules server
cd ..
aws s3 cp --profile simply Remix.zip s3://simply-dev-test-lambda-upload
aws s3 cp --profile simply --recursive public/build/ s3://simplyss-frontend-website-dev/_static/build/
aws lambda update-function-code --profile simply --function-name SimplyFrontEnd-content-dev --s3-bucket simply-dev-test-lambda-upload --s3-key Remix.zip


rm -rf dist
mkdir dist
scp -r server package.json dist
cd dist
npm install --production
npm install -g depcheck typescript
depcheck .         
npm install modclean -g
modclean --patterns="default:*" -r
rm package-lock.json
rm -f ../test-packaging.zip
zip -q -r ../test-packaging.zip  node_modules server
cd ..
du -sh test-packaging.zip
du -sh dist/node_modules dist/server
