import { createRequestHandler } from "@remix-run/architect";
import * as build from "@remix-run/dev/server-build";

// const AWSXRay = require('aws-xray-sdk-core')
// const AWS = AWSXRay.captureAWS(require('aws-sdk'))

// // Create client outside of handler to reuse
// const lambda = new AWS.Lambda();

if (process.env.NODE_ENV !== "production") {
  require("./mocks");
}

export const handler = createRequestHandler({
  build,
  mode: process.env.NODE_ENV,
});
